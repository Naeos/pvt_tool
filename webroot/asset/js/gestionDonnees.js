
var nomExploitant ; 
var adresseExploitant ; 
var villeExploitant ; 
var paysExploitant ; 
var mailExploitant ; 
var photoExploitant ; 

var nomExploitantNouveau;
var adresseExploitantNouveau;
var villeExploitantNouveau;
var paysExploitantNouveau;
var mailExploitantNouveau;
var photoExploitantNouveau;
var listeSitesNouveau;

var infoChaqueCavite = [] ;  //Liste des informations pour chaque cavités (Profondeur,Hauteur, Diamètre, Volume)

var nomSiteActuel;  //Après connexion à un site, placer le nom du site ici
var nomCaviteActuelle;

var infoChaqueComposant = [] ;  //Liste des informations pour chaque élément chimique (Masse molaire, Pression et température critique)

var stringListeComposantOptionGeneral; //Pour être récupéré lors de l'ajout d'une ligne pour ajouter un composant à un gaz

var listeComposantGaz = []; //liste des composant d'un gaz 
var listeJsonGazComposant;

//Variables qui se remplissent quand l'utilisateur souhaite modifier un élément chimique
var modificationElementAncienNom;
var modificationElementNouveauNom;
var modificationElementNouveauFormule;
var modificationElementNouveauMasseMolaire;
var modificationElementNouveauPression;
var modificationElementNouveauTemperature;
var modificationElementNouveauDensite;

//Variable qui se remplie quand un utilisateur souhaite supprimer un élément chimique
var suppressionElementNom;


//Variables qui se remplissent quand on ajoute une cavité
var nomCaviteNouvelle;
var hauteurCaviteNouvelle;
var profondeurCaviteNouvelle;
var diametreCaviteNouvelle;
var volumeCaviteNouvelle;

//Variables qui se remplissent quand on ajoute un calage
var dateDebut;
var dateFin;
var listeDataPression;
var listeDataTemperature;
var checkBoxCalage;

//var clickNouveauCalage = true; //savoir si on peut cliquer sur le bouton pour afficher un nouveau calage

// Savoir si l'overlay est ouvert ( 0 => fermé, 1 => ouvert)
var optionCorrectionDonneesOuvert = 0;

//Mot de passe tapé par l'utilisateur quand il veut supprimer une cavité
var passwordCavite;


//Json contenant les points à afficher pour les différents graphes
var dataTempJsonReel;
var dataTempJsonReelWithIncoherent; //Contient les données incohérentes
var dataTempJsonSimule = [];
var dataPresJsonReel = [];
var dataPresJsonReelWithIncoherent = []; //Contient les données incohérentes
var dataPresJsonSimule = [] ;
var dataVolumeJsonReel = [];
var dataVolumeJsonReelWithIncoherent = []; //Contient les données incohérentes
var dataDebitJsonReel = [];
var dataTempCasingShoe = [];
var dataPresCasingShoe = [];
var dataTempCaverne= [];
var dataPresCaverne = [];


var erreurNulCorrectionManuel = false; //Correction manuelle : savoir si pour une date, tous les champs nuls ont été remplis où pas encore
var erreurNulCorrectionDate = ""; //liste des dates où pour une date, tous les champs nuls ont n'ont pas été remplis 
var tableauFinalCorrectionManuel = []; //tableau qui contient les correction qui ont été fait manuellement

//Tableau sauvegardant contenant les points à afficher pour chaque courbes de chaque calage en fonction de l'id du calage
var tabDataTempJsonReel = [];
var tabDataTempJsonSimule = [];
var tabDataPresJsonReel = [];
var tabDataPresJsonSimule = [];
var tabDataVolumeJsonReel = [];
var tabDataDebitJsonReel = [];


//Date de debut et de fin pour l'affichage des infos d'une cavité en fonction de ces deux dates
var dateDebutCavite ;
var dateFinCavite ;

//Id du calage à supprimer et l'autre à modifier et l'autre que l'on simule
var idCalageSuppression;
var idCalageModifie;
var idCalageSimule;


//Liste de booléen pour savoir si il est possible de cacher l'écran de chargement
var boolTempCaviteCharge = false;
var boolPresCaviteCharge = false;
var boolVolCaviteCharge = false;

var boolTempCaviteChargeDate = false;
var boolPresCaviteChargeDate = false;
var boolVolCaviteChargeDate = false;






$(document).ready(function() 
{
    initializeGet(); //Récupère liste des informations
        
 
    /**
    *   Page : index.html
    *
    *   Quand on essaye de se connecter, vérifier le mot de passe 
    */
    $(document).on('submit','div.formConnexion form', function(evt)
    {
        siteNameConnexion =$(this).attr('id') ;
        passwordConnexion = $(this).find('.searchfieldjs').val();
        
        sessionStorage.setItem("NomSiteActuel",siteNameConnexion) //stocker dans un Web Storage 

        evt.preventDefault();
      //  document.location.href="./sites/caviteSite.html";
        connexionSite(siteNameConnexion, passwordConnexion );
    });
    

    
    /**
    *   Page : parametreSite.html
    *
    *   Vérifier et envoyer les données d'un nouveau élément chimique
    */
    $(document).on('click','.overlay-ajoutElementChimique #ajoutElementChimiqueBottom p.boutonVert', function()
    {
        var nomElement = $("input[name='nEC5']").val() ;
        var formuleElement = $("input[name='nEC1']").val() ;
        var masseMolaireElement = $("input[name='nEC2']").val() ;
        var pressionCritiqueElement = $("input[name='nEC3']").val() ;
        var temperatureCritiqueElement = $("input[name='nEC4']").val() ;
        var densiteElement = $("input[name='nEC6']").val() ;

        
        
        //Vérifier les champs
        if (verifierFormAjoutElementChimique())
            {
                ajoutElementChimique(nomElement, formuleElement, masseMolaireElement, pressionCritiqueElement, temperatureCritiqueElement,densiteElement);
            }

    });
    
    
    
        
    /**
    *   Page : parametreSite.html
    *
    *   Vérifier et envoyer les composants et proportions du gaz
    */
    $(document).on('click','#parametreSite #parametreSiteBottom p.boutonVert', function()
    {
        if (verifierAjoutGaz())
            {
                ajoutGaz();
            }
    });
    

        
    /**
    *   Page : caviteSite.html
    *
    *   Vérifier le mot de passe pour savoir si le site peut être supprimé 
    */
    $(document).on('click','#suppressionSiteBottom p.boutonVert', function()
    {
        siteNameConnexion = nomSiteActuel;
        passwordConnexion = $(this).parent().parent().find('input[name="mdpSupprimer"]').val();
        
        if(passwordConnexion == "")
            $('p.erreurSupression').slideDown('fast');
        else
            supressionSite(siteNameConnexion, passwordConnexion );
    });
    
    
    /**
    *   Page : caviteSite.html
    *
    *   Vérifier les mot de passe pour savoir si le mot de passe du site peut être supprimé 
    */
    $(document).on('click','#modifMdpSiteBottom p.boutonVert', function()
    {
        siteNameConnexion = sessionStorage.getItem("NomSiteActuel");
        var nouveauMdp1 = $(this).parent().parent().find('input[name="mdpModifier1"]').val();
        var nouveauMdp2 = $(this).parent().parent().find('input[name="mdpModifier2"]').val();
        
        if(nouveauMdp1 != nouveauMdp2)
            $('p.erreurModification').slideDown('fast');
        else
            modificationMdp(siteNameConnexion, nouveauMdp1 );
    });
    
    
    
    /**
    *   Page : exploitants.html
    *
    *   Supprimer un exploitants 
    */
    $(document).on('click','#suppressionExploitantBottom p.boutonRouge', function()
    {
        
        var nameExploitant = $(this).attr("name");
        
        supressionExploitant(nameExploitant );
        
    });
    
    

    
});




/******************
**
**  index.html  ***
**
******************/


    /**
    *
    *   Exécuter des fonctions selon la pae sur laquel l'utilisateur est
    **/
    function initializeGet()
    {
        if (document.title == "Accueil")
            {
                getListeSites();
                
                //Effacer la variable donnant la cavité à null
                sessionStorage.setItem("NomCaviteActuel", "")
            }
        
        if(document.title == "Liste des exploitants")
            getListeExploitant();
        
        if(document.title == "Ajouter un nouveau site")
            {
                getListeExploitantOption();
                
                if ($(location).attr('href').split('?')[1] == "exploitantEnregistre")
                    {
                        $('input[name="nS1"]').val( sessionStorage.getItem("NomSiteNouveau") );
                        $('input[name="nS2"]').val( sessionStorage.getItem("AdresseSiteNouveau") );
                        $('input[name="nS3"]').val( sessionStorage.getItem("VilleSiteNouveau") );
                        $('input[name="nS4"]').val( sessionStorage.getItem("PaysSiteNouveau") );
                        $('input[name="nS7"]').val( sessionStorage.getItem("MdpSiteNouveau") );
                        $('input[name="nS8"]').val( sessionStorage.getItem("MdpConfirmSiteNouveau") );
                        
                    }
            }
        
        
        if(document.title == "Cavité") // caviteSite.html  
            {
                presenceDonneesNull = false;
                presenceDonneesIncoherent = false;
                
                $('.erreurDonneesIncoherente').hide(); //Cacher les erreurs liées au données des cavités
                $('.erreurDonneesNull').hide(); 
                $('#caviteMenuBottom a:last-child').attr('href','gererCalage.html');
                
                
                //récupérer nom du site sur lequel nous sommes
                nomSiteActuel = sessionStorage.getItem("NomSiteActuel"); 
                $('span.nomSite').text(nomSiteActuel);
                
                
                getInfoSite();
                
                
                
                getListeCavites().done(function()  //Attendre la fin de cette fonction
                { 
                    if (sessionStorage.getItem("NomSiteActuel") == "") //Si on arrive pour la première efois sur le site
                        {
                            var nomCaviteActuelTmp = $('select#choix_cavite').val();
                            sessionStorage.setItem("NomCaviteActuel", nomCaviteActuelTmp); //Assigner nom de la cavité actuelle
                        }
                    else 
                        {  //Recharger la cavité sur laquelle nous étions avant
                            var nomCaviteActuelTmp = sessionStorage.getItem("NomCaviteActuel");
                            
                            $('select#choix_cavite').val(nomCaviteActuelTmp);
                            
                        }
                    
                    

                    //Récupérer données des cavités
                    getDataTempCavitePointReel(); //Récuperer les données de la courbes réelle 

                    getDataPresCavitePointReel(); //Récuperer les données de la courbes réelle 

                    getDataVolumeCavitePointReel(); //Récuperer les données de la courbes réelle 
                    
                    getDataTempCasingShoePoint();
                    
                    getDataPresCasingShoePoint();

                    getDataTempCavernePoint();
                    
                    getDataPresCavernePoint();

                });             
                
            }
        
        if (document.title == "Gestion des données relevées")
            {
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                
                getDataTempCavitePointReel();
                getDataPresCavitePointReel();
                getDataVolumeCavitePointReel();
                
                
                //Il y a eu une insertion de donnée qui a rencontré des conflits avec des valeurs existantes
               if ($(location).attr('href').split('?')[1] == "confVal")
                    {
                        //Afficher overlay
                        $('.overlay-conflitDonnees').fadeIn();
                        
                        getListeConflit();
                        
                    }
            }
        
        if (document.title == "Gestion des calages")
            {
                
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                
                //Modifier le logo
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                  
                getListeCalages();
            }
        
        if (document.title == "Graphe Température")
            {
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                
                initialiserGraphesDonneeTemp();
                getInfoGrapheTemp();
            }
        
        if (document.title == "Graphe Pression")
            {
                
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
               
                initialiserGraphesDonneePression();
                getInfoGraphePression();
            }
        
        if (document.title == "Graphe Volume")
            {
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                
                initialiserGraphesDonneeVolume();
            }
        
        if (document.title == "Graphe Température Casing Shoe")
            {
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                
                initialiserGraphesDonneeTempCasingShoe();
            }
        
        if (document.title == "Graphe Pression Casing Shoe")
            {
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                
                initialiserGraphesDonneePresCasingShoe();
            }
            
        if (document.title == "Graphe Température Cavern")
            {
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                
                initialiserGraphesDonneeTempCavern();
            }
        
        if (document.title == "Graphe Pression Cavern")
            {
                $('span.nomCavite').text(sessionStorage.getItem("NomCaviteActuel"));
                $('#caviteStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                
                initialiserGraphesDonneePresCavern();
            }
        
        
        
        
        
        
        if (document.title == "Paramètres du site")
            {
                $('span.nomSite').text(sessionStorage.getItem("NomSiteActuel"));

                $('#parametreStyleTopLogo img').attr('src',sessionStorage.getItem("LogoCaviteExp"));
                
                getListeComposantsInformation();
                //getListeComposantGaz();  //Appellée à la fin de l'execution de getListeComposantsInformation()
            }
        
        if (document.title == "Liste des contacts")
            {
               getListeContactExploitant();
                $('input[name="nC3"]').val(sessionStorage.getItem("NomExploitantActuel"));
                $('#contacts p.nomExploitant').text(sessionStorage.getItem("NomExploitantActuel"));
                
            }
        
        if (document.title == "Liste des contacts d'un site")
            {
                getListeContactSite();
                $('#contacts p.nomSite').text(sessionStorage.getItem("NomSiteActuel"));
                $('input[name="nC11"]').val(sessionStorage.getItem("NomSiteActuel"));
                
                getListeContactSiteAssignation();
            }
        
        
    }



    /**
    *
    *   Recevoir liste des sites
    **/
    function getListeSites()
    {
        $.get("/api/getAllSite").done(function(response)
        {
            preparerListeSites(response);
            
            $('div#chargementPage').fadeOut();   
        })
        .fail(function()
              {
             $('div#listeSites ul').append('<p style="margin-top: 50px;">La liste des sites n\'a pas pu être récupérée.</p>');
            
            $('div#chargementPage').fadeOut();   
        });
    }



    /** 
    *
    *   Afficher la liste des sites en paramètre via json
    **/ 
    function preparerListeSites(json)
    {
            var data = json;

            var stringSite = "";
            
            
        
            for (i=0; i < data.length; i++)
            {
                stringSite += '<li> <img src="' + data[i].ImageSite + '"/>  <div class="infoSite"> <div class="infoSiteLeft">  <h3>' + data[i].SiteName +'</h3><p class="date"><span class="trn">Créé le</span><span> ' + data[i].Date  + '</span></p></div><div class="infoSiteRight"> <img src="' + data[i].LogoExploitant + '"/></div> </div>  <div class="boutonConnexion">  <p class="trn">Connexion</p>   <div class="formConnexion">  <form id="' + data[i].SiteName +'" name="searchform2" method="get" action="#"> <div class="fieldcontainer">  <input type="password" name="s2" id="s2" class="searchfieldjs" placeholder="Mot de Passe" tabindex="2"><input type="submit" name="search2btn" value="" id="search2btn"> </div>  </form> </div> </div></li>';
            }

            $('div#listeSites ul').append(stringSite);
        
            if (sessionStorage.getItem("Langue") == "en")
                traductionAnglaise();
            else    
                traductionFrançaise();
        
        }


    
    /**
    *
    *   Envoyer mot de passe et nom du site auquel utilisateur se connecte pour vérifier s'il peut
    **/
    function connexionSite(NomSite, Password)
    {
        $.post("/api/sendLoginForSite",
	    JSON.stringify({SiteName : NomSite, Password:Password}))
	    .done(function(response)
        {
            document.location.href = '/sites/caviteSite.html';
        })
        .fail(function()
              {
               $('#infoConnexion').remove();
               $('div#listeSites').prepend('<p id="infoConnexion">Mauvais mot de passe</p>')
        });
    }
    
    

    /**
    *
    *   Recevoir la liste des site selon le champs de recherche
    **/
    function searchSite(champs)
    {
        $.post("/api/getListSiteBySearch",
               JSON.stringify({NameSearch : champs}))
	    .done(function(response)
        {
            $('div#chargementPage').fadeIn('fast');
            
            $('#listeSites ul li').remove(); //supprimer les anciens sites
            preparerListeSites(response);
            
            $('.erreurSearchSite').slideUp();
            $('div#chargementPage').fadeOut();
        })
        .fail(function()
        {
            $('.erreurSearchSite').slideDown();
            
        });
    }


/************************
**
**  exploitants.html  ***
**
******************/



    /**
    *
    *   Recevoir liste des exploitants
    **/
    function getListeExploitant()
    {
        
        $.get("/api/getListExploitant").done(function(response)
        {
            preparerListeExploitants(response);
        })
        .fail(function()
              {

        });
    }




    /** 
    *
    *   Afficher la liste des exploitants
    **/ 
    function preparerListeExploitants(json)
    {
        var data =  json;

        var stringExploitant = "";
        var stringListeSite ="";


        for (i=0; i < data.length; i++)
        {
            for (j=0 ; j < data[i].ListNameSite.length ; j++) //récupérer liste des sites de l'exploitant
            {
                stringListeSite += '<p>' + data[i].ListNameSite[j].SiteName +  '</p>' ;                
            }
            
            if (sessionStorage.getItem("Langue") == "fr")
                stringExploitant +=   '<div class="informationExploitant" name="' + data[i].Name + '"> <div class="nomExploitant"><img src="' + data[i].Logo  + '"/><p class="ancienNomExploitant">' + data[i].Name + '</p>  </div><div class="adresseExploitant"> <p>' + data[i].Adress + '</p><p>' + data[i].Town + '</p>  <p>' + data[i].Country + '</p> <p>' + data[i].Mail + '</p> </div><div class="sitesExploitant">' + stringListeSite + '</div> <div class="boutonExploitant">  <p class="boutonTurquoise modifContact trn">Modifier</p>   <a href="contacts.html"><p class="boutonTurquoise voirContact trn">Voir les contacts</p></a>  <p class="boutonTurquoise exportExploitant trn">Exporter</p> </div>  <!-- Modifier un exploitant -->   <form action="/api/updateExploitant" method=POST enctype="multipart/form-data"> <div class="modifierInformationExploitant"><div class="modifierNomExploitant"> <p><label for="file-input">   <img src="' + data[i].Logo + '" accept="image/*">    </label> <input id="file-input" name="photoExploitant" exploitant="' + data[i].Name + '" type="file" style="width:250px; font-size:13px;"> <input type="text" value="' + data[i].Name + '" name="mE8" style="display:none" /> <input  type="text"  value="/sites/exploitants.html" name="mE7" style="display:none" /></p><p><input type="text" name="nomExploitant" value="' + data[i].Name + '" initialvalue="' + data[i].Name + '"></p>  </div> <div class="modifierAdresseExploitant">    <p><input type="text" name="adresseExploitant" value="' + data[i].Adress + '" initialvalue="' + data[i].Adress + '"></p>   <p><input type="text" name="villeExploitant" value="' + data[i].Town + '" initialvalue="' + data[i].Town + '"></p><p><input type="text" name="paysExploitant" value="' + data[i].Country + '" initialvalue="' + data[i].Country + '"></p> <p><input type="text" name="mailExploitant" value="' + data[i].Mail + '" initialvalue="' + data[i].Mail + '"></p>  </div> <div class="modifierSitesExploitant">' + stringListeSite + '  </div> <div class="modifierBoutonExploitant"> <p class="boutonTurquoise trn">Enregistrer</p></form> <p class="boutonGris trn">Annuler</p><p class="boutonRouge trn">Supprimer</p> </div></div></div>';
            else
                stringExploitant +=   '<div class="informationExploitant" name="' + data[i].Name + '"> <div class="nomExploitant"><img src="' + data[i].Logo  + '"/><p class="ancienNomExploitant">' + data[i].Name + '</p>  </div><div class="adresseExploitant"> <p>' + data[i].Adress + '</p><p>' + data[i].Town + '</p>  <p>' + data[i].Country + '</p> <p>' + data[i].Mail + '</p> </div><div class="sitesExploitant">' + stringListeSite + '</div> <div class="boutonExploitant">  <p class="boutonTurquoise modifContact trn">' + t["Modifier"].en + '</p>   <a href="contacts.html"><p class="boutonTurquoise voirContact trn">' + t["Voir les contacts"].en + '</p></a>  <p class="boutonTurquoise exportExploitant trn">' + t["Exporter"].en + '</p> </div>  <!-- Modifier un exploitant -->   <form action="/api/updateExploitant" method=POST enctype="multipart/form-data"> <div class="modifierInformationExploitant"><div class="modifierNomExploitant"> <p><label for="file-input">   <img src="' + data[i].Logo + '" accept="image/*">    </label> <input id="file-input" name="photoExploitant" exploitant="' + data[i].Name + '" type="file" style="width:250px; font-size:13px;"> <input type="text" value="' + data[i].Name + '" name="mE8" style="display:none" /> <input  type="text"  value="/sites/exploitants.html" name="mE7" style="display:none" /></p><p><input type="text" name="nomExploitant" value="' + data[i].Name + '" initialvalue="' + data[i].Name + '"></p>  </div> <div class="modifierAdresseExploitant">    <p><input type="text" name="adresseExploitant" value="' + data[i].Adress + '" initialvalue="' + data[i].Adress + '"></p>   <p><input type="text" name="villeExploitant" value="' + data[i].Town + '" initialvalue="' + data[i].Town + '"></p><p><input type="text" name="paysExploitant" value="' + data[i].Country + '" initialvalue="' + data[i].Country + '"></p> <p><input type="text" name="mailExploitant" value="' + data[i].Mail + '" initialvalue="' + data[i].Mail + '"></p>  </div> <div class="modifierSitesExploitant">' + stringListeSite + '  </div> <div class="modifierBoutonExploitant"> <p class="boutonTurquoise trn">' + t["Enregistrer"].en + '</p></form> <p class="boutonGris trn">' + t["Annuler"].en + '</p><p class="boutonRouge trn">' + t["Supprimer"].en + '</p> </div></div></div>';
                
            stringListeSite = "";
        }

        $('div#exploitants div.table').append(stringExploitant); //Insérer liste des exploitant
        
        $('div#chargementPage').fadeOut();
    }

  
    
    /**
    *
    *   Envoyer nom de l'exploitant pour le supprimer
    **/
    
    function supressionExploitant(NomExploitantSupression)
    {
        $.post("/api/removeExploitant",
	    JSON.stringify({ExploitantName : NomExploitantSupression}))
	    .done(function(response)
        {
             $('.overlay').fadeOut();
            $('.overlay-suppressionExploitant').fadeOut();
            
            window.location.href = "exploitants.html";
            
        })
        .fail(function()
        {
            $('.overlay-suppressionExploitant').fadeOut();
            
            //$('.overlay').fadeIn();
            $('div.overlay-erreurSupressionExploitant').fadeIn();
        });
    }
    
    






/************************
**
**  nouveauSite.html  ***
**
******************/


    /**
    *
    *   Recevoir liste des exploitants pour assigner à un site
    **/
    function getListeExploitantOption()
    {
        $.get("/api/getListNameExploitant").done(function(response)
        {
            preparerListeExploitantsOption(response);
        })
        .fail(function()
              {

        });
    }



    /** 
    *
    *   Afficher la liste des noms des exploitants
    **/ 
       function preparerListeExploitantsOption(json)
        {
            var data =  json;

            var stringExploitant ="";
            
            for (i=0; i < data.length; i++)
            {
                stringExploitant += '<option value="' + data[i].Name  +  '">' + data[i].Name  +  '</option>' ;
            }

            $('select#choix_exploitant').append(stringExploitant); //Insérer liste des noms des exploitant
        }




/**********************
**
**  contacts.html  ****
**
******************/

  

    /**
    *
    *   Recevoir liste des contacts pour un exploitant
    **/
    function getListeContactExploitant(NomSite, Password)
    {
         $.post("/api/getListContactForExp",
	    JSON.stringify({ExploitantName :  sessionStorage.getItem("NomExploitantActuel")}))
	    .done(function(response)
        {
            preparerListeContact(response);
             
            $('div#chargementPage').fadeOut();
        })
        .fail(function()
              { 
             
             
            $('div#chargementPage').fadeOut();
        });
    }
    


    /** 
    *
    *   Afficher la liste des contacts
    **/ 
       function preparerListeContact(json)
        {
            var data =  json;

            var stringContact = "";
            var stringListeSite ="";

            for (i=0; i < data.length; i++)
            {
                if (data[i].IsAffected)
                    stringListeSite = '<p><span class="siteNameContact">' + data[i].SiteName + '<span></p>' + '<p>' + data[i].SiteTown + '</p>';
                else
                    stringListeSite = '<p> </p>'

                    
                 if (sessionStorage.getItem("Langue") == "fr")    
                     stringContact += '<div class="informationContact" id="' + data[i].Id + '"> <div class="nomContact"><img src="' + data[i].Image + '"/><p>' + data[i].Name + ' ' + data[i].LastName + '</p>  <p class="entrepriseContact">' + data[i].ExploitantName + '</p>  </div>   <div class="fonctionContact"> <p>' + data[i].Function + '</p>  <p class="serviceContact">' + data[i].Service + '</p>  </div>  <div class="telContact">    <p>' + data[i].Phone + '</p>        <p class="mailContact">' + data[i].Email + '</p>  </div> <div class="sitesContact">    ' +  stringListeSite + '    </div> <div class="menuContact">        <p class="modifierContact"><img src="../asset/img/exploitants/modifier.png"/><span class="trn">Modifier</span></p><p class="SupprimerContact"><img src="../asset/img/exploitants/supprimer.png"/><span class="trn">Supprimer</span></p>  </div>  <!-- Modifier un contact --> <div class="modifierInformationContact"><form action="/api/updateContact" method="POST" enctype="multipart/form-data"><div class="modifierNomContact">  <label for="file-input-nE6">  <img src="' + data[i].Image + '" class="modifierImagePrincipale"/>  </label>  <p><input type="text" name="mC11" value="' + data[i].Name + '" style="display:none"><input type="text" style="display:none" name="mC12" value="' + data[i].LastName + '"><input type="text" style="display:none" name="mC8" value="/sites/contacts.html"></p><p><input type="text" name="nomContact" value="' + data[i].Name + '" initialvalue="' + data[i].Name + '"><input type="text" name="prenomContact" value="' + data[i].LastName + '" initialvalue="' + data[i].LastName + '"></p> <p><input id="file-input-nE6" name="photoContact" contact="' + data[i].LastName + '" type="file" accept="image/*" style="width:250px; font-size:13px; margin-top:10px;"></p> <p class="modifierEntrepriseContact"><input type="text" name="entrepriseContact" value="' + data[i].ExploitantName + '" style="display:none;"></p>   </div><div class="modifierFonctionContact"> <p><input type="text" name="fonctionContact" value="' + data[i].Function + '" initialvalue="' + data[i].Function + '"></p> <p class="modifierServiceContact"><input type="text" name="serviceContact" value="' + data[i].Service + '" initialvalue="' + data[i].Service + '"></p> </div>    <div class="modifierTelContact">    <p><input type="text" name="telContact" value="' + data[i].Phone + '" initialvalue="' + data[i].Phone + '"></p>   <p class="modifierMailContact"><input type="text" name="mailContact" value="' + data[i].Email + '" initialvalue="' + data[i].Email + '"></p>     </div> <div class="modifierMenuContact"><p class="enregistrerContact"><span class="trn">Enregistrer</span></p>  <p class="annulerContact"><span class="trn">Annuler</span></p>  </div> </div> </form></div>';
                else
                      stringContact += '<div class="informationContact" id="' + data[i].Id + '"> <div class="nomContact"><img src="' + data[i].Image + '"/><p>' + data[i].Name + ' ' + data[i].LastName + '</p>  <p class="entrepriseContact">' + data[i].ExploitantName + '</p>  </div>   <div class="fonctionContact"> <p>' + data[i].Function + '</p>  <p class="serviceContact">' + data[i].Service + '</p>  </div>  <div class="telContact">    <p>' + data[i].Phone + '</p>        <p class="mailContact">' + data[i].Email + '</p>  </div> <div class="sitesContact">    ' +  stringListeSite + '    </div> <div class="menuContact">        <p class="modifierContact"><img src="../asset/img/exploitants/modifier.png"/><span class="trn">' + t["Modifier"].en + '</span></p><p class="SupprimerContact"><img src="../asset/img/exploitants/supprimer.png"/><span class="trn">' + t["Supprimer"].en + '</span></p>  </div>  <!-- Modifier un contact --> <div class="modifierInformationContact"><form action="/api/updateContact" method="POST" enctype="multipart/form-data"><div class="modifierNomContact">  <label for="file-input-nE6">  <img src="' + data[i].Image + '" class="modifierImagePrincipale"/>  </label>  <p><input type="text" name="mC11" value="' + data[i].Name + '" style="display:none"><input type="text" style="display:none" name="mC12" value="' + data[i].LastName + '"><input type="text" style="display:none" name="mC8" value="/sites/contacts.html"></p><p><input type="text" name="nomContact" value="' + data[i].Name + '" initialvalue="' + data[i].Name + '"><input type="text" name="prenomContact" value="' + data[i].LastName + '" initialvalue="' + data[i].LastName + '"></p> <p><input id="file-input-nE6" name="photoContact" contact="' + data[i].LastName + '" type="file" accept="image/*" style="width:250px; font-size:13px; margin-top:10px;"></p> <p class="modifierEntrepriseContact"><input type="text" name="entrepriseContact" value="' + data[i].ExploitantName + '" style="display:none;"></p>   </div><div class="modifierFonctionContact"> <p><input type="text" name="fonctionContact" value="' + data[i].Function + '" initialvalue="' + data[i].Function + '"></p> <p class="modifierServiceContact"><input type="text" name="serviceContact" value="' + data[i].Service + '" initialvalue="' + data[i].Service + '"></p> </div>    <div class="modifierTelContact">    <p><input type="text" name="telContact" value="' + data[i].Phone + '" initialvalue="' + data[i].Phone + '"></p>   <p class="modifierMailContact"><input type="text" name="mailContact" value="' + data[i].Email + '" initialvalue="' + data[i].Email + '"></p>     </div> <div class="modifierMenuContact"><p class="enregistrerContact"><span class="trn">' + t["Enregistrer"].en + '</span></p>  <p class="annulerContact"><span class="trn">' + t["Annuler"].en + '</span></p>  </div> </div> </form></div>';                   
                    
                    
                stringListeSite = "";
               
            }
            $('div#contacts div.table').append(stringContact); //Insérer liste des contacts
            
        }


    /** 
    *
    *   Supprimer un contact
    **/ 
    $(document).on('click','.overlay-suppressionContact p.boutonRouge', function()
    {
        $.post("/api/removeContact",
	    JSON.stringify({SiteName : sessionStorage.getItem("suppressionContactSiteName"), IdContact :sessionStorage.getItem("suppressionContactId"), ExploitantName : sessionStorage.getItem("suppressionContactExploitant") }))
	    .done(function(response)
        {  
            window.location.reload();   
        })
        .fail(function()
        {
            
        });
    });







/**************************
**
**  contactsSite.html  ****
**
******************/

    /**
    *
    *   Recevoir liste des contacts pour un site
    **/
     function getListeContactSite()
    {  
        $.post("/api/getListContactForSite",
	    JSON.stringify({SiteName :  sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            preparerListeContactSite(response);
        })
        .fail(function()
              {
        });
        
    }


    /** 
    *
    *   Afficher la liste des contacts
    **/ 
    function preparerListeContactSite(json)
    {
            var data =  json;

            var stringContact = "";

            for (i=0; i < data.length; i++)
            {  
                stringContact += '<div class="informationContact" id="' + data[i].Id + '"> <div class="nomContact"><img src="' + data[i].Image + '"/><p>' + data[i].Name + ' ' + data[i].LastName + '</p>  <p class="entrepriseContact">' + data[i].ExploitantName + '</p>  </div>   <div class="fonctionContact"> <p>' + data[i].Function + '</p>  <p class="serviceContact">' + data[i].Service + '</p>  </div>  <div class="telContact">    <p>' + data[i].Phone + '</p>        <p class="mailContact">' + data[i].Email + '</p>  </div> <div class="sitesContact">    <p><span class="siteNameContact">' + data[i].SiteName + '</span></p>' + '<p>' + data[i].SiteTown + '</p> </div> <div class="menuContact">        <p class="modifierContact"><img src="../asset/img/exploitants/modifier.png"/> Modifier</p><p class="SupprimerContact"><img src="../asset/img/exploitants/supprimer.png"/>Supprimer</p> <p class="desassignerContact"><img src="../asset/img/desassigner.png"/>Désaffecter</p>    </div>  <!-- Modifier un contact --> <div class="modifierInformationContact"><form action="/api/updateContact" method="POST" enctype="multipart/form-data"><div class="modifierNomContact">  <label for="file-input-nE6">  <img src="' + data[i].Image + '" class="modifierImagePrincipale"/>  </label>  <p><input type="text" name="mC11" value="' + data[i].Name + '" style="display:none"><input type="text" style="display:none" name="mC12" value="' + data[i].LastName + '"><input type="text" style="display:none" name="mC8" value="/sites/contactsSite.html"></p><p><input type="text" name="nomContact" value="' + data[i].Name + '" initialvalue="' + data[i].Name + '"><input type="text" name="prenomContact" value="' + data[i].LastName + '" initialvalue="' + data[i].LastName + '"></p> <p><input id="file-input-nE6" name="photoContact" contact="' + data[i].LastName + '" type="file" accept="image/*" style="width:250px; font-size:13px; margin-top:10px;"></p>   <p class="modifierEntrepriseContact"><input type="text" name="entrepriseContact" value="' + data[i].ExploitantName + '" style="display:none"></p> </div><div class="modifierFonctionContact"> <p><input type="text" name="fonctionContact" value="' + data[i].Function + '" initialvalue="' + data[i].Function + '"></p> <p class="modifierServiceContact"><input type="text" name="serviceContact" value="' + data[i].Service + '" initialvalue="' + data[i].Service + '"></p> </div>    <div class="modifierTelContact">    <p><input type="text" name="telContact" value="' + data[i].Phone + '" initialvalue="' + data[i].Phone + '"></p>   <p class="modifierMailContact"><input type="text" name="mailContact" value="' + data[i].Email + '" initialvalue="' + data[i].Email + '"></p>     </div> <div class="modifierMenuContact"><p class="enregistrerContact">Enregistrer</p>  <p class="annulerContact">Annuler</p>  </div> </div> </form></div>';
            }
            
            $('div#contacts div.table').append(stringContact); //Insérer liste des contacts
        
            $('div#chargementPage').fadeOut();
        }


    /**
    *
    *   Recevoir liste des contacts qui n'ont pas de site assigné
    **/
     function getListeContactSiteAssignation()
    {  
        $.post("/api/getListContactForExpWithoutSite",
	    JSON.stringify({SiteName :  sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            preparerListeContactSiteAssignation(response);
        })
        .fail(function()
              {
        });
    }







    /** 
    *
    *   Préparer la liste des contacts sans site à assigner potentiellement
    **/ 
       function preparerListeContactSiteAssignation(json)
        {
            var data =  json;
            var stringContact = "";

            for (i=0; i < data.length; i++)
            {     
                 stringContact += ' <option value="' + data[i].Id + '">' + data[i].Name + ' ' + data[i].LastName + '</option>';
            }
            $('.overlay-assignerContactSite select').append(stringContact); //Insérer liste des contacts
        }




    /**
    *
    *   Assigner des contacts à un site
    **/
     function assignerContactsSite(tabContact)
    {
        $.post("/api/affectContactToSite",
	    JSON.stringify({SiteName :  sessionStorage.getItem("NomSiteActuel"), TabContact : tabContact }))
	    .done(function(response)
        {
            $('.overlay-assignerContactSite').fadeOut('fast');
            window.location.reload();
        })
        .fail(function()
              {
        });
        
    }


    /** 
    *
    *   Désassiginer un contact
    **/ 
    $(document).on('click','.overlay-desassignationContact p.boutonRouge', function()
    {
        $.post("/api/unAffectContactFromSite",
	    JSON.stringify({SiteName : sessionStorage.getItem("desassignationContactSiteName"), IdContact :sessionStorage.getItem("desassignationContactId") }))
	    .done(function(response)
        {  
            window.location.reload();   
        })
        .fail(function()
        {
            
        });
    });



/************************
**
**  caviteSite.html  ****
**
******************/


    /**
    *
    *   Recevoir les informations du site
    **/
    function getInfoSite()
    {
         $.post("/api/getSiteInfo", 
	    JSON.stringify({SiteName : nomSiteActuel}))
	    .done(function(response)
        {
             afficherSiteInfo(response);
        })
        .fail(function()
        {
             
        });
    }


    /**
    *
    *   Afficher les informations du site sur lequel nous sommes
    **/
    function afficherSiteInfo(json)
    {
        $('input[name="mS1"]').attr("initialvalue", json.Name )   
        $('input[name="mS2"]').attr("initialvalue", json.Adress )   
        $('input[name="mS3"]').attr("initialvalue", json.Town )   
        $('input[name="mS4"]').attr("initialvalue", json.Country )       
        
        
        $('input[name="mS6"]').val( $('input[name="mS1"]').attr("initialvalue"));    
    }

    /**
    *
    *   Recevoir liste des cavité pour un site donné 
    **/
    function getListeCavites()
    {
          var dfd = $.Deferred();
        
          setTimeout(function () {
            dfd.resolve(42);
          }, 300);
        
        
         $.post("/api/getListCavity", 
	    JSON.stringify({SiteName : nomSiteActuel}))
	    .done(function(response)
        {
            //gérer la réponse
             preparerListeCavites(response);
        })
        .fail(function()
              {
        });
          return dfd.promise();
    }


        
    /**
    *
    *   Ajout d'une cavité
    */
   function ajoutCavite()
    {
        $.post("/api/addCavity", 
	    JSON.stringify({SiteName :  sessionStorage.getItem("NomSiteActuel"), CavityName : nomCaviteNouvelle, Height : hauteurCaviteNouvelle, Depth : profondeurCaviteNouvelle, Diameter : diametreCaviteNouvelle, Volume : volumeCaviteNouvelle }))
	    .done(function(response)
        {
            $('#nouvelleCavite p.caviteErreurNom').slideUp();
            $('#nouvelleCavite p.caviteErreurChamps').slideUp();
            location.reload();
             
        })
        .fail(function()
              {
                $('#nouvelleCavite p.caviteErreurChamps').slideUp();
                $('#nouvelleCavite p.caviteErreurNom').slideDown();

        });
    }

    /**
    *
    *   Modifier une cavité
    */
   function enregistrerCavite()
    {
        $.post("/api/updateCavity", 
	    JSON.stringify({SiteName :  sessionStorage.getItem("NomSiteActuel"), NewCavityName : nomCaviteNouvelle, Height : hauteurCaviteNouvelle, Depth : profondeurCaviteNouvelle, Diameter : diametreCaviteNouvelle, Volume : volumeCaviteNouvelle, OldCavityName : sessionStorage.getItem("NomCaviteActuel")  }))
	    .done(function(response)
        {
            $('#modifierCavite p.caviteErreurNom').slideUp();
            $('#modifierCavite p.caviteErreurChamps').slideUp();
            location.reload();
             
        })
        .fail(function()
              {
                $('#modifierCavite p.caviteErreurChamps').slideUp();
                $('#modifierCavite p.caviteErreurNom').slideDown();

        });
    }
 

     /**
    *
    *   Supprimer une cavité
    */
    function supprimerCavite(passwordCavite)
    {
        
        $.post("/api/removeCavity", 
	    JSON.stringify({SiteName :  sessionStorage.getItem("NomSiteActuel"), CavityName : sessionStorage.getItem("NomCaviteActuel"), Password : passwordCavite  }))
	    .done(function(response)
        {
            sessionStorage.setItem("NomCaviteActuel", "" );
            location.reload();
            $('p.caviteErreurChamps').slideUp();
            $('p.erreurMdpCav').slideUp();
        })
        .fail(function()
        {
            //erreur mdp a afficher
            $('p.caviteErreurChamps').slideUp();
            $('p.erreurMdpCav').slideDown();

        });
        
    }
    
                     

 
    /** 
    *
    *   Préparer un tableau qui contient la liste des cavités et leurs informations liées
    **/ 
       function preparerListeCavites(json)
        {
            var data =  json;

            var stringSelect = "";
            var stringTableauInfo ="";
            
            //pour l'export par mail
            var stringListeNomCaviteForMail = '<p><input type="checkbox" value="' + sessionStorage.getItem("NomSiteActuel") + '"> <span>Site</span> : ' + sessionStorage.getItem("NomSiteActuel") + '</p>';

            
            for (i=0 ; i < data.length; i++)
            {

                $('#caviteStyleTopLogo img').attr("src", data[i].LogoExp);
                
                sessionStorage.setItem("LogoCaviteExp", data[i].LogoExp);

                stringSelect +=' <option value="' + data[i].CavityName + '">' + data[i].CavityName + ' </option>';
                       
          
                if (sessionStorage.getItem("Langue") == "fr")
                    stringListeNomCaviteForMail += '<p><input type="checkbox" value="' + data[i].CavityName + '"> <span class="trn">Cavité</span> : ' + data[i].CavityName + '</p>';
                else 
                    stringListeNomCaviteForMail += '<p><input type="checkbox" value="' + data[i].CavityName + '"> <span class="trn">Cavity</span> : ' + data[i].CavityName + '</p>';
                    
                
                
                //infoChaqueCavite.push(data[i].CaviteName);
                infoChaqueCavite[data[i].CavityName] = new Array(data[i].Height, data[i].Diameter, data[i].Depth, data[i].Volume  );
                    
                
                //Si on arrive pour la première fois sur le site (=> une cavité pas déjà chargée)
                 if (sessionStorage.getItem("NomCaviteActuel") == "")
                 {
                     if(i==0) //charger de la cavité chargée par défaut
                        {
                            sessionStorage.setItem("NomCaviteActuel", data[i].CavityName);

                            $('span.nomCavite').text(data[i].CavityName);
                            $('input[name="infoCavite1"]').val(data[i].Height);
                            $('input[name="infoCavite2"]').val(data[i].Diameter);
                            $('input[name="infoCavite3"]').val(data[i].Depth);
                            $('input[name="infoCavite4"]').val(data[i].Volume);
                        }
                 }
                else //Recharger les infos de la bonne cavité en cours
                    {
                        if ( data[i].CavityName == sessionStorage.getItem("NomCaviteActuel") )
                        {
                            $('span.nomCavite').text(data[i].CavityName);
                            $('input[name="infoCavite1"]').val(data[i].Height);
                            $('input[name="infoCavite2"]').val(data[i].Diameter);
                            $('input[name="infoCavite3"]').val(data[i].Depth);
                            $('input[name="infoCavite4"]').val(data[i].Volume);
                        }
                    }
                    
                    
                
            }

            $('select#choix_cavite').append(stringSelect); //Insérer liste des cavités
            $('div.listeCaviteMail').append(stringListeNomCaviteForMail); //Insérer liste des cavités (export mail)
            
        }

  

    
    /**
    *
    *   Envoyer mot de passe et nom du site pour le supprimer
    **/
    function supressionSite(NomSite, Password)
    {
        $.post("/api/removeSite",
	    JSON.stringify({SiteName : NomSite, Password:Password}))
	    .done(function(response)
        {
            document.location.href = '../index.html';
        })
        .fail(function()
              {
               $('.erreurSupression').slideUp();
               $('.erreurMdp').slideDown();
        });
    }
    
    

    
    /**
    *
    *   Envoyer mot de passe et nom du site pour le modifier
    **/
    function modificationMdp(NomSite, Password)
    {
        $.post("/api/updatePasswordSite",
	    JSON.stringify({SiteName : NomSite, Password:Password}))
	    .done(function(response)
        {
            document.location.href = 'caviteSite.html';
        })
        .fail(function()
              {
               $('.erreurModification').slideUp();

        });
    }
    


    /**
    *
    *   Recevoir liste des points pour afficher courbe TEMPERATURE - point réel 
    **/
    function getDataTempCavitePointReel()
    {
         $.post("/api/getRealTemperatureForCavity",
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataTempJsonReelWithIncoherent = response;
            dataTempJsonReel = prepareDataReelCourbes(response);
                //si nous sommes sur la page de Gestion des données relevées pas besoin de lancer les fonctions suivantes
            if (document.title != "Gestion des données relevées")
                getDataTempCavitePointSimule();
       
         })
        .fail(function()
        {
            dataTempJsonReel = [];
            dataTempJsonSimule = []; 
             
            if (document.title != "Gestion des données relevées") 
                afficherDoubleGraphTemp();
             
            bloquerPageCalage();
        });
        
    }


    /**
    *
    *   Recevoir liste des points pour afficher courbe TEMPERATURE - point simulée 
    **/
    function getDataTempCavitePointSimule()
    {
         $.post("/api/getSimulatedTemperatureForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataTempJsonSimule = prepareDataSimuleCourbes(response);
        
            afficherDoubleGraphTemp(); //Charger les données de la deuxieme courbes ensuites, sinon risque d'afficher les deux courbes sans que chaques données n'aient été récupérées
        
        })
        .fail(function()
        {
            dataTempJsonSimule = []; 
            afficherDoubleGraphTemp();
        });
    }

    /**
    *
    *   Afficher les deux cours pour la  TEMPERATURE - 
    **/
    function afficherDoubleGraphTemp()
    {        
        var chartTemp = new CanvasJS.Chart("chartContainerTempCavite",
        {
            zoomEnabled: true, 
            legend:
            {
                verticalAlign : 'top',
                horizontalAlign : 'right',
            },
            axisX:{      
                valueFormatString: "MM-YYYY" ,
                interval: 5,
                    title: "Time",
                labelAngle: -50,
                labelFontColor: "black",
                gridColor: "#d2d2d2",
                gridThickness: 1    
            },
            axisY: {
                labelFontColor: "black",
                title: "Temperature",
                valueFormatString: "####",
                gridColor: "#d2d2d2",
                includeZero: false,
                labelFontColor: "#328fe1",
                titleFontColor: "#328fe1",
                lineColor: "#328fe1"
            },
            data: [
            {        
                type: "line",
                connectNullData: false,
                color: "#328fe1",
                legendText: "Real data",
                showInLegend: true, 
                markerSize:3,
                dataPoints:dataTempJsonReel
            },
            {        
                type: "line",
                connectNullData: false,
                color: "#e90d3f",
                legendText: "Simulated data",
                        showInLegend: true, 
                markerSize:3,
                dataPoints:dataTempJsonSimule
            },

            ]
        });

        chartTemp.render();
        
        boolTempCaviteCharge = true;
        if (boolPresCaviteCharge && boolVolCaviteCharge)
            $('div#chargementPage').fadeOut();
        
    }


    /**
    *
    *   Recevoir liste des points pour afficher courbe PRESSION - point réel 
    **/
    function getDataPresCavitePointReel()
    {
         $.post("/api/getRealPressionForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
             dataPresJsonReelWithIncoherent = response;
             
            dataPresJsonReel = prepareDataReelCourbes(response);
             
            //si nous sommes sur la page de Gestion des données relevées pas besoin de lancer les fonctions suivantes
            if (document.title != "Gestion des données relevées") 
                getDataPresCavitePointSimule(); //Charger les données de la deuxieme courbes ensuites, sinon risque d'afficher les deux courbes sans que chaques données n'aient été récupérées
             
        })
        .fail(function()
        {
            dataPresJsonReel = [] ;
            dataPresJsonSimule = [] ;
             
            if (document.title != "Gestion des données relevées") 
                afficherDoubleGraphPres();
            
            bloquerPageCalage();
        });
    }


    /**
    *
    *   Recevoir liste des points pour afficher courbe PRESSION - point simulée 
    **/
    function getDataPresCavitePointSimule()
    {
         $.post("/api/getSimulatedPressionForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataPresJsonSimule = prepareDataSimuleCourbes(response);
        
            afficherDoubleGraphPres();
        })
        .fail(function()
        {
            dataPresJsonSimule = [] ;
            afficherDoubleGraphPres();
        });
    }



    /**
    *
    *   Afficher les deux cours pour la  PRESSION - 
    **/
    function afficherDoubleGraphPres()
    {
    
        var chartTemp = new CanvasJS.Chart("chartContainerPressionCavite",
        {
           zoomEnabled: true, 
            legend:
            {
                verticalAlign : 'top',
                horizontalAlign : 'right',
            },
            axisX:{      
            valueFormatString: "MM-YYYY" ,
            interval: 5,
            title: "Time",
            labelAngle: -50,
            labelFontColor: "black",
            gridColor: "#d2d2d2",
            gridThickness: 1    
        },
        axisY: {
            labelFontColor: "black",
            title: "Pressure",
            includeZero: false,
            valueFormatString: "####",
            gridColor: "#d2d2d2",
            labelFontColor: "#328fe1",
            titleFontColor: "#328fe1",
            lineColor: "#328fe1"

        },
            data: [
            {        
                type: "line",
                connectNullData: false,
                legendText: "Real data",
                        showInLegend: true, 
                color: "#328fe1",
                markerSize:3,
                dataPoints:dataPresJsonReel
            },
            {        
                type: "line",
                connectNullData: false,
                legendText: "Simulated data",
                        showInLegend: true, 
                color: "#e90d3f",
                markerSize:3,
                dataPoints:dataPresJsonSimule
            }

            ]
        });

        chartTemp.render();
        
        boolPresCaviteCharge = true;
        if (boolTempCaviteCharge && boolVolCaviteCharge)
            $('div#chargementPage').fadeOut();
        
        
        
    }

   

     /**
    *
    *   Recevoir liste des points pour afficher courbe VOLUME - point réel 
    **/
    function getDataVolumeCavitePointReel()
    {
        
        $.post("/api/getRealVolumeForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataVolumeJsonReelWithIncoherent = response;
            
            dataVolumeJsonReel = prepareDataReelCourbes(response);
            
            //si nous sommes sur la page de Gestion des données relevées pas besoin de lancer les fonctions suivantes 
            //if (document.title != "Gestion des données relevées") 
                getDataDebitCavitePointReel(); //Charger les données de la deuxieme courbes ensuites, sinon risque d'afficher les deux courbes sans que chaques données n'aient été récupérées
           //else
            
             
        })
        .fail(function()
        {
            dataVolumeJsonReel = [];
            dataDebitJsonReel = [];
            
             if (document.title != "Gestion des données relevées") 
                afficherDoubleGraphVolume();
            else
                getDataDebitCavitePointReel();
            
        });
    }


    /**
    *
    *   Recevoir liste des points pour afficher courbe DEBIT -point réel 
    **/
    function getDataDebitCavitePointReel()
    {
         $.post("/api/getRealDebitForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataDebitJsonReel = prepareDataReelCourbes(response);
            
             
            if (document.title != "Gestion des données relevées") 
                afficherDoubleGraphVolume();
            else
                initialiserGraphesDonnee(); //page gererDonneesRelevees.html
        })
        .fail(function()
        {
             
            dataDebitJsonReel = [] ;
             
            if (document.title != "Gestion des données relevées") 
                afficherDoubleGraphVolume();
            else
                initialiserGraphesDonnee(); //page gererDonneesRelevees.html

             
        });
    }

  
   /**
    *
    *   Afficher les deux cours pour le VOLUME - 
    **/
    function afficherDoubleGraphVolume()
    {
            var chartTemp = new CanvasJS.Chart("chartContainerVolumeCavite",
            {
                zoomEnabled: true, 
                legend:
                {
                    verticalAlign : 'top',
                    horizontalAlign : 'right',
                },
                axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Volume",
                    lineColor: "#328fe1",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
			        includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    interval: 40,
                    valueFormatString: "0.00"
                },
                axisY2:{
                    title: "Flow",
                    interval: 2,
			        includeZero: false,
                    lineColor: "#da5f08",
                    labelFontColor: "#da5f08",
                    titleFontColor: "#da5f08"
		              },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Real data - Volume",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataVolumeJsonReel
                    },
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Real data - Flow",   
                        showInLegend: true,            
		                axisYType: "secondary",
                        color: "#da5f08",
                        markerSize:3,
                        dataPoints:dataDebitJsonReel
                    }
                    ]
                });

                chartTemp.render();
        
           
        boolVolCaviteCharge = true;
        if (boolPresCaviteCharge && boolTempCaviteCharge)
            $('div#chargementPage').fadeOut();
        
        
    }

   
    /**
    *
    *   Recevoir les données JSON de Température / Pression ET Volume pour les afficher à l'écran
    **/
      function prepareDataCourbesForDisplay(jsonTemp, jsonPres, jsonVol)
    {
       
        var dataTemp = dataTempJsonReel;
        var dataTempIncoherent = dataTempJsonReelWithIncoherent;
        var dataVol = dataVolumeJsonReel;
        var dataVolIncoherent = dataVolumeJsonReelWithIncoherent;
        var dataPres = dataPresJsonReel;
        var dataPresIncoherent = dataPresJsonReelWithIncoherent;
        var dataDeb = dataDebitJsonReel;
        
        
        
        var chaineTableau = ""; //string listant les informations de la page
        
        var valTemps;  //valeur température / volume / pression à une date précise
        var valVol;
        var valPres;
        var valDebit;
        
        
        for (i=2; i < dataTemp.length; i++)
        {            
            var dateTemp = dataTemp[i].x; //date à laquelle nous somme
                      
            var anneeTemp = dateTemp.getFullYear();
            var moisTemp = dateTemp.getMonth() + 1;
            var jourTemp = dateTemp.getDate();
            
            if (jourTemp < 10)
                jourTemp = "0" + jourTemp;
            
            
            if (moisTemp < 10)
                moisTemp = "0" + moisTemp;
            

            date = jourTemp +"/" + moisTemp + "/" +anneeTemp;

            
            valTemps = dataTemp[i].y;
            valPres = dataPres[i].y;
            valVol  = dataVol[i].y;
            valDebit = dataDeb[i].y ;
             
            valTempsTmp = dataTemp[i].y;
            valPresTmp = dataPres[i].y;
            valVolTmp  = dataVol[i].y;
            valDebitTmp = dataDeb[i].y;
            
            
            
            
            valTempsInco = dataTempIncoherent[i].isIncoherent;
            valPresInco = dataPresIncoherent[i].isIncoherent;
            valVolInco = dataVolIncoherent[i].isIncoherent;
            
            
            if ( valTemps == 0.000000421337 ||  valVol == 0.000000421337 || valPres == 0.000000421337 || valDebit == 0.000000421337 || valTempsInco || valPresInco || valVolInco )
                {
                    
                    if (valTemps == 0.000000421337 )
                        {
                            valTemps = '<td class="sansDonnee"><input type="text" typeChamps="temp" /></td> <td>-</td>';
                        }
                    else
                        if (valTempsInco)
                            {
                                valTemps = '<td> - </td> <td  class="sansDonnee" valChamps="' + valTemps + '" ><input type="text"  typeChamps="temp" name="temp' + jourTemp + moisTemp + anneeTemp + '"/></td>';
                            }
                        else
                        valTemps = '<td> - </td> <td valChamps="' + valTemps + '"> - </td> ';

                    
                     if (valPres == 0.000000421337 )
                        {
                            valPres = '<td class="sansDonnee"><input type="text"  typeChamps="pres" /></td> <td>-</td>';
                        }
                        else
                         if (valPresInco)
                            {
                                valPres = '<td> - </td> <td  class="sansDonnee"  valChamps="' + valPres + '" ><input type="text" typeChamps="pres" name="temp' + jourTemp + moisTemp + anneeTemp + '"/></td>';
                            }
                        else
                         valPres = '<td> - </td> <td valChamps="' + valPres + '"> - </td> ';
                    
                    if (valVol == 0.000000421337)
                        {
                            valVol = '<td class="sansDonnee"><input type="text" typeChamps="vol"/></td> <td>-</td>';
                        }
                    else
                         if (valVolInco)
                            {
                                valVol = '<td> - </td> <td class="sansDonnee" valChamps="' + valVol + '"><input type="text" typeChamps="vol" name="temp' + jourTemp + moisTemp + anneeTemp + '"/></td >';
                            }
                        else
                         valVol = '<td> - </td> <td valChamps="' + valVol + '"> - </td> ';

                    if (valDebit == 0.000000421337 )
                        {
                            valDebit = '<td class="sansDonnee" ><input type="text" typeChamps="deb" name="deb' + jourTemp + moisTemp + anneeTemp + '"/></td><td> - </td>';
                        }
                        else
                             if (valVolInco)
                                {
                                    valDebit = '<td> - </td> <td class="sansDonnee" valChamps="' + valDebit + '"><input type="text" typeChamps="deb" name="temp' + jourTemp + moisTemp + anneeTemp + '"/></td >';
                                }
                            else
                                valDebit = '<td> - </td > <td valChamps="' + valDebit + '"> - </td>';

                    
                    if (valTempsTmp == 0.000000421337 ||  valVolTmp == 0.000000421337 || valPresTmp == 0.000000421337 || valDebitTmp == 0.000000421337)
                        chaineTableau += '<tr raison="null"> <td>'+ jourTemp + '/' + moisTemp + '/' + anneeTemp +  '</td>' + valPres + valTemps + valVol + valDebit + '</tr>';
                    else 
                        chaineTableau += '<tr raison="incoherent"> <td>'+ jourTemp + '/' + moisTemp + '/' + anneeTemp +  '</td>' + valPres + valTemps + valVol + valDebit + '</tr>';
                }
        }
        
        //insérer les données dans la page
        $('div.overlay-correctionManuelleDonnees div#listeDonneesVides table').append(chaineTableau);
        
    }
  

    /**
    *
    * Recevoir les données JSON de Température / Pression ET Volume en fonction de dates
    **/                    
    function getDonneesCaviteAvecDate(dateD, dateF)
    {
        getDataTempCavitePointReelDate(dateD, dateF);
        getDataPresCavitePointReelDate(dateD, dateF);
        getDataVolumeCavitePointReelDate(dateD, dateF);
    }
    

    /**
    *
    *   Recevoir liste des points pour afficher courbe TEMPERATURE - point réel 
    **/
    function getDataTempCavitePointReelDate(dateD, dateF)
    { 
         $.post("/api/getRealTemperatureForCavityDate", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD, DateTo : dateF}))
	    .done(function(response)
        {
            dataTempJsonReel = prepareDataReelCourbes(response);
                //si nous sommes sur la page de Gestion des données relevées pas besoin de lancer les fonctions suivantes
            if (document.title != "Gestion des données relevées")
                getDataTempCavitePointSimuleDate(dateD, dateF);             
        })
        .fail(function()
        {
            dataTempJsonReel = [];
            dataTempJsonSimule = []; 
            afficherDoubleGraphTemp();
        });
        
    }


    /**
    *
    *   Recevoir liste des points pour afficher courbe TEMPERATURE - point simulée 
    **/
    function getDataTempCavitePointSimuleDate(dateD, dateF)
    {
         $.post("/api/getSimulatedTemperatureForCavityDate", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD, DateTo : dateF}))
	    .done(function(response)
        {
            dataTempJsonSimule = prepareDataSimuleCourbes(response);
        
            afficherDoubleGraphTemp(); //Charger les données de la deuxieme courbes ensuites, sinon risque d'afficher les deux courbes sans que chaques données n'aient été récupérées
        
        })
        .fail(function()
        {
            dataTempJsonSimule = []; 
            afficherDoubleGraphTemp();
        });
    }


    /**
    *
    *   Recevoir liste des points pour afficher courbe PRESSION - point réel 
    **/
    function getDataPresCavitePointReelDate(dateD, dateF)
    {
         $.post("/api/getRealPressionForCavityDate", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD, DateTo : dateF}))
	    .done(function(response)
        {
            dataPresJsonReel = prepareDataReelCourbes(response);
             
            //si nous sommes sur la page de Gestion des données relevées pas besoin de lancer les fonctions suivantes
            if (document.title != "Gestion des données relevées") 
                getDataPresCavitePointSimuleDate(dateD, dateF); //Charger les données de la deuxieme courbes ensuites, sinon risque d'afficher les deux courbes sans que chaques données n'aient été récupérées
             
        })
        .fail(function()
        {
             dataPresJsonReel = [] ;
             dataPresJsonSimule = [] ;
             afficherDoubleGraphPres();
             
        });
    }


    /**
    *
    *   Recevoir liste des points pour afficher courbe PRESSION - point simulée 
    **/
    function getDataPresCavitePointSimuleDate(dateD, dateF)
    {
         $.post("/api/getSimulatedPressionForCavityDate", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD, DateTo : dateF}))
	    .done(function(response)
        {
            dataPresJsonSimule = prepareDataSimuleCourbes(response);
        
            afficherDoubleGraphPres();
        })
        .fail(function()
        {
            dataPresJsonSimule = [] ;
            afficherDoubleGraphPres();
        });
    }



     /**
    *
    *   Recevoir liste des points pour afficher courbe VOLUME - point réel 
    **/
    function getDataVolumeCavitePointReelDate(dateD, dateF)
    {
        
        $.post("/api/getRealVolumeForCavityDate", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD, DateTo : dateF}))
	    .done(function(response)
        {
            dataVolumeJsonReel = prepareDataReelCourbes(response);
            
            //si nous sommes sur la page de Gestion des données relevées pas besoin de lancer les fonctions suivantes 
            //if (document.title != "Gestion des données relevées") 
                getDataDebitCavitePointReelDate(dateD, dateF); //Charger les données de la deuxieme courbes ensuites, sinon risque d'afficher les deux courbes sans que chaques données n'aient été récupérées

        })
        .fail(function()
        {
            dataVolumeJsonReel = [];
            dataDebitJsonReel = [];
            
            afficherDoubleGraphVolume();
            
        });
    }


    /**
    *
    *   Recevoir liste des points pour afficher courbe DEBIT -point réel 
    **/
    function getDataDebitCavitePointReelDate(dateD, dateF)
    {
         $.post("/api/getRealDebitForCavityDate", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD, DateTo : dateF}))
	    .done(function(response)
        {
            dataDebitJsonReel = prepareDataReelCourbes(response);
    
             
            if (document.title != "Gestion des données relevées") 
                afficherDoubleGraphVolume();
            else
                initialiserGraphesDonnee(); //page gererDonneesRelevees.html
             
        })
        .fail(function()
        {
            dataDebitJsonReel = [] ;
            afficherDoubleGraphVolume();
        });
    }


    /**
    *
    *   Envoyer au serveur les infos (mail, mdp et cavités choisiées) pour envoyer un mail
    **/
    function envoiMailCavite(champsMail, champsMdp, tabCavite)
    {
        $.post("/api/exportByMail", 
	    JSON.stringify({SiteName : sessionStorage.getItem("NomSiteActuel"),Mails : champsMail, Password : champsMdp, Data : tabCavite}))
	    .done(function(response)
        {
            $('.erreurMail').slideUp();
            $('.succesMail').slideDown();
            setTimeout(function(){ location.reload() } , 1000);
            
        })
        .fail(function()
        {
            $('div#chargementPage').fadeOut();
            setTimeout(function(){ $('.erreurMail').slideDown()  } , 500);
        });
    }



    /**
    *
    *   Recevoir liste des points pour afficher courbe TEMPERATURE - point  CASING SHOE
    **/
    function getDataTempCasingShoePoint()
    {
        $.post("/api/getCasingShoeTemperature", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            
            dataTempCasingShoe = prepareDataReelCourbes(response);
            
          
            var chartTemp = new CanvasJS.Chart("chartContainerTempCaviteCasingShoe",
            {
                 zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Temperature",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempCasingShoe
                    }
                    ]
                });

            chartTemp.render();
        
           
            
             
        })
        .fail(function()
        {
            dataTempCasingShoe = [];
            
                    
          
            var chartTemp = new CanvasJS.Chart("chartContainerTempCaviteCasingShoe",
            {
              zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Temperature",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempCasingShoe
                    }
                    ]
                });

            chartTemp.render();
        
           
            
        });
}


    /**
    *
    *   Recevoir liste des points pour afficher courbe PRESSION - point  CASING SHOE
    **/
    function getDataPresCasingShoePoint()
    {
        $.post("/api/getCasingShoePression", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataPresCasingShoe = prepareDataReelCourbes(response);
            
            var chartTemp = new CanvasJS.Chart("chartContainerPressionCaviteCasingShoe",
            {
                zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresCasingShoe
                    }
                    ]
                });

            chartTemp.render();
             
        })
        .fail(function()
        {
            dataPresCasingShoe = [];
        
            var chartTemp = new CanvasJS.Chart("chartContainerPressionCaviteCasingShoe",
            {
                 zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresCasingShoe
                    }
                    ]
                });

            chartTemp.render();
        
           
            
        });
}



    /**
    *
    *   Recevoir liste des points pour afficher courbe TEMPERATURE - point  CAVERNE
    **/
    function getDataTempCavernePoint()
    {
        $.post("/api/getCavernTemperature", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            
            dataTempCaverne = prepareDataReelCourbes(response);
            
          
            var chartTemp = new CanvasJS.Chart("chartContainerTempCaviteCaverne",
            {
                zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Temperature",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempCaverne
                    }
                    ]
                });

            chartTemp.render();
        
           
            
             
        })
        .fail(function()
        {
            dataTempCaverne = [];
            
                    
          
            var chartTemp = new CanvasJS.Chart("chartContainerTempCaviteCaverne",
            {
               zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Temperature",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempCaverne
                    }
                    ]
                });

            chartTemp.render();
        
           
            
        });
}


    /**
    *
    *   Recevoir liste des points pour afficher courbe PRESSION - point  CAVERNE
    **/
    function getDataPresCavernePoint()
    {
        $.post("/api/getCavernPression", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataPresCaverne = prepareDataReelCourbes(response);
            
            var chartTemp = new CanvasJS.Chart("chartContainerPressionCaviteCaverne",
            {
                zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresCaverne
                    }
                    ]
                });

            chartTemp.render();
             
        })
        .fail(function()
        {
            dataPresCaverne = [];
        
            var chartTemp = new CanvasJS.Chart("chartContainerPressionCaviteCaverne",
            {
               zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresCaverne
                    }
                    ]
                });

            chartTemp.render();
        
           
            
        });
}

    /**
    *
    *   Génerer un rapport docx
    **/
    function genererRapport(dateDeb, boolTemp, boolCavern, boolCasingShoe)
    {
        $.post("/api/generateDocx", 
	    JSON.stringify({SiteName : sessionStorage.getItem("NomSiteActuel"), DateTo : dateFin, GenerateTemp :boolTemp, GenerateCavern : boolCavern, GenerateCasingShoe : boolCasingShoe  }))
	    .done(function(response)
        {
            $('div.overlay-genererRapport .erreurGeneration').slideUp();
            
            $('.overlay-genererRapport input').val("");
        
            $('.overlay-genererRapport').fadeOut();
            $('.overlay').fadeOut();
            
             $('div#chargementPage').fadeOut();
        })
        .fail(function()
        {
            $('div#chargementPage').fadeOut();
            
            $('div.overlay-genererRapport .erreurGeneration').slideDown();
            
        });
    }



/**************************
**
**  parametreSite.html  ***
**
******************/


    /**
    *
    *   Recevoir liste des gaz et de leur informations (masse molaire, temperature et pression critique)
    **/
    function getListeComposantsInformation()
    {         
         $.get("/api/getListGaz").done(function(response)
        {
             preparerListeComposantsInformation(response);           
        })
        .fail(function()
              {

        });
    }


    /**
    *
    *   Recevoir la liste des composant du gaz d'un site
    **/
    function getListeComposantGaz()
    {
        
        $.post("/api/getListCompositionGaz",  
	    JSON.stringify({SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            //gérer la réponse
             preparerListeComposantsGaz(response);
        })
        .fail(function()
              {
        });
    }


 
    /**
    *
    *   Envoyer les nouvelles informations pour modifier un élément chimique
    **/   
    function modificationElement()
    {
        $.post("/api/updateGaz",
	    JSON.stringify({GazName : modificationElementAncienNom, NewGazName:modificationElementNouveauNom, Formule:modificationElementNouveauFormule, MasseMolaire: modificationElementNouveauMasseMolaire, TemperatureCritique:modificationElementNouveauTemperature, PressionCritique:modificationElementNouveauPression, Densite:modificationElementNouveauDensite}))
	    .done(function(response)
        {
            //Mettre à jour la page
            getListeComposantsInformation();
            
            $('.overlay-double').fadeOut();
            $('.overlay-validationModification').fadeOut();
        })
        .fail(function()
         {
            //Nouveau nom existe déjà pour un autre élément
                $('.overlay-validationModification').fadeOut();  
                $('.overlay-erreurNouveauNomElement').fadeIn();  
         });
    }
    
        
    /**
    *
    *   Envoyer nom de l'élément pour supprimer un élément chimique
    **/
    function suppressionElement()
    {
        $.post("/api/removeGaz",
	    JSON.stringify({GazName : suppressionElementNom}))
	    .done(function(response)
        {
            $('.overlay-double').fadeOut();
            $('.overlay-validationSupression').fadeOut();
            getListeComposantsInformation();
        })
        .fail(function()
         {
        });
    }
    
   

    /**
    *
    *   Afficher liste des composants d'un gaz
    **/
    function preparerListeComposantsGaz(json)
    {
            var data =  json;
            
            var stringComposant = "";
            var stringListeComposantOption = "";
            
            for (i=0 ; i < data.length; i++)
            {
                //créer liste des options
                for (var j = 0; j < listeJsonGazComposant.length ; j++ )
                {
                        //Si c'est le composant en cours, dire qu'il doit être sélectionné
                        if (listeJsonGazComposant[j].Name == data[i].Composant )
                            stringListeComposantOption += ' <option value="' + listeJsonGazComposant[j].Name + '" selected="selected">' + listeJsonGazComposant[j].Name + '</option>' ;
                        else
                            stringListeComposantOption += ' <option value="' + listeJsonGazComposant[j].Name + '">' + listeJsonGazComposant[j].Name + '</option>';
                }


                 var nomComposantTmp = data[i].Composant.split(" ");
                 var nomComposantFinal= "";
                 for(var j = 0 ; j< nomComposantTmp.length ; j++)
                     nomComposantFinal += nomComposantTmp[j]
                     
                     
                if (sessionStorage.getItem("Langue") == "fr")     
                    stringComposant += ' <li class="' + nomComposantFinal + '"><div class="infoComposant"> <label class="trn">Composant chimique</label><select id="composantsChimiques" list="composantsChimiques" class="searchfieldjs" id="choix_composant">' + stringListeComposantOption + '</select> </div> <div class="infoComposant"> <label class="trn">Proportions (%)</label> <input type="text" name="proportions" id="" class="searchfieldjs" placeholder="Remplir" value="' + data[i].Proportions + '"  > <p class="erreurProportion trn">Proportions invalides.</p> </div> <div class="infoComposant">   <label class="trn">Masse molaire</label><input type="text" name="masseMolaire" id="" class="searchfieldjs" value="" disabled="disabled">  </div> <div class="infoComposant">   <label class="trn">Pression critique</label> <input type="text" name="pressionCritique" id="" class="searchfieldjs" value="" disabled="disabled">  </div> <div class="infoComposant">  <label class="trn">Température critique</label><input type="text" name="temperatureCritique" id="" class="searchfieldjs" value="" disabled="disabled"> </div><div class="infoComposant"><label class="trn">Densité</label><input type="text" name="densite" id="" class="searchfieldjs" value="" disabled="disabled"> </div>    <div class="infoComposant supprimerComposant"><img src="../asset/img/exploitants/supprimer.png"/> </div> </li>';
                else
                    stringComposant += '<li class="' + nomComposantFinal + '"><div class="infoComposant"> <label class="trn">Chemical component</label><select id="composantsChimiques" list="composantsChimiques" class="searchfieldjs" id="choix_composant">' + stringListeComposantOption + '</select> </div> <div class="infoComposant"> <label class="trn">Proportions (%)</label> <input type="text" name="proportions" id="" class="searchfieldjs" placeholder="Fill" value="' + data[i].Proportions + '"  > <p class="erreurProportion trn">Invalid proportion.</p> </div> <div class="infoComposant">   <label class="trn">Molar mass</label><input type="text" name="masseMolaire" id="" class="searchfieldjs" value="" disabled="disabled">  </div> <div class="infoComposant">   <label class="trn">Critical pressure</label> <input type="text" name="pressionCritique" id="" class="searchfieldjs" value="" disabled="disabled">  </div> <div class="infoComposant">  <label class="trn">Critical temperature</label><input type="text" name="temperatureCritique" id="" class="searchfieldjs" value="" disabled="disabled"> </div><div class="infoComposant"><label class="trn">Density</label><input type="text" name="densite" id="" class="searchfieldjs" value="" disabled="disabled"> </div>    <div class="infoComposant supprimerComposant"><img src="../asset/img/exploitants/supprimer.png"/> </div> </li>';
                    
                    
                //Vider les options
                stringListeComposantOption = "" ;
                
            }
            
            $('#parametreSite div.compositionGaz ul li').remove()
            $('#parametreSite div.compositionGaz ul').append(stringComposant); //Insérer liste des composants
            chargerDonneesComposants();
    }



    /** 
    *
    *   Préparer un tableau qui contient la liste des composants et leurs informations liées
    **/ 
       function preparerListeComposantsInformation(json)
        {
            var data =  json;
            listeJsonGazComposant = json;
            
            
            if (sessionStorage.getItem("Langue") == "fr")     
                var stringListeElementChimique = "<div class='tr'><div class='th trn'>Nom</div><div class='th trn'>Formule</div> <div class='th trn'>Masse molaire (g/mol)</div> <div class='th trn'>Pression critique (MPa)</div> <div class='th trn'>Température critique (K)</div> <div class='th trn'>Densité(kg/m3) </div></div>"; //Sera afficher dans l'overlay contenant la liste des éléments
            else
                var stringListeElementChimique = "<div class='tr'><div class='th trn'>Name</div><div class='th trn'>Formula</div> <div class='th trn'>Molar mass (g/mol)</div> <div class='th trn'>Critical pressure (MPa)</div> <div class='th trn'>Critical temperature (K)</div> <div class='th trn'>Density(kg/m3) </div></div>"; //Sera afficher dans l'overlay contenant la liste des éléments
                
                
            stringListeComposantOptionGeneral = ' <option value="Sélectionnez un composant">Sélectionnez un composant</option>';
            for (i=0 ; i < data.length; i++)
            {
                infoChaqueComposant[data[i].Name] = new Array(data[i].MasseMolaire, data[i].Pression, data[i].Temperature , data[i].Densite );
                
                stringListeComposantOptionGeneral += ' <option value="' + data[i].Name + '">' + data[i].Name + '</option>';
                
                stringListeElementChimique += '<div class="tr"> <div class="td"><input value="'+ data[i].Name +'" anciennom="'+ data[i].Name +'" name="nomElement" /></div><div class="td"> <input value="'+ data[i].Formule +'" name="formule"/>  </div><div class="td"> <input value="'+ data[i].MasseMolaire +'" name="masseMolaire"/>  </div><div class="td"> <input value="'+ data[i].Pression +'" name="pression"/>  </div><div class="td" > <input value="'+ data[i].Temperature +'" name="temperature"/>  </div><div class="td" > <input value="'+ data[i].Densite +'" name="densite"/>  </div><div class="td validerModif"><img src="../asset/img/valider.png"></div><div class="td supprimerElement"><img src="../asset/img/exploitants/supprimer.png"></div></div>';
                
            }

           chargerDonneesComposants(); //Afficher les données pour chaque composants maintenant qu'on les a récupérées

            $('.overlay-listeElementsChimiques #listeElementsChimiques div.table').html(stringListeElementChimique);
            
            getListeComposantGaz();

        }



    /**
    *
    *   Afficher les données associés à un composant du gaz d'un site au chargement de la page
    **/
    function chargerDonneesComposants()
    {
        var nombreComposant = $('.compositionGaz ul > *').length;
        var nomComposant ="";
            
        for (var i = 1; i <= nombreComposant ; i++)
            {
                nomComposant = $('.compositionGaz ul li:nth-child(' + i +') select#composantsChimiques').val();

                var nomComposantTmp = nomComposant.split(" ");
                var nomComposantFinal= "";
                for(var j = 0 ; j< nomComposantTmp.length ; j++)
                    nomComposantFinal += nomComposantTmp[j]

                $('li.' + nomComposantFinal ).find('input[name="masseMolaire"]').val(infoChaqueComposant[nomComposant][0] + " g/mol");
                $('li.' + nomComposantFinal ).find('input[name="pressionCritique"]').val(infoChaqueComposant[nomComposant][1] + " MPa");
                $('li.' + nomComposantFinal ).find('input[name="temperatureCritique"]').val(infoChaqueComposant[nomComposant][2] + " K");
                $('li.' + nomComposantFinal ).find('input[name="densite"]').val(infoChaqueComposant[nomComposant][3] + " kg/m3");

                if (i==nombreComposant)
                        afficherCompositionTotale();
            }
    }
    


    /**
    *
    *   Envoyer données lors de l'ajout d'un nouvel élément chimique
    **/
    function ajoutElementChimique(nom, form, masseM, pres, temp,densite)
    {
        $.post("/api/addGaz",
	     JSON.stringify({GazName : nom, Formule:form, MasseMolaire:masseM, PressionCritique:pres, TemperatureCritique:temp,Densite:densite}))
	    .done(function(response)
        {
            document.location.href = '/sites/parametreSite.html';
        })
        .fail(function()
              {

        });
    }
    
    


    /**
    *
    *   Envoyer données lors de l'ajout d'un nouvel élément chimique
    **/
    function ajoutGaz()
    {
        $.post("/api/updateCompositionGazList",
	     JSON.stringify({SiteName : sessionStorage.getItem("NomSiteActuel"), ListCompoGaz:listeComposantGaz}))
	    .done(function(response)
        {
            document.location.href = '/sites/parametreSite.html';
        })
        .fail(function()
              {

        });
    }

 
/************************
**
**  grapheTemp.html  ****
**
******************/


    /**
    *
    *   Afficher la courbe donnée réel pour la TEMPERATURE  
    **/
    function initialiserGraphesDonneeTemp()
    {
        $.post("/api/getRealTemperatureForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataTempJsonReel = prepareDataReelCourbes(response);
              
            initialiserGraphesDonneeTempSimule();
            
            prepareDataNullCourbe(dataTempJsonReel);
        })
        .fail(function()
        {
        
        });

    }
    
  
    /*
    *
    *   Afficher la courbe donnée simule pour la TEMPERATURE  
    **/
    function initialiserGraphesDonneeTempSimule()
    {
       
        $.post("/api/getSimulatedTemperatureForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataTempJsonSimule = prepareDataSimuleCourbes(response);
        
            var chartTemp = new CanvasJS.Chart("chartContainerTempCavite",
                {
                    zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
			        includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                    data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Real data",
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempJsonReel
                    },
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        color: "#e90d3f",
                        markerSize:3,
                        dataPoints:dataTempJsonSimule
                    }
                        
                    ]
                });
    
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
        })
        .fail(function()
        {
            var chartTemp = new CanvasJS.Chart("chartContainerTempCavite",
                {
                    zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
			        includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                    data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Real data",
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempJsonReel
                    }
                    ]
                });
    
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
        });

    }


    /**
    *
    *   Récupérer données de la courbe TEMPERATURE  
    **/
    function getInfoGrapheTemp()
    {
        
        $.post("/api/getMatchingStatTemperatureForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            var infoGrapheTemp = "";
            
            if (sessionStorage.getItem("Langue") == "fr")
                infoGrapheTemp = '<div class="infoGraphe"> <p>' + response.standardDeviationTemp + '</p>  <p class="trn">Déviation standard</p> </div><div class="infoGraphe"> <p>' + response.minDeviationTemp + '</p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p>' + response.maxDeviationTemp + '</p> <p class="trn">Déviation max</p> </div> <div class="infoGraphe"><p>' + response.sommeEcartsQuadratiquesTemp + '</p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p>' + response.moyDeviationTemp + '</p><p class="trn">Déviations moyennes</p> </div>';
            else
                infoGrapheTemp = '<div class="infoGraphe"> <p>' + response.standardDeviationTemp + '</p>  <p class="trn">Standard deviation</p> </div><div class="infoGraphe"> <p>' + response.minDeviationTemp + '</p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p>' + response.maxDeviationTemp + '</p> <p class="trn">Max deviation</p> </div> <div class="infoGraphe"><p>' + response.sommeEcartsQuadratiquesTemp + '</p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe">  <p>' + response.moyDeviationTemp + '</p><p class="trn">Average deviations</p> </div>';
                
            $('.infoGraphe').remove(); 
            $('#listeInfoGraphe').prepend(infoGrapheTemp);   
           
            
        })
        .fail(function()
        {
            var infoGrapheTemp = "";
            
            if (sessionStorage.getItem("Langue") == "fr")
                infoGrapheTemp = '<div class="infoGraphe"> <p> 00 </p>  <p class="trn">Déviation standard</p> </div><div class="infoGraphe"> <p> 00 </p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p> 00 </p> <p class="trn">Déviation max</p> </div> <div class="infoGraphe"><p> 00 </p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p> 00 </p><p class="trn">Déviations moyennes</p> </div>';
            else
                infoGrapheTemp = '<div class="infoGraphe"> <p> 00 </p>  <p class="trn">Standard deviation</p> </div><div class="infoGraphe"> <p> 00 </p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p> 00 </p> <p class="trn">Max deviation</p> </div> <div class="infoGraphe"><p> 00 </p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe">  <p> 00 </p><p class="trn">Average deviations</p> </div>';
                
                
            $('.infoGraphe').remove();
            $('#listeInfoGraphe').prepend(infoGrapheTemp);   
            
        });

    }
  


    /**
    *
    *   Afficher la courbe donnée réel pour la TEMPERATURE  
    **/
    function getInfoGrapheTempDate(dateD, dateF)
    {  
        $.post("/api/getMatchingStatTemperatureForCavityDate", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD , DateTo : dateF }))
	    .done(function(response)
        {
            var infoGrapheTemp = "";
            
            if (sessionStorage.getItem("Langue") == "fr")
                infoGrapheTemp = '<div class="infoGraphe"> <p>' + response.standardDeviationTemp + '</p>  <p class="trn">Déviation standard</p> </div><div class="infoGraphe"> <p>' + response.minDeviationTemp + '</p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p>' + response.maxDeviationTemp + '</p> <p class="trn">Déviation max</p> </div> <div class="infoGraphe"><p>' + response.sommeEcartsQuadratiquesTemp + '</p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p>' + response.moyDeviationTemp + '</p><p class="trn">Déviations moyennes</p> </div>'; 
            else
                infoGrapheTemp = '<div class="infoGraphe"> <p>' + response.standardDeviationTemp + '</p>  <p class="trn">Standard deviation</p> </div><div class="infoGraphe"> <p>' + response.minDeviationTemp + '</p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p>' + response.maxDeviationTemp + '</p> <p class="trn">Max deviation</p> </div> <div class="infoGraphe"><p>' + response.sommeEcartsQuadratiquesTemp + '</p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe">  <p>' + response.moyDeviationTemp + '</p><p class="trn">Average deviations</p> </div>'; 
                
                
            $('.infoGraphe').remove();
            $('#listeInfoGraphe').prepend(infoGrapheTemp);   
           
            
        })
        .fail(function()
        {
            var infoGrapheTemp = "";
            
            if (sessionStorage.getItem("Langue") == "fr")
                infoGrapheTemp = '<div class="infoGraphe"> <p> 00 </p>  <p class="trn">Déviation standard</p> </div><div class="infoGraphe"> <p> 00 </p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p> 00 </p> <p class="trn">Déviation max</p> </div> <div class="infoGraphe"><p> 00 </p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p> 00 </p><p class="trn">Déviations moyennes</p> </div>';
            else    
                infoGrapheTemp = '<div class="infoGraphe"> <p> 00 </p>  <p class="trn">Standard deviation</p> </div><div class="infoGraphe"> <p> 00 </p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p> 00 </p> <p class="trn">Max deviation</p> </div> <div class="infoGraphe"><p> 00 </p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe">  <p> 00 </p><p class="trn">Average deviations</p> </div>';
                
                
            $('.infoGraphe').remove();
            $('#listeInfoGraphe').prepend(infoGrapheTemp);   
            
        });

    }
    

/****************************
**
**  graphePression.html  ****
**
******************/

    /**
    *
    *   Afficher la courbe donnée réel pour la PRESSION  
    **/
    function initialiserGraphesDonneePression()
    {
       
        $.post("/api/getRealPressionForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataPresJsonReel = prepareDataReelCourbes(response);
              
            initialiserGraphesDonneePressionSimule();
            
         //   prepareDataNullCourbe(dataPresJsonReel);
    
        })
        .fail(function()
        {
        
        });

    }
    
  
    /*
    *
    *   Afficher la courbe donnée simule pour la PRESSION  
    **/
    function initialiserGraphesDonneePressionSimule()
    {
       
        $.post("/api/getSimulatedPressionForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataPresJsonSimule = prepareDataSimuleCourbes(response);
              
            
        
		   var chartTemp = new CanvasJS.Chart("chartContainerPressionCavite",
                {
                    zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1"
                    
                },
                    data: [
                    {        
                        type: "line",
                        showInLegend: true, 
                        legendText: "Real data",
                        connectNullData: false,
                        click: onClickPointNullGraph,
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresJsonReel
                    },
                    {        
                        type: "line",
                        showInLegend: true, 
                        legendText: "Simulated data",
                        connectNullData: false,
                        color: "#e90d3f",
                        markerSize:3,
                        dataPoints:dataPresJsonSimule
                    }
                        
                    ]
                });
    
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
            
            
        })
        .fail(function()
        {
            var chartTemp = new CanvasJS.Chart("chartContainerPressionCavite",
                {
                    zoomEnabled: true, 
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
			         includeZero: false,
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                    data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Real data",
                        showInLegend: true, 
                        click: onClickPointNullGraph,
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresJsonReel
                    }
                        
                    ]
                });
    
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
        });

    }
    
    /**
    *
    *   Récupérer données de la courbe PRESSION  
    **/
    function getInfoGraphePression()
    {
        
        $.post("/api/getMatchingStatPressionForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"), SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            var infoGraphePression = "";
            
            if (sessionStorage.getItem("Langue") == "fr")
                infoGraphePression = '<div class="infoGraphe"> <p>' + response.standardDeviationPression + '</p>  <p class="trn">Déviation standard</p> </div><div class="infoGraphe"> <p>' + response.minDeviationPression + '</p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p>' + response.maxDeviationPression + '</p> <p class="trn">Déviation max</p> </div> <div class="infoGraphe"><p>' + response.sommeEcartsQuadratiquesPression + '</p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p>' + response.moyDeviationPression + '</p><p class="trn">Déviations moyennes</p> </div>';
            else
                infoGraphePression = '<div class="infoGraphe"> <p>' + response.standardDeviationPression + '</p>  <p class="trn">Standard deviation</p> </div><div class="infoGraphe"> <p>' + response.minDeviationPression + '</p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p>' + response.maxDeviationPression + '</p> <p class="trn">Max deviation</p> </div> <div class="infoGraphe"><p>' + response.sommeEcartsQuadratiquesPression + '</p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe">  <p>' + response.moyDeviationPression + '</p><p class="trn">Average deviations</p> </div>';
                
            $('.infoGraphe').remove();
            $('#listeInfoGraphe').prepend(infoGraphePression);   
           
            
        })
        .fail(function()
        {
            var infoGraphePression = "";
            
            if (sessionStorage.getItem("Langue") == "fr")
                infoGraphePression = '<div class="infoGraphe"> <p> 00 </p>  <p class="trn">Déviation standard</p> </div><div class="infoGraphe"> <p> 00 </p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p> 00 </p> <p class="trn">Déviation max</p> </div> <div class="infoGraphe"><p> 00 </p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p> 00 </p><p class="trn">Déviations moyennes</p> </div>';
            else
                infoGraphePression = '<div class="infoGraphe"> <p> 00 </p>  <p class="trn">Standard deviation</p> </div><div class="infoGraphe"> <p> 00 </p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p> 00 </p> <p class="trn">Max deviation</p> </div> <div class="infoGraphe"><p> 00 </p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe">  <p> 00 </p><p class="trn">Average deviations</p> </div>';
            
            
            $('.infoGraphe').remove();
            $('#listeInfoGraphe').prepend(infoGraphePression);   
            
        });

    }
    

    /**
    *
    * Recevoir les données JSON de PRESSION ou TEMPERATURE en fonction de dates
    **/                    
    function getDonneesGraphesCaviteAvecDate(dateD, dateF)
    {
        //Selon la page sur laquelle nous sommes
        if(document.title == "Graphe Pression")
        {
            getDataPresCavitePointReelDate(dateD, dateF);
            getInfoGraphePressionDate(dateD, dateF);        
        }
            else if(document.title == "Graphe Température")
            {
                getDataTempCavitePointReelDate(dateD, dateF);
                getInfoGrapheTempDate(dateD, dateF);        
            }
                else if(document.title == "Graphe Volume")
                {
                    getDataVolumeCavitePointReelDate(dateD, dateF);  
                }

    }
  
    /**
    *
    *   Afficher la courbe donnée réel pour la PRESSION  
    **/
    function getInfoGraphePressionDate(dateD, dateF)
    {
        
        $.post("/api/getMatchingStatPressionForCavityDate", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD , DateTo : dateF }))
	    .done(function(response)
        {
            var infoGraphePression = "";
            
            if (sessionStorage.getItem("Langue") == "fr")
                infoGraphePression = '<div class="infoGraphe"> <p>' + response.standardDeviationPression + '</p>  <p class="trn">Déviation standard</p> </div><div class="infoGraphe"> <p>' + response.minDeviationPression + '</p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p>' + response.maxDeviationPression + '</p> <p class="trn">Déviation max</p> </div> <div class="infoGraphe"><p>' + response.sommeEcartsQuadratiquesPression + '</p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p>' + response.moyDeviationPression + '</p><p class="trn">Déviations moyennes</p> </div>';
            else
                infoGraphePression = '<div class="infoGraphe"> <p>' + response.standardDeviationPression + '</p>  <p class="trn">Standard deviation</p> </div><div class="infoGraphe"> <p>' + response.minDeviationPression + '</p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p>' + response.maxDeviationPression + '</p> <p class="trn">Max deviation</p> </div> <div class="infoGraphe"><p>' + response.sommeEcartsQuadratiquesPression + '</p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe">  <p>' + response.moyDeviationPression + '</p><p class="trn">Average deviations</p> </div>'; 
            
            $('.infoGraphe').remove();
            $('#listeInfoGraphe').prepend(infoGraphePression);   
           
            
        })
        .fail(function()
        {
            var infoGraphePression = "";
            
            if (sessionStorage.getItem("Langue") == "fr")
                infoGraphePression = '<div class="infoGraphe"> <p> 00 </p>  <p class="trn">Déviation standard</p> </div><div class="infoGraphe"> <p> 00 </p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p> 00 </p> <p class="trn">Déviation max</p> </div> <div class="infoGraphe"><p> 00 </p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p> 00 </p><p class="trn">Déviations moyennes</p> </div>';
            else
                infoGraphePression = '<div class="infoGraphe"> <p> 00 </p>  <p class="trn">Standard deviation</p> </div><div class="infoGraphe"> <p> 00 </p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p> 00 </p> <p class="trn">Max deviation</p> </div> <div class="infoGraphe"><p> 00 </p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe">  <p> 00 </p><p class="trn">Average deviations</p> </div>';
                
            $('.infoGraphe').remove();
            $('#listeInfoGraphe').prepend(infoGraphePression);   
            
        });

    }
    




/**************************
**
**  grapheVolume.html  ****
**
******************/


    /**
    *
    *   Afficher la courbe donnée réel pour le VOLUME  
    **/
    function initialiserGraphesDonneeVolume()
    {
        $.post("/api/getRealVolumeForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataVolumeJsonReel = prepareDataReelCourbes(response);
            initialiserGraphesDonneeDebit();
        })
        .fail(function()
        {
        
        });

    }
    
  
    /*
    *
    *   Afficher la courbe donnée simule pour la VOLUME  
    **/
    function initialiserGraphesDonneeDebit()
    {
       
        $.post("/api/getRealDebitForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataDebitJsonReel = prepareDataReelCourbes(response);
            
            var chartTemp = new CanvasJS.Chart("chartContainerVolumeCavite",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Volume",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                axisY2:{
                    title: "Flow",
                    interval: 2,
                    lineColor: "#da5f08",
                    labelFontColor: "#da5f08",
                    titleFontColor: "#da5f08"
		              },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Real data - Volume",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataVolumeJsonReel
                    },
                    {        
                        type: "line",
                        connectNullData: false,         
                        legendText: "Real data - Flow",  
                        showInLegend: true,    
		                axisYType: "secondary",
                        color: "#da5f08",
                        markerSize:3,
                        dataPoints:dataDebitJsonReel
                    }
                    ]
                });
    
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
            
        })
        .fail(function()
        {
        
        });

    }


    
    

/**************************
**
**  grapheTempCasingShoe.html  ****
**
******************/


    /**
    *
    *   Afficher la courbe donnée réel pour le TEMP CASING SHOE  
    **/
    function initialiserGraphesDonneeTempCasingShoe()
    {
        $.post("/api/getCasingShoeTemperature", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataTempCasingShoe = prepareDataReelCourbes(response);
                      
            var chartTemp = new CanvasJS.Chart("chartContainerTempCasingShoe",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Temperature",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempCasingShoe
                    }
                    ]
                });
                                               
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
        })
        .fail(function()
        {
            dataTempCasingShoe = [];
             var chartTemp = new CanvasJS.Chart("chartContainerTempCasingShoe",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Temperature",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempCasingShoe
                    }
                    ]
                });
   
             chartTemp.render();
            $('div#chargementPage').fadeOut();
        
        });

    }
    
  
  

/**************************
**
**  graphePresCasingShoe.html  ****
**
******************/


    /**
    *
    *   Afficher la courbe donnée réel pour le PRES CASING SHOE  
    **/
    function initialiserGraphesDonneePresCasingShoe()
    {
        $.post("/api/getCasingShoePression", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {
            dataPresCasingShoe = prepareDataReelCourbes(response);
                      
            var chartTemp = new CanvasJS.Chart("chartContainerPressionCasingShoe",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresCasingShoe
                    }
                    ]
                });
                                               
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
        })
        .fail(function()
        {
            dataPresCasingShoe = [];
             var chartTemp = new CanvasJS.Chart("chartContainerPressionCasingShoe",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresCasingShoe
                    }
                    ]
                });
   
             chartTemp.render();
            $('div#chargementPage').fadeOut();
        
        });

    }
    
  


    
     
  

/**************************
**
**  grapheTempCavern.html  ****
**
******************/


    /**
    *
    *   Afficher la courbe donnée réel pour le TEMP CAVERN  
    **/
    function initialiserGraphesDonneeTempCavern()
    {
        $.post("/api/getCavernTemperature", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {            
            dataTempCaverne = prepareDataReelCourbes(response);
                      
            var chartTemp = new CanvasJS.Chart("chartContainerTempCavern",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Temperature",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempCaverne
                    }
                    ]
                });
                                               
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
        })
        .fail(function()
        {
            dataTempCaverne = [];
             var chartTemp = new CanvasJS.Chart("chartContainerTempCavern",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Temperature",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataTempCaverne
                    }
                    ]
                });
   
             chartTemp.render();
            $('div#chargementPage').fadeOut();
        
        });

    }
    
  


    
    
  

/**************************
**
**  graphePressionCavern.html  ****
**
******************/


    /**
    *
    *   Afficher la courbe donnée réel pour le TEMP CAVERN  
    **/
    function initialiserGraphesDonneePresCavern()
    {
        $.post("/api/getCavernPression", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        {            
            dataPresCaverne = prepareDataReelCourbes(response);
                      
            var chartTemp = new CanvasJS.Chart("chartContainerPresCavern",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresCaverne
                    }
                    ]
                });
                                               
            chartTemp.render();
            
            $('div#chargementPage').fadeOut();
        })
        .fail(function()
        {
            dataPresCaverne = [];
             var chartTemp = new CanvasJS.Chart("chartContainerPresCavern",
                {
                    zoomEnabled: true,  
                    legend:
                    {
                        verticalAlign : 'top',
                        horizontalAlign : 'right',
                    },
                    axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    title: "Pressure",
                    valueFormatString: "####",
			         includeZero: false,
                    gridColor: "#d2d2d2",
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                        lineColor: "#328fe1"
                    
                },
                data: [
                    {        
                        type: "line",
                        connectNullData: false,
                        legendText: "Simulated data",
                        showInLegend: true, 
                        color: "#328fe1",
                        markerSize:3,
                        dataPoints:dataPresCaverne
                    }
                    ]
                });
   
             chartTemp.render();
            $('div#chargementPage').fadeOut();
        
        });

    }
    
  


    
    



/**********************************
**
**  gererDonneesRelevees.html  ****
**
******************/


    /**
    *
    *   Afficher la courbe donnée réel pour le VOLUME  + TEMPERATURE + VOLUME
    **/
    function initialiserGraphesDonnee()
    {
        
        
        prepareDataCourbesForDisplay(dataTempJsonReel, dataPresJsonReel, dataVolumeJsonReel);
    
        var chartTemp = new CanvasJS.Chart("chartContainerTemp",
		{
            zoomEnabled: true, 
            legend:
            {
                verticalAlign : 'top',
                horizontalAlign : 'right',
            },
            axisX:{      
                valueFormatString: "MM-YYYY" ,
                labelAngle: -50,
                title: "Time",
                labelFontColor: "black",
                gridColor: "#d2d2d2",
                gridThickness: 1    
			},
			axisY: {
				labelFontColor: "black",
                title: "Temperature",
				valueFormatString: "####",
                gridColor: "#d2d2d2",
                includeZero: false,
                labelFontColor: "#328fe1",
                titleFontColor: "#328fe1",
                lineColor: "#328fe1"
			},
			data: [
			{        
                type: "line",
			     click: onClickPointNullGraph,
                connectNullData: false,
                legendText: "Real data",
                showInLegend: true, 
				color: "#328fe1",
				markerSize:3,
				dataPoints:dataTempJsonReel
			}		
			]
		});
    
        chartTemp.render();



       var chartPression = new CanvasJS.Chart("chartContainerPression",
            {
                zoomEnabled: true, 
                legend:
                {
                    verticalAlign : 'top',
                    horizontalAlign : 'right',
                },
                axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    labelAngle: -50,
                    title: "Time",
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    title: "Pressure",
                    lineColor: "#328fe1",
                    valueFormatString: "####",
                    includeZero: false,
                    gridColor: "#d2d2d2"
                },
                data: [
                {        
                    type: "line",
                    click: onClickPointNullGraph,
                    connectNullData: false,
                    color: "#328fe1",
                    legendText: "Real data",
                    showInLegend: true, 
                    markerSize:3,
                    dataPoints:dataPresJsonReel
                }		
                ]
            });

        chartPression.render();



       var chartVolume = new CanvasJS.Chart("chartContainerVolume",
            {
                zoomEnabled: true,
                legend:
                {
                    verticalAlign : 'top',
                    horizontalAlign : 'right',
                }, 
                axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    labelAngle: -50,
                    labelFontColor: "black",
                    title: "Time",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "#328fe1",
                    title: "Volume",
                    titleFontColor: "#328fe1",
                    lineColor: "#328fe1",
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
                    includeZero: false,
                    valueFormatString: "0.00",
                },
                data: [
                {        
                    type: "line",
                    click: onClickPointNullGraph,
                    connectNullData: false,
                    legendText: "Real data",
                        showInLegend: true, 
                    color: "#328fe1",
                    markerSize:3,
                    dataPoints:dataVolumeJsonReel 
                }		
                ]
            });

        chartVolume.render();


         var chartGip = new CanvasJS.Chart("chartContainerGip",
            {
                zoomEnabled: true, 
                legend:
                {
                    verticalAlign : 'top',
                    horizontalAlign : 'right',
                },
                axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    labelAngle: -50,
                    title: "Time",
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "#328fe1",
                    titleFontColor: "#328fe1",
                    title: "Flow",
                    lineColor: "#328fe1",
                    valueFormatString: "####",
                    includeZero: false,
                    gridColor: "#d2d2d2",
                    interval: 2,
                    valueFormatString: "0.00",
                },
                data: [
                {        
                    type: "line",
                    click: onClickPointNullGraph,
                    connectNullData: false,
                    legendText: "Real data",
                        showInLegend: true, 
                    color: "#328fe1",
                    markerSize:3,
                    dataPoints:dataDebitJsonReel 
                }		
                ]
            });

        chartGip.render();
        
        $('div#chargementPage').fadeOut();
        
    }
     

    //au click d'un point null : afficher le menu déroulant
    function onClickPointNullGraph(e)
    { 
        if(document.title != "Graphe Pression" && document.title != "Graphe Pression" && document.title != "Graphe Temperature" )
            {
                if(e.dataPoint.y == 0.000000421337)
                {
                    $('div.overlay-correctionManuelleDonnees').fadeOut();

                    $('.overlay-optionCorrectionDonnees').attr('style','');
                    $('.overlay-optionCorrectionDonnees').fadeIn();
                    //$('.overlay-optionCorrectionDonnees').css({'top':mouseY,'left':mouseX}).fadeIn();
                }
            }

    }
    
    /**
    *
    *   Recevoir chaine JSON REEL (données Température / pression et Volume) et la préparer pour utiliser ses coordonnées
    **/
    function prepareDataReelCourbes(json)
    {
        var data = json;
        var final = [];    
        
        for (i=0; i < data.length; i++)
        {            
            var firstDate = data[i].x;
            var tokens = firstDate.split(",");
                        
            var annee = tokens[0];
            var mois = tokens[1] - 1 ;
            var jour = tokens[2];            
            
            if (data[i].y == 'null') //Si pas de valeur, placer un marqueur au niveau de l'info manquante (y = 1.01 par défaut)
                {
                    final.push({ 'x': new Date(annee, mois, jour), 'y':0.000000421337,  markerType: 'cross',markerColor: "#276FAE", markerSize: 12, cursor: "pointer"});
                    
                     bloquerPageCalage(); //Vu qu'il y a des valeurs manquantes, ne pas avoir accès à la page des calages
                }
            else
                if (data[i].isIncoherent == true )
                    {
                        $('.erreurDonneesIncoherente').show();
                        
                        //Si on est sur la page des zoom, ne pas appliquer de fonction pour afficher overlay pour les correction
                        if(document.title != "Graphe Pression" && document.title != "Graphe Volume" && document.title != "Graphe Temperature" )
                            final.push({ 'x': new Date(annee, mois, jour), 'y':data[i].y,  markerType: 'triangle',markerColor: "#e90d3f", markerSize: 12, cursor: "pointer", click: function(){ $('div.overlay-correctionManuelleDonnees').fadeOut(); $('.overlay-optionCorrectionDonnees').attr('style',''); $('.overlay-optionCorrectionDonnees').fadeIn();}});
                        else
                            final.push({ 'x': new Date(annee, mois, jour), 'y':data[i].y,  markerType: 'triangle',markerColor: "#e90d3f", markerSize: 12, cursor: "pointer" });
                    }
            else
                final.push({ 'x': new Date(annee, mois, jour), 'y':data[i].y});
        }
        return final;
    }
    
    /**
    *
    *   Recevoir chaine JSON SIMULE (données Température / pression et Volume) et la préparer pour utiliser ses coordonnées
    **/
    function prepareDataSimuleCourbes(json)
    {
        var data = json;
        var final = [];
        for (i=0; i < data.length; i++)
        {            
            
            var firstDate = data[i].x;
            
            var tokens = firstDate.split(",");
                        
            var annee = tokens[0];
            var mois = tokens[1] - 1 ;
            var jour = tokens[2];
            
            if (data[i].y == 'null') //Si pas de valeur, placer un marqueur au niveau de l'info manquante (y = 1.01 par défaut)
                {
                    final.push({ 'x': new Date(annee, mois, jour), 'y':null});
                }
                
            else
            final.push({ 'x': new Date(annee, mois, jour), 'y':data[i].y});
        }
        return final;        
    }
    
    
    /**
    *
    * Reevoir la liste des conflits après l'import d'une liste de données
    **/
    function getListeConflit()
    {
         $.get("/api/getConflictingValuesForRealData")
	    .done(function(response)
        {             
            var valTempsNew;
            var valTempsOld;
            var valPressionNew;
            var valPressionOld;
            var valVolumeNew;
            var valVolumeOld;
            var valDebitNew;
            var valDebitOld;

            var dateVal; 

            var valTemps;
            var valVolume;
            var valPression;
            var valDebit;

            var chaineTableau ="";
             
            for (i=0; i < response.length; i++)
            {            
                var dateVal = response[i].Date; //date à laquelle nous somme

                
                var tokens = dateVal.split("-");
                
                
                var jour = tokens[0];
                var mois = tokens[1] ;
                var annee = tokens[2];

                valTempsNew = response[i].TempNew;
                valTempsOld = response[i].TempOld;
                valPressionNew = response[i].PressionNew;
                valPressionOld = response[i].PressionOld;
                valVolumeNew = response[i].VolumeNew;
                valVolumeOld = response[i].VolumeOld;
                valDebitNew = response[i].DebitNew;
                valDebitOld = response[i].DebitOld;


                valTemps = '<td> ' + valTempsOld + '  </td> <td> ' + valTempsNew + '  </td>';
                valPression = '<td> ' + valPressionOld + '  </td> <td> ' + valPressionNew + '  </td>';
                valVolume = '<td> ' + valVolumeOld + '  </td> <td> ' + valVolumeNew + '  </td>';
                valDebit = '<td> ' + valDebitOld + '  </td> <td> ' + valDebitNew + '  </td>';


                chaineTableau += '<tr> <td>'+  + jour + '/' + mois + '/' + annee +  '</td>' + valPression + valTemps + valVolume + valDebit + '</tr>';


            }

            //insérer les données dans la page
            $('div.overlay-conflitDonnees div#listeDonneesConflit table').append(chaineTableau);
             
             
        })
        .fail(function()
        {
             
             

        });
    }

    
    /**
    *
    * Valider le changement des valeurs en conflits
    **/
    function confirmDataConflit()
    {
         $.get("/api/confirmForRealDataForCavity")
	    .done(function()
        {           
            location.href = "./gererDonneesRelevees.html";
        });
    }
    
    
    /**
    *
    * Refuser le changement des valeurs en conflits
    **/
    function annuleDataConflit()
    {
         $.get("/api/clickOnAnnule")
	    .done(function()
        {           
            location.href = "./gererDonneesRelevees.html";
        });
    }
    
    
    /*
    *
    * Envoeyr le tableau de correction manuelle
    **/
    function confirmDataCorrectionManuelle(tableauDonnees, commentaire)
    {
        $.post("/api/correctRealDataForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"), SiteName : sessionStorage.getItem("NomSiteActuel"), LogModif : commentaire , TabRealData: tableauDonnees }))
	    .done(function()
        {           
            location.href = "./gererDonneesRelevees.html";
        });
    }


     
/**************************
**
**  gererCalage.html  ****
**
******************/


    /**
    *
    *   Recevoir liste des calages
    **/
    function getListeCalages()
    {
        $.post("/api/getListCalageForCavity",
	     JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel")}))
	    .done(function(response)
        { 
            preparerListeCalages(response);
        })
        .fail(function()
        {
            
        });
    }


    /** 
    *
    *   Afficher la liste des calages
    **/ 
    function preparerListeCalages(json)
        {
            var data =  json;

            var stringCalage = "";

            if (sessionStorage.getItem("Langue") == "fr")
                {

                for (var i=0; i < data.length; i++)
                {
                    if (i != 0)
                        stringCalage =   '<div class="sectionCalage" idCalage=' + data[i].id + '><div class="sectionCalageTitre"> <div> <p><span class="trn">Du</span> : <input type="text" id="dateDebut" name="dateDebut" initialValue="' + data[i].dateFrom + '" value="' + data[i].dateFrom + '"></p>   <p><span class="trn">Pression</span> : <span class="trn">ds</span> <span class="calagePressionSd" initialValue="' + data[i].standardDeviationPression + '">' + data[i].standardDeviationPression + '</span>  / <span class="trn">dév min</span> <span class="calagePressionDevMin" initialValue="' + data[i].minDeviationPression + '">' + data[i].minDeviationPression + '</span> / <span class="trn">dév max</span> <span class="calagePressionDevMax" initialValue="' + data[i].maxDeviationPression + '">' + data[i].maxDeviationPression + '</span> / <span class="calagePressionSeq" initialValue="' + data[i].sommeEcartsQuadratiquesPression + '">' + data[i].sommeEcartsQuadratiquesPression + '</span> <span class="trn">seq</span> / <span class="calagePressionSde" initialValue="' + data[i].moyDeviationPression + '">' + data[i].moyDeviationPression + '</span> <span class="trn">sde</span> </p> </div>   <div><p><span class="trn">Au</span> : <input type="text" id="dateFin" name="dateFin" initialValue="' + data[i].dateTo + '" value="' + data[i].dateTo + '"></p><p> <span class="trn">Température</span> : <span class="trn">ds</span> <span class="calageTemperatureSd" initialValue="' + data[i].standardDeviationTemp + '">' + data[i].standardDeviationTemp + '</span>  / <span class="trn">dév min</span> <span class="calageTemperatureDevMin" initialValue="' + data[i].minDeviationTemp + '">' + data[i].minDeviationTemp + '</span> / <span class="trn">dév max</span> <span class="calageTemperatureDevMax" initialValue="' + data[i].maxDeviationTemp + '">' + data[i].maxDeviationTemp + '</span> / <span class="calageTemperatureSeq" initialValue="' + data[i].sommeEcartsQuadratiquesTemp + '">' + data[i].sommeEcartsQuadratiquesTemp + '</span> <span class="trn">seq</span> / <span class="calageTemperatureSde" initialValue="' + data[i].moyDeviationTemp + '">' + data[i].moyDeviationTemp + '</span> <span class="trn">sde</span> </p></div> <div class="checkboxAfficherCalage"> <p style="color: rgb(46, 204, 113);"> <input type="checkbox" name="checkbox" value="checkbox" style="color: rgb(46, 204, 113); background-color: rgb(255, 255, 255);"><span class="trn">Afficher sur le graphe</span></p></div><img src="../asset/img/caviteSite/flecheBasBlanc.png"/></div> <div class="sectionCalageContenu"> <div class="sectionCalageContent"><div class="sectionCalageContentLeft"><table><tr><th></th> <th class="trn">V stocké</th><th class="trn">Débit</th>   <th class="trn">P simul</th>  <th class="trn">DP arma</th>  </tr> <tr> <td> Coeff P-3</td> <td><input type="text" name="case11P" value="' + data[i].p_3_volumeStock + '" initialValue="' + data[i].p_3_volumeStock + '" ></td><td><input type="text" name="case12P" value="' + data[i].p_3_debit + '" initialValue="' + data[i].p_3_debit + '"></td><td><input type="text" name="case13P" value="' + data[i].p_3_psimul + '" initialValue="' + data[i].p_3_psimul + '"></td> <td> <b>-</b> </td></tr> <tr> <td> Coeff P-2</td> <td><input type="text" name="case21P" value="' + data[i].p_2_volumeStock + '" initialValue="' + data[i].p_2_volumeStock + '"></td>  <td><input type="text" name="case22P" value="' + data[i].p_2_debit + '" initialValue="' + data[i].p_2_debit + '"></td><td><input type="text" name="case23P" value="' + data[i].p_2_psimul + '" initialValue="' + data[i].p_2_psimul + '"></td>  <td><input type="text" name="case24P" value="' + data[i].p_2_dp_arma + '" initialValue="' + data[i].p_2_dp_arma + '"></td></tr><tr><td> Coeff P-1</td> <td><input type="text" name="case31P" value="' + data[i].p_1_volumeStock + '" initialValue="' + data[i].p_1_volumeStock + '"></td><td><input type="text" name="case32P" value="' + data[i].p_1_debit + '" initialValue="' + data[i].p_1_debit + '"></td> <td><input type="text" name="case33P" value="' + data[i].p_1_psimul + '" initialValue="' + data[i].p_1_psimul + '"></td><td><input type="text" name="case34P" value="' + data[i].p_1_dp_arma + '" initialValue="' + data[i].p_1_dp_arma + '"></td>  </tr>  <tr><td> Coeff P-0</td><td><input type="text" name="case41P" value="' + data[i].p_0_volumeStock + '" initialValue="' + data[i].p_0_volumeStock + '"></td><td><input type="text" name="case42P" value="' + data[i].p_0_debit + '" initialValue="' + data[i].p_0_debit + '"></td>  <td><input type="text" name="case43P" value="' + data[i].p_0_psimul + '" initialValue="' + data[i].p_0_psimul + '"></td> <td><input type="text" name="case44P" value="' + data[i].p_0_dp_arma + '" initialValue="' + data[i].p_0_dp_arma + '"></td> </tr> </table></div>  <div class="sectionCalageContentRight"><table> <tr> <th></th><th class="trn">PArma</th><th class="trn">V stocké</th>  <th class="trn">Débit</th><th class="trn">T simul</th> <th class="trn">DP arma</th>  </tr>  <tr><td> Coeff T-3</td><td><input type="text" name="case11T" value="' + data[i].t_3_pArma + '" initialValue="' + data[i].t_3_pArma + '"></td><td><input type="text" name="case12T" value="' + data[i].t_3_volumeStock + '" initialValue="' + data[i].t_3_volumeStock + '"></td> <td><input type="text" name="case13T" value="' + data[i].t_3_debit + '" initialValue="' + data[i].t_3_debit + '"></td><td><input type="text" name="case14T" value="' + data[i].t_3_tsimul + '" initialValue="' + data[i].t_3_tsimul + '"></td> <td> <b>-</b> </td> </tr> <tr>  <td> Coeff T-2</td><td><input type="text" name="case21T" value="' + data[i].t_2_pArma + '" initialValue="' + data[i].t_2_pArma + '"></td> <td><input type="text" name="case22T" value="' + data[i].t_2_volumeStock + '" initialValue="' + data[i].t_2_volumeStock + '"></td><td><input type="text" name="case23T" value="' + data[i].t_2_debit + '" initialValue="' + data[i].t_2_debit + '"></td> <td><input type="text" name="case24T" value="' + data[i].t_2_tsimul + '" initialValue="' + data[i].t_2_tsimul + '"></td> <td><input type="text" name="case25T" value="' + data[i].t_2_dp_arma + '" initialValue="' + data[i].t_2_dp_arma + '"></td> </tr>   <tr>  <td> Coeff T-1</td> <td><input type="text" name="case31T" value="' + data[i].t_1_pArma + '" initialValue="' + data[i].t_1_pArma + '"></td>    <td><input type="text" name="case32T" value="' + data[i].t_1_volumeStock + '" initialValue="' + data[i].t_1_volumeStock + '"></td>  <td><input type="text" name="case33T" value="' + data[i].t_1_debit + '" initialValue="' + data[i].t_1_debit + '"></td>  <td><input type="text" name="case34T" value="' + data[i].t_1_tsimul + '" initialValue="' + data[i].t_1_tsimul + '"></td>  <td>  <b>-</b> </td>  </tr>  <tr>  <td> Coeff T-0</td><td><input type="text" name="case41T" value="' + data[i].t_0_pArma + '" initialValue="' + data[i].t_0_pArma + '"></td>  <td><input type="text" name="case42T" value="' + data[i].t_0_volumeStock + '" initialValue="' + data[i].t_0_volumeStock + '"></td><td><input type="text" name="case43T" value="' + data[i].t_0_debit + '" initialValue="' + data[i].t_0_debit + '"></td>  <td><input type="text" name="case44T" value="' + data[i].t_0_tsimul + '" initialValue="' + data[i].t_0_tsimul + '"></td> <td>  <b>-</b> </td> </tr>   </table>  </div> <p class="boutonVert simulerCourbes trn">Simuler</p> <p class="simulerCourbesErreur trn">Pas assez de données pour pouvoir simuler entre ces dates</p></div> <div id="graphesGestionCalage"> <div id="graphesGestionCalageLeft"> <h3 class="trn">Pression</h3>  <div id="grapheTemperature"> <div id="chartContainerPressionCalage' + data[i].id +'" style="height: 190px; width: 100%;"></div> </div> <div id="listeInfoGraphe">  <div class="infoGraphe"> <p><span class="calagePresSd" initialValue="' + data[i].standardDeviationPression + '">' + data[i].standardDeviationPression + '</span></p> <p class="trn">Déviation standard</p>   </div> <div class="infoGraphe"><p><span class="calagePresDevMin" initialValue="' + data[i].minDeviationPression + '">' + data[i].minDeviationPression + '</span></p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p><span class="calagePresDevMax" initialValue="' + data[i].maxDeviationPression + '">' + data[i].maxDeviationPression + '</span></p><p class="trn">Déviation max</p> </div>  <div class="infoGraphe">  <p><span class="calagePresSeq" initialValue="' + data[i].sommeEcartsQuadratiquesPression + '">' + data[i].sommeEcartsQuadratiquesPression + '</span></p>  <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe"><p><span class="calagePresSde" initialValue="' + data[i].moyDeviationPression + '">' + data[i].moyDeviationPression + '</span></p><p class="trn">Déviations moyennes</p></div>  </div>  </div>  <div id="graphesGestionCalageRight"><h3 class="trn">Température</h3>    <div id="graphePression"><div id="chartContainerTempCalage' + data[i].id +'" style="height: 190px; width: 100%;"></div></div> <div id="listeInfoGraphe"><div class="infoGraphe"><p><span class="calageTempSd" initialValue="' + data[i].standardDeviationTemp + '">' + data[i].standardDeviationTemp + '</span></p> <p class="trn">Déviation standard</p></div>  <div class="infoGraphe"> <p><span class="calageTempDevMin" initialValue="' + data[i].minDeviationTemp + '">' + data[i].minDeviationTemp + '</span></p> <p class="trn">Déviation min</p></div><div class="infoGraphe"> <p><span class="calageTempDevMax" initialValue="' + data[i].maxDeviationTemp + '">' + data[i].maxDeviationTemp + '</span></p> <p class="trn">Déviation max</p> </div>  <div class="infoGraphe"> <p><span class="calageTempSeq" initialValue="' + data[i].sommeEcartsQuadratiquesTemp + '">' + data[i].sommeEcartsQuadratiquesTemp + '</span></p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe"> <p><span class="calageTempSde" initialValue="' + data[i].moyDeviationTemp + '">' + data[i].moyDeviationTemp + '</span></p> <p class="trn">Déviations moyennes</p>  </div> </div> </div>  </div> <div id="gestionCalagesBottom"> <p class="boutonRouge trn">Supprimer</p><p class="boutonGris trn">Annuler</p> <p class="boutonVert enregistrerCalage trn">Enregistrer</p> <p class="erreurDate trn">Les champs dates ne sont pas remplis ou incorrects.</p> <p class="erreurChampsCoef trn">Des champs coefficients ne sont pas remplis ou incorrects.</p> </div> </div> </div>';
                    else
                          stringCalage =   '<div class="sectionCalage sectionCalageActive" idCalage=' + data[i].id + '><div class="sectionCalageTitre"> <div> <p><span class="trn">Du</span> : <input type="text" id="dateDebut" name="dateDebut" initialValue="' + data[i].dateFrom + '" value="' + data[i].dateFrom + '"></p>   <p><span class="trn">Pression</span> : <span class="trn">ds</span> <span class="calagePressionSd" initialValue="' + data[i].standardDeviationPression + '">' + data[i].standardDeviationPression + '</span>  / <span class="trn">dév min</span> <span class="calagePressionDevMin" initialValue="' + data[i].minDeviationPression + '">' + data[i].minDeviationPression + '</span> / <span class="trn">dév max</span> <span class="calagePressionDevMax" initialValue="' + data[i].maxDeviationPression + '">' + data[i].maxDeviationPression + '</span> / <span class="calagePressionSeq" initialValue="' + data[i].sommeEcartsQuadratiquesPression + '">' + data[i].sommeEcartsQuadratiquesPression + '</span> <span class="trn">seq</span> / <span class="calagePressionSde" initialValue="' + data[i].moyDeviationPression + '">' + data[i].moyDeviationPression + '</span> <span class="trn">sde</span> </p> </div>   <div><p><span class="trn">Au</span> : <input type="text" id="dateFin" name="dateFin" initialValue="' + data[i].dateTo + '" value="' + data[i].dateTo + '"></p><p> <span class="trn">Température</span> : <span class="trn">ds</span> <span class="calageTemperatureSd" initialValue="' + data[i].standardDeviationTemp + '">' + data[i].standardDeviationTemp + '</span>  / <span class="trn">dév min</span> <span class="calageTemperatureDevMin" initialValue="' + data[i].minDeviationTemp + '">' + data[i].minDeviationTemp + '</span> / <span class="trn">dév max</span> <span class="calageTemperatureDevMax" initialValue="' + data[i].maxDeviationTemp + '">' + data[i].maxDeviationTemp + '</span> / <span class="calageTemperatureSeq" initialValue="' + data[i].sommeEcartsQuadratiquesTemp + '">' + data[i].sommeEcartsQuadratiquesTemp + '</span> <span class="trn">seq</span> / <span class="calageTemperatureSde" initialValue="' + data[i].moyDeviationTemp + '">' + data[i].moyDeviationTemp + '</span> <span class="trn">sde</span> </p></div> <div class="checkboxAfficherCalage"> <p style="color: rgb(46, 204, 113);"> <input type="checkbox" name="checkbox" value="checkbox" style="color: rgb(46, 204, 113); background-color: rgb(255, 255, 255);"><span class="trn">Afficher sur le graphe</span></p></div><img src="../asset/img/caviteSite/flecheHautVert.png"/></div> <div class="sectionCalageContenu"> <div class="sectionCalageContent">  <div class="sectionCalageContentLeft"><table><tr><th></th> <th class="trn">V stocké</th><th class="trn">Débit</th>   <th class="trn">P simul</th>  <th class="trn">DP arma</th>  </tr> <tr> <td> Coeff P-3</td> <td><input type="text" name="case11P" value="' + data[i].p_3_volumeStock + '" initialValue="' + data[i].p_3_volumeStock + '" ></td><td><input type="text" name="case12P" value="' + data[i].p_3_debit + '" initialValue="' + data[i].p_3_debit + '"></td><td><input type="text" name="case13P" value="' + data[i].p_3_psimul + '" initialValue="' + data[i].p_3_psimul + '"></td> <td> <b>-</b> </td></tr> <tr> <td> Coeff P-2</td> <td><input type="text" name="case21P" value="' + data[i].p_2_volumeStock + '" initialValue="' + data[i].p_2_volumeStock + '"></td>  <td><input type="text" name="case22P" value="' + data[i].p_2_debit + '" initialValue="' + data[i].p_2_debit + '"></td><td><input type="text" name="case23P" value="' + data[i].p_2_psimul + '" initialValue="' + data[i].p_2_psimul + '"></td>  <td><input type="text" name="case24P" value="' + data[i].p_2_dp_arma + '" initialValue="' + data[i].p_2_dp_arma + '"></td></tr><tr><td> Coeff P-1</td> <td><input type="text" name="case31P" value="' + data[i].p_1_volumeStock + '" initialValue="' + data[i].p_1_volumeStock + '"></td><td><input type="text" name="case32P" value="' + data[i].p_1_debit + '" initialValue="' + data[i].p_1_debit + '"></td> <td><input type="text" name="case33P" value="' + data[i].p_1_psimul + '" initialValue="' + data[i].p_1_psimul + '"></td><td><input type="text" name="case34P" value="' + data[i].p_1_dp_arma + '" initialValue="' + data[i].p_1_dp_arma + '"></td>  </tr>  <tr><td> Coeff P-0</td><td><input type="text" name="case41P" value="' + data[i].p_0_volumeStock + '" initialValue="' + data[i].p_0_volumeStock + '"></td><td><input type="text" name="case42P" value="' + data[i].p_0_debit + '" initialValue="' + data[i].p_0_debit + '"></td>  <td><input type="text" name="case43P" value="' + data[i].p_0_psimul + '" initialValue="' + data[i].p_0_psimul + '"></td> <td><input type="text" name="case44P" value="' + data[i].p_0_dp_arma + '" initialValue="' + data[i].p_0_dp_arma + '"></td> </tr> </table></div>  <div class="sectionCalageContentRight"><table> <tr> <th></th><th class="trn">PArma</th><th class="trn">V stocké</th>  <th class="trn">Débit</th><th class="trn">T simul</th> <th class="trn">DP arma</th>  </tr>  <tr><td>Coeff T-3</td><td><input type="text" name="case11T" value="' + data[i].t_3_pArma + '" initialValue="' + data[i].t_3_pArma + '"></td><td><input type="text" name="case12T" value="' + data[i].t_3_volumeStock + '" initialValue="' + data[i].t_3_volumeStock + '"></td> <td><input type="text" name="case13T" value="' + data[i].t_3_debit + '" initialValue="' + data[i].t_3_debit + '"></td><td><input type="text" name="case14T" value="' + data[i].t_3_tsimul + '" initialValue="' + data[i].t_3_tsimul + '"></td> <td> <b>-</b> </td> </tr> <tr>  <td> Coeff T-2</td><td><input type="text" name="case21T" value="' + data[i].t_2_pArma + '" initialValue="' + data[i].t_2_pArma + '"></td> <td><input type="text" name="case22T" value="' + data[i].t_2_volumeStock + '" initialValue="' + data[i].t_2_volumeStock + '"></td><td><input type="text" name="case23T" value="' + data[i].t_2_debit + '" initialValue="' + data[i].t_2_debit + '"></td> <td><input type="text" name="case24T" value="' + data[i].t_2_tsimul + '" initialValue="' + data[i].t_2_tsimul + '"></td> <td><input type="text" name="case25T" value="' + data[i].t_2_dp_arma + '" initialValue="' + data[i].t_2_dp_arma + '"></td> </tr>   <tr>  <td> Coeff T-1</td> <td><input type="text" name="case31T" value="' + data[i].t_1_pArma + '" initialValue="' + data[i].t_1_pArma + '"></td>    <td><input type="text" name="case32T" value="' + data[i].t_1_volumeStock + '" initialValue="' + data[i].t_1_volumeStock + '"></td>  <td><input type="text" name="case33T" value="' + data[i].t_1_debit + '" initialValue="' + data[i].t_1_debit + '"></td>  <td><input type="text" name="case34T" value="' + data[i].t_1_tsimul + '" initialValue="' + data[i].t_1_tsimul + '"></td>  <td>  <b>-</b> </td>  </tr>  <tr>  <td> Coeff T-0</td><td><input type="text" name="case41T" value="' + data[i].t_0_pArma + '" initialValue="' + data[i].t_0_pArma + '"></td>  <td><input type="text" name="case42T" value="' + data[i].t_0_volumeStock + '" initialValue="' + data[i].t_0_volumeStock + '"></td><td><input type="text" name="case43T" value="' + data[i].t_0_debit + '" initialValue="' + data[i].t_0_debit + '"></td>  <td><input type="text" name="case44T" value="' + data[i].t_0_tsimul + '" initialValue="' + data[i].t_0_tsimul + '"></td> <td>  <b>-</b> </td> </tr>   </table>  </div> <p class="boutonVert simulerCourbes trn">Simuler</p> <p class="simulerCourbesErreur trn">Pas assez de données pour pouvoir simuler entre ces dates</p> </div> <div id="graphesGestionCalage">     <div id="graphesGestionCalageLeft"> <h3 class="trn">Pression</h3>  <div id="grapheTemperature"> <div id="chartContainerPressionCalage' + data[i].id +'" style="height: 190px; width: 100%;"></div> </div> <div id="listeInfoGraphe">  <div class="infoGraphe"> <p><span class="calagePresSd" initialValue="' + data[i].standardDeviationPression + '">' + data[i].standardDeviationPression + '</span></p> <p class="trn">Déviation standard</p>   </div> <div class="infoGraphe"><p><span class="calagePresDevMin" initialValue="' + data[i].minDeviationPression + '">' + data[i].minDeviationPression + '</span></p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p><span class="calagePresDevMax" initialValue="' + data[i].maxDeviationPression + '">' + data[i].maxDeviationPression + '</span></p><p class="trn">Déviation max</p> </div>  <div class="infoGraphe"><p><span class="calagePresSeq" initialValue="' + data[i].sommeEcartsQuadratiquesPression + '">' + data[i].sommeEcartsQuadratiquesPression + '</span></p>  <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe"><p><span class="calagePresSde" initialValue="' + data[i].moyDeviationPression + '">' + data[i].moyDeviationPression + '</span></p><p class="trn">Déviations moyennes</p></div>  </div>  </div>  <div id="graphesGestionCalageRight"><h3 class="trn">Température</h3>    <div id="graphePression"><div id="chartContainerTempCalage' + data[i].id +'" style="height: 190px; width: 100%;"></div></div> <div id="listeInfoGraphe"><div class="infoGraphe"><p><span class="calageTempSd" initialValue="' + data[i].standardDeviationTemp + '">' + data[i].standardDeviationTemp + '</span></p> <p class="trn">Déviation standard</p></div>  <div class="infoGraphe"> <p><span class="calageTempDevMin" initialValue="' + data[i].minDeviationTemp + '">' + data[i].minDeviationTemp + '</span></p> <p class="trn">Déviation min</p></div><div class="infoGraphe"> <p><span class="calageTempDevMax" initialValue="' + data[i].maxDeviationTemp + '">' + data[i].maxDeviationTemp + '</span></p> <p class="trn">Déviation max</p> </div>  <div class="infoGraphe"> <p><span class="calageTempSeq" initialValue="' + data[i].sommeEcartsQuadratiquesTemp + '">' + data[i].sommeEcartsQuadratiquesTemp + '</span></p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe"> <p><span class="calageTempSde" initialValue="' + data[i].moyDeviationTemp + '">' + data[i].moyDeviationTemp + '</span></p> <p class="trn">Déviations moyennes</p>  </div> </div> </div>  </div> <div id="gestionCalagesBottom"> <p class="boutonRouge trn">Supprimer</p><p class="boutonGris trn">Annuler</p> <p class="boutonVert enregistrerCalage trn">Enregistrer</p> <p class="erreurDate trn"> Les champs dates ne sont pas remplis ou incorrects.</p> <p class="erreurChampsCoef trn">Des champs coefficients ne sont pas remplis ou incorrects.</p> </div> </div> </div>';



                     //Envoyer les graphes 
                    var courbeTempJsonReel = prepareDataReelCourbes(data[i].dataTempReel);
                    var courbeTempJsonSimule = prepareDataSimuleCourbes(data[i].dataTempSimul);
                    var courbePresJsonReel = prepareDataReelCourbes(data[i].dataPressionReel);
                    var courbePresJsonSimule = prepareDataSimuleCourbes(data[i].dataPressionSimul);


                    tabDataTempJsonReel[data[i].id] = courbeTempJsonReel;
                    tabDataTempJsonSimule[data[i].id] = courbeTempJsonSimule;
                    tabDataPresJsonReel[data[i].id] = courbePresJsonReel;
                    tabDataPresJsonSimule[data[i].id] = courbePresJsonSimule;




                    $('#listeCalage').prepend(stringCalage);


                    //Si case doit être cochée
                    if (data[i].isLocked)
                        $('div[idcalage="' + data[i].id+'"] input[type="checkbox"]').prop('checked',true);


                    afficherDoubleGraphCalages(courbeTempJsonReel, courbeTempJsonSimule, courbePresJsonReel, courbePresJsonSimule, data[i].id);

                }


                }
            else
                {
                    
                for (var i=0; i < data.length; i++)
                {
                    if (i != 0)
                        stringCalage =   '<div class="sectionCalage" idCalage=' + data[i].id + '><div class="sectionCalageTitre"> <div> <p><span class="trn">From</span> : <input type="text" id="dateDebut" name="dateDebut" initialValue="' + data[i].dateFrom + '" value="' + data[i].dateFrom + '"></p>   <p><span class="trn">Pressure</span> : <span class="trn">sd</span> <span class="calagePressionSd" initialValue="' + data[i].standardDeviationPression + '">' + data[i].standardDeviationPression + '</span>  / <span class="trn">min dev</span> <span class="calagePressionDevMin" initialValue="' + data[i].minDeviationPression + '">' + data[i].minDeviationPression + '</span> / <span class="trn">max dev</span> <span class="calagePressionDevMax" initialValue="' + data[i].maxDeviationPression + '">' + data[i].maxDeviationPression + '</span> / <span class="calagePressionSeq" initialValue="' + data[i].sommeEcartsQuadratiquesPression + '">' + data[i].sommeEcartsQuadratiquesPression + '</span> <span class="trn">sqd</span> / <span class="calagePressionSde" initialValue="' + data[i].moyDeviationPression + '">' + data[i].moyDeviationPression + '</span> <span class="trn">sde</span> </p> </div>   <div><p><span class="trn">To</span> : <input type="text" id="dateFin" name="dateFin" initialValue="' + data[i].dateTo + '" value="' + data[i].dateTo + '"></p><p> <span class="trn">Temperature</span> : <span class="trn">sd</span> <span class="calageTemperatureSd" initialValue="' + data[i].standardDeviationTemp + '">' + data[i].standardDeviationTemp + '</span>  / <span class="trn">min dev</span> <span class="calageTemperatureDevMin" initialValue="' + data[i].minDeviationTemp + '">' + data[i].minDeviationTemp + '</span> / <span class="trn">max dev</span> <span class="calageTemperatureDevMax" initialValue="' + data[i].maxDeviationTemp + '">' + data[i].maxDeviationTemp + '</span> / <span class="calageTemperatureSeq" initialValue="' + data[i].sommeEcartsQuadratiquesTemp + '">' + data[i].sommeEcartsQuadratiquesTemp + '</span> <span class="trn">sqd</span> / <span class="calageTemperatureSde" initialValue="' + data[i].moyDeviationTemp + '">' + data[i].moyDeviationTemp + '</span> <span class="trn">sde</span> </p></div> <div class="checkboxAfficherCalage"> <p style="color: rgb(46, 204, 113);"> <input type="checkbox" name="checkbox" value="checkbox" style="color: rgb(46, 204, 113); background-color: rgb(255, 255, 255);"><span class="trn">Show on the graph</span></p></div><img src="../asset/img/caviteSite/flecheBasBlanc.png"/></div> <div class="sectionCalageContenu"> <div class="sectionCalageContent"><div class="sectionCalageContentLeft"><table><tr><th></th> <th class="trn">stored V</th><th class="trn">Flow</th>   <th class="trn">simul P</th>  <th class="trn">DP arma</th>  </tr> <tr> <td> Coeff P-3</td> <td><input type="text" name="case11P" value="' + data[i].p_3_volumeStock + '" initialValue="' + data[i].p_3_volumeStock + '" ></td><td><input type="text" name="case12P" value="' + data[i].p_3_debit + '" initialValue="' + data[i].p_3_debit + '"></td><td><input type="text" name="case13P" value="' + data[i].p_3_psimul + '" initialValue="' + data[i].p_3_psimul + '"></td> <td> <b>-</b> </td></tr> <tr> <td> Coeff P-2</td> <td><input type="text" name="case21P" value="' + data[i].p_2_volumeStock + '" initialValue="' + data[i].p_2_volumeStock + '"></td>  <td><input type="text" name="case22P" value="' + data[i].p_2_debit + '" initialValue="' + data[i].p_2_debit + '"></td><td><input type="text" name="case23P" value="' + data[i].p_2_psimul + '" initialValue="' + data[i].p_2_psimul + '"></td>  <td><input type="text" name="case24P" value="' + data[i].p_2_dp_arma + '" initialValue="' + data[i].p_2_dp_arma + '"></td></tr><tr><td> Coeff P-1</td> <td><input type="text" name="case31P" value="' + data[i].p_1_volumeStock + '" initialValue="' + data[i].p_1_volumeStock + '"></td><td><input type="text" name="case32P" value="' + data[i].p_1_debit + '" initialValue="' + data[i].p_1_debit + '"></td> <td><input type="text" name="case33P" value="' + data[i].p_1_psimul + '" initialValue="' + data[i].p_1_psimul + '"></td><td><input type="text" name="case34P" value="' + data[i].p_1_dp_arma + '" initialValue="' + data[i].p_1_dp_arma + '"></td>  </tr>  <tr><td> Coeff P-0</td><td><input type="text" name="case41P" value="' + data[i].p_0_volumeStock + '" initialValue="' + data[i].p_0_volumeStock + '"></td><td><input type="text" name="case42P" value="' + data[i].p_0_debit + '" initialValue="' + data[i].p_0_debit + '"></td>  <td><input type="text" name="case43P" value="' + data[i].p_0_psimul + '" initialValue="' + data[i].p_0_psimul + '"></td> <td><input type="text" name="case44P" value="' + data[i].p_0_dp_arma + '" initialValue="' + data[i].p_0_dp_arma + '"></td> </tr> </table></div>  <div class="sectionCalageContentRight"><table> <tr> <th></th><th class="trn">PArma</th><th class="trn">stored V</th>  <th class="trn">Flow</th><th class="trn">simul T</th> <th class="trn">DP arma</th>  </tr>  <tr><td> Coeff T-3</td><td><input type="text" name="case11T" value="' + data[i].t_3_pArma + '" initialValue="' + data[i].t_3_pArma + '"></td><td><input type="text" name="case12T" value="' + data[i].t_3_volumeStock + '" initialValue="' + data[i].t_3_volumeStock + '"></td> <td><input type="text" name="case13T" value="' + data[i].t_3_debit + '" initialValue="' + data[i].t_3_debit + '"></td><td><input type="text" name="case14T" value="' + data[i].t_3_tsimul + '" initialValue="' + data[i].t_3_tsimul + '"></td> <td> <b>-</b> </td> </tr> <tr>  <td> Coeff T-2</td><td><input type="text" name="case21T" value="' + data[i].t_2_pArma + '" initialValue="' + data[i].t_2_pArma + '"></td> <td><input type="text" name="case22T" value="' + data[i].t_2_volumeStock + '" initialValue="' + data[i].t_2_volumeStock + '"></td><td><input type="text" name="case23T" value="' + data[i].t_2_debit + '" initialValue="' + data[i].t_2_debit + '"></td> <td><input type="text" name="case24T" value="' + data[i].t_2_tsimul + '" initialValue="' + data[i].t_2_tsimul + '"></td> <td><input type="text" name="case25T" value="' + data[i].t_2_dp_arma + '" initialValue="' + data[i].t_2_dp_arma + '"></td> </tr>   <tr>  <td> Coeff T-1</td> <td><input type="text" name="case31T" value="' + data[i].t_1_pArma + '" initialValue="' + data[i].t_1_pArma + '"></td>    <td><input type="text" name="case32T" value="' + data[i].t_1_volumeStock + '" initialValue="' + data[i].t_1_volumeStock + '"></td>  <td><input type="text" name="case33T" value="' + data[i].t_1_debit + '" initialValue="' + data[i].t_1_debit + '"></td>  <td><input type="text" name="case34T" value="' + data[i].t_1_tsimul + '" initialValue="' + data[i].t_1_tsimul + '"></td>  <td>  <b>-</b> </td>  </tr>  <tr>  <td> Coeff T-0</td><td><input type="text" name="case41T" value="' + data[i].t_0_pArma + '" initialValue="' + data[i].t_0_pArma + '"></td>  <td><input type="text" name="case42T" value="' + data[i].t_0_volumeStock + '" initialValue="' + data[i].t_0_volumeStock + '"></td><td><input type="text" name="case43T" value="' + data[i].t_0_debit + '" initialValue="' + data[i].t_0_debit + '"></td>  <td><input type="text" name="case44T" value="' + data[i].t_0_tsimul + '" initialValue="' + data[i].t_0_tsimul + '"></td> <td>  <b>-</b> </td> </tr>   </table>  </div> <p class="boutonVert simulerCourbes trn">Simulate</p> <p class="simulerCourbesErreur trn">Not enough data to simulate between these dates</p></div> <div id="graphesGestionCalage"> <div id="graphesGestionCalageLeft"> <h3 class="trn">Pressure</h3>  <div id="grapheTemperature"> <div id="chartContainerPressionCalage' + data[i].id +'" style="height: 190px; width: 100%;"></div> </div> <div id="listeInfoGraphe">  <div class="infoGraphe"> <p><span class="calagePresSd" initialValue="' + data[i].standardDeviationPression + '">' + data[i].standardDeviationPression + '</span></p> <p class="trn">Standard deviation</p>   </div> <div class="infoGraphe"><p><span class="calagePresDevMin" initialValue="' + data[i].minDeviationPression + '">' + data[i].minDeviationPression + '</span></p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p><span class="calagePresDevMax" initialValue="' + data[i].maxDeviationPression + '">' + data[i].maxDeviationPression + '</span></p><p class="trn">Max deviation</p> </div>  <div class="infoGraphe">  <p><span class="calagePresSeq" initialValue="' + data[i].sommeEcartsQuadratiquesPression + '">' + data[i].sommeEcartsQuadratiquesPression + '</span></p>  <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe"><p><span class="calagePresSde" initialValue="' + data[i].moyDeviationPression + '">' + data[i].moyDeviationPression + '</span></p><p class="trn">Average deviations</p></div>  </div>  </div>  <div id="graphesGestionCalageRight"><h3 class="trn">Temperature</h3>    <div id="graphePression"><div id="chartContainerTempCalage' + data[i].id +'" style="height: 190px; width: 100%;"></div></div> <div id="listeInfoGraphe"><div class="infoGraphe"><p><span class="calageTempSd" initialValue="' + data[i].standardDeviationTemp + '">' + data[i].standardDeviationTemp + '</span></p> <p class="trn">Standard deviation</p></div>  <div class="infoGraphe"> <p><span class="calageTempDevMin" initialValue="' + data[i].minDeviationTemp + '">' + data[i].minDeviationTemp + '</span></p> <p class="trn">Min deviation</p></div><div class="infoGraphe"> <p><span class="calageTempDevMax" initialValue="' + data[i].maxDeviationTemp + '">' + data[i].maxDeviationTemp + '</span></p> <p class="trn">Max deviation</p> </div>  <div class="infoGraphe"> <p><span class="calageTempSeq" initialValue="' + data[i].sommeEcartsQuadratiquesTemp + '">' + data[i].sommeEcartsQuadratiquesTemp + '</span></p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe"> <p><span class="calageTempSde" initialValue="' + data[i].moyDeviationTemp + '">' + data[i].moyDeviationTemp + '</span></p> <p class="trn">Average deviations</p>  </div> </div> </div>  </div> <div id="gestionCalagesBottom"> <p class="boutonRouge trn">Remove</p><p class="boutonGris trn">Cancel</p> <p class="boutonVert enregistrerCalage trn">Save</p> <p class="erreurDate trn">The date fields are not filled or incorrect.</p> <p class="erreurChampsCoef trn">Coefficient fields are not filled or incorrect.</p> </div> </div> </div>';
                    else
                          stringCalage = '<div class="sectionCalage sectionCalageActive" idCalage=' + data[i].id + '><div class="sectionCalageTitre"> <div> <p><span class="trn">From</span> : <input type="text" id="dateDebut" name="dateDebut" initialValue="' + data[i].dateFrom + '" value="' + data[i].dateFrom + '"></p>   <p><span class="trn">Pressure</span> : <span class="trn">sd</span> <span class="calagePressionSd" initialValue="' + data[i].standardDeviationPression + '">' + data[i].standardDeviationPression + '</span>  / <span class="trn">min dev</span> <span class="calagePressionDevMin" initialValue="' + data[i].minDeviationPression + '">' + data[i].minDeviationPression + '</span> / <span class="trn">max dev</span> <span class="calagePressionDevMax" initialValue="' + data[i].maxDeviationPression + '">' + data[i].maxDeviationPression + '</span> / <span class="calagePressionSeq" initialValue="' + data[i].sommeEcartsQuadratiquesPression + '">' + data[i].sommeEcartsQuadratiquesPression + '</span> <span class="trn">sqd</span> / <span class="calagePressionSde" initialValue="' + data[i].moyDeviationPression + '">' + data[i].moyDeviationPression + '</span> <span class="trn">sde</span> </p> </div>   <div><p><span class="trn">To</span> : <input type="text" id="dateFin" name="dateFin" initialValue="' + data[i].dateTo + '" value="' + data[i].dateTo + '"></p><p> <span class="trn">Temperature</span> : <span class="trn">sd</span> <span class="calageTemperatureSd" initialValue="' + data[i].standardDeviationTemp + '">' + data[i].standardDeviationTemp + '</span>  / <span class="trn">min dev</span> <span class="calageTemperatureDevMin" initialValue="' + data[i].minDeviationTemp + '">' + data[i].minDeviationTemp + '</span> / <span class="trn">max dev</span> <span class="calageTemperatureDevMax" initialValue="' + data[i].maxDeviationTemp + '">' + data[i].maxDeviationTemp + '</span> / <span class="calageTemperatureSeq" initialValue="' + data[i].sommeEcartsQuadratiquesTemp + '">' + data[i].sommeEcartsQuadratiquesTemp + '</span> <span class="trn">sqd</span> / <span class="calageTemperatureSde" initialValue="' + data[i].moyDeviationTemp + '">' + data[i].moyDeviationTemp + '</span> <span class="trn">sde</span> </p></div> <div class="checkboxAfficherCalage"> <p style="color: rgb(46, 204, 113);"> <input type="checkbox" name="checkbox" value="checkbox" style="color: rgb(46, 204, 113); background-color: rgb(255, 255, 255);"><span class="trn">Show on the graph</span></p></div><img src="../asset/img/caviteSite/flecheHautVert.png"/></div> <div class="sectionCalageContenu"> <div class="sectionCalageContent">  <div class="sectionCalageContentLeft"><table><tr><th></th> <th class="trn">stored V</th><th class="trn">Flow</th>   <th class="trn">simul P</th>  <th class="trn">DP arma</th>  </tr> <tr> <td> Coeff P-3</td> <td><input type="text" name="case11P" value="' + data[i].p_3_volumeStock + '" initialValue="' + data[i].p_3_volumeStock + '" ></td><td><input type="text" name="case12P" value="' + data[i].p_3_debit + '" initialValue="' + data[i].p_3_debit + '"></td><td><input type="text" name="case13P" value="' + data[i].p_3_psimul + '" initialValue="' + data[i].p_3_psimul + '"></td> <td> <b>-</b> </td></tr> <tr> <td> Coeff P-2</td> <td><input type="text" name="case21P" value="' + data[i].p_2_volumeStock + '" initialValue="' + data[i].p_2_volumeStock + '"></td>  <td><input type="text" name="case22P" value="' + data[i].p_2_debit + '" initialValue="' + data[i].p_2_debit + '"></td><td><input type="text" name="case23P" value="' + data[i].p_2_psimul + '" initialValue="' + data[i].p_2_psimul + '"></td>  <td><input type="text" name="case24P" value="' + data[i].p_2_dp_arma + '" initialValue="' + data[i].p_2_dp_arma + '"></td></tr><tr><td> Coeff P-1</td> <td><input type="text" name="case31P" value="' + data[i].p_1_volumeStock + '" initialValue="' + data[i].p_1_volumeStock + '"></td><td><input type="text" name="case32P" value="' + data[i].p_1_debit + '" initialValue="' + data[i].p_1_debit + '"></td> <td><input type="text" name="case33P" value="' + data[i].p_1_psimul + '" initialValue="' + data[i].p_1_psimul + '"></td><td><input type="text" name="case34P" value="' + data[i].p_1_dp_arma + '" initialValue="' + data[i].p_1_dp_arma + '"></td>  </tr>  <tr><td> Coeff P-0</td><td><input type="text" name="case41P" value="' + data[i].p_0_volumeStock + '" initialValue="' + data[i].p_0_volumeStock + '"></td><td><input type="text" name="case42P" value="' + data[i].p_0_debit + '" initialValue="' + data[i].p_0_debit + '"></td>  <td><input type="text" name="case43P" value="' + data[i].p_0_psimul + '" initialValue="' + data[i].p_0_psimul + '"></td> <td><input type="text" name="case44P" value="' + data[i].p_0_dp_arma + '" initialValue="' + data[i].p_0_dp_arma + '"></td> </tr> </table></div>  <div class="sectionCalageContentRight"><table> <tr> <th></th><th class="trn">PArma</th><th class="trn">stored V</th>  <th class="trn">Flow</th><th class="trn">simul T</th> <th class="trn">DP arma</th>  </tr>  <tr><td>Coeff T-3</td><td><input type="text" name="case11T" value="' + data[i].t_3_pArma + '" initialValue="' + data[i].t_3_pArma + '"></td><td><input type="text" name="case12T" value="' + data[i].t_3_volumeStock + '" initialValue="' + data[i].t_3_volumeStock + '"></td> <td><input type="text" name="case13T" value="' + data[i].t_3_debit + '" initialValue="' + data[i].t_3_debit + '"></td><td><input type="text" name="case14T" value="' + data[i].t_3_tsimul + '" initialValue="' + data[i].t_3_tsimul + '"></td> <td> <b>-</b> </td> </tr> <tr>  <td> Coeff T-2</td><td><input type="text" name="case21T" value="' + data[i].t_2_pArma + '" initialValue="' + data[i].t_2_pArma + '"></td> <td><input type="text" name="case22T" value="' + data[i].t_2_volumeStock + '" initialValue="' + data[i].t_2_volumeStock + '"></td><td><input type="text" name="case23T" value="' + data[i].t_2_debit + '" initialValue="' + data[i].t_2_debit + '"></td> <td><input type="text" name="case24T" value="' + data[i].t_2_tsimul + '" initialValue="' + data[i].t_2_tsimul + '"></td> <td><input type="text" name="case25T" value="' + data[i].t_2_dp_arma + '" initialValue="' + data[i].t_2_dp_arma + '"></td> </tr>   <tr>  <td> Coeff T-1</td> <td><input type="text" name="case31T" value="' + data[i].t_1_pArma + '" initialValue="' + data[i].t_1_pArma + '"></td>    <td><input type="text" name="case32T" value="' + data[i].t_1_volumeStock + '" initialValue="' + data[i].t_1_volumeStock + '"></td>  <td><input type="text" name="case33T" value="' + data[i].t_1_debit + '" initialValue="' + data[i].t_1_debit + '"></td>  <td><input type="text" name="case34T" value="' + data[i].t_1_tsimul + '" initialValue="' + data[i].t_1_tsimul + '"></td>  <td>  <b>-</b> </td>  </tr>  <tr>  <td> Coeff T-0</td><td><input type="text" name="case41T" value="' + data[i].t_0_pArma + '" initialValue="' + data[i].t_0_pArma + '"></td>  <td><input type="text" name="case42T" value="' + data[i].t_0_volumeStock + '" initialValue="' + data[i].t_0_volumeStock + '"></td><td><input type="text" name="case43T" value="' + data[i].t_0_debit + '" initialValue="' + data[i].t_0_debit + '"></td>  <td><input type="text" name="case44T" value="' + data[i].t_0_tsimul + '" initialValue="' + data[i].t_0_tsimul + '"></td> <td>  <b>-</b> </td> </tr>   </table>  </div> <p class="boutonVert simulerCourbes trn">Simulate</p> <p class="simulerCourbesErreur trn">Not enough data to simulate between these dates</p> </div> <div id="graphesGestionCalage">     <div id="graphesGestionCalageLeft"> <h3 class="trn">Pressure</h3>  <div id="grapheTemperature"> <div id="chartContainerPressionCalage' + data[i].id +'" style="height: 190px; width: 100%;"></div> </div> <div id="listeInfoGraphe">  <div class="infoGraphe"> <p><span class="calagePresSd" initialValue="' + data[i].standardDeviationPression + '">' + data[i].standardDeviationPression + '</span></p> <p class="trn">Standard deviation</p>   </div> <div class="infoGraphe"><p><span class="calagePresDevMin" initialValue="' + data[i].minDeviationPression + '">' + data[i].minDeviationPression + '</span></p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p><span class="calagePresDevMax" initialValue="' + data[i].maxDeviationPression + '">' + data[i].maxDeviationPression + '</span></p><p class="trn">Max deviation</p> </div>  <div class="infoGraphe"><p><span class="calagePresSeq" initialValue="' + data[i].sommeEcartsQuadratiquesPression + '">' + data[i].sommeEcartsQuadratiquesPression + '</span></p>  <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe"><p><span class="calagePresSde" initialValue="' + data[i].moyDeviationPression + '">' + data[i].moyDeviationPression + '</span></p><p class="trn">Average deviations</p></div>  </div>  </div>  <div id="graphesGestionCalageRight"><h3 class="trn">Temperature</h3>    <div id="graphePression"><div id="chartContainerTempCalage' + data[i].id +'" style="height: 190px; width: 100%;"></div></div> <div id="listeInfoGraphe"><div class="infoGraphe"><p><span class="calageTempSd" initialValue="' + data[i].standardDeviationTemp + '">' + data[i].standardDeviationTemp + '</span></p> <p class="trn">Standard deviation</p></div>  <div class="infoGraphe"> <p><span class="calageTempDevMin" initialValue="' + data[i].minDeviationTemp + '">' + data[i].minDeviationTemp + '</span></p> <p class="trn">Min deviation</p></div><div class="infoGraphe"> <p><span class="calageTempDevMax" initialValue="' + data[i].maxDeviationTemp + '">' + data[i].maxDeviationTemp + '</span></p> <p class="trn">Max deviation</p> </div>  <div class="infoGraphe"> <p><span class="calageTempSeq" initialValue="' + data[i].sommeEcartsQuadratiquesTemp + '">' + data[i].sommeEcartsQuadratiquesTemp + '</span></p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe"> <p><span class="calageTempSde" initialValue="' + data[i].moyDeviationTemp + '">' + data[i].moyDeviationTemp + '</span></p> <p class="trn">Average deviations</p>  </div> </div> </div>  </div> <div id="gestionCalagesBottom"> <p class="boutonRouge trn">Remove</p><p class="boutonGris trn">Cancel</p> <p class="boutonVert enregistrerCalage trn">Save</p> <p class="erreurDate trn"> The date fields are not filled or incorrect.</p> <p class="erreurChampsCoef trn">Coefficient fields are not filled or incorrect.</p> </div> </div> </div>';



                     //Envoyer les graphes 
                    var courbeTempJsonReel = prepareDataReelCourbes(data[i].dataTempReel);
                    var courbeTempJsonSimule = prepareDataSimuleCourbes(data[i].dataTempSimul);
                    var courbePresJsonReel = prepareDataReelCourbes(data[i].dataPressionReel);
                    var courbePresJsonSimule = prepareDataSimuleCourbes(data[i].dataPressionSimul);


                    tabDataTempJsonReel[data[i].id] = courbeTempJsonReel;
                    tabDataTempJsonSimule[data[i].id] = courbeTempJsonSimule;
                    tabDataPresJsonReel[data[i].id] = courbePresJsonReel;
                    tabDataPresJsonSimule[data[i].id] = courbePresJsonSimule;




                    $('#listeCalage').prepend(stringCalage);


                    //Si case doit être cochée
                    if (data[i].isLocked)
                        $('div[idcalage="' + data[i].id+'"] input[type="checkbox"]').prop('checked',true);


                    afficherDoubleGraphCalages(courbeTempJsonReel, courbeTempJsonSimule, courbePresJsonReel, courbePresJsonSimule, data[i].id);

                }



                    
                }
                
                
                /*  Traduire le champs Date des input **/
                $( "#dateDebut, #dateFin" ).datepicker(
                {
                    altField: "#datepicker",
                    closeText: 'Fermer',
                    prevText: 'Précédent',
                    nextText: 'Suivant',
                    currentText: 'Aujourd\'hui',
                    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                    weekHeader: 'Sem.',
                    dateFormat: 'dd-mm-yy'
                });

                $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
                $( "#dateDebut" ).datepicker();
                $( "#dateFin" ).datepicker();
            
            
            
                $('.simulerCourbes').click(actionSimulerCalage);
                $('div#chargementPage').fadeOut();
            
        }


    /**
    *
    *   Afficher les deux courbes sur les deux graphes
    **/
    function afficherDoubleGraphCalages(courbeTempJsonReel, courbeTempJsonSimule, courbePresJsonReel, courbePresJsonSimule, id)
    {
        var grapheTemperature = "chartContainerTempCalage" + id;  
        var graphePression = "chartContainerPressionCalage" + id;  
        
        var chartTemp = new CanvasJS.Chart(grapheTemperature,
        {
            height: 190,
            width: 430,
            zoomEnabled: true,
            legend:
            {
                verticalAlign : 'top',
                horizontalAlign : 'right',
            }, 
            axisX:
            {      
                valueFormatString: "MM-YYYY" ,
                title: "Time",
                labelAngle: -50,
                labelFontColor: "black",
                gridColor: "#d2d2d2",
                gridThickness: 1    
            },
            axisY: 
            {
                labelFontColor: "black",
                title: "Temperature",
                valueFormatString: "####",
                gridColor: "#d2d2d2",
			    includeZero: false,
                titleFontColor: "#328fe1",
                labelFontColor: "#328fe1",
                lineColor: "#328fe1"
                
            },
            data: [
            {        
                type: "line",
                connectNullData: false,
                legendText: "Real data",
                        showInLegend: true, 
                color: "#328fe1",
                markerSize:3,
                dataPoints:courbeTempJsonReel
            },
            {        
                type: "line",
                connectNullData: false,
                legendText: "Simulated data",
                        showInLegend: true, 
                color: "#e90d3f",
                markerSize:3,
                dataPoints:courbeTempJsonSimule
            }

            ]
        });

        chartTemp.render();
        
        
        
        var chartPression = new CanvasJS.Chart(graphePression,
        {
            height: 190,
            width: 430,
            legend:
            {
                verticalAlign : 'top',
                horizontalAlign : 'right',
            },
            zoomEnabled : true,
            axisX:{      
                valueFormatString: "MM-YYYY" ,
                labelAngle: -50,
                labelFontColor: "black",
                gridColor: "#d2d2d2",
                gridThickness: 1    
            },
            axisY: {
                labelFontColor: "black",
                valueFormatString: "####",
                gridColor: "#d2d2d2"
            },
            data: 
            [
                {        
                    type: "line",
                    connectNullData: false,
                    legendText: "Real data",
                        showInLegend: true, 
                    color: "#328fe1",
                    markerSize:3,
                    dataPoints:courbePresJsonReel
                },
                {        
                    type: "line",
                    connectNullData: false,
                    legendText: "Simulated data",
                        showInLegend: true, 
                    color: "#e90d3f",
                    markerSize:3,
                    dataPoints:courbePresJsonSimule
                }
            ]
        });

        chartPression.render();
    }


   /**
    *
    *   Ajouter un callage
    **/
    function ajoutCalage()
    {
        $.post("/api/addCalage", 
	    JSON.stringify({CavityName :  sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateDebut, DateTo : dateFin, CoefPression : listeDataPression, CoefTemperature : listeDataTemperature, IsChecked : checkBoxCalage}))
	    .done(function(response)
        {
            $('p.erreurChampsCoef').slideUp();
            $('p.erreurDate').slideUp();
            location.reload();
             
            reinitialiserCurseur();
        })
        .fail(function()
        {
            reinitialiserCurseur();
        });
    }


   /**
    *
    *   Modifier un callage
    **/
    function enregistrerCalage()
    {
        $.post("/api/updateCalage", 
	    JSON.stringify({Id : idCalageModifie,SiteName : sessionStorage.getItem("NomSiteActuel") , CavityName :  sessionStorage.getItem("NomCaviteActuel"), DateFrom : dateDebut, DateTo : dateFin, CoefPression : listeDataPression, CoefTemperature : listeDataTemperature, IsChecked : checkBoxCalage}))
	    .done(function(response)
        {
            $('p.erreurChampsCoef').slideUp();
            $('p.erreurDate').slideUp();
            location.reload();
            
            reinitialiserCurseur();
             
        })
        .fail(function()
        {
            reinitialiserCurseur();
        });
    }


    /**
    *
    *   Envoyer ID du calage pour le supprimer
    **/
    function suppressionCalage()
    {
        $.post("/api/deleteCalage",
	    JSON.stringify({Id : idCalageSuppression}))
	    .done(function(response)
        {
            $('.overlay').fadeOut();
            $('.overlay-suppressionCalage').fadeOut();
            
              location.reload();
        })
        .fail(function()
         {
        });
    }
    


    /**
    *
    *   Simuler pression
    **/
    function simulerCalage(dateD, dateF)
    {
        $.post("/api/simulateForCavity", 
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"),SiteName : sessionStorage.getItem("NomSiteActuel"), DateFrom : dateD, DateTo : dateF, CoefPression : listeDataPression, CoefTemperature : listeDataTemperature}))
	    .done(function(response)
        {
            $('.simulerCourbesErreur').slideUp();

            dataTempJsonReel = prepareDataReelCourbes(response.dataTempReel);
            dataTempJsonSimule = prepareDataSimuleCourbes(response.dataTempSimul);
            dataPresJsonReel = prepareDataReelCourbes(response.dataPressionReel);
            dataPresJsonSimule = prepareDataSimuleCourbes(response.dataPressionSimul);        
            
            var infoGraphePression = "";
            infoGraphePression = '<div class="infoGraphe"> <p> <span class="calagePresSd">' + response.standardDeviationPression + '</span></p>  <p>Standard deviation</p> </div><div class="infoGraphe"> <p> <span class="calagePresDevMin">' + response.minDeviationPression + '</span></p><p>Déviation min</p> </div> <div class="infoGraphe"> <p> <span class="calagePresDevMax">' + response.maxDeviationPression + '</span></p> <p>Déviation max</p> </div> <div class="infoGraphe"><p> <span class="calagePresSeq">' + response.sommeEcartsQuadratiquesPression + '</span></p> <p>Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p> <span class="calagePresSde">' + response.moyDeviationPression + '</span></p><p>Déviations moyennes</p> </div>';
                  
            
            var infoGrapheTemp = "";
            infoGrapheTemp = '<div class="infoGraphe"> <p> <span class="calageTempSd">' + response.standardDeviationTemp + '</span></p>  <p>Standard deviation</p> </div><div class="infoGraphe"> <p><span class="calageTempDevMin">' + response.minDeviationTemp + '</span></p><p>Déviation min</p> </div> <div class="infoGraphe"> <p><span class="calageTempDevMax">' + response.maxDeviationTemp + '</span></p> <p>Déviation max</p> </div> <div class="infoGraphe"><p><span class="calageTempSeq">' + response.sommeEcartsQuadratiquesTemp + '</span></p> <p>Somme des écarts quadratiques</p> </div> <div class="infoGraphe">  <p><span class="calageTempSde">' + response.moyDeviationTemp + '</span></p><p>Déviations moyennes</p> </div>';
                  
            $('.infoGraphe').remove();
            $('div[idcalage="' + idCalageSimule  + '"] #graphesGestionCalageLeft #listeInfoGraphe').prepend(infoGraphePression);
            $('div[idcalage="' + idCalageSimule + '"] #graphesGestionCalageRight #listeInfoGraphe').prepend(infoGrapheTemp);
            
            
            //récupérer à quel id afficher la courbe
            if (idCalageSimule == undefined)
            {
                var graphePression = "chartContainerPressionCalage0";
                var grapheTemperature = "chartContainerTempCalage0";
            }
            else
                {
                    var graphePression = "chartContainerPressionCalage" + idCalageSimule;  
                    var grapheTemperature = "chartContainerTempCalage" + idCalageSimule;  
                }
        
            
            
            var chartPression = new CanvasJS.Chart(graphePression,
            {
                zoomEnabled: true, 
                legend:
                {
                    verticalAlign : 'top',
                    horizontalAlign : 'right',
                },
                axisX:{      
                    valueFormatString: "MM-YYYY" ,
                    interval: 5,
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: {
                    labelFontColor: "black",
                    valueFormatString: "####",
                    gridColor: "#d2d2d2"
                },
                data: 
                [
                    {        
                        type: "line",
                        connectNullData: false,
                        color: "#328fe1",
                        legendText: "Real data",
                        showInLegend: true, 
                        markerSize:3,
                        dataPoints:dataPresJsonReel
                    },
                    {        
                        type: "line",
                        connectNullData: false,
                        color: "#e90d3f",
                        legendText: "Simulated data",
                        showInLegend: true, 
                        markerSize:3,
                        dataPoints:dataPresJsonSimule
                    }
                ]
            });

            chartPression.render();
            
            
            var chartTemp = new CanvasJS.Chart(grapheTemperature,
            {
                zoomEnabled: true,
                legend:
                {
                    verticalAlign : 'top',
                    horizontalAlign : 'right',
                }, 
                axisX:
                {      
                    valueFormatString: "MM-YYYY" ,
                    title: "Time",
                    labelAngle: -50,
                    labelFontColor: "black",
                    gridColor: "#d2d2d2",
                    gridThickness: 1    
                },
                axisY: 
                {
                    labelFontColor: "black",
                    title: "Temperature",
                    valueFormatString: "####",
                    gridColor: "#d2d2d2",
			        includeZero: false,
                    titleFontColor: "#328fe1",
                    labelFontColor: "#328fe1",
                    lineColor: "#328fe1"
                },
                data: [
                {        
                    type: "line",
                    connectNullData: false,
                    legendText: "Real data",
                        showInLegend: true, 
                    color: "#328fe1",
                    markerSize:3,
                    dataPoints:dataTempJsonReel
                },
                {        
                    type: "line",
                    connectNullData: false,
                    legendText: "Simulated data",
                        showInLegend: true, 
                    color: "#e90d3f",
                    markerSize:3,
                    dataPoints:dataTempJsonSimule
                }
                ]
            });

            chartTemp.render();
            
            
            reinitialiserCurseur();
            $('div#chargementPage').fadeOut();
            
        })
        .fail(function()
        {
            
            $('.simulerCourbesErreur').slideDown();
            
            
            reinitialiserCurseur();
            $('div#chargementPage').fadeOut();
            
        });
    }
    




/******************
**
**  Export  *******
**
******************/


    /**
    *
    *   Envoyer mot de passe pour définir nom du zip
    **/
    function exportSite(Password)
    {
        $.post("/api/exportSite",
	    JSON.stringify({SiteName : sessionStorage.getItem("NomSiteActuel"), Password:Password}))
	    .done(function(response)
        {
            $(".overlay").fadeOut();
            $(".overlay-exportSite").fadeOut(function()
            {
                $('div#chargementPage').fadeOut();
                
                //Supprimer l'overlay créé précédament
                $(".overlay-exportSite").remove();

            });
        })
        .fail(function()
        {
        });
    }


    /**
    *
    *   Envoyer mot de passe pour définir nom du zip
    **/
    function exportExploitant()
    {
        $.post("/api/exportExploitant",
	    JSON.stringify({ExploitantName : sessionStorage.getItem("NomExploitantExport")}))
	    .done(function(response)
        {
            $(".overlay").fadeOut();
            $(".overlay-exportExploitant").fadeOut(function()
            {
                //Supprimer l'overlay créé précédament
                $(".overlay-exportExploitant").remove();
            
            });
        })
        .fail(function()
        {
        });
    }


    /**
    *
    *   Envoyer mot de passe pour définir nom du zip
    **/
    function exportCavite(Password)
    {
        $.post("/api/exportCavity",
	    JSON.stringify({CavityName : sessionStorage.getItem("NomCaviteActuel"), SiteName : sessionStorage.getItem("NomSiteActuel"),Password: Password}))
	    .done(function(response)
        {            
            $(".overlay").fadeOut();
            $(".overlay-exportCavite").fadeOut(function()
            {
                $('div#chargementPage').fadeOut();
                
                //Supprimer l'overlay créé précédament
                $(".overlay-exportCavite").remove();

            });
            
        })
        .fail(function()
        {
        });
    }














