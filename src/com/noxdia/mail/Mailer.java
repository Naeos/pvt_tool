package com.noxdia.mail;

import com.noxdia.logger.SaspLogger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;

public class Mailer {

    private final SaspLogger saspLogger = SaspLogger.getInstance();

    /**
     * Envoie un mail au destinataire "to" , avec comme sujet "subject" et comme corps "bodyText"
     * Et la liste des nom de fichiers à envoyer "filesNames"
     * Ces fichiers doivent se situer à la racine de l'executable, ou alors indiquer leurs chemin
     *
     * @param to         Adresse du destinataire
     * @param subject    Sujet du mail
     * @param bodyText   Corps du mail
     * @param filesNames Liste de nom des fichiers à envoyer
     * @return True si envoie reussie, false sinon
     */
    public boolean sendMailTo(String to, String subject, String bodyText, List<String> filesNames) {
        Objects.requireNonNull(filesNames);
        Objects.requireNonNull(to);
        Objects.requireNonNull(subject);
        Objects.requireNonNull(bodyText);
        saspLogger.log(Level.INFO, "In send mail to");

        Properties props = getProperties();

        // Identifiants du mail
        String userName = "sasp@horisis.com";
        String password = "PVT!!Horisis2017";

        String from = "sasp@horisis.com";

        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userName, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(subject);
            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setText(bodyText);

            Multipart multipart = new MimeMultipart();

            multipart.addBodyPart(messageBodyPart);

            if (!filesNames.isEmpty()) {
                for (String fileName : filesNames) {
                    if (fileName != null && !fileName.isEmpty()) {
                        messageBodyPart = new MimeBodyPart();
                        DataSource source = new FileDataSource(fileName);
                        messageBodyPart.setDataHandler(new DataHandler(source));
                        String finalFileName = cleanFileNameString(fileName);
                        messageBodyPart.setFileName(finalFileName);
                        multipart.addBodyPart(messageBodyPart);
                    }
                }
            }
            // Send the complete message parts
            message.setContent(multipart);
            Transport.send(message);
        } catch (MessagingException e) {
            saspLogger.log(Level.WARNING, e.getMessage());
            return false;
        }
        saspLogger.log(Level.INFO, "Send mail to finished with no error");
        return true;
    }

    /**
     * Nettoie le nom du fichier quand il est envoyé dans le mail
     *
     * @param fileName String a nettoyer
     * @return String nettoyé
     */
    private String cleanFileNameString(String fileName) {
        return fileName.replace(".\\export\\", "")
                .replace("./export/", "")
                .replace("./", "")
                .replace(".\\", "");
    }

    /**
     * @return Les propriétés fixé pour le mail
     */
    private Properties getProperties() {
        Properties props = new Properties();

        String host = "ssl0.ovh.net"; // Changer une fois les info sur le mail connu
        String port = "587"; // Changer une fois les info sur le mail connu

        props.put("mail.smtp.host", host);
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", port);

        return props;
    }
}
