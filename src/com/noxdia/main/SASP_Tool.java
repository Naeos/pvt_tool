package com.noxdia.main;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.web.browser.Browser;
import com.noxdia.web.server.Server;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;


public class SASP_Tool extends Application {
    private Scene scene;
    private Browser browser;

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        try {
            Database db = new Database(Paths.get("./db"), "database");

            // Evite de garder les fichier en cache le temps du dev
            Properties properties = System.getProperties();
            properties.setProperty("vertx.disableFileCaching", "true");
            properties.setProperty("vertx.disableFileCPResolvingg", "true");

            // Assure que tous sera encodé en utf-8
            // La belle reflection
            System.setProperty("file.encoding", "UTF-8");
            Field charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);

            VertxOptions options = new VertxOptions();
            // Evite que le blockedThreadChecker de vertx se  declenche
            options.setBlockedThreadCheckInterval(1000 * 60 * 60);

            Server server = new Server(db);
            Vertx vertx = Vertx.vertx(options);

            vertx.deployVerticle(server);
            launch(args);
            vertx.close();
        } catch (Exception e) {
            SaspLogger.getInstance().log(Level.WARNING, e.getMessage());
        }

    }

    @Override
    public void start(Stage stage) {
        // create the scene
        stage.setTitle("SASP Tool");
        browser = new Browser();
        scene = new Scene(browser, 1200, 720, Color.web("#666970"));

        stage.setMinWidth(1219);
        stage.setMaxWidth(1219);
        stage.setMinHeight(550);
        stage.setMaxHeight(758);

        Image image = null;
        try {
            // Chemin vers le logo de l'appli
            image = new Image("file:./webroot/asset/img/logo/pvt_logo_noir.png");
        } catch (Exception e) {
            //
        }
        // Ajout d'une icone pour l'appli
        stage.getIcons().add(image);
        stage.setScene(scene);

        stage.show();
    }
}


