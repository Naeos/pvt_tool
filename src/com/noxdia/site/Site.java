package com.noxdia.site;

import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.gaz.CompositionGaz;
import com.noxdia.tools.Tools;
import com.noxdia.zip.Zipper;
import io.vertx.core.json.JsonObject;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

public class Site {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();
    private final LocalDate dateCreation;
    private final Charset charset = Charset.forName("ISO-8859-1");
    private final Database database;
    private final List<CompositionGaz> listCompositionGaz = new ArrayList<>();
    private final List<Cavity> listCavity = new ArrayList<>();
    private final Exploitant exploitant;

    private String imageSiteServer;
    private String imageSiteClient;
    private int id = -1;
    private String name;
    private String adress;
    private String country;
    private String town;
    private String passwordHash;

    private boolean isImageDefault = true;

    Site(String name, String adress, String country, String town, String passwordHash, LocalDate dateCreation, String imageSiteClient, String imageSiteServer, Database database, Exploitant exploitant, boolean isImageDefault) {
        this.name = Objects.requireNonNull(name);
        this.adress = Objects.requireNonNull(adress);
        this.country = Objects.requireNonNull(country);
        this.town = Objects.requireNonNull(town);
        this.passwordHash = Objects.requireNonNull(passwordHash);
        this.dateCreation = Objects.requireNonNull(dateCreation);
        this.imageSiteClient = Objects.requireNonNull(imageSiteClient);
        this.imageSiteServer = Objects.requireNonNull(imageSiteServer);
        this.database = Objects.requireNonNull(database);
        this.exploitant = Objects.requireNonNull(exploitant);
        this.isImageDefault = isImageDefault;
    }

    @Override
    public String toString() {
        return "Site{" +
                "dateCreation=" + dateCreation +
                ", imageSiteServer='" + imageSiteServer + '\'' +
                ", imageSiteClient='" + imageSiteClient + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                ", country='" + country + '\'' +
                ", town='" + town + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", database=" + database +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Site site = (Site) o;

        if (!dateCreation.equals(site.dateCreation)) return false;
        if (!name.equals(site.name)) return false;
        if (!adress.equals(site.adress)) return false;
        if (!country.equals(site.country)) return false;
        return town.equals(site.town);
    }

    @Override
    public int hashCode() {
        int result = dateCreation.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + adress.hashCode();
        result = 31 * result + country.hashCode();
        result = 31 * result + town.hashCode();
        return result;
    }

    /**
     * Indique si l'image utilisé est celle par default ou non
     *
     * @return True si c'est l'image par defaut, False sinon
     */
    public boolean isImageDefault() {
        return isImageDefault;
    }

    /**
     * @param passwordHash the password hash you want to check -> you need to hash the password before giving it to the function
     * @return true if the input password is correct, false otherwise
     */
    public boolean checkPassword(String passwordHash) {
        return passwordHash.equals(this.passwordHash);
    }

    /**
     * @param passwordHash new password hash to store in database
     */
    public void updatePassword(String passwordHash) {
        database.updateSitePassword(this, passwordHash);
        this.passwordHash = passwordHash;
    }

    /**
     * @return La temperature critique Total du Gaz utilisé dans le site
     */
    public double calculTempCritiqueTotal() {
        return listCompositionGaz.stream()
                .map(g -> (g.getProportion() / 100.0) * g.getGaz().getCriticalTemperature())
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    /**
     * @return La temperature critique Total du Gaz utilisé dans le site
     */
    public double calculPressionCritiqueTotal() {
        return listCompositionGaz.stream()
                .map(g -> (g.getProportion() / 100.0) * g.getGaz().getCriticalPression())
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    /**
     * @return La densiteTotal du Gaz utilisé dans le site
     */
    public double calculDensiteTotal() {
        if (listCompositionGaz.isEmpty()) {
            listCompositionGaz.addAll(database.getListCompoGazForSite(this));
            if (listCompositionGaz.isEmpty()) {
                return -1;
            }
        }
        return calculMasseMolaireTotal() / 1000 / (22.4 / 1000.) / 1.20531;
//        return listCompositionGaz
//                .stream()
//                .map(compositionGaz -> (compositionGaz.getProportion() / 100.0) * compositionGaz.getGaz().getDensite())
//                .mapToDouble(Double::doubleValue)
//                .sum();
    }

    /**
     * @return La masse molaire Total du Gaz utilisé dans le site
     */
    public double calculMasseMolaireTotal() {
        if (listCompositionGaz.isEmpty()) {
            listCompositionGaz.addAll(database.getListCompoGazForSite(this));
            if (listCompositionGaz.isEmpty()) {
                return -1;
            }
        }
        return calculMasseMolaireTotalAnnex();
    }

    /**
     * @return La masse molaire Total du Gaz utilisé dans le site
     */
    private double calculMasseMolaireTotalAnnex() {
        return listCompositionGaz.stream()
                .map(g -> (g.getProportion() / 100.0) * g.getGaz().getMasseMolaire())
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    /**
     * @return La somme des proportions du Gaz utilisé dans le site
     */
    private double calculTotalPourcent() {
        return listCompositionGaz.stream()
                .map(CompositionGaz::getProportion)
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    /**
     * Ajoute une composition de gaz à la liste, ainsi que dans la base de données
     **/
    public void addCompositionGaz(CompositionGaz compositionGaz) {
        Objects.requireNonNull(compositionGaz);
        if (!listCompositionGaz.contains(compositionGaz)) {
            database.insertCompoGaz(compositionGaz);
            listCompositionGaz.add(compositionGaz);
        } else {
            saspLogger.log(Level.WARNING, "Gaz " + compositionGaz.getGaz().getName() + " already in list");
        }
    }

    /**
     * Supprime la composition Gaz "compositionGaz" de la liste ainsi que la base de données
     *
     * @param compositionGaz CompositionGaz à supprimer
     */
    public void deleteCompositionGaz(CompositionGaz compositionGaz) {
        Objects.requireNonNull(compositionGaz);
        if (listCompositionGaz.contains(compositionGaz)) {
            database.deleteCompoGaz(compositionGaz);
            listCompositionGaz.remove(compositionGaz);
        } else {
            saspLogger.log(Level.WARNING, "Gaz " + compositionGaz.getGaz().getName() + " not in list");
        }
    }

    /**
     * Ajoute la cavite à la liste, ainsi que dans la base de données
     *
     * @param cavity Cavite à ajouter
     */
    public boolean addCavity(Cavity cavity) {
        Objects.requireNonNull(cavity);
        if (!listCavity.contains(cavity)) {
            cavity.insertInDb();
            listCavity.add(cavity);
            return true;
        } else {
            saspLogger.log(Level.WARNING, "Cavity \'" + cavity.getName() + "\' already exists");
            return false;
        }
    }

    /**
     * Supprime la cavite de la liste, ainsi que de la base de données
     *
     * @param cavity Cavite à supprimer
     */
    public void removeCavity(Cavity cavity) {
        Objects.requireNonNull(cavity);
        if (listCavity.contains(cavity)) {
            database.deleteCavity(cavity);
            listCavity.remove(cavity);
        } else {
            saspLogger.log(Level.WARNING, "Cavity \'" + cavity.getName() + "\' is not in list");
        }
    }

    /**
     * Met à jour le site avec les valeurs du site "updated"
     *
     * @param updated Le site temporaire à partir du quel mettre les valeurs à jours
     */
    public void update(Site updated) {
        Objects.requireNonNull(updated);
        if (!this.exploitant.equals(updated.exploitant)) {
            saspLogger.log(Level.WARNING, "Cannot update a site with a different exploitant");
            return;
        }
        if (!this.name.equals(updated.name)) {
            this.name = updated.name;
        }
        if (!this.adress.equals(updated.adress)) {
            this.adress = updated.adress;
        }
        if (!this.country.equals(updated.country)) {
            this.country = updated.country;
        }
        if (!this.town.equals(updated.town)) {
            this.town = updated.town;
        }
        this.imageSiteClient = updated.imageSiteClient;
        this.imageSiteServer = updated.imageSiteServer;
        this.isImageDefault = updated.isImageDefault;

        database.updateSite(this);
    }


    /**
     * Exporte le site sous forme de fichier zip . Le format est décrit dans le manuel utilisateur
     *
     * @param password Mot de passe du fichier zip
     * @return Le nom du fichier créé
     */
    public String exportToZip(String password) {
        String exportFolderRoot = "./EXPORT_SITE_" + name;
        Path exportFolderPath = Paths.get(exportFolderRoot);
        tools.createFolder(exportFolderPath);

        writeCompGazCSVFile(exportFolderRoot);
        writeInfoCSVFile(exportFolderRoot);
        writeImageFile(exportFolderRoot);

        String cavityFolderString = exportFolderRoot + "/cavites";
        Path cavityFolder = Paths.get(cavityFolderString);
        tools.createFolder(cavityFolder);

        if (listCavity.isEmpty()) {
            List<Cavity> cavityList = database.getListCavityForSite(this);
            cavityList.forEach(cavity -> cavity.export(cavityFolderString));
        } else {
            listCavity.forEach(cavity -> cavity.export(cavityFolderString));
        }

        // Creation du fichier zip avec le dossier exporter
        Zipper zipper = new Zipper();

        // Supprimer le fichier zip, s'il existe, avant de le recreer
        String zipFileName = "EXPORT_SITE_" + name + ".zip";
        Path zipFilePath = Paths.get(zipFileName);

        tools.deleteFileIfExists(zipFilePath);
        // Export le dossier sous le format zip
        zipper.packWithPassword(zipFileName, exportFolderRoot, password);

        Path folderExportPath = Paths.get("./export");

        tools.createFolder(folderExportPath);

        Path newLocation = Paths.get("./export/" + zipFileName);
        tools.moveFileToLocation(zipFilePath, newLocation);

        // Supprimer le dossier creer une fois qu'il a été zippé
        tools.deleteDirectoryRecurs(exportFolderPath);
        return newLocation.toString();
    }

    /**
     * Ecrit le fichier image dans le dossier donnée en paramètre
     *
     * @param exportFolderRoot Dossier ou ecrire l'image
     */
    private void writeImageFile(String exportFolderRoot) {
        Path source = Paths.get(imageSiteServer);
        if (!Files.exists(source)) {
            saspLogger.log(Level.WARNING, "Source \"" + imageSiteServer + "\" does not exist");
            return;
        }
        Path sourceFileName = source.getFileName();
        if (null == sourceFileName) {
            saspLogger.log(Level.WARNING, "Source \"" + imageSiteServer + "\" does not exist");
            return;
        }

        String imageSiteFileName = sourceFileName.toString();
        Path dirImage = Paths.get(exportFolderRoot + "/images/");
        tools.createFolder(dirImage);
        Path target = Paths.get(exportFolderRoot + "/images/" + imageSiteFileName);
        tools.copySourceToTargetIfnotExists(source, target);
    }


    /**
     * @param exportFolderRoot Dossier ou ecrire le fichier
     */
    private void writeCompGazCSVFile(String exportFolderRoot) {
        tools.writeFile(toCSVCompoGaz().getBytes(charset), exportFolderRoot + "/compo_gaz.csv");
    }

    /**
     * @param exportFolderRoot Dossier ou ecrire le fichier
     */
    private void writeInfoCSVFile(String exportFolderRoot) {
        tools.writeFile(toCSVInfo(exportFolderRoot).getBytes(charset), exportFolderRoot + "/info.csv");
    }

    /**
     * Ecrit la string csv des informations du site
     *
     * @param exportFolder Dossier ou ecrire le fichier
     * @return La string infoCSV du site
     */
    private String toCSVInfo(String exportFolder) {
        String sep = ";";
        String expName = "Pas d'exploitant";
        if (null != exploitant) {
            expName = exploitant.getName();
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Path imageSite = Paths.get(imageSiteClient);
        Path fileName = imageSite.getFileName();
        if (null == fileName) {
            return "";
        }
        String imageSiteFileName = "/images/" + fileName.toString();
        Path imageExportFolder = Paths.get(exportFolder + "/images");
        tools.createFolder(imageExportFolder);

        return "Nom du site" + sep + name + "\n" +
                "Nom exploitant" + sep + expName + "\n" +
                "Adresse" + sep + adress + "\n" +
                "Ville" + sep + town + "\n" +
                "Pays" + sep + country + "\n" +
                "Cree le" + sep + dtf.format(dateCreation) + "\n" +
                "Password Hash" + sep + passwordHash + "\n" +
                "Image" + sep + imageSiteFileName;
    }

    /**
     * @return La string "compoGaz" au format CSV
     */
    private String toCSVCompoGaz() {
        String sep = ";";
        String ligne1 = sep + "Mol%" + sep + "Formule" + sep + "Masse Molaire(g/mol)" + sep + "Pression Critique(MPa)" + sep + "Temperature Critique(K)" + sep + "Densite\n";
        String ligne2 = "Total" + sep + calculTotalPourcent() + sep + sep + calculMasseMolaireTotal() + sep + calculPressionCritiqueTotal() + sep + calculTempCritiqueTotal() + sep + calculDensiteTotal() + "\n";
        StringBuilder sb = new StringBuilder();
        sb.append(ligne1).append(ligne2);
        listCompositionGaz.forEach(gaz -> sb.append(gaz.toCSV()).append("\n"));
        return sb.toString();
    }

    /**
     * Permet de supprimer l'image d'un site du dossier ou elle est stocké
     */
    public void deleteImage() {
        if (!isImageDefault) {
            Path imagePath = Paths.get(imageSiteServer);
            tools.deleteFileIfExists(imagePath);
        }
    }

    // Getters

    public String getAdress() {
        return adress;
    }

    public String getTown() {
        return town;
    }

    public String getCountry() {
        return country;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<CompositionGaz> getListCompoGaz() {
        return Collections.unmodifiableList(listCompositionGaz);
    }

    public List<Cavity> getListCavity() {
        return Collections.unmodifiableList(listCavity);
    }

    public String getName() {
        return name;
    }

    public LocalDate getDate() {
        return dateCreation;
    }

    public String getImageSiteClient() {
        return imageSiteClient;
    }

    public String getImageSiteServer() {
        return imageSiteServer;
    }

    public Exploitant getExploitant() {
        return exploitant;
    }

    /**
     * Permet de recuperer les informations du site au format "JsonObject" implémenté par vertx
     *
     * @return Le site au format JsonObject
     */
    public JsonObject asJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("Name", name);
        jsonObject.put("Adress", adress);
        jsonObject.put("Town", town);
        jsonObject.put("Country", country);
        return jsonObject;
    }

}
