package com.noxdia.site;

import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;

import java.time.LocalDate;

public class SiteFactory {

    public static Site createSite(String name, String adress, String country, String town, String password, LocalDate dateCreation, String imageSiteClient, String imageSiteServer, Database database, Exploitant exploitant, boolean isImageDefault) {
        return new Site(name, adress, country, town, password, dateCreation, imageSiteClient, imageSiteServer, database, exploitant, isImageDefault);
    }

}
