package com.noxdia.site.gaz;

import com.noxdia.site.Site;

public class GazFactory {

    public static Gaz createGaz(String name, String formule, double masseMolaire, double criticalTemperature, double criticalPression, double densite) {
        return new Gaz(name, formule, masseMolaire, criticalTemperature, criticalPression, densite);
    }

    public static CompositionGaz createCompositionGaz(Site site, Gaz gaz, double proportion) {
        return new CompositionGaz(site, gaz, proportion);
    }
}
