package com.noxdia.site.gaz;

import com.noxdia.site.Site;
import com.noxdia.tools.Tools;

import java.util.Objects;


public class CompositionGaz {

    private final Site site;
    private final Gaz gaz;
    private final double proportion;
    private int id;

    CompositionGaz(Site site, Gaz gaz, double proportion) {
        this.site = Objects.requireNonNull(site);
        this.gaz = Objects.requireNonNull(gaz);
        this.proportion = Tools.getInstance().checkNotNegative(proportion);
    }

    @Override
    public String toString() {
        return "CompositionGaz{" +
                "site=" + site.getName() +
                ", gaz=" + gaz +
                ", proportion=" + proportion +
                '}';
    }

    public String toCSV() {
        String sep = ";";
        return gaz.getName() + sep +
                proportion + sep +
                gaz.getFormule() + sep +
                gaz.getMasseMolaire() + sep +
                gaz.getCriticalPression() + sep +
                gaz.getCriticalTemperature() + sep +
                gaz.getDensite();
    }

    public double getProportion() {
        return proportion;
    }

    public Gaz getGaz() {
        return gaz;
    }

    public Site getSite() {
        return site;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
