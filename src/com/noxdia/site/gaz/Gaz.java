package com.noxdia.site.gaz;


import com.noxdia.tools.Tools;

import java.util.Objects;

public class Gaz {

    private final Tools tools = Tools.getInstance();
    private int id;
    private String name;
    private String formule;
    private double masseMolaire;
    private double criticalPression;
    private double criticalTemperature;
    private double densite;

    Gaz(String name, String formule, double masseMolaire, double criticalTemperature, double criticalPression, double densite) {
        this.name = Objects.requireNonNull(name);
        this.formule = Objects.requireNonNull(formule);
        this.masseMolaire = tools.checkNotNegative(masseMolaire);
        this.criticalPression = tools.checkNotNegative(criticalPression);
        this.criticalTemperature = criticalTemperature;
        this.densite = densite;
    }

    @Override
    public String toString() {
        return "Gaz{" +
                "name='" + name + '\'' +
                ", formule='" + formule + '\'' +
                ", masseMolaire=" + masseMolaire +
                ", criticalPression=" + criticalPression +
                ", criticalTemperature=" + criticalTemperature +
                ", densite=" + densite +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getFormule() {
        return formule;
    }

    public double getMasseMolaire() {
        return masseMolaire;
    }

    public double getCriticalPression() {
        return criticalPression;
    }

    public double getCriticalTemperature() {
        return criticalTemperature;
    }

    public double getDensite() {
        return densite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void update(Gaz newGaz) {
        Objects.requireNonNull(newGaz);
        this.name = Objects.requireNonNull(newGaz.name);
        this.formule = Objects.requireNonNull(newGaz.formule);
        this.masseMolaire = tools.checkNotNegative(newGaz.masseMolaire);
        this.criticalPression = tools.checkNotNegative(newGaz.criticalPression);
        this.criticalTemperature = newGaz.criticalTemperature;
        this.densite = newGaz.densite;
    }
}
