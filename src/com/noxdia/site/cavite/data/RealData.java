package com.noxdia.site.cavite.data;

import com.noxdia.site.cavite.Cavity;

import java.time.LocalDate;
import java.util.Objects;

public class RealData {

    private final LocalDate dateInsertion;
    private final Cavity cavity;
    private final double volume;
    private final double debit;
    private final LocalDate dateLastModif;
    private final String logModif;
    private double temperatureWelHead;
    private double pressionWelHead;


    RealData(LocalDate dateInsertion, Cavity cavity, double temperatureWelHead, double pressionWelHead, double volume, double debit, LocalDate dateLastModif, String logModif) {
        this.dateInsertion = Objects.requireNonNull(dateInsertion);
        this.cavity = Objects.requireNonNull(cavity);
        this.temperatureWelHead = temperatureWelHead;
        this.pressionWelHead = pressionWelHead;
        this.volume = volume;
        this.debit = debit;
        this.dateLastModif = Objects.requireNonNull(dateLastModif);
        this.logModif = Objects.requireNonNull(logModif);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RealData realData = (RealData) o;

        if (Double.compare(realData.volume, volume) != 0) return false;
        if (Double.compare(realData.debit, debit) != 0) return false;
        if (Double.compare(realData.temperatureWelHead, temperatureWelHead) != 0) return false;
        if (Double.compare(realData.pressionWelHead, pressionWelHead) != 0) return false;
        if (!dateInsertion.equals(realData.dateInsertion)) return false;
        return cavity.equals(realData.cavity);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = dateInsertion.hashCode();
        result = 31 * result + cavity.hashCode();
        temp = Double.doubleToLongBits(volume);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(temperatureWelHead);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(pressionWelHead);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        String sep = ";";
        return dateInsertion + sep + temperatureWelHead + sep + pressionWelHead + sep + volume + sep + debit;
    }

    public String toCSV() {
        String sep = ";";
        return temperatureWelHead + sep + pressionWelHead + sep + volume + sep + debit;
    }

    // Getters

    public LocalDate getDateInsertion() {
        return dateInsertion;
    }

    public Cavity getCavity() {
        return cavity;
    }

    public double getTemperatureWelHead() {
        return temperatureWelHead;
    }

    public double getPressionWelHead() {
        return pressionWelHead;
    }

    public double getVolume() {
        return volume;
    }

    public double getDebit() {
        return debit;
    }

    public LocalDate getDateLastModif() {
        return dateLastModif;
    }

    public String getLogLastModif() {
        return logModif;
    }


}
