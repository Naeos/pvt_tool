package com.noxdia.site.cavite.data;

import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;

import java.time.LocalDate;

public class DataFactory {

    public static RealData createRealData(LocalDate dateInsertion, Cavity cavity, double twelHead, double pwelHead, double volume, double debit, LocalDate dateLastModif, String logModif) {
        return new RealData(dateInsertion, cavity, twelHead, pwelHead, volume, debit, dateLastModif, logModif);
    }

    public static SimulatedData createSimulatedData(LocalDate dateInsertion, Cavity cavity, double twelHead, double pwelHead, double volume, double debit, Calage calage) {
        return new SimulatedData(dateInsertion, cavity, twelHead, pwelHead, volume, debit, calage);
    }
}
