package com.noxdia.site.cavite.data;

import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;

import java.time.LocalDate;
import java.util.Objects;

public class SimulatedData {

    private final LocalDate dateInsertion;
    private final Cavity cavity;
    private final Calage calage;

    private final double temperatureWelHead;
    private final double pressionWelHead;
    private final double volume;
    private final double debit;

    SimulatedData(LocalDate dateInsertion, Cavity cavity, double temperatureWelHead, double pressionWelHead, double volume, double debit, Calage calage) {
        this.cavity = Objects.requireNonNull(cavity);
        this.dateInsertion = Objects.requireNonNull(dateInsertion);
        this.temperatureWelHead = temperatureWelHead;
        this.pressionWelHead = pressionWelHead;
        this.volume = volume;
        this.debit = debit;
        this.calage = Objects.requireNonNull(calage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimulatedData that = (SimulatedData) o;

        if (Double.compare(that.temperatureWelHead, temperatureWelHead) != 0) return false;
        if (Double.compare(that.pressionWelHead, pressionWelHead) != 0) return false;
        if (Double.compare(that.volume, volume) != 0) return false;
        if (Double.compare(that.debit, debit) != 0) return false;
        if (!dateInsertion.equals(that.dateInsertion)) return false;
        if (!cavity.equals(that.cavity)) return false;
        return calage.equals(that.calage);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = dateInsertion.hashCode();
        result = 31 * result + cavity.hashCode();
        result = 31 * result + calage.hashCode();
        temp = Double.doubleToLongBits(temperatureWelHead);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(pressionWelHead);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(volume);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "SimulatedData{" +
                "dateInsertion=" + dateInsertion +
                ", calage=" + calage +
                ", temperatureWelHead=" + temperatureWelHead +
                ", pressionWelHead=" + pressionWelHead +
                ", volume=" + volume +
                ", debit=" + debit +
                '}';
    }

    public String toCSV() {
        String sep = ";";
        return temperatureWelHead + sep + pressionWelHead;
    }

    // Getters

    public LocalDate getDateInsertion() {
        return dateInsertion;
    }

    public Cavity getCavity() {
        return cavity;
    }

    public double getTemperatureWelHead() {
        return temperatureWelHead;
    }

    public double getPressionWelHead() {
        return pressionWelHead;
    }

    public double getVolume() {
        return volume;
    }

    public double getDebit() {
        return debit;
    }

    public Calage getCalage() {
        return calage;
    }


}
