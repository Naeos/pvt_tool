package com.noxdia.site.cavite;

import com.noxdia.database.Database;
import com.noxdia.site.Site;

import java.time.LocalDate;

public class CavityFactory {

    public static Cavity createCavity(Site site, String name, double height, double diameter, double depth, double volume, LocalDate dateCreation, Database database) {
        return new Cavity(site, name, height, diameter, depth, volume, dateCreation,database);
    }
}
