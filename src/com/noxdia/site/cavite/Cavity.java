package com.noxdia.site.cavite;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.cavite.data.SimulatedData;
import com.noxdia.tools.Tools;
import com.noxdia.zip.Zipper;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;


public class Cavity {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Site site;
    private final LocalDate dateCreation;
    private final Map<LocalDate, Map<RealData, SimulatedData>> dataMap = new TreeMap<>();
    private final Database database;
    private final Charset charset = Charset.forName("ISO-8859-1");
    private final Tools tools = Tools.getInstance();
    private String name;
    private double hauteurTubage;
    private double diametreTubage;
    private double depth;
    private double volume;
    private int id;

    Cavity(Site site, String name, double hauteurTubage, double diametreTubage, double depth, double volume, LocalDate dateCreation, Database database) {
        this.site = Objects.requireNonNull(site);
        this.name = Objects.requireNonNull(name);
        this.hauteurTubage = tools.checkNotNegative(hauteurTubage);
        this.diametreTubage = tools.checkNotNegative(diametreTubage);
        this.depth = tools.checkNotNegative(depth);
        this.volume = tools.checkNotNegative(volume);
        this.dateCreation = Objects.requireNonNull(dateCreation);
        this.database = Objects.requireNonNull(database);
    }

    @Override
    public String toString() {
        return "Cavity{" +
                "dateCreation=" + dateCreation +
                ", name='" + name + '\'' +
                ", hauteurTubage=" + hauteurTubage +
                ", diametreTubage=" + diametreTubage +
                ", depth=" + depth +
                ", volume=" + volume +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cavity cavity = (Cavity) o;

        if (Double.compare(cavity.hauteurTubage, hauteurTubage) != 0) return false;
        if (Double.compare(cavity.diametreTubage, diametreTubage) != 0) return false;
        if (Double.compare(cavity.depth, depth) != 0) return false;
        if (Double.compare(cavity.volume, volume) != 0) return false;
        if (!site.equals(cavity.site)) return false;
        if (!dateCreation.equals(cavity.dateCreation)) return false;
        return name.equals(cavity.name);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = site.hashCode();
        result = 31 * result + dateCreation.hashCode();
        result = 31 * result + name.hashCode();
        temp = Double.doubleToLongBits(hauteurTubage);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(diametreTubage);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(depth);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(volume);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /**
     * @return La string info de cavity au format CSV
     */
    private String toInfoCSV() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String sep = ";";
        String exp = "Pas d'exploitant";
        if (null != site.getExploitant()) {
            exp = site.getExploitant().getName();
        }
        return "Nom de la Cavite" + sep + name + "\n" +
                "Nom du site" + sep + site.getName() + "\n" +
                "Nom exploitant" + sep + exp + "\n" +
                "Hauteur Tube" + sep + hauteurTubage + "\n" +
                "Diametre Tube" + sep + diametreTubage + "\n" +
                "Profondeur" + sep + depth + "\n" +
                "Volume Cavite" + sep + volume + "\n" +
                "Cree le" + sep + dtf.format(dateCreation) + "\n";
    }

    /**
     * Verifie si le nom donné est argument est le même que celui de la cavity
     *
     * @param nameToTest Nom a tester
     * @return True si egaux, False sinon
     */
    public boolean checkName(String nameToTest) {
        return name.equals(nameToTest);
    }

    /**
     * Met à jour la cavity avec les informations d'une autre cavity
     *
     * @param updated La cavity a partir de laquelle mettre a jour
     */
    public void update(Cavity updated) {
        Objects.requireNonNull(updated);
        this.name = updated.name;
        this.hauteurTubage = updated.hauteurTubage;
        this.diametreTubage = updated.diametreTubage;
        this.depth = updated.depth;
        this.volume = updated.volume;
        database.updateCavity(this);
    }

    /**
     * Ajoute un realData dans la map de l'objet
     * S'il y a deja une donnée à la date d'insertion du realData, il ne sera pas ajouté
     *
     * @param realData Le realData à ajouter
     */
    public void addRealData(RealData realData) {
        Objects.requireNonNull(realData);
        LocalDate date = realData.getDateInsertion();
        if (dataMap.containsKey(date)) {
            saspLogger.log(Level.WARNING, "There is already some data at current date : " + date);
            return;
        }
        database.insertRealData(realData);
        Map<RealData, SimulatedData> tmpMap = new HashMap<>();
        tmpMap.put(realData, null);
        dataMap.put(date, tmpMap);
    }

    /**
     * Supprime un realData de la map de l'objet
     * S'il la date d'insertion du realData n'est pas connu par la map , on ne pas le supprimer
     *
     * @param realData Le realData à supprimer
     */
    public void removeRealData(RealData realData) {
        Objects.requireNonNull(realData);
        LocalDate date = realData.getDateInsertion();
        if (!dataMap.containsKey(date)) {
//                saspLogger.log(Level.WARNING, "There is not data at date : " + date);
            return;
        }
        database.deleteRealData(realData);
        dataMap.remove(date);
    }

    /**
     * Ajout de simulated Data dans la map
     *
     * @param simulatedData Simulated data à ajouter dans la map
     */
    private void addSimulatedData(SimulatedData simulatedData) {
        Objects.requireNonNull(simulatedData);
        LocalDate date = simulatedData.getDateInsertion();
        if (!dataMap.containsKey(date)) {
//            saspLogger.log(Level.WARNING, "Can't add simulated data, no real data inserted");
            return;
        }
        Map<RealData, SimulatedData> tmpMap = dataMap.get(date);
        RealData realData = (RealData) tmpMap.keySet().toArray()[0];
        tmpMap.put(realData, simulatedData);
        dataMap.put(date, tmpMap);
    }

    /**
     * Suppression de simulated Data dans la map
     *
     * @param simulatedData Simulated data à supprimer de la map
     */
    private void removeSimulatedData(SimulatedData simulatedData) {
        Objects.requireNonNull(simulatedData);
        LocalDate date = simulatedData.getDateInsertion();
        if (!dataMap.containsKey(date)) {
//            saspLogger.log(Level.WARNING, "Can't remove simulated data, no data inserted");
            return;
        }
        Map<RealData, SimulatedData> tmpMap = dataMap.get(date);
        RealData realData = (RealData) tmpMap.keySet().toArray()[0];
        tmpMap.replace(realData, null);
        dataMap.replace(date, tmpMap);
    }

    /**
     * Permet d'exporter toutes informations d'un cavity sous formes dossiers / fichiers csv
     * Correspondant aux differents éléments de la cavity
     *
     * @param parentFolder Dossier dans lequel exporter
     */
    public String export(String parentFolder) {
        String sep = ";";
        String currentFolder = parentFolder + "/EXPORT_CAVITE_" + name;
        Path currentFold = Paths.get(currentFolder);
        tools.createFolder(currentFold);

        // Export Real data & Simulated data & comparedData
        String ligne1_realData = "Date" + sep + "TempWelHead(C)" + sep + "PressionWelHead" + sep + "Volume(MNm3)" + sep + "Debit(MNm3/j)" + "\n";
        String ligne1_simulatedData = "Date" + sep + "TempWelHead(C)" + sep + "PressionWelHead" + "\n";
        String ligne1_compareData = "Date" + sep + "TempWelHead(C)" + sep + "PressionWelHead" + sep + "Volume(MNm3)" + sep + "Debit(MNm3/j)" + sep + "Simul TempWelHead(C)" + sep + "Simul PressionWelHead(C)" + "\n";

        StringBuilder realDataOutputSb = new StringBuilder();
        StringBuilder simulatedDataOutputSb = new StringBuilder();
        StringBuilder compareDataOutputSb = new StringBuilder();

        realDataOutputSb.append(ligne1_realData);
        simulatedDataOutputSb.append(ligne1_simulatedData);
        compareDataOutputSb.append(ligne1_compareData);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        String calageFolderName = currentFolder + "/calages";

        List<Calage> calageList = database.getListCalageForCavity(this);
        if (!calageList.isEmpty()) {
            Path calageFolder = Paths.get(calageFolderName);
            tools.createFolder(calageFolder);
        }

        if (dataMap.isEmpty()) {
            List<RealData> realDataList = database.getRealDataForCavity(this);
            realDataList.forEach(this::addRealData);
        }

        calageList.forEach(calage -> {
                    // Faire les simulation pour chacun des calages et donc pouvoir ecrire les données dans les fichiers
                    calage.simulateTemperature(); // Simule la temperature et la pression
                    calage.export(calageFolderName);

                    // On ecrit ajoute les données simulées dans la hashMap uniquement si le calage est locked
                    if (calage.isLocked()) {
                        List<SimulatedData> simulatedDataList = calage.getSimulatedDataList();
                        // Ajout des données dans la treeMap pour pouvoir les ecrire dans les fichiers
                        simulatedDataList.forEach(this::addSimulatedData);
                    }
                }
        );

        final boolean[] writeSimulatedDataFile = {false};
        dataMap.forEach((date, dataMap2) -> {

            // Pour chacune des dates , ecrire les informations relative à chaque csv
            dataMap2.forEach((realData, simulatedData) -> {
                realDataOutputSb.append(dtf.format(date)).append(sep);
                realDataOutputSb.append(realData.toCSV())
                        .append("\n");

                if (null != simulatedData) {
                    writeSimulatedDataFile[0] = true;
                    simulatedDataOutputSb.append(dtf.format(date)).append(sep);
                    compareDataOutputSb.append(dtf.format(date)).append(sep);
                    simulatedDataOutputSb.append(simulatedData.toCSV())
                            .append("\n");

                    compareDataOutputSb.append(realData.toCSV())
                            .append(sep)
                            .append(simulatedData.toCSV())
                            .append("\n");
                    removeSimulatedData(simulatedData);
                }
            });
        });

        writeRealDataToFile(currentFolder, realDataOutputSb.toString());
        if (writeSimulatedDataFile[0]) {
            writeSimulatedDataToFile(currentFolder, simulatedDataOutputSb.toString());
            writeCompareDataToFile(currentFolder, compareDataOutputSb.toString());
        }
        writeInfoToFile(currentFolder);
        return currentFolder;
    }

    /**
     * Permet d'exporter toutes informations d'un cavity sous formes dossiers / fichiers csv
     * Correspondant aux differents éléments de la cavity
     * Et crée ensuite un fichier zip sécurisé par le mot de passe donné en paramètre
     *
     * @param password Mot du passe du dossier fichier zip
     * @return Le nom du fichier créé
     */
    public String exportToZip(String password) {

        String folderName = export(".");

        Zipper zipper = new Zipper();

        // Supprimer le fichier zip, s'il existe, avant de le recreer
        String zipFileName = "./EXPORT_CAVITE_" + name + ".zip";
        Path zipFilePath = Paths.get(zipFileName);

        tools.deleteFileIfExists(zipFilePath);
        // Export le dossier sous le format zip
        zipper.packWithPassword(zipFileName, folderName, password);

        // Creer le dossier si jamais il n'existe pas
        Path folderExportPath = Paths.get("./export");
        if (Files.notExists(folderExportPath)) {
            tools.createFolder(folderExportPath);
        }
        // Deplace le dossier zip dans le bon repertoire
        Path exportLocation = Paths.get("./export/" + zipFilePath.toString());
        tools.moveFileToLocation(zipFilePath, exportLocation);

        // Supprimer le dossier creer une fois qu'il a été zippé
        Path pathFolder = Paths.get(folderName);

        tools.deleteDirectoryRecurs(pathFolder);
        return exportLocation.toString().replace("./", "");
    }

    /**
     * Ecrit le fichier real_data.csv
     *
     * @param parentFolder   Dossier ou ecrire le fichier
     * @param realDataString String à écrire dans le fichier
     */
    private void writeRealDataToFile(String parentFolder, String realDataString) {
        tools.writeFile(realDataString.getBytes(charset), parentFolder + "/real_data.csv");
    }

    /**
     * Ecrit le fichier simulated_data.csv
     *
     * @param parentFolder        Dossier ou ecrire le fichier
     * @param simulatedDataString String à écrire dans le fichier
     */
    private void writeSimulatedDataToFile(String parentFolder, String simulatedDataString) {
        tools.writeFile(simulatedDataString.getBytes(charset), parentFolder + "/simulated_data.csv");
    }

    /**
     * Ecrit le fichier compare_data.csv
     *
     * @param parentFolder      Dossier ou ecrire le fichier
     * @param compareDataString String à écrire dans le fichier
     */
    private void writeCompareDataToFile(String parentFolder, String compareDataString) {
        tools.writeFile(compareDataString.getBytes(charset), parentFolder + "/compare_data.csv");
    }

    /**
     * Ecrit le fichier
     *
     * @param parentFolder Dossier ou ecrire le fichier
     */
    private void writeInfoToFile(String parentFolder) {
        tools.writeFile(toInfoCSV().getBytes(charset), parentFolder + "/info.csv");
    }

    /**
     * Insert la cavite dans la base de données
     */
    public void insertInDb() {
        database.insertCavity(this);
    }

    /// Getters

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Site getSite() {
        return site;
    }

    public double getHauteurTubage() {
        return hauteurTubage;
    }

    public double getDiametreTubage() {
        return diametreTubage;
    }

    public double getDepth() {
        return depth;
    }

    public double getVolume() {
        return volume;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

}
