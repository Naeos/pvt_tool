package com.noxdia.site.cavite.calage;

import com.noxdia.database.Database;
import com.noxdia.site.cavite.Cavity;

import java.time.LocalDate;

public class CalageFactory {

    public static Calage createCalage(LocalDate from, LocalDate to, Cavity cavity, Database database,
                                      double p_3_volumeStock, double p_3_debit, double p_3_Psimul,
                                      double p_2_volumeStock, double p_2_debit, double p_2_psimul, double p_2_dp_arma,
                                      double p_1_volumeStock, double p_1_debit, double p_1_psimul, double p_1_dp_arma,
                                      double p_0_volumeStock, double p_0_debit, double p_0_psimul, double p_0_dp_arma,
                                      double t_3_pArma, double t_3_volumeStock, double t_3_debit, double t_3_Tsimul,
                                      double t_2_pArma, double t_2_volumeStock, double t_2_debit, double t_2_Tsimul, double t_2_dp_arma,
                                      double t_1_pArma, double t_1_volumeStock, double t_1_debit, double t_1_Tsimul,
                                      double t_0_pArma, double t_0_volumeStock, double t_0_debit, double t_0_Tsimul) {
        return new Calage(from, to, cavity, database,
                p_3_volumeStock, p_3_debit, p_3_Psimul,
                p_2_volumeStock, p_2_debit, p_2_psimul, p_2_dp_arma,
                p_1_volumeStock, p_1_debit, p_1_psimul, p_1_dp_arma,
                p_0_volumeStock, p_0_debit, p_0_psimul, p_0_dp_arma,
                t_3_pArma, t_3_volumeStock, t_3_debit, t_3_Tsimul,
                t_2_pArma, t_2_volumeStock, t_2_debit, t_2_Tsimul, t_2_dp_arma,
                t_1_pArma, t_1_volumeStock, t_1_debit, t_1_Tsimul,
                t_0_pArma, t_0_volumeStock, t_0_debit, t_0_Tsimul);
    }


}
