package com.noxdia.site.cavite.calage;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.data.DataFactory;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.cavite.data.SimulatedData;
import com.noxdia.tools.Tools;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;

public class Calage {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    private final Database database;
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    // Liste des données simulées
    private final List<SimulatedData> simulatedDataList = new ArrayList<>();
    private final Charset charset = Charset.forName("ISO-8859-1");

    private final List<Double> deviationPressionList = new ArrayList<>();
    private final List<Double> deviationTempList = new ArrayList<>();
    private final TreeMap<LocalDate, Double> temperatureSimuleeTreeMap = new TreeMap<>();
    private final TreeMap<LocalDate, Double> pressionSimuleeTreeMap = new TreeMap<>();

    private LocalDate from;
    private LocalDate to;
    private int id;
    private Cavity cavity;
    private boolean isLocked = false;
    private boolean simulationPressionComplete = false;
    private boolean simulationTemperatureComplete = false;

    // Coef pression
    private double P_3_VolumeStock;
    private double P_3_Debit;
    private double P_3_Psimul;
    private double P_2_VolumeStock;
    private double P_2_Debit;
    private double P_2_Psimul;
    private double P_2_DP_Arma;
    private double P_1_VolumeStock;
    private double P_1_Debit;
    private double P_1_Psimul;
    private double P_1_DP_Arma;
    private double P_0_VolumeStock;
    private double P_0_Debit;
    private double P_0_Psimul;
    private double P_0_DP_Arma;
    // Coef temperature
    private double T_3_PArma;
    private double T_3_VolumeStock;
    private double T_3_Debit;
    private double T_3_Tsimul;
    private double T_2_PArma;
    private double T_2_VolumeStock;
    private double T_2_Debit;
    private double T_2_Tsimul;
    private double T_2_DP_Arma;
    private double T_1_PArma;
    private double T_1_VolumeStock;
    private double T_1_Debit;
    private double T_1_Tsimul;
    private double T_0_PArma;
    private double T_0_VolumeStock;
    private double T_0_Debit;
    private double T_0_Tsimul;

    Calage(LocalDate from, LocalDate to, Cavity cavity, Database database,
           double p_3_volumeStock, double p_3_debit, double p_3_Psimul,
           double p_2_volumeStock, double p_2_debit, double p_2_psimul, double p_2_dp_arma,
           double p_1_volumeStock, double p_1_debit, double p_1_psimul, double p_1_dp_arma,
           double p_0_volumeStock, double p_0_debit, double p_0_psimul, double p_0_dp_arma,
           double t_3_pArma, double t_3_volumeStock, double t_3_debit, double t_3_Tsimul,
           double t_2_pArma, double t_2_volumeStock, double t_2_debit, double t_2_psimul, double t_2_dp_arma,
           double t_1_pArma, double t_1_volumeStock, double t_1_debit, double t_1_Tsimul,
           double t_0_pArma, double t_0_volumeStock, double t_0_debit, double t_0_Tsimul) {
        this.from = Objects.requireNonNull(from);
        this.to = Objects.requireNonNull(to);
        this.cavity = Objects.requireNonNull(cavity);
        this.database = Objects.requireNonNull(database);
        P_3_VolumeStock = p_3_volumeStock;
        P_3_Debit = p_3_debit;
        P_3_Psimul = p_3_Psimul;
        P_2_VolumeStock = p_2_volumeStock;
        P_2_Debit = p_2_debit;
        P_2_Psimul = p_2_psimul;
        P_2_DP_Arma = p_2_dp_arma;
        P_1_VolumeStock = p_1_volumeStock;
        P_1_Debit = p_1_debit;
        P_1_Psimul = p_1_psimul;
        P_1_DP_Arma = p_1_dp_arma;
        P_0_VolumeStock = p_0_volumeStock;
        P_0_Debit = p_0_debit;
        P_0_Psimul = p_0_psimul;
        P_0_DP_Arma = p_0_dp_arma;
        T_3_PArma = t_3_pArma;
        T_3_VolumeStock = t_3_volumeStock;
        T_3_Debit = t_3_debit;
        T_3_Tsimul = t_3_Tsimul;
        T_2_PArma = t_2_pArma;
        T_2_VolumeStock = t_2_volumeStock;
        T_2_Debit = t_2_debit;
        T_2_Tsimul = t_2_psimul;
        T_2_DP_Arma = t_2_dp_arma;
        T_1_PArma = t_1_pArma;
        T_1_VolumeStock = t_1_volumeStock;
        T_1_Debit = t_1_debit;
        T_1_Tsimul = t_1_Tsimul;
        T_0_PArma = t_0_pArma;
        T_0_VolumeStock = t_0_volumeStock;
        T_0_Debit = t_0_debit;
        T_0_Tsimul = t_0_Tsimul;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Calage calage = (Calage) o;

        if (Double.compare(calage.P_3_VolumeStock, P_3_VolumeStock) != 0) return false;
        if (Double.compare(calage.P_3_Debit, P_3_Debit) != 0) return false;
        if (Double.compare(calage.P_3_Psimul, P_3_Psimul) != 0) return false;
        if (Double.compare(calage.P_2_VolumeStock, P_2_VolumeStock) != 0) return false;
        if (Double.compare(calage.P_2_Debit, P_2_Debit) != 0) return false;
        if (Double.compare(calage.P_2_Psimul, P_2_Psimul) != 0) return false;
        if (Double.compare(calage.P_2_DP_Arma, P_2_DP_Arma) != 0) return false;
        if (Double.compare(calage.P_1_VolumeStock, P_1_VolumeStock) != 0) return false;
        if (Double.compare(calage.P_1_Debit, P_1_Debit) != 0) return false;
        if (Double.compare(calage.P_1_Psimul, P_1_Psimul) != 0) return false;
        if (Double.compare(calage.P_1_DP_Arma, P_1_DP_Arma) != 0) return false;
        if (Double.compare(calage.P_0_VolumeStock, P_0_VolumeStock) != 0) return false;
        if (Double.compare(calage.P_0_Debit, P_0_Debit) != 0) return false;
        if (Double.compare(calage.P_0_Psimul, P_0_Psimul) != 0) return false;
        if (Double.compare(calage.P_0_DP_Arma, P_0_DP_Arma) != 0) return false;
        if (Double.compare(calage.T_3_PArma, T_3_PArma) != 0) return false;
        if (Double.compare(calage.T_3_VolumeStock, T_3_VolumeStock) != 0) return false;
        if (Double.compare(calage.T_3_Debit, T_3_Debit) != 0) return false;
        if (Double.compare(calage.T_3_Tsimul, T_3_Tsimul) != 0) return false;
        if (Double.compare(calage.T_2_PArma, T_2_PArma) != 0) return false;
        if (Double.compare(calage.T_2_VolumeStock, T_2_VolumeStock) != 0) return false;
        if (Double.compare(calage.T_2_Debit, T_2_Debit) != 0) return false;
        if (Double.compare(calage.T_2_Tsimul, T_2_Tsimul) != 0) return false;
        if (Double.compare(calage.T_2_DP_Arma, T_2_DP_Arma) != 0) return false;
        if (Double.compare(calage.T_1_PArma, T_1_PArma) != 0) return false;
        if (Double.compare(calage.T_1_VolumeStock, T_1_VolumeStock) != 0) return false;
        if (Double.compare(calage.T_1_Debit, T_1_Debit) != 0) return false;
        if (Double.compare(calage.T_1_Tsimul, T_1_Tsimul) != 0) return false;
        if (Double.compare(calage.T_0_PArma, T_0_PArma) != 0) return false;
        if (Double.compare(calage.T_0_VolumeStock, T_0_VolumeStock) != 0) return false;
        if (Double.compare(calage.T_0_Debit, T_0_Debit) != 0) return false;
        if (Double.compare(calage.T_0_Tsimul, T_0_Tsimul) != 0) return false;
        if (!from.equals(calage.from)) return false;
        if (!to.equals(calage.to)) return false;
        return cavity.equals(calage.cavity);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + cavity.hashCode();
        temp = Double.doubleToLongBits(P_3_VolumeStock);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_3_Debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_3_Psimul);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_2_VolumeStock);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_2_Debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_2_Psimul);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_2_DP_Arma);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_1_VolumeStock);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_1_Debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_1_Psimul);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_1_DP_Arma);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_0_VolumeStock);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_0_Debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_0_Psimul);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(P_0_DP_Arma);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_3_PArma);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_3_VolumeStock);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_3_Debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_3_Tsimul);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_2_PArma);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_2_VolumeStock);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_2_Debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_2_Tsimul);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_2_DP_Arma);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_1_PArma);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_1_VolumeStock);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_1_Debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_1_Tsimul);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_0_PArma);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_0_VolumeStock);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_0_Debit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(T_0_Tsimul);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Calage{" +
                ", from=" + from +
                ", to=" + to +
                ", id=" + id +
                ", isLocked=" + isLocked +
                ", simulationPressionComplete=" + simulationPressionComplete +
                ", simulationTemperatureComplete=" + simulationTemperatureComplete +
                ", P_3_VolumeStock=" + P_3_VolumeStock +
                ", P_3_Debit=" + P_3_Debit +
                ", P_3_Psimul=" + P_3_Psimul +
                ", P_2_VolumeStock=" + P_2_VolumeStock +
                ", P_2_Debit=" + P_2_Debit +
                ", P_2_Psimul=" + P_2_Psimul +
                ", P_2_DP_Arma=" + P_2_DP_Arma +
                ", P_1_VolumeStock=" + P_1_VolumeStock +
                ", P_1_Debit=" + P_1_Debit +
                ", P_1_Psimul=" + P_1_Psimul +
                ", P_1_DP_Arma=" + P_1_DP_Arma +
                ", P_0_VolumeStock=" + P_0_VolumeStock +
                ", P_0_Debit=" + P_0_Debit +
                ", P_0_Psimul=" + P_0_Psimul +
                ", P_0_DP_Arma=" + P_0_DP_Arma +
                ", T_3_PArma=" + T_3_PArma +
                ", T_3_VolumeStock=" + T_3_VolumeStock +
                ", T_3_Debit=" + T_3_Debit +
                ", T_3_Tsimul=" + T_3_Tsimul +
                ", T_2_PArma=" + T_2_PArma +
                ", T_2_VolumeStock=" + T_2_VolumeStock +
                ", T_2_Debit=" + T_2_Debit +
                ", T_2_Tsimul=" + T_2_Tsimul +
                ", T_2_DP_Arma=" + T_2_DP_Arma +
                ", T_1_PArma=" + T_1_PArma +
                ", T_1_VolumeStock=" + T_1_VolumeStock +
                ", T_1_Debit=" + T_1_Debit +
                ", T_1_Tsimul=" + T_1_Tsimul +
                ", T_0_PArma=" + T_0_PArma +
                ", T_0_VolumeStock=" + T_0_VolumeStock +
                ", T_0_Debit=" + T_0_Debit +
                ", T_0_Tsimul=" + T_0_Tsimul +
                '}';
    }

    /**
     * Met à jour le calage avec les informations de 'updatedCalage' ,
     * Renvoie false si le calage à partir du quel on met à jour n'est pas sur la même cavity, true sinon
     *
     * @param updatedCalage Calage à partir duquel on met à jour
     * @return False si pas même cavity, True sinon
     */
    public boolean update(Calage updatedCalage) {
        if (!updatedCalage.cavity.equals(cavity)) {
            saspLogger.log(Level.WARNING, "Can't update calage from calage with different cavity");
            return false;
        }
        from = updatedCalage.from;
        to = updatedCalage.to;
        P_3_VolumeStock = updatedCalage.P_3_VolumeStock;
        P_3_Debit = updatedCalage.P_3_Debit;
        P_3_Psimul = updatedCalage.P_3_Psimul;

        P_2_VolumeStock = updatedCalage.P_2_VolumeStock;
        P_2_Debit = updatedCalage.P_2_Debit;
        P_2_Psimul = updatedCalage.P_2_Psimul;
        P_2_DP_Arma = updatedCalage.P_2_DP_Arma;

        P_1_VolumeStock = updatedCalage.P_1_VolumeStock;
        P_1_Debit = updatedCalage.P_1_Debit;
        P_1_Psimul = updatedCalage.P_1_Psimul;
        P_1_DP_Arma = updatedCalage.P_1_DP_Arma;

        P_0_VolumeStock = updatedCalage.P_0_VolumeStock;
        P_0_Debit = updatedCalage.P_0_Debit;
        P_0_Psimul = updatedCalage.P_0_Psimul;
        P_0_DP_Arma = updatedCalage.P_0_DP_Arma;

        T_3_PArma = updatedCalage.T_3_PArma;
        T_3_VolumeStock = updatedCalage.T_3_VolumeStock;
        T_3_Debit = updatedCalage.T_3_Debit;
        T_3_Tsimul = updatedCalage.T_3_Tsimul;

        T_2_PArma = updatedCalage.T_2_PArma;
        T_2_VolumeStock = updatedCalage.T_2_VolumeStock;
        T_2_Debit = updatedCalage.T_2_Debit;
        T_2_Tsimul = updatedCalage.T_2_Tsimul;
        T_2_DP_Arma = updatedCalage.T_2_DP_Arma;

        T_1_PArma = updatedCalage.T_1_PArma;
        T_1_VolumeStock = updatedCalage.T_1_VolumeStock;
        T_1_Debit = updatedCalage.T_1_Debit;
        T_1_Tsimul = updatedCalage.T_1_Tsimul;

        T_0_PArma = updatedCalage.T_0_PArma;
        T_0_VolumeStock = updatedCalage.T_0_VolumeStock;
        T_0_Debit = updatedCalage.T_0_Debit;
        T_0_Tsimul = updatedCalage.T_0_Tsimul;

        database.updateCalageNarma(this);
        return true;
    }

    /**
     * Export le calage dans le dossier donné en paramètre
     *
     * @param calageFolderName Nom du dossier ou vont être exporter les calages
     */
    public void export(String calageFolderName) {
        String folderName = calageFolderName + "/" + dtf.format(from) + "_" + dtf.format(to);
        Path path = Paths.get(folderName);

        // Creation du dossier s'il n'existe pas
        // Format JJ-MM-AAAA_JJ-MM-AAAA
        tools.createFolder(path);
        toCSVTemp(folderName);
        toCSVPressure(folderName);
        saspLogger.log(Level.INFO, "Calage exported correctly");
    }

    /**
     * Ecrit le fichier temperature.csv
     *
     * @param parentFolder Emplacement du dossier du fichier
     */
    private void toCSVTemp(String parentFolder) {
        StringBuilder stringBuilder = new StringBuilder();
        String sep = ";";
        String line1 = sep + sep + "PArma" + sep + "Volume Stock" + sep + "Debit" + sep + "T simul" + sep + "DP Arma";
        double sommeEcartsQuadratiqueTemperature = calculSommeEcartsQuadratiqueTemperature();
        String line2 = sep + "Coef T-3" + sep + T_3_PArma + sep + T_3_VolumeStock + sep + T_3_Debit + sep + T_3_Tsimul + sep + sep + sep + "Sum of quared deviations" + sep + sommeEcartsQuadratiqueTemperature;
        String line3 = sep + "Coef T-2" + sep + T_2_PArma + sep + T_2_VolumeStock + sep + T_2_Debit + sep + T_2_Tsimul + sep + T_2_DP_Arma + sep + sep + "Standard deviation" + sep + calculEcartTypeTemperature();
        String line4 = sep + "Coef T-1" + sep + T_1_PArma + sep + T_1_VolumeStock + sep + T_1_Debit + sep + T_1_Tsimul + sep + sep + sep + "Deviation Min" + sep + calculDeviationMinTemperature();
        String line5 = sep + "Coef T-0" + sep + T_0_PArma + sep + T_0_VolumeStock + sep + T_0_Debit + sep + T_0_Tsimul + sep + sep + sep + "Deviation Max" + sep + calculDeviationMaxTemperature();

        double deviationMoyenneTemperature = calculDeviationMoyenneTemperature();
        String line6 = sep + sep + sep + sep + sep + sep + sep + sep + "Deviation Moyenne" + sep + deviationMoyenneTemperature;
        double optim = deviationMoyenneTemperature + sommeEcartsQuadratiqueTemperature;
        String line7 = sep + sep + sep + sep + sep + sep + sep + sep + "Optim" + sep + optim;
        String line8 = "Date" + sep + "Temperature Mesures" + sep + "Pression simulee" + sep + "Volume stocke" + sep + "Debit" + sep + "Temperature Simulee" + sep + "Deviation Arma";

        stringBuilder.append(line1).append("\n")
                .append(line2).append("\n")
                .append(line3).append("\n")
                .append(line4).append("\n")
                .append(line5).append("\n")
                .append(line6).append("\n")
                .append(line7).append("\n")
                .append(line8).append("\n");

        simulatedDataList.forEach(simulatedData -> {
            LocalDate date = simulatedData.getDateInsertion();
            Optional<RealData> optRealData = database.getRealDataForCavityAt(cavity, date);
            if (!optRealData.isPresent()) {
                // Ne devrait jamais arriver !
                saspLogger.log(Level.WARNING, "Missing real data at date : " + date);
                return;
            }
            RealData realData = optRealData.get();
            double temperatureMesuree = realData.getTemperatureWelHead();
            double deviation = calculDeviation(simulatedData.getTemperatureWelHead(), temperatureMesuree);
            double pressionSimule = simulatedData.getPressionWelHead();
            String line = simulatedData.getDateInsertion() + sep + temperatureMesuree + sep + pressionSimule + sep + simulatedData.getVolume() + sep + simulatedData.getDebit() + sep + simulatedData.getTemperatureWelHead() + sep + deviation;

            stringBuilder.append(line).append("\n");
        });

        String toWrite = stringBuilder.toString();

        byte[] data = toWrite.getBytes(charset);
        String fileName = parentFolder + "/temperature.csv";
        tools.writeFile(data, fileName);
    }

    /**
     * Ecrit le fichier pression.csv
     *
     * @param parentFolder Emplacement du dossier du fichier
     */
    private void toCSVPressure(String parentFolder) {
        StringBuilder stringBuilder = new StringBuilder();
        String sep = ";";
        String line1 = sep + sep + "Volume Stock" + sep + "Debit" + sep + "P simul" + sep + "DP Arma";
        double sommeEcartsQuadratiquePression = calculSommeEcartsQuadratiquePression();
        String line2 = sep + "Coef P-3" + sep + P_3_VolumeStock + sep + P_3_Debit + sep + P_3_Psimul + sep + sep + sep + "Sum of quared deviations" + sep + sommeEcartsQuadratiquePression;
        String line3 = sep + "Coef P-2" + sep + P_2_VolumeStock + sep + P_2_Debit + sep + P_2_Psimul + sep + P_2_DP_Arma + sep + sep + "Standard deviation" + sep + calculEcartTypePression();
        String line4 = sep + "Coef P-1" + sep + P_1_VolumeStock + sep + P_1_Debit + sep + P_1_Psimul + sep + P_1_DP_Arma + sep + sep + "Deviation Min" + sep + calculDeviationMinPression();
        String line5 = sep + "Coef P-0" + sep + P_0_VolumeStock + sep + P_0_Debit + sep + P_0_Psimul + sep + P_0_DP_Arma + sep + sep + "Deviation Max" + sep + calculDeviationMaxPression();

        double deviationMoyennePression = calculDeviationMoyennePression();
        String line6 = sep + sep + sep + sep + sep + sep + sep + "Deviation Moyenne" + sep + deviationMoyennePression;
        double optim = sommeEcartsQuadratiquePression + deviationMoyennePression;
        String line7 = sep + sep + sep + sep + sep + sep + sep + "Optim" + sep + optim;
        String line8 = "Date" + sep + "Pression Mesures" + sep + "Volume stocke" + sep + "Debit" + sep + "Pression Simulee" + sep + "Deviation Arma";

        stringBuilder.append(line1).append("\n")
                .append(line2).append("\n")
                .append(line3).append("\n")
                .append(line4).append("\n")
                .append(line5).append("\n")
                .append(line6).append("\n")
                .append(line7).append("\n")
                .append(line8).append("\n");

        simulatedDataList.forEach(simulatedData -> {
            LocalDate date = simulatedData.getDateInsertion();
            Optional<RealData> optRealData = database.getRealDataForCavityAt(cavity, date);
            if (!optRealData.isPresent()) {
                // Ne devrait jamais arriver !
                saspLogger.log(Level.WARNING, "Missing real data at date : " + date);
                return;
            }
            RealData realData = optRealData.get();
            double pressionMesuree = realData.getPressionWelHead();
            double deviation = calculDeviation(simulatedData.getPressionWelHead(), pressionMesuree);
            String line = date + sep + pressionMesuree + sep + simulatedData.getVolume() + sep + simulatedData.getDebit() + sep + simulatedData.getPressionWelHead() + sep + deviation;

            stringBuilder.append(line).append("\n");
        });

        String toWrite = stringBuilder.toString();

        byte[] data = toWrite.getBytes(charset);
        String fileName = parentFolder + "/pression.csv";
        tools.writeFile(data, fileName);
    }

    /////////////////////////////////////////////////// Pression ////////////////////////////////////////////////////

    /**
     * @return True si la simulation est incomplete, false sinon
     */
    private boolean simulationPressionIncomplete() {
        if (!simulationPressionComplete) {
            saspLogger.log(Level.WARNING, "You need to simulate the data before trying to get stats about it !");
            return true;
        }
        return false;
    }

    /**
     * Calcul de la deviation moyenne de la pression
     *
     * @return Deviation moyenne de la pression si la simulation a été faite, -1 sinon
     */
    public double calculDeviationMoyennePression() {
        return simulationPressionIncomplete() ? -1 : tools.calculDeviationMoyenne(deviationPressionList);
    }

    /**
     * Calcul de l'ecart type de la pression
     *
     * @return Ecart Type de la pression si la simulation a été faite, -1 sinon
     */
    public double calculEcartTypePression() {
        return simulationPressionIncomplete() ? -1 : tools.calculEcartType(deviationPressionList);
    }

    /**
     * Calcul de la deviation minimal
     *
     * @return Deviation minimal si la simulation a été faite, -1 sinon
     */
    public double calculDeviationMinPression() {
        return simulationPressionIncomplete() ? -1 : tools.calculMin(deviationPressionList);
    }

    /**
     * Calcul de la deviation maximal
     *
     * @return Deviation maximal si la simulation a été faite, -1 sinon
     */
    public double calculDeviationMaxPression() {
        return simulationPressionIncomplete() ? -1 : tools.calculMax(deviationPressionList);
    }

    /**
     * Calcul de la somme des ecarts quadratique
     *
     * @return Somme des ecarts quadratique si la simulation a été faite, -1 sinon
     */
    public double calculSommeEcartsQuadratiquePression() {
        return simulationPressionIncomplete() ? -1 : tools.calculSommeEcartsQuadratique(deviationPressionList);
    }

    ////////////////////////////////////////////////// Temperature //////////////////////////////////////////////////

    /**
     * @return True sur la simulation de la temperature n'est pas terminé, false sinon
     */
    private boolean simulationTemperatureIncomplete() {
        if (!simulationTemperatureComplete) {
            saspLogger.log(Level.WARNING, "You need to simulateTemperature the data before trying to get stats about it !");
            return true;
        }
        return false;
    }

    /**
     * Calcul de la deviation moyenne de la temperature
     *
     * @return Deviation moyenne si la simulation a été faite, -1 sinon
     */
    public double calculDeviationMoyenneTemperature() {
        return simulationTemperatureIncomplete() ? -1 : tools.calculDeviationMoyenne(deviationTempList);
    }

    /**
     * Calcul de l'ecart type de la temperature
     *
     * @return L'ecart type si la simulation a été faite, -1 sinon
     */
    public double calculEcartTypeTemperature() {
        return simulationTemperatureIncomplete() ? -1 : tools.calculEcartType(deviationTempList);
    }

    /**
     * Calcul de la deviation minimal de la temperature
     *
     * @return Deviation minimal si la simulation a été faite, -1 sinon
     */
    public double calculDeviationMinTemperature() {
        return simulationTemperatureIncomplete() ? -1 : tools.calculMin(deviationTempList);
    }

    /**
     * Calcul de la deviation maximal de la temperature
     *
     * @return Deviation maximal si la simulation a été faite, -1 sinon
     */
    public double calculDeviationMaxTemperature() {
        return simulationTemperatureIncomplete() ? -1 : tools.calculMax(deviationTempList);
    }

    /**
     * Calcul de la somme des ecarts quadratique de la temperature
     *
     * @return La somme des ecarts quadratique si la simulation a été faite, -1 sinon
     */
    public double calculSommeEcartsQuadratiqueTemperature() {
        return simulationTemperatureIncomplete() ? -1 : tools.calculSommeEcartsQuadratique(deviationTempList);
    }

    /**
     * @param simulee Valeur simulé
     * @param reel    Valeur réel
     * @return Difference entre simulee et reel
     */
    private double calculDeviation(double simulee, double reel) {
        return simulee - reel;
    }

    /**
     * Calcul de la pression simulée à une date "date"
     *
     * @param date Date à laquel calculer la pression
     * @return La pression simulée, sinon -1 en cas d'erreur
     */
    private double calculPressionAtDate(LocalDate date) {
        if (pressionSimuleeTreeMap.size() < 4) {
            Optional<RealData> optRealData_0 = database.getRealDataForCavityAt(cavity, date);
            if (!optRealData_0.isPresent()) {
                saspLogger.log(Level.WARNING, "Missing real data to simulate pression :" + date);
                return -1;
            }
            RealData realData = optRealData_0.get();
            return realData.getPressionWelHead();
        }

        LocalDate dateMinus1Day = date.minusDays(1);
        LocalDate dateMinus2Day = date.minusDays(2);
        LocalDate dateMinus3Day = date.minusDays(3);
        LocalDate dateMinus4Day = date.minusDays(4);
        Optional<RealData> optRealData_0 = database.getRealDataForCavityAt(cavity, date);
        if (!optRealData_0.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate pression :" + date);
            return -1;
        }

        Optional<RealData> optRealData_1 = database.getRealDataForCavityAt(cavity, dateMinus1Day);
        if (!optRealData_1.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate pression :" + dateMinus1Day);
            return -1;
        }

        Optional<RealData> optRealData_2 = database.getRealDataForCavityAt(cavity, dateMinus2Day);
        if (!optRealData_2.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate pression :" + dateMinus2Day);
            return -1;
        }

        Optional<RealData> optRealData_3 = database.getRealDataForCavityAt(cavity, dateMinus3Day);
        if (!optRealData_3.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate pression :" + dateMinus3Day);
            return -1;
        }

        Optional<RealData> optRealData_4 = database.getRealDataForCavityAt(cavity, dateMinus4Day);
        if (!optRealData_4.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate pression :" + dateMinus4Day);
            return -1;
        }

        RealData realData_date_0 = optRealData_0.get();
        RealData realData_date_1 = optRealData_1.get();
        RealData realData_date_2 = optRealData_2.get();
        RealData realData_date_3 = optRealData_3.get();

        double d0 = realData_date_0.getDebit();
        double v0 = realData_date_0.getVolume();

        double p_1 = pressionSimuleeTreeMap.get(dateMinus1Day);
        double d_1 = realData_date_1.getDebit();
        double v_1 = realData_date_1.getVolume();

        double p_2 = pressionSimuleeTreeMap.get(dateMinus2Day);
        double d_2 = realData_date_2.getDebit();
        double v_2 = realData_date_2.getVolume();

        double p_3 = pressionSimuleeTreeMap.get(dateMinus3Day);
        double d_3 = realData_date_3.getDebit();
        double v_3 = realData_date_3.getVolume();

        double p_4 = pressionSimuleeTreeMap.get(dateMinus4Day);

        return P_2_DP_Arma +
                P_0_Debit * d0 +
                P_0_DP_Arma * d0 * Math.abs(d0) +
                P_0_Psimul * p_1 +
                P_0_VolumeStock * v0 +

                P_1_Debit * d_1 +
                P_1_DP_Arma * d0 * p_1 +
                P_1_VolumeStock * v_1 +
                P_1_Psimul * p_2 +

                P_2_Debit * d_2 +
                P_2_VolumeStock * v_2 +
                P_2_Psimul * p_3 +

                P_3_Debit * d_3 +
                P_3_VolumeStock * v_3 +
                P_3_Psimul * p_4;

    }

    /**
     * Calcul de la temperature simulée à une date "date"
     *
     * @param date Date à laquel calculer la temperature
     * @return La temperature simulée, sinon -1 en cas d'erreur
     */
    private double calculTemperatureAtDate(LocalDate date) {
        if (temperatureSimuleeTreeMap.size() < 4) {
            Optional<RealData> optRealData_0 = database.getRealDataForCavityAt(cavity, date);
            if (!optRealData_0.isPresent()) {
                saspLogger.log(Level.WARNING, "Missing real data to simulate Temperature :" + date);
                return -1;
            }
            RealData realData = optRealData_0.get();
            return realData.getTemperatureWelHead();
        }

        LocalDate dateMinus1Day = date.minusDays(1);
        LocalDate dateMinus2Day = date.minusDays(2);
        LocalDate dateMinus3Day = date.minusDays(3);
        LocalDate dateMinus4Day = date.minusDays(4);

        Optional<RealData> optRealData_0 = database.getRealDataForCavityAt(cavity, date);
        if (!optRealData_0.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate Temperature :" + date);
            return -1;
        }

        Optional<RealData> optRealData_1 = database.getRealDataForCavityAt(cavity, dateMinus1Day);
        if (!optRealData_1.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate Temperature :" + dateMinus1Day);
            return -1;
        }

        Optional<RealData> optRealData_2 = database.getRealDataForCavityAt(cavity, dateMinus2Day);
        if (!optRealData_2.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate Temperature :" + dateMinus2Day);
            return -1;
        }

        Optional<RealData> optRealData_3 = database.getRealDataForCavityAt(cavity, dateMinus3Day);
        if (!optRealData_3.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate Temperature :" + dateMinus3Day);
            return -1;
        }

        Optional<RealData> optRealData_4 = database.getRealDataForCavityAt(cavity, dateMinus4Day);
        if (!optRealData_4.isPresent()) {
            saspLogger.log(Level.WARNING, "Missing real data to simulate Temperature :" + dateMinus4Day);
            return -1;
        }

        RealData realData_date_0 = optRealData_0.get();
        RealData realData_date_1 = optRealData_1.get();
        RealData realData_date_2 = optRealData_2.get();
        RealData realData_date_3 = optRealData_3.get();
        double p0 = pressionSimuleeTreeMap.get(date);
        double d0 = realData_date_0.getDebit();
        double v0 = realData_date_0.getVolume();
        double t_1 = temperatureSimuleeTreeMap.get(dateMinus1Day);
        double p_1 = pressionSimuleeTreeMap.get(dateMinus1Day); // Null pointer -> normal vu qu'il manque pression simulee a date-1
        double d_1 = realData_date_1.getDebit();
        double v_1 = realData_date_1.getVolume();

        double t_2 = temperatureSimuleeTreeMap.get(dateMinus2Day);
        double p_2 = pressionSimuleeTreeMap.get(dateMinus2Day);
        double d_2 = realData_date_2.getDebit();
        double v_2 = realData_date_2.getVolume();

        double t_3 = temperatureSimuleeTreeMap.get(dateMinus3Day);
        double p_3 = pressionSimuleeTreeMap.get(dateMinus3Day);
        double d_3 = realData_date_3.getDebit();
        double v_3 = realData_date_3.getVolume();

        double t_4 = temperatureSimuleeTreeMap.get(dateMinus4Day);

        return T_2_DP_Arma +
                T_0_PArma * p0 +
                T_0_Debit * d0 +
                T_0_VolumeStock * v0 +
                T_0_Tsimul * t_1 +

                T_1_PArma * p_1 +
                T_1_Debit * d_1 +
                T_1_VolumeStock * v_1 +
                T_1_Tsimul * t_2 +

                T_2_PArma * p_2 +
                T_2_Debit * d_2 +
                T_2_VolumeStock * v_2 +
                T_2_Tsimul * t_3 +

                T_3_PArma * p_3 +
                T_3_Debit * d_3 +
                T_3_VolumeStock * v_3 +
                T_3_Tsimul * t_4;
    }

    /**
     * Simulation de la pression
     *
     * @return true si pas d'erreur pendant le simulation, false sinon
     */
    private boolean simulatePression() {
        final boolean[] noError = {true};
        simulationPressionComplete = false;
        // Reset des données à chaque nouvelle simulation
        deviationPressionList.clear();
        pressionSimuleeTreeMap.clear();

        // Aura toujours suffisament de données pour simulé
        // Données reelles pour le calcul de la pression
        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        if (realDataList.isEmpty() || realDataList.size() < 4) {
            saspLogger.log(Level.WARNING, "Not enough data to simulate pression");
            return !noError[0];
        }
        // Calcul pression pour chacune des dates voulus
        realDataList.forEach(realData -> {
            LocalDate date = realData.getDateInsertion();
            double pressionSimul = calculPressionAtDate(date);
            if (pressionSimul == -1) {
                noError[0] = false;
            }
            double pressionReel = realData.getPressionWelHead();
            deviationPressionList.add(calculDeviation(pressionSimul, pressionReel));
            pressionSimuleeTreeMap.put(date, pressionSimul);
        });
        if (!noError[0]) {
            return false;
        }

        simulationPressionComplete = true;
        return noError[0];
    }

    /**
     * Simulation de la temperature
     *
     * @return true si pas d'erreur pendant le simulation, false sinon
     */
    public boolean simulateTemperature() {
        simulationTemperatureComplete = false;
        deviationTempList.clear();
        temperatureSimuleeTreeMap.clear();

        // Recup data from database -> From / to
        // Recuperation des données reels sur la periode voulu afin de simuler les données
        // From -> Date debut de la simulation
        // To -> Date de la fin de la simulation
        // Cavity -> Cavity sur laquelle on va simulé les données
        int nbDaysMiniToSimulate = 4;
        List<RealData> testRealDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        if (testRealDataList.isEmpty() || testRealDataList.size() < nbDaysMiniToSimulate) {
            saspLogger.log(Level.WARNING, "Not enough data to simulate Temperature");
            return false;
        }

        // True si pas d'erreur
        // False sinon
        boolean noError = true;
        if (!simulationPressionComplete) {
            noError = simulatePression();
        }
        if (!noError) {
            saspLogger.log(Level.WARNING, "Simulation pression failed");
            return false;
        }

        final boolean isThereErrorTemperature[] = {false};

        // Calcul de la temperature simulée
        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        realDataList.forEach(realData -> {
            LocalDate date = realData.getDateInsertion();
            double temperatureSimul = calculTemperatureAtDate(date);
            if (temperatureSimul == -1) {
                isThereErrorTemperature[0] = true;
            }
            double temperatureReel = realData.getTemperatureWelHead();
            deviationTempList.add(calculDeviation(temperatureSimul, temperatureReel));
            temperatureSimuleeTreeMap.put(date, temperatureSimul);
        });

        if (isThereErrorTemperature[0]) {
            return false;
        }

        realDataList.forEach(realData -> {
            LocalDate date = realData.getDateInsertion();
            double temperatureSimul = temperatureSimuleeTreeMap.get(date);
            double pressionSimul = pressionSimuleeTreeMap.get(date);
            double volume = realData.getVolume();
            double debit = realData.getDebit();
            SimulatedData simulatedData = DataFactory.createSimulatedData(date, cavity, temperatureSimul, pressionSimul, volume, debit, this);
            simulatedDataList.add(simulatedData);
        });
        simulationTemperatureComplete = true;
        return true;
    }

    /**
     * Insert le calage dans la base de donnée
     */
    public void insertInDb() {
        database.insertCalageNarma(this);
    }

    /**
     * Verrouille le calage
     */
    public void lock() {
        this.isLocked = true;
        database.updateCalageNarmaStatus(this, true);
    }

    /**
     * Deverrouille le calage
     */
    public void unlock() {
        this.isLocked = false;
        database.updateCalageNarmaStatus(this, false);
    }

    /**
     * Indique si le calage est verrouillé
     *
     * @return True si verrouillé , False sinon
     */
    public Boolean isLocked() {
        return isLocked;
    }

    /**
     * Indique si les periodes des 2 calages se superposent
     *
     * @param calageToTest Calage avec lequel tester
     * @return True si les periodes se superposent, False sinon
     */
    public boolean checkIfPeriodOverlap(Calage calageToTest) {
        LocalDate fromCalageToTest = calageToTest.from;
        LocalDate toCalageToTest = calageToTest.to;
        // (StartDate1 <= EndDate2) and (StartDate2 <= EndDate1)
        return from.isBefore(toCalageToTest) && fromCalageToTest.isBefore(to);
    }

    /**
     * Copie des paramètres d'un calage vers un nouveau calage avec des periodes differents
     *
     * @param from Date de debut du calage
     * @param to   Date de fin du calage
     * @return Nouveau calage avec les periodes changé
     */
    public Calage copyWithNewPeriod(LocalDate from, LocalDate to) {
        return new Calage(from, to, cavity, database,
                P_3_VolumeStock, P_3_Debit, P_3_Psimul,
                P_2_VolumeStock, P_2_Debit, P_2_Psimul, P_2_DP_Arma,
                P_1_VolumeStock, P_1_Debit, P_1_Psimul, P_1_DP_Arma,
                P_0_VolumeStock, P_0_Debit, P_0_Psimul, P_0_DP_Arma,
                T_3_PArma, T_3_VolumeStock, T_3_Debit, T_3_Tsimul,
                T_2_PArma, T_2_VolumeStock, T_2_Debit, T_2_Tsimul, T_2_DP_Arma,
                T_1_PArma, T_1_VolumeStock, T_1_Debit, T_1_Tsimul,
                T_0_PArma, T_0_VolumeStock, T_0_Debit, T_0_Tsimul);
    }

    // getters

    public List<SimulatedData> getSimulatedDataList() {
        return simulatedDataList;
    }

    public LocalDate getFrom() {
        return from;
    }

    public LocalDate getTo() {
        return to;
    }

    public double getP_3_VolumeStock() {
        return P_3_VolumeStock;
    }

    public double getP_3_Debit() {
        return P_3_Debit;
    }

    public double getP_2_VolumeStock() {
        return P_2_VolumeStock;
    }

    public double getP_2_Debit() {
        return P_2_Debit;
    }

    public double getP_2_Psimul() {
        return P_2_Psimul;
    }

    public double getP_2_DP_Arma() {
        return P_2_DP_Arma;
    }

    public double getP_1_VolumeStock() {
        return P_1_VolumeStock;
    }

    public double getP_1_Debit() {
        return P_1_Debit;
    }

    public double getP_1_Psimul() {
        return P_1_Psimul;
    }

    public double getP_1_DP_Arma() {
        return P_1_DP_Arma;
    }

    public double getP_0_VolumeStock() {
        return P_0_VolumeStock;
    }

    public double getP_0_Debit() {
        return P_0_Debit;
    }

    public double getP_0_Psimul() {
        return P_0_Psimul;
    }

    public double getP_0_DP_Arma() {
        return P_0_DP_Arma;
    }

    public double getT_3_PArma() {
        return T_3_PArma;
    }

    public double getT_3_VolumeStock() {
        return T_3_VolumeStock;
    }

    public double getT_3_Debit() {
        return T_3_Debit;
    }

    public double getT_2_PArma() {
        return T_2_PArma;
    }

    public double getT_2_VolumeStock() {
        return T_2_VolumeStock;
    }

    public double getT_2_Debit() {
        return T_2_Debit;
    }

    public double getT_2_Tsimul() {
        return T_2_Tsimul;
    }

    public double getT_2_DP_Arma() {
        return T_2_DP_Arma;
    }

    public double getT_1_PArma() {
        return T_1_PArma;
    }

    public double getT_1_VolumeStock() {
        return T_1_VolumeStock;
    }

    public double getT_1_Debit() {
        return T_1_Debit;
    }

    public double getT_0_PArma() {
        return T_0_PArma;
    }

    public double getT_0_VolumeStock() {
        return T_0_VolumeStock;
    }

    public double getT_0_Debit() {
        return T_0_Debit;
    }

    public Cavity getCavity() {
        return cavity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getP_3_Psimul() {
        return P_3_Psimul;
    }

    public double getT_3_Tsimul() {
        return T_3_Tsimul;
    }

    public double getT_1_Tsimul() {
        return T_1_Tsimul;
    }

    public double getT_0_Tsimul() {
        return T_0_Tsimul;
    }

    public void setLockStatus(boolean lockStatus) {
        this.isLocked = lockStatus;
    }
}
