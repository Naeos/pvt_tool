package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.data.DataFactory;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.tools.Tools;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.answerToRequest;
import static com.noxdia.web.server.RealDataHandlers.*;

public class CavernHandlers {

    private final static SaspLogger saspLogger = SaspLogger.getInstance();
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private final Tools tools = Tools.getInstance();

    CavernHandlers() {
    }

    /*#############################################*/
    /////////// Fonctions de calculs ///////////////
    /*#############################################*/

    /**
     * Calcul de PCavern
     */
    public static double calculPCavern(double pressionCasingShoe, double temperatureCasingShoe, double tubeLength, double profondeurCavity, double densiteTotal, double tempCritique, double pressionCritique) {
        if (pressionCasingShoe == -1 || temperatureCasingShoe == -1) {
            return -1;
        }
        return pressionCasingShoe / Math.exp((-0.03415 * ((profondeurCavity - tubeLength) / 2) * densiteTotal) / (((CasingShoeHandlers.calculZFactor((temperatureCasingShoe + 273.15) / tempCritique, pressionCasingShoe * 0.1 / pressionCritique)) * (temperatureCasingShoe + 273.15))));
    }

    /**
     * Calcul de TCavern
     * <p>
     * VolumGaz = GIP
     */
    public static double calculTCavern(double pressionCavern, double pressionCasingShoe, double temperatureCasingShoe, double volumeGaz, double volumeCavity, double tempCritique, double pressionCritique) {
        if (pressionCavern == -1 || pressionCasingShoe == -1 || temperatureCasingShoe == -1) {
            return -1;
        }
        double T_0 = temperatureCasingShoe;
        double T_1 = calculT1(pressionCavern, T_0, volumeGaz, volumeCavity, tempCritique, pressionCritique);
        long startTime = System.currentTimeMillis();
        while (Math.abs(T_1 - T_0) > 0.1) {
            T_0 = (T_1 + T_0) / 2;
            T_1 = calculT1(pressionCavern, T_0, volumeGaz, volumeCavity, tempCritique, pressionCritique);

            // Evite les soucis de boucle infini qui peuvent arriver si la composition de gaz du site n'est pas bonne
            if (System.currentTimeMillis() - startTime > (15 * 1000)) {
                saspLogger.log(Level.WARNING, "Loop in CalculTCavern took more than 15s -> breaking");
                break;
            }
        }
        return T_1;
    }

    private static double calculT1(double pc, double t_0, double volumeGaz, double volumeCavity, double tempCritique, double pressionCritique) {
        return ((pc * volumeCavity) / (CasingShoeHandlers.calculZFactor((t_0 + 273.15) / tempCritique, (pc * 0.1 / pressionCritique)) * 0.000082057 * volumeGaz) * 0.000000022414) - 273.15;
    }

    /*#############################################################*/
    ////////////// Get Cavern Pression Handler /////////////////////
    /*#############################################################*/

    /**
     * Récupère la liste de la pression simulé pour PCavern
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getCavernPression(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Cavern pression request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetCavernPressionRequest(response, json, database);
        }
    }

    private void analyzeGetCavernPressionRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        if (tools.verifyEmptyOrNull(siteName, cavityName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();


        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "No cavity with name \'" + cavityName + "\' for site \'" + siteName + "\'");
            return;
        }
        Cavity cavity = optCavity.get();

        double densiteTotal = site.calculDensiteTotal();
        double masseMolaireTotal = site.calculMasseMolaireTotal();
        double tempCritique = site.calculTempCritiqueTotal();
        double pressionCritique = site.calculPressionCritiqueTotal();

        if (densiteTotal == -1 || masseMolaireTotal == -1 || tempCritique == -1 || pressionCritique == -1) {
            answerToRequest(response, 400, "No gaz for site \'" + siteName + "\'");
            return;
        }

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No realData in cavity");
            return;
        }

        realDataList.sort(tools.dataRealComparator);
        LocalDate from = realDataList.get(0).getDateInsertion();
        LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();
        createAndAnswerPressionCavern(response, realDataList, from, to, cavity, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
    }

    private void createAndAnswerPressionCavern(HttpServerResponse response, List<RealData> realDataList, LocalDate from, LocalDate to, Cavity cavity, double densiteTotal, double masseMolaireTotal, double tempCritique, double pressionCritique) {
        double tubeLength = cavity.getHauteurTubage();
        double diameter = cavity.getDiametreTubage();
        double profondeurCavity = cavity.getDepth();

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        // S'il manque des dates -> ajoute des null dans la liste renvoyé
        final int[] count = {0, 0};

        // Ajout des données incohérente dans la liste de données incohérentes
        List<RealData> incoherenceDataList = new ArrayList<>();
        realDataList.forEach(realData -> {
            if (count[1] > nbDataBeforeCanCheckIncoherence) {
                if (checkIfPressionIsIncoherente(realData, incoherenceDataList, realDataList)) {
                    incoherenceDataList.add(realData);
                }
            }
            count[1]++;
        });

        List<JsonObject> jsonObjectList = realDataList
                .stream()
                .map(realData -> {
                    JsonObject jsonObject = new JsonObject();
                    LocalDate currentDate = realData.getDateInsertion();
                    double tempWellHead = realData.getTemperatureWelHead();
                    double pressionWellHead = realData.getPressionWelHead();
                    double debit = realData.getDebit();
                    double pressionCasingShoe = CasingShoeHandlers.calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
                    double temperatureCasingShoe = CasingShoeHandlers.calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
                    double pCavern = calculPCavern(pressionCasingShoe, temperatureCasingShoe, tubeLength, profondeurCavity, densiteTotal, tempCritique, pressionCritique);
                    if (pCavern == -1) { // Check si manque date
                        jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                        jsonObject.put("isIncoherent", true);
                    } else {
                        // traitement incohérence
                        if (count[0] < nbDataBeforeCanCheckIncoherence) {
                            jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                            jsonObject.put("y", pCavern);
                            jsonObject.put("isIncoherent", false);
                        } else {
                            if (incoherenceDataList.contains(realData)) {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", pCavern);
                                jsonObject.put("isIncoherent", true);
                            } else {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", pCavern);
                                jsonObject.put("isIncoherent", false);
                            }
                        }
                        count[0]++;
                    }
                    return jsonObject;
                }).collect(Collectors.toList());

        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    ////////////// Get Cavern pression date Handler //////////////////
    /*##############################################################*/

    /**
     * Récupère la liste de la pression simulé pour PCavern entre les date 'from' et 'to'
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getCavernPressionDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Cavern pression date request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetCavernPressionDateRequest(response, json, database);
        }
    }

    private void analyzeGetCavernPressionDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        if (tools.verifyEmptyOrNull(siteName, cavityName, dateFrom, dateTo)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "No cavity with name \'" + cavityName + "\' for site \'" + siteName + "\'");
            return;
        }
        Cavity cavity = optCavity.get();

        double densiteTotal = site.calculDensiteTotal();
        double masseMolaireTotal = site.calculMasseMolaireTotal();
        double tempCritique = site.calculTempCritiqueTotal();
        double pressionCritique = site.calculPressionCritiqueTotal();

        if (densiteTotal == -1 || masseMolaireTotal == -1 || tempCritique == -1 || pressionCritique == -1) {
            answerToRequest(response, 400, "No gaz for site \'" + siteName + "\'");
            return;
        }

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No realData in cavity");
            return;
        }

        realDataList.sort(tools.dataRealComparator);
        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));

        createAndAnswerPressionCavern(response, realDataList, from, to, cavity, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
    }

    /*##############################################################*/
    //////////// Get Cavern Temperature date Handler /////////////////
    /*##############################################################*/

    /**
     * Récupère la liste de la pression simulé pour TCavern
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la temperature à afficher pour le graphe
     */
    void getCavernTemperature(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Cavern temperature request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetCavernTemperatureRequest(response, json, database);
        }
    }

    private void analyzeGetCavernTemperatureRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        if (tools.verifyEmptyOrNull(siteName, cavityName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "No cavity with name \'" + cavityName + "\' for site \'" + siteName + "\'");
            return;
        }
        Cavity cavity = optCavity.get();

        double densiteTotal = site.calculDensiteTotal();
        double masseMolaireTotal = site.calculMasseMolaireTotal();
        double tempCritique = site.calculTempCritiqueTotal();
        double pressionCritique = site.calculPressionCritiqueTotal();

        if (densiteTotal == -1 || masseMolaireTotal == -1 || tempCritique == -1 || pressionCritique == -1) {
            answerToRequest(response, 400, "No gaz for site \'" + siteName + "\'");
            return;
        }

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No realData in cavity");
            return;
        }

        realDataList.sort(tools.dataRealComparator);
        LocalDate from = realDataList.get(0).getDateInsertion();
        LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();
        createAndAnswerTemperatureCavern(response, realDataList, from, to, cavity, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
    }

    private void createAndAnswerTemperatureCavern(HttpServerResponse response, List<RealData> realDataList, LocalDate from, LocalDate to, Cavity cavity, double densiteTotal, double masseMolaireTotal, double tempCritique, double pressionCritique) {
        double tubeLength = cavity.getHauteurTubage();
        double diameter = cavity.getDiametreTubage();
        double volumeCavity = cavity.getVolume();
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        // S'il manque des dates -> ajoute des null dans la liste renvoyé
        final int[] count = {0, 0};

        // Ajout des données incohérente dans la liste de données incohérentes
        List<RealData> incoherenceDataList = new ArrayList<>();
        realDataList.forEach(realData -> {
            if (count[1] > nbDataBeforeCanCheckIncoherence) {
                if (realData.getDebit() == 0 && checkIfTemperatureIsIncoherente(realData, incoherenceDataList, realDataList)) {
                    incoherenceDataList.add(realData);
                }
            }
            count[1]++;
        });

        double profondeurCavity = cavity.getDepth();
        List<JsonObject> jsonObjectList = realDataList
                .stream()
                .map(realData -> {
                    JsonObject jsonObject = new JsonObject();
                    LocalDate currentDate = realData.getDateInsertion();

                    double tempWellHead = realData.getTemperatureWelHead();
                    double pressionWellHead = realData.getPressionWelHead();
                    double debit = realData.getDebit();
                    double pressionCasingShoe = CasingShoeHandlers.calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
                    double temperatureCasingShoe = CasingShoeHandlers.calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
                    double pressionCavern = calculPCavern(pressionCasingShoe, temperatureCasingShoe, tubeLength, profondeurCavity, densiteTotal, tempCritique, pressionCritique);

                    double volumeGaz = realData.getVolume();
                    double tCavern = calculTCavern(pressionCavern, pressionCasingShoe, temperatureCasingShoe, volumeGaz, volumeCavity, tempCritique, pressionCritique);

                    if (tCavern == -1) { // Check si manque date
                        jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                        jsonObject.put("isIncoherent", true);
                    } else {
                        // traitement incohérence
                        if (count[0] < nbDataBeforeCanCheckIncoherence) {
                            jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                            jsonObject.put("y", tCavern);
                            jsonObject.put("isIncoherent", false);
                        } else {
                            if (incoherenceDataList.contains(realData)) {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", tCavern);
                                jsonObject.put("isIncoherent", true);
                            } else {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", tCavern);
                                jsonObject.put("isIncoherent", false);
                            }
                        }
                        count[0]++;
                    }
                    return jsonObject;
                }).collect(Collectors.toList());

        answerToRequest(response, 200, jsonObjectList);
    }

    /*##########################################################*/
    //////////// Get Cavern Temperature Handler /////////////////
    /*##########################################################*/

    /**
     * Récupère la liste de la pression simulé pour TCavern entre les date 'from' et 'to'
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la temperature à afficher pour le graphe
     */
    void getCavernTemperatureDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Cavern temperature date request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetCavernTemperatureDateRequest(response, json, database);
        }
    }

    private void analyzeGetCavernTemperatureDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        if (tools.verifyEmptyOrNull(siteName, cavityName, dateFrom, dateTo)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();


        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "No cavity with name \'" + cavityName + "\' for site \'" + siteName + "\'");
            return;
        }
        Cavity cavity = optCavity.get();

        double densiteTotal = site.calculDensiteTotal();
        double masseMolaireTotal = site.calculMasseMolaireTotal();
        double tempCritique = site.calculTempCritiqueTotal();
        double pressionCritique = site.calculPressionCritiqueTotal();

        if (densiteTotal == -1 || masseMolaireTotal == -1 || tempCritique == -1 || pressionCritique == -1) {
            answerToRequest(response, 400, "No gaz for site \'" + siteName + "\'");
            return;
        }

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No realData in cavity");
            return;
        }

        realDataList.sort(tools.dataRealComparator);
        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));
        createAndAnswerTemperatureCavern(response, realDataList, from, to, cavity, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
    }


}
