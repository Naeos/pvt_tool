package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.calage.CalageFactory;
import com.noxdia.tools.Tools;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.answerToRequest;

class CalageHandlers {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private final DecimalFormat df = new DecimalFormat("0.##");
    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    CalageHandlers() {
    }

    /*##############################################################*/
    ////////////// Get list calage for Cavity Handler ////////////////
    /*##############################################################*/

    /**
     * Recupère la liste des calages dans la base de données pour la cavité ayant pour nom "CavityName"
     * Cette information est envoyé au format JSON par le client
     * Renvoie la liste des calages récupéré
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Si le nom de la cavité est incorrect
     */
    void getListCalageForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get list calage for cavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetListCalageForCavityRequest(response, json, database);
        }
    }

    private void analyzeGetListCalageForCavityRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cannot find cavity \'" + cavityName + "\' in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<Calage> calageList = database.getListCalageForCavity(cavity);

        List<JsonObject> jsonObjectList = calageList.stream().map(calage -> {
            int id = calage.getId();
            LocalDate dateFrom = calage.getFrom();
            LocalDate dateTo = calage.getTo();

            calage.simulateTemperature();

            double ecartTypeTemp = calage.calculEcartTypeTemperature();
            double deviationMinTemp = calage.calculDeviationMinTemperature();
            double deviationMaxTemp = calage.calculDeviationMaxTemperature();
            double sommeEcartsQuadratiqueTemp = calage.calculSommeEcartsQuadratiqueTemperature();
            double deviationMoyenneTemp = calage.calculDeviationMoyenneTemperature();

            double ecartTypePression = calage.calculEcartTypePression();
            double deviationMinPression = calage.calculDeviationMinPression();
            double deviationMaxPression = calage.calculDeviationMaxPression();
            double sommeEcartsQuadratiquePression = calage.calculSommeEcartsQuadratiquePression();
            double deviationMoyennePression = calage.calculDeviationMoyennePression();

            double p_3_volumeStock = calage.getP_3_VolumeStock();
            double p_3_debit = calage.getP_3_Debit();
            double p_3_psimul = calage.getP_3_Psimul();

            double p_2_volumeStock = calage.getP_2_VolumeStock();
            double p_2_debit = calage.getP_2_Debit();
            double p_2_psimul = calage.getP_2_Psimul();
            double p_2_dp_arma = calage.getP_2_DP_Arma();

            double p_1_volumeStock = calage.getP_1_VolumeStock();
            double p_1_debit = calage.getP_1_Debit();
            double p_1_psimul = calage.getP_1_Psimul();
            double p_1_dp_arma = calage.getP_1_DP_Arma();

            double p_0_volumeStock = calage.getP_0_VolumeStock();
            double p_0_debit = calage.getP_0_Debit();
            double p_0_psimul = calage.getP_0_Psimul();
            double p_0_dp_arma = calage.getP_0_DP_Arma();

            double t_3_pArma = calage.getT_3_PArma();
            double t_3_volumeStock = calage.getT_3_VolumeStock();
            double t_3_debit = calage.getT_3_Debit();
            double t_3_tsimul = calage.getT_3_Tsimul();

            double t_2_pArma = calage.getT_2_PArma();
            double t_2_volumeStock = calage.getT_2_VolumeStock();
            double t_2_debit = calage.getT_2_Debit();
            double t_2_tsimul = calage.getT_2_Tsimul();
            double t_2_dp_arma = calage.getT_2_DP_Arma();

            double t_1_pArma = calage.getT_1_PArma();
            double t_1_volumeStock = calage.getT_1_VolumeStock();
            double t_1_debit = calage.getT_1_Debit();
            double t_1_tsimul = calage.getT_1_Tsimul();

            double t_0_pArma = calage.getT_0_PArma();
            double t_0_volumeStock = calage.getT_0_VolumeStock();
            double t_0_debit = calage.getT_0_Debit();
            double t_0_tsimul = calage.getT_0_Tsimul();

            // Ajout des valeurs dans l'objet json a envoyer
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("id", id);

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            jsonObject.put("dateFrom", dtf.format(dateFrom));
            jsonObject.put("dateTo", dtf.format(dateTo));
            jsonObject.put("isLocked", calage.isLocked());

            jsonObject.put("standardDeviationTemp", df.format(ecartTypeTemp));
            jsonObject.put("minDeviationTemp", df.format(deviationMinTemp));
            jsonObject.put("maxDeviationTemp", df.format(deviationMaxTemp));
            jsonObject.put("sommeEcartsQuadratiquesTemp", df.format(sommeEcartsQuadratiqueTemp));
            jsonObject.put("moyDeviationTemp", df.format(deviationMoyenneTemp));

            jsonObject.put("standardDeviationPression", df.format(ecartTypePression));
            jsonObject.put("minDeviationPression", df.format(deviationMinPression));
            jsonObject.put("maxDeviationPression", df.format(deviationMaxPression));
            jsonObject.put("sommeEcartsQuadratiquesPression", df.format(sommeEcartsQuadratiquePression));
            jsonObject.put("moyDeviationPression", df.format(deviationMoyennePression));

            jsonObject.put("p_3_volumeStock", p_3_volumeStock);
            jsonObject.put("p_3_debit", p_3_debit);
            jsonObject.put("p_3_psimul", p_3_psimul);

            jsonObject.put("p_2_volumeStock", p_2_volumeStock);
            jsonObject.put("p_2_debit", p_2_debit);
            jsonObject.put("p_2_psimul", p_2_psimul);
            jsonObject.put("p_2_dp_arma", p_2_dp_arma);

            jsonObject.put("p_1_volumeStock", p_1_volumeStock);
            jsonObject.put("p_1_debit", p_1_debit);
            jsonObject.put("p_1_psimul", p_1_psimul);
            jsonObject.put("p_1_dp_arma", p_1_dp_arma);

            jsonObject.put("p_0_volumeStock", p_0_volumeStock);
            jsonObject.put("p_0_debit", p_0_debit);
            jsonObject.put("p_0_psimul", p_0_psimul);
            jsonObject.put("p_0_dp_arma", p_0_dp_arma);

            jsonObject.put("t_3_pArma", t_3_pArma);
            jsonObject.put("t_3_volumeStock", t_3_volumeStock);
            jsonObject.put("t_3_debit", t_3_debit);
            jsonObject.put("t_3_tsimul", t_3_tsimul);

            jsonObject.put("t_2_pArma", t_2_pArma);
            jsonObject.put("t_2_volumeStock", t_2_volumeStock);
            jsonObject.put("t_2_debit", t_2_debit);
            jsonObject.put("t_2_tsimul", t_2_tsimul);
            jsonObject.put("t_2_dp_arma", t_2_dp_arma);

            jsonObject.put("t_1_pArma", t_1_pArma);
            jsonObject.put("t_1_volumeStock", t_1_volumeStock);
            jsonObject.put("t_1_debit", t_1_debit);
            jsonObject.put("t_1_tsimul", t_1_tsimul);

            jsonObject.put("t_0_pArma", t_0_pArma);
            jsonObject.put("t_0_volumeStock", t_0_volumeStock);
            jsonObject.put("t_0_debit", t_0_debit);
            jsonObject.put("t_0_tsimul", t_0_tsimul);

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            List<JsonObject> dataTemperatureReel = generateJsonDataTemperatureReel(database, cavity, calage);
            jsonObject.put("dataTempReel", dataTemperatureReel);

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            List<JsonObject> dataTemperatureSimul = generateJsonDataTemperatureSimul(calage);
            jsonObject.put("dataTempSimul", dataTemperatureSimul);

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            List<JsonObject> dataPressionReel = generateJsonDataPressionReel(database, cavity, calage);
            jsonObject.put("dataPressionReel", dataPressionReel);

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            List<JsonObject> dataPressionSimul = generateJsonDataPressionSimul(calage);
            jsonObject.put("dataPressionSimul", dataPressionSimul);

            return jsonObject;
        }).collect(Collectors.toList());

        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    ///////////////// Simulate for Cavity Handler ////////////////////
    /*##############################################################*/

    /**
     * Fait les simulation pour la cavité donné, avec les informations envoyé par le client,
     * Et renvoie les données pour affiché les courbes
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Erreur lors de la simulation
     */
    void simulateForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In simulate temperature request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeSimulateForCavityRequest(response, json, database);
        }
    }

    private void analyzeSimulateForCavityRequest(HttpServerResponse response, JsonObject json, Database database) {
        String dateFromString = json.getString("DateFrom");
        String dateToString = json.getString("DateTo");
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        JsonArray coefPression = json.getJsonArray("CoefPression");
        JsonArray coefTemperature = json.getJsonArray("CoefTemperature");

        if (tools.verifyEmptyOrNull(dateFromString, dateToString, cavityName, siteName) ||
                coefPression == null || coefPression.isEmpty() ||
                coefTemperature == null || coefTemperature.isEmpty()) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        String p_3_volumeStockString = coefPression.getString(0);
        String p_2_volumeStockString = coefPression.getString(3);
        String p_1_volumeStockString = coefPression.getString(7);
        String p_0_volumeStockString = coefPression.getString(11);

        String p_3_debitString = coefPression.getString(1);
        String p_2_debitString = coefPression.getString(4);
        String p_1_debitString = coefPression.getString(8);
        String p_0_debitString = coefPression.getString(12);

        String p_3_PSimulString = coefPression.getString(2);
        String p_2_PSimulString = coefPression.getString(5);
        String p_1_PSimulString = coefPression.getString(9);
        String p_0_PSimulString = coefPression.getString(13);

        String p_2_dp_armaString = coefPression.getString(6);
        String p_1_dp_armaString = coefPression.getString(10);
        String p_0_dp_armaString = coefPression.getString(14);

        String t_3_pArmaString = coefTemperature.getString(0);
        String t_2_pArmaString = coefTemperature.getString(4);
        String t_1_pArmaString = coefTemperature.getString(9);
        String t_0_pArmaString = coefTemperature.getString(13);

        String t_3_volumeStockString = coefTemperature.getString(1);
        String t_2_volumeStockString = coefTemperature.getString(5);
        String t_1_volumeStockString = coefTemperature.getString(10);
        String t_0_volumeStockString = coefTemperature.getString(14);

        String t_3_debitString = coefTemperature.getString(2);
        String t_2_debitString = coefTemperature.getString(6);
        String t_1_debitString = coefTemperature.getString(11);
        String t_0_debitString = coefTemperature.getString(15);

        String t_3_TsimulString = coefTemperature.getString(3);
        String t_2_TsimulString = coefTemperature.getString(7);
        String t_1_TsimulString = coefTemperature.getString(12);
        String t_0_TsimulString = coefTemperature.getString(16);

        String t_2_dpArmaString = coefTemperature.getString(8);

        if (tools.verifyEmptyOrNull(p_3_volumeStockString, p_2_volumeStockString, p_1_volumeStockString, p_0_volumeStockString,
                p_3_debitString, p_2_debitString, p_1_debitString, p_0_debitString,
                p_3_PSimulString, p_2_PSimulString, p_1_PSimulString, p_0_PSimulString,
                p_2_dp_armaString, p_1_dp_armaString, p_0_dp_armaString,
                t_3_pArmaString, t_2_pArmaString, t_1_pArmaString, t_0_pArmaString,
                t_3_volumeStockString, t_2_volumeStockString, t_1_volumeStockString, t_0_volumeStockString,
                t_3_debitString, t_2_debitString, t_1_debitString, t_0_debitString,
                t_3_TsimulString, t_2_TsimulString, t_1_TsimulString, t_0_TsimulString,
                t_2_dpArmaString
        )) {
            answerToRequest(response, 400, "Missing input data in array");
            return;
        }

        LocalDate from = LocalDate.from(dtf.parse(dateFromString));
        LocalDate to = LocalDate.from(dtf.parse(dateToString));

        double p_3_volumeStock = Double.parseDouble(p_3_volumeStockString);
        double p_2_volumeStock = Double.parseDouble(p_2_volumeStockString);
        double p_1_volumeStock = Double.parseDouble(p_1_volumeStockString);
        double p_0_volumeStock = Double.parseDouble(p_0_volumeStockString);

        double p_3_debit = Double.parseDouble(p_3_debitString);
        double p_2_debit = Double.parseDouble(p_2_debitString);
        double p_1_debit = Double.parseDouble(p_1_debitString);
        double p_0_debit = Double.parseDouble(p_0_debitString);

        double p_3_PSimul = Double.parseDouble(p_3_PSimulString);
        double p_2_PSimul = Double.parseDouble(p_2_PSimulString);
        double p_1_PSimul = Double.parseDouble(p_1_PSimulString);
        double p_0_PSimul = Double.parseDouble(p_0_PSimulString);

        double p_2_dp_arma = Double.parseDouble(p_2_dp_armaString);
        double p_1_dp_arma = Double.parseDouble(p_1_dp_armaString);
        double p_0_dp_arma = Double.parseDouble(p_0_dp_armaString);

        double t_3_pArma = Double.parseDouble(t_3_pArmaString);
        double t_2_pArma = Double.parseDouble(t_2_pArmaString);
        double t_1_pArma = Double.parseDouble(t_1_pArmaString);
        double t_0_pArma = Double.parseDouble(t_0_pArmaString);

        double t_3_volumeStock = Double.parseDouble(t_3_volumeStockString);
        double t_2_volumeStock = Double.parseDouble(t_2_volumeStockString);
        double t_1_volumeStock = Double.parseDouble(t_1_volumeStockString);
        double t_0_volumeStock = Double.parseDouble(t_0_volumeStockString);

        double t_3_debit = Double.parseDouble(t_3_debitString);
        double t_2_debit = Double.parseDouble(t_2_debitString);
        double t_1_debit = Double.parseDouble(t_1_debitString);
        double t_0_debit = Double.parseDouble(t_0_debitString);

        double t_3_Tsimul = Double.parseDouble(t_3_TsimulString);
        double t_2_Tsimul = Double.parseDouble(t_2_TsimulString);
        double t_1_Tsimul = Double.parseDouble(t_1_TsimulString);
        double t_0_Tsimul = Double.parseDouble(t_0_TsimulString);

        double t_2_dpArma = Double.parseDouble(t_2_dpArmaString);

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cannot find cavity \'" + cavityName + "\' in database");
            return;
        }
        Cavity cavity = optCavity.get();

        Calage calage = CalageFactory.createCalage(from, to, cavity, database,
                p_3_volumeStock, p_3_debit, p_3_PSimul,
                p_2_volumeStock, p_2_debit, p_2_PSimul, p_2_dp_arma,
                p_1_volumeStock, p_1_debit, p_1_PSimul, p_1_dp_arma,
                p_0_volumeStock, p_0_debit, p_0_PSimul, p_0_dp_arma,
                t_3_pArma, t_3_volumeStock, t_3_debit, t_3_Tsimul,
                t_2_pArma, t_2_volumeStock, t_2_debit, t_2_Tsimul, t_2_dpArma,
                t_1_pArma, t_1_volumeStock, t_1_debit, t_1_Tsimul,
                t_0_pArma, t_0_volumeStock, t_0_debit, t_0_Tsimul);

        if (!calage.simulateTemperature()) {
            answerToRequest(response, 400, "Error while simulating");
            return;
        }

        double ecartTypeTemp = calage.calculEcartTypeTemperature();
        double deviationMinTemp = calage.calculDeviationMinTemperature();
        double deviationMaxTemp = calage.calculDeviationMaxTemperature();
        double sommeEcartsQuadratiqueTemp = calage.calculSommeEcartsQuadratiqueTemperature();
        double deviationMoyenneTemp = calage.calculDeviationMoyenneTemperature();

        double ecartTypePression = calage.calculEcartTypePression();
        double deviationMinPression = calage.calculDeviationMinPression();
        double deviationMaxPression = calage.calculDeviationMaxPression();
        double sommeEcartsQuadratiquePression = calage.calculSommeEcartsQuadratiquePression();
        double deviationMoyennePression = calage.calculDeviationMoyennePression();

        // Ajout des valeurs dans l'objet json a envoyer
        JsonObject jsonObject = new JsonObject();

        jsonObject.put("standardDeviationTemp", df.format(ecartTypeTemp));
        jsonObject.put("minDeviationTemp", df.format(deviationMinTemp));
        jsonObject.put("maxDeviationTemp", df.format(deviationMaxTemp));
        jsonObject.put("sommeEcartsQuadratiquesTemp", df.format(sommeEcartsQuadratiqueTemp));
        jsonObject.put("moyDeviationTemp", df.format(deviationMoyenneTemp));

        jsonObject.put("standardDeviationPression", df.format(ecartTypePression));
        jsonObject.put("minDeviationPression", df.format(deviationMinPression));
        jsonObject.put("maxDeviationPression", df.format(deviationMaxPression));
        jsonObject.put("sommeEcartsQuadratiquesPression", df.format(sommeEcartsQuadratiquePression));
        jsonObject.put("moyDeviationPression", df.format(deviationMoyennePression));

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        List<JsonObject> dataTemperatureReel = generateJsonDataTemperatureReel(database, cavity, calage);
        jsonObject.put("dataTempReel", dataTemperatureReel);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        List<JsonObject> dataTemperatureSimul = generateJsonDataTemperatureSimul(calage);
        jsonObject.put("dataTempSimul", dataTemperatureSimul);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        List<JsonObject> dataPressionReel = generateJsonDataPressionReel(database, cavity, calage);
        jsonObject.put("dataPressionReel", dataPressionReel);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        List<JsonObject> dataPressionSimul = generateJsonDataPressionSimul(calage);
        jsonObject.put("dataPressionSimul", dataPressionSimul);
        answerToRequest(response, 200, jsonObject);
    }

    /*##############################################################*/
    ////////////////////// Add calage Handler ////////////////////////
    /*##############################################################*/

    /**
     * Ajoute un calage dans la base données en prenant tous les paramètres envoyé par le client, avec le format JSON
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     */
    void addCalage(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In add calage request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeAddCalageRequest(response, json, database);
        }
    }

    private void analyzeAddCalageRequest(HttpServerResponse response, JsonObject json, Database database) {
        String dateFromString = json.getString("DateFrom");
        String dateToString = json.getString("DateTo");
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        boolean isLocked = json.getBoolean("IsChecked");
        JsonArray coefPression = json.getJsonArray("CoefPression");
        JsonArray coefTemperature = json.getJsonArray("CoefTemperature");

        if (tools.verifyEmptyOrNull(dateFromString, dateToString, cavityName, siteName) ||
                coefPression == null || coefPression.isEmpty() ||
                coefTemperature == null || coefTemperature.isEmpty()) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        String p_3_volumeStockString = coefPression.getString(0);
        String p_2_volumeStockString = coefPression.getString(3);
        String p_1_volumeStockString = coefPression.getString(7);
        String p_0_volumeStockString = coefPression.getString(11);

        String p_3_debitString = coefPression.getString(1);
        String p_2_debitString = coefPression.getString(4);
        String p_1_debitString = coefPression.getString(8);
        String p_0_debitString = coefPression.getString(12);

        String p_3_PSimulString = coefPression.getString(2);
        String p_2_PSimulString = coefPression.getString(5);
        String p_1_PSimulString = coefPression.getString(9);
        String p_0_PSimulString = coefPression.getString(13);

        String p_2_dp_armaString = coefPression.getString(6);
        String p_1_dp_armaString = coefPression.getString(10);
        String p_0_dp_armaString = coefPression.getString(14);

        String t_3_pArmaString = coefTemperature.getString(0);
        String t_2_pArmaString = coefTemperature.getString(4);
        String t_1_pArmaString = coefTemperature.getString(9);
        String t_0_pArmaString = coefTemperature.getString(13);

        String t_3_volumeStockString = coefTemperature.getString(1);
        String t_2_volumeStockString = coefTemperature.getString(5);
        String t_1_volumeStockString = coefTemperature.getString(10);
        String t_0_volumeStockString = coefTemperature.getString(14);

        String t_3_debitString = coefTemperature.getString(2);
        String t_2_debitString = coefTemperature.getString(6);
        String t_1_debitString = coefTemperature.getString(11);
        String t_0_debitString = coefTemperature.getString(15);

        String t_3_TsimulString = coefTemperature.getString(3);
        String t_2_TsimulString = coefTemperature.getString(7);
        String t_1_TsimulString = coefTemperature.getString(12);
        String t_0_TsimulString = coefTemperature.getString(16);

        String t_2_dpArmaString = coefTemperature.getString(8);


        if (tools.verifyEmptyOrNull(p_3_volumeStockString, p_2_volumeStockString, p_1_volumeStockString, p_0_volumeStockString,
                p_3_debitString, p_2_debitString, p_1_debitString, p_0_debitString,
                p_3_PSimulString, p_2_PSimulString, p_1_PSimulString, p_0_PSimulString,
                p_2_dp_armaString, p_1_dp_armaString, p_0_dp_armaString,
                t_3_pArmaString, t_2_pArmaString, t_1_pArmaString, t_0_pArmaString,
                t_3_volumeStockString, t_2_volumeStockString, t_1_volumeStockString, t_0_volumeStockString,
                t_3_debitString, t_2_debitString, t_1_debitString, t_0_debitString,
                t_3_TsimulString, t_2_TsimulString, t_1_TsimulString, t_0_TsimulString,
                t_2_dpArmaString
        )) {
            answerToRequest(response, 400, "Missing input data in array");
            return;
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate from = LocalDate.from(dtf.parse(dateFromString));
        LocalDate to = LocalDate.from(dtf.parse(dateToString));

        double p_3_volumeStock = Double.parseDouble(p_3_volumeStockString);
        double p_2_volumeStock = Double.parseDouble(p_2_volumeStockString);
        double p_1_volumeStock = Double.parseDouble(p_1_volumeStockString);
        double p_0_volumeStock = Double.parseDouble(p_0_volumeStockString);

        double p_3_debit = Double.parseDouble(p_3_debitString);
        double p_2_debit = Double.parseDouble(p_2_debitString);
        double p_1_debit = Double.parseDouble(p_1_debitString);
        double p_0_debit = Double.parseDouble(p_0_debitString);

        double p_3_PSimul = Double.parseDouble(p_3_PSimulString);
        double p_2_PSimul = Double.parseDouble(p_2_PSimulString);
        double p_1_PSimul = Double.parseDouble(p_1_PSimulString);
        double p_0_PSimul = Double.parseDouble(p_0_PSimulString);

        double p_2_dp_arma = Double.parseDouble(p_2_dp_armaString);
        double p_1_dp_arma = Double.parseDouble(p_1_dp_armaString);
        double p_0_dp_arma = Double.parseDouble(p_0_dp_armaString);

        double t_3_pArma = Double.parseDouble(t_3_pArmaString);
        double t_2_pArma = Double.parseDouble(t_2_pArmaString);
        double t_1_pArma = Double.parseDouble(t_1_pArmaString);
        double t_0_pArma = Double.parseDouble(t_0_pArmaString);

        double t_3_volumeStock = Double.parseDouble(t_3_volumeStockString);
        double t_2_volumeStock = Double.parseDouble(t_2_volumeStockString);
        double t_1_volumeStock = Double.parseDouble(t_1_volumeStockString);
        double t_0_volumeStock = Double.parseDouble(t_0_volumeStockString);

        double t_3_debit = Double.parseDouble(t_3_debitString);
        double t_2_debit = Double.parseDouble(t_2_debitString);
        double t_1_debit = Double.parseDouble(t_1_debitString);
        double t_0_debit = Double.parseDouble(t_0_debitString);

        double t_3_Tsimul = Double.parseDouble(t_3_TsimulString);
        double t_2_Tsimul = Double.parseDouble(t_2_TsimulString);
        double t_1_Tsimul = Double.parseDouble(t_1_TsimulString);
        double t_0_Tsimul = Double.parseDouble(t_0_TsimulString);

        double t_2_dpArma = Double.parseDouble(t_2_dpArmaString);

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cannot find cavity \'" + cavityName + "\' in database");
            return;
        }
        Cavity cavity = optCavity.get();

        Calage calage = CalageFactory.createCalage(from, to, cavity, database,
                p_3_volumeStock, p_3_debit, p_3_PSimul,
                p_2_volumeStock, p_2_debit, p_2_PSimul, p_2_dp_arma,
                p_1_volumeStock, p_1_debit, p_1_PSimul, p_1_dp_arma,
                p_0_volumeStock, p_0_debit, p_0_PSimul, p_0_dp_arma,
                t_3_pArma, t_3_volumeStock, t_3_debit, t_3_Tsimul,
                t_2_pArma, t_2_volumeStock, t_2_debit, t_2_Tsimul, t_2_dpArma,
                t_1_pArma, t_1_volumeStock, t_1_debit, t_1_Tsimul,
                t_0_pArma, t_0_volumeStock, t_0_debit, t_0_Tsimul);
        calage.insertInDb();

        if (isLocked) {
            List<Calage> calageList = database.getListCalageForCavity(cavity);
            calageList.forEach(calageToTest -> {
                // Verifier les dates pour chaques calage et si date coincide avec celle qu'on veut inserer
                // Passer le locked ( des dates qui coincide ) à false
                if (calage.checkIfPeriodOverlap(calageToTest)) {
                    calageToTest.unlock();
                }
            });
            calage.lock();
        } else {
            calage.unlock();
        }
        answerToRequest(response, 200, "Calage correctly created");
    }

    /*##############################################################*/
    /////////////////// Delete calage Handler ////////////////////////
    /*##############################################################*/

    /**
     * Supprime un calage dans la base données en prenant l'ID que le client envoie au format JSON
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Calage introuvable dans la base de donnée
     */
    void deleteCalage(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In delete calage request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeDeleteCalageRequest(response, json, database);
        }
    }

    private void analyzeDeleteCalageRequest(HttpServerResponse response, JsonObject json, Database database) {
        String idString = json.getString("Id");
        if (tools.verifyEmptyOrNull(idString)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }
        int id = Integer.parseInt(idString);
        // Verifier qu'un callage existe avec cet ID , si oui , le supprimer de la bdd
        boolean isThereCallageForId = database.calageExistForId(id);
        if (!isThereCallageForId) {
            answerToRequest(response, 400, "Can't find calage with id : \'" + id + "\'");
            return;
        }
        database.deleteCalageNarma(id);
        answerToRequest(response, 200, "Calage with \'" + id + "\' deleted");
    }

    /*##############################################################*/
    //////////////////// Update calage Handler ///////////////////////
    /*##############################################################*/

    /**
     * Met à jour un calage dans la base données en prenant tous les paramètres envoyé par le client, avec le format JSON
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité introuvable dans la base de données
     * - Calage introuvable dans la base de données
     */
    void updateCalage(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In update calage request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeUpdateCalageRequest(response, json, database);
        }
    }

    private void analyzeUpdateCalageRequest(HttpServerResponse response, JsonObject json, Database database) {
        String idString = json.getString("Id");
        String dateFromString = json.getString("DateFrom");
        String dateToString = json.getString("DateTo");
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        boolean isLockedChecked = json.getBoolean("IsChecked");
        JsonArray coefPression = json.getJsonArray("CoefPression");
        JsonArray coefTemperature = json.getJsonArray("CoefTemperature");

        if (tools.verifyEmptyOrNull(idString, dateFromString, dateToString, cavityName, siteName) ||
                coefPression == null || coefPression.isEmpty() ||
                coefTemperature == null || coefTemperature.isEmpty()) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        String p_3_volumeStockString = coefPression.getString(0);
        String p_2_volumeStockString = coefPression.getString(3);
        String p_1_volumeStockString = coefPression.getString(7);
        String p_0_volumeStockString = coefPression.getString(11);

        String p_3_debitString = coefPression.getString(1);
        String p_2_debitString = coefPression.getString(4);
        String p_1_debitString = coefPression.getString(8);
        String p_0_debitString = coefPression.getString(12);

        String p_3_PSimulString = coefPression.getString(2);
        String p_2_PSimulString = coefPression.getString(5);
        String p_1_PSimulString = coefPression.getString(9);
        String p_0_PSimulString = coefPression.getString(13);

        String p_2_dp_armaString = coefPression.getString(6);
        String p_1_dp_armaString = coefPression.getString(10);
        String p_0_dp_armaString = coefPression.getString(14);

        String t_3_pArmaString = coefTemperature.getString(0);
        String t_2_pArmaString = coefTemperature.getString(4);
        String t_1_pArmaString = coefTemperature.getString(9);
        String t_0_pArmaString = coefTemperature.getString(13);

        String t_3_volumeStockString = coefTemperature.getString(1);
        String t_2_volumeStockString = coefTemperature.getString(5);
        String t_1_volumeStockString = coefTemperature.getString(10);
        String t_0_volumeStockString = coefTemperature.getString(14);

        String t_3_debitString = coefTemperature.getString(2);
        String t_2_debitString = coefTemperature.getString(6);
        String t_1_debitString = coefTemperature.getString(11);
        String t_0_debitString = coefTemperature.getString(15);

        String t_3_TsimulString = coefTemperature.getString(3);
        String t_2_TsimulString = coefTemperature.getString(7);
        String t_1_TsimulString = coefTemperature.getString(12);
        String t_0_TsimulString = coefTemperature.getString(16);

        String t_2_dpArmaString = coefTemperature.getString(8);

        if (tools.verifyEmptyOrNull(p_3_volumeStockString, p_2_volumeStockString, p_1_volumeStockString, p_0_volumeStockString,
                p_3_debitString, p_2_debitString, p_1_debitString, p_0_debitString,
                p_3_PSimulString, p_2_PSimulString, p_1_PSimulString, p_0_PSimulString,
                p_2_dp_armaString, p_1_dp_armaString, p_0_dp_armaString,
                t_3_pArmaString, t_2_pArmaString, t_1_pArmaString, t_0_pArmaString,
                t_3_volumeStockString, t_2_volumeStockString, t_1_volumeStockString, t_0_volumeStockString,
                t_3_debitString, t_2_debitString, t_1_debitString, t_0_debitString,
                t_3_TsimulString, t_2_TsimulString, t_1_TsimulString, t_0_TsimulString,
                t_2_dpArmaString
        )) {
            answerToRequest(response, 400, "Missing input data in array");
            return;
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate from = LocalDate.from(dtf.parse(dateFromString));
        LocalDate to = LocalDate.from(dtf.parse(dateToString));

        double p_3_volumeStock = Double.parseDouble(p_3_volumeStockString);
        double p_2_volumeStock = Double.parseDouble(p_2_volumeStockString);
        double p_1_volumeStock = Double.parseDouble(p_1_volumeStockString);
        double p_0_volumeStock = Double.parseDouble(p_0_volumeStockString);

        double p_3_debit = Double.parseDouble(p_3_debitString);
        double p_2_debit = Double.parseDouble(p_2_debitString);
        double p_1_debit = Double.parseDouble(p_1_debitString);
        double p_0_debit = Double.parseDouble(p_0_debitString);

        double p_3_PSimul = Double.parseDouble(p_3_PSimulString);
        double p_2_PSimul = Double.parseDouble(p_2_PSimulString);
        double p_1_PSimul = Double.parseDouble(p_1_PSimulString);
        double p_0_PSimul = Double.parseDouble(p_0_PSimulString);

        double p_2_dp_arma = Double.parseDouble(p_2_dp_armaString);
        double p_1_dp_arma = Double.parseDouble(p_1_dp_armaString);
        double p_0_dp_arma = Double.parseDouble(p_0_dp_armaString);

        double t_3_pArma = Double.parseDouble(t_3_pArmaString);
        double t_2_pArma = Double.parseDouble(t_2_pArmaString);
        double t_1_pArma = Double.parseDouble(t_1_pArmaString);
        double t_0_pArma = Double.parseDouble(t_0_pArmaString);

        double t_3_volumeStock = Double.parseDouble(t_3_volumeStockString);
        double t_2_volumeStock = Double.parseDouble(t_2_volumeStockString);
        double t_1_volumeStock = Double.parseDouble(t_1_volumeStockString);
        double t_0_volumeStock = Double.parseDouble(t_0_volumeStockString);

        double t_3_debit = Double.parseDouble(t_3_debitString);
        double t_2_debit = Double.parseDouble(t_2_debitString);
        double t_1_debit = Double.parseDouble(t_1_debitString);
        double t_0_debit = Double.parseDouble(t_0_debitString);

        double t_3_Tsimul = Double.parseDouble(t_3_TsimulString);
        double t_2_Tsimul = Double.parseDouble(t_2_TsimulString);
        double t_1_Tsimul = Double.parseDouble(t_1_TsimulString);
        double t_0_Tsimul = Double.parseDouble(t_0_TsimulString);

        double t_2_dpArma = Double.parseDouble(t_2_dpArmaString);

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cannot find cavity \'" + cavityName + "\' in database");
            return;
        }

        Cavity cavity = optCavity.get();

        Calage calageTempo = CalageFactory.createCalage(from, to, cavity, database,
                p_3_volumeStock, p_3_debit, p_3_PSimul,
                p_2_volumeStock, p_2_debit, p_2_PSimul, p_2_dp_arma,
                p_1_volumeStock, p_1_debit, p_1_PSimul, p_1_dp_arma,
                p_0_volumeStock, p_0_debit, p_0_PSimul, p_0_dp_arma,
                t_3_pArma, t_3_volumeStock, t_3_debit, t_3_Tsimul,
                t_2_pArma, t_2_volumeStock, t_2_debit, t_2_Tsimul, t_2_dpArma,
                t_1_pArma, t_1_volumeStock, t_1_debit, t_1_Tsimul,
                t_0_pArma, t_0_volumeStock, t_0_debit, t_0_Tsimul);

        int idCalage = Integer.parseInt(idString);
        Optional<Calage> optCalage = database.getCalageById(idCalage);
        if (!optCalage.isPresent()) {
            answerToRequest(response, 400, "Can't find calage with id \'" + idCalage + "\'");
            return;
        }
        Calage calage = optCalage.get();
        calage.update(calageTempo);

        if (isLockedChecked) {
            List<Calage> calageList = database.getListCalageForCavity(cavity);
            calageList.forEach(calageToTest -> {
                // Verifier les dates pour chaques calage et si date coincide avec celle qu'on veut inserer
                // Passer le locked ( des dates qui coincide ) à false
                if (calage.checkIfPeriodOverlap(calageToTest)) {
                    calageToTest.unlock();
                }
            });
            calage.lock();
        } else {
            calage.unlock();
        }
        answerToRequest(response, 200, "Calage \'" + idCalage + "\' updated");
    }

    /*##############################################################*/
    //////////////////////// Usefull Stuff ///////////////////////////
    /*##############################################################*/

    /**
     * Génère la liste des json ,concernant la pression réel, à envoyé au client
     */
    private List<JsonObject> generateJsonDataPressionReel(Database database, Cavity cavity, Calage calage) {
        return database.getRealDataForCavityFromTo(cavity, calage.getFrom(), calage.getTo())
                .stream()
                .map(realData -> {
                    JsonObject jsonPressionReel = new JsonObject();
                    LocalDate date = realData.getDateInsertion();
                    jsonPressionReel.put("x", date.getYear() + "," + date.getMonthValue() + "," + date.getDayOfMonth());
                    jsonPressionReel.put("y", realData.getPressionWelHead());
                    return jsonPressionReel;
                })
                .collect(Collectors.toList());
    }

    /**
     * Génère la liste des json ,concernant la pression simulée, à envoyé au client
     */
    private List<JsonObject> generateJsonDataPressionSimul(Calage calage) {
        return calage.getSimulatedDataList()
                .stream()
                .map(simulatedData -> {
                    JsonObject jsonPressionSimul = new JsonObject();
                    LocalDate date = simulatedData.getDateInsertion();
                    jsonPressionSimul.put("x", date.getYear() + "," + date.getMonthValue() + "," + date.getDayOfMonth());
                    jsonPressionSimul.put("y", simulatedData.getPressionWelHead());
                    return jsonPressionSimul;
                })
                .collect(Collectors.toList());
    }

    /**
     * Génère la liste des json ,concernant la temperature réel, à envoyé au client
     */
    private List<JsonObject> generateJsonDataTemperatureReel(Database database, Cavity cavity, Calage calage) {
        return database.getRealDataForCavityFromTo(cavity, calage.getFrom(), calage.getTo())
                .stream()
                .map(realData -> {
                    JsonObject jsonTempReel = new JsonObject();
                    LocalDate date = realData.getDateInsertion();
                    jsonTempReel.put("x", date.getYear() + "," + date.getMonthValue() + "," + date.getDayOfMonth());
                    jsonTempReel.put("y", realData.getTemperatureWelHead());
                    return jsonTempReel;
                })
                .collect(Collectors.toList());
    }

    /**
     * Génère la liste des json ,concernant la temperature simulée, à envoyé au client
     */
    private List<JsonObject> generateJsonDataTemperatureSimul(Calage calage) {
        return calage.getSimulatedDataList()
                .stream()
                .map(simulatedData -> {
                    JsonObject jsonTempSimul = new JsonObject();
                    LocalDate date = simulatedData.getDateInsertion();
                    jsonTempSimul.put("x", date.getYear() + "," + date.getMonthValue() + "," + date.getDayOfMonth());
                    jsonTempSimul.put("y", simulatedData.getTemperatureWelHead());
                    return jsonTempSimul;
                })
                .collect(Collectors.toList());
    }

}
