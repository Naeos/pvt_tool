package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.gaz.Gaz;
import com.noxdia.site.gaz.GazFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.StaticHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

public class Server extends AbstractVerticle {

    private final static int KB = 1024;
    private final static int MB = 1024 * KB;
    private final static int maxUploadSize = 50 * MB;
    private final static String gazJsonFileName = "./data/gazData.json";

    private final List<Exploitant> listExploitant = new ArrayList<>();
    private final List<RealData> realDataListTemporary = new ArrayList<>(); // Stocker les valeurs du csv reçu et les renvoyer pour gerer l'ecrasement de données
    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Database database;
    private final CalageHandlers calageHandlers = new CalageHandlers();
    private final CasingShoeHandlers casingShoeHandlers = new CasingShoeHandlers();
    private final CavernHandlers cavernHandlers = new CavernHandlers();
    private final CavityHandlers cavityHandlers = new CavityHandlers();
    private final ContactHandlers contactHandlers = new ContactHandlers();
    private final ExploitantHandlers exploitantHandlers = new ExploitantHandlers();
    private final GazHandlers gazHandlers = new GazHandlers();
    private final GenerateDocxHandlers generateDocxHandlers = new GenerateDocxHandlers();
    private final MailHandlers mailHandlers = new MailHandlers();
    private final MatchingStatsHandlers matchingStatsHandlers = new MatchingStatsHandlers();
    private final RealDataHandlers realDataHandlers = new RealDataHandlers();
    private final SimulatedDataHandlers simulatedDataHandlers = new SimulatedDataHandlers();
    private final SiteHandlers siteHandlers = new SiteHandlers();

    public Server(Database database) {
        Objects.requireNonNull(database);
        this.database = database;
    }

    @Override
    public void start(Future<Void> fut) {

        int bindPort = 8080;
        Router router = Router.router(vertx);
        allRoutes(router);

        // Charger le contenu de la bdd
        loadDatabaseData();

        // Charger la liste des gaz de base
        loadBaseGazList();

        // start HTTP web.server
        vertx.createHttpServer().requestHandler(router::accept).listen(bindPort);
    }

    /**
     * Chargement de la base de donnée en mémoire
     */
    private void loadDatabaseData() {
        database.loadAll(listExploitant, saspLogger);
    }

    /**
     * Charge la liste des gaz par default dans l'application
     */
    private void loadBaseGazList() {
        saspLogger.log(Level.INFO, "Load gaz data from json file");
        Path path = Paths.get(gazJsonFileName);

        boolean error;
        List<Gaz> gazList = new ArrayList<>();
        try {
            String jsonString = buildJsonString(path);
            error = parseJson(gazList, jsonString);

        } catch (IOException e) {
            error = true;
            saspLogger.log(Level.WARNING, e.getMessage());
        }

        if (error) {
            saspLogger.log(Level.WARNING, "Error while reading file");
            saspLogger.log(Level.INFO, "Loading hard coded gaz list");
            loadHardCodedGazList();
            saspLogger.log(Level.INFO, "Loading hard coded gaz list finished");
        } else {
            gazList.forEach(database::updateGaz);
            saspLogger.log(Level.INFO, "Load gaz data from json file finished");
        }
    }

    /**
     * @param path Path du fichier
     * @return La string au format json lu dans le fichier
     * @throws IOException S'il y a une error lors de la lecture du fichier
     */
    private String buildJsonString(Path path) throws IOException {
        List<String> lines = Files.readAllLines(path);
        StringBuilder sb = new StringBuilder();
        lines.forEach(sb::append);
        String jsonString;
        jsonString = sb.toString();
        return jsonString;
    }

    /**
     * Parse la string json et extrait tous les gaz
     *
     * @param gazList    Liste des gazs à charger dans l'application
     * @param jsonString La string au format json
     * @return True s'il y a une erreur, false sinon
     */
    private boolean parseJson(List<Gaz> gazList, String jsonString) {
        try {
            JsonArray jsonArray = new JsonArray(jsonString);
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject gazJson = jsonArray.getJsonObject(i);

                String name = gazJson.getString("Name");
                String formule = gazJson.getString("Formule");

                double masseMolaire = gazJson.getDouble("MolarMass");
                double temperatureCritique = gazJson.getDouble("CriticalTemperature");
                double pressionCritique = gazJson.getDouble("CriticalPressure");
                double densite = gazJson.getDouble("Density");
                Gaz gaz = GazFactory.createGaz(name, formule, masseMolaire, temperatureCritique, pressionCritique, densite);
                gazList.add(gaz);
            }
        } catch (Exception e) {
            saspLogger.log(Level.WARNING, e.getMessage());
            return true;
        }
        return false;
    }

    /**
     * Charge la liste des gaz
     */
    private void loadHardCodedGazList() {
        Gaz methane = GazFactory.createGaz("Methane", "CH4", 16.042, 190.6, 4.604, 0.716);
        insertInDB(methane);

        Gaz ethane = GazFactory.createGaz("Ethane", "C2H6", 30.068, 305.4, 4.88, 1.3562);
        insertInDB(ethane);

        Gaz propane = GazFactory.createGaz("Propane", "C3H8", 44.094, 369.8, 4.249, 2.0098);
        insertInDB(propane);

        Gaz n_butane = GazFactory.createGaz("n-Butane", "C4H10", 54.088, 425.2, 3.797, 2.48);
        insertInDB(n_butane);

        Gaz i_butane = GazFactory.createGaz("i-Butane", "C4H10", 58.12, 408.1, 3.648, 2.51);
        insertInDB(i_butane);

        Gaz n_pentane = GazFactory.createGaz("n-Pentane", "C5H12", 72.146, 469.7, 3.369, 0.626);
        insertInDB(n_pentane);

        Gaz i_pentane = GazFactory.createGaz("i-Pentane", "C5H12", 72.146, 460.4, 3.381, 0.616);
        insertInDB(i_pentane);

        Gaz hexane = GazFactory.createGaz("Hexane", "C6H14", 86.172, 507.4, 3.012, 0.6548);
        insertInDB(hexane);

        Gaz heptane = GazFactory.createGaz("Heptane", "C7H16", 100.198, 540.3, 2.736, 0.6795);
        insertInDB(heptane);

        Gaz octane = GazFactory.createGaz("Octane", "C8H18", 114.224, 568.8, 2.486, 0.703);
        insertInDB(octane);

        Gaz hydrogen_sulphide = GazFactory.createGaz("Hydrogen Sulphide", "H2S", 2.016, 373.5, 8.937, 1.363);
        insertInDB(hydrogen_sulphide);

        Gaz hydrogen = GazFactory.createGaz("Hydrogen", "H2", 2.016, 33.3, 1.297, 0.069);
        insertInDB(hydrogen);

        Gaz carbon_dioxide = GazFactory.createGaz("Carbon Dioxide", "CO2", 44.01, 304.2, 7.382, 1.977);
        insertInDB(carbon_dioxide);

        Gaz nitrogen = GazFactory.createGaz("Nitrogen", "N2", 28.2, 126.1, 3.394, 1.251);
        insertInDB(nitrogen);

        Gaz oxygen = GazFactory.createGaz("Oxygen", "O2", 32, 154.6, 5.043, 1.429);
        insertInDB(oxygen);
    }

    /**
     * Insère le gaz "gaz" dans la base de données
     *
     * @param gaz Gaz à insérer dans la base de données
     */
    private void insertInDB(Gaz gaz) {
        database.insertGaz(gaz);
    }

    public void stop(Future<Void> fut) {
        fut.complete();
    }

    /**
     * Toutes les routes pour le serveur
     *
     * @param router Router sur lequel ajouté les routes
     */
    private void allRoutes(Router router) {
        router.route().handler(CookieHandler.create());
        router.route().handler(BodyHandler.create().setBodyLimit(maxUploadSize).setUploadsDirectory("webroot/asset/uploads"));
        listOfRequest(router);
        router.route().handler(StaticHandler.create().setCachingEnabled(false));
    }

    /**
     * Liste de toutes les api du serveur
     *
     * @param router Router sur lequel ajouté les routes
     */
    private void listOfRequest(Router router) {
        calageRoutes(router);
        casingShoeRoutes(router);
        cavernRoutes(router);
        contactRoutes(router);
        exploitantRoutes(router);
        gazRoutes(router);
        generateDocxRoutes(router);
        cavityRoutes(router);
        matchingStatsRoutes(router);
        mailRoutes(router);
        realDataRoutes(router);
        simulatedDataRoutes(router);
        siteRoutes(router);
    }

    ////////////////////
    /// Casing shoe ////
    ////////////////////
    // Fichier CasingShoeHandlers
    private void casingShoeRoutes(Router router) {
        // Fonctionne
        router.post("/api/getCasingShoePression").handler(routingContext -> casingShoeHandlers.getCasingShoePression(routingContext, database));
        // Fonctionne
        router.post("/api/getCasingShoePressionDate").handler(routingContext -> casingShoeHandlers.getCasingShoePressionDate(routingContext, database));
        // Fonctionne
        router.post("/api/getCasingShoeTemperature").handler(routingContext -> casingShoeHandlers.getCasingShoeTemperature(routingContext, database));
        // Fonctionne
        router.post("/api/getCasingShoeTemperatureDate").handler(routingContext -> casingShoeHandlers.getCasingShoeTemperatureDate(routingContext, database));
    }

    ////////////////////
    ////// Cavern //////
    ////////////////////
    // Fichier CavernHandlers
    private void cavernRoutes(Router router) {
        // Fonctionne
        router.post("/api/getCavernPression").handler(routingContext -> cavernHandlers.getCavernPression(routingContext, database));
        // Fonctionne
        router.post("/api/getCavernPressionDate").handler(routingContext -> cavernHandlers.getCavernPressionDate(routingContext, database));
        // Fonctionne
        router.post("/api/getCavernTemperature").handler(routingContext -> cavernHandlers.getCavernTemperature(routingContext, database));
        // Fonctionne
        router.post("/api/getCavernTemperatureDate").handler(routingContext -> cavernHandlers.getCavernTemperatureDate(routingContext, database));
    }

    ////////////////////
    /// Generate docx //
    ////////////////////
    // Fichier GenerateDocxHandlers
    private void generateDocxRoutes(Router router) {
        // Fonctionne
        router.post("/api/generateDocx").handler(routingContext -> generateDocxHandlers.generateDocx(routingContext, database));
    }

    ////////////////////
    // Export by mail //
    ////////////////////
    // Fichier MailHandlers
    private void mailRoutes(Router router) {
        // Fonctionne
        router.post("/api/exportByMail").handler(routingContext -> mailHandlers.exportByMail(routingContext, database));
    }

    /////////////
    // Contact //
    /////////////
    // Fichier ContactHandlers
    private void contactRoutes(Router router) {
        // Fonctionne
        router.post("/api/getListContactForExp").handler(routingContext -> contactHandlers.getListContactForExp(routingContext, database, listExploitant));
        // Fonctionne
        router.post("/api/getListContactForSite").handler(routingContext -> contactHandlers.getListContactForSite(routingContext, database, listExploitant));
        // Fonctionne
        router.post("/api/getListContactForExpWithoutSite").handler(routingContext -> contactHandlers.getListContactForExpWithoutSite(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/addContact").handler(routingContext -> contactHandlers.addContact(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/removeContact").handler(routingContext -> contactHandlers.removeContact(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/updateContact").handler(routingContext -> contactHandlers.updateContact(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/updateContactWithImage").handler(routingContext -> contactHandlers.updateContactWithImage(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/affectContactToSite").handler(routingContext -> contactHandlers.affectContactToSite(routingContext, database, listExploitant));
        // Fonctionne
        router.post("/api/unAffectContactFromSite").handler(routingContext -> contactHandlers.unAffectContactFromSite(routingContext, database, listExploitant));
        // Fonctionne
        router.post("/api/addContactWithSite").handler(routingContext -> contactHandlers.addContactWithSite(routingContext, database, listExploitant));
    }

    ////////////////////
    // Simulated data //
    ////////////////////
    // Fichier SimulatedDataHandlers
    private void simulatedDataRoutes(Router router) {
        // Fonctionne
        router.post("/api/getSimulatedPressionForCavity").handler(routingContext -> simulatedDataHandlers.getSimulatedPressionForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/getSimulatedTemperatureForCavity").handler(routingContext -> simulatedDataHandlers.getSimulatedTemperatureForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/getSimulatedPressionForCavityDate").handler(routingContext -> simulatedDataHandlers.getSimulatedPressionForCavityDate(routingContext, database));
        // Fonctionne
        router.post("/api/getSimulatedTemperatureForCavityDate").handler(routingContext -> simulatedDataHandlers.getSimulatedTemperatureForCavityDate(routingContext, database));
    }

    ////////////////////
    // Matching Stats //
    ///////////////////
    // Fichier MatchingStatsHandlers
    private void matchingStatsRoutes(Router router) {
        // Fonctionne
        router.post("/api/getMatchingStatPressionForCavity").handler(routingContext -> matchingStatsHandlers.getMatchingStatPressionForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/getMatchingStatPressionForCavityDate").handler(routingContext -> matchingStatsHandlers.getMatchingStatPressionForCavityDate(routingContext, database));
        // Fonctionne
        router.post("/api/getMatchingStatTemperatureForCavity").handler(routingContext -> matchingStatsHandlers.getMatchingStatTemperatureForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/getMatchingStatTemperatureForCavityDate").handler(routingContext -> matchingStatsHandlers.getMatchingStatTemperatureForCavityDate(routingContext, database));
    }

    //////////////
    // Calages  //
    //////////////
    // Fichier calageHandlers
    private void calageRoutes(Router router) {
        // Fonctionne
        router.post("/api/getListCalageForCavity").handler(routingContext -> calageHandlers.getListCalageForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/simulateForCavity").handler(routingContext -> calageHandlers.simulateForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/addCalage").handler(routingContext -> calageHandlers.addCalage(routingContext, database));
        // Fonctionne
        router.post("/api/deleteCalage").handler(routingContext -> calageHandlers.deleteCalage(routingContext, database));
        // Fonctionne
        router.post("/api/updateCalage").handler(routingContext -> calageHandlers.updateCalage(routingContext, database));
    }

    ///////////////
    // Real data //
    ///////////////
    // Fichier RealDataHandlers
    private void realDataRoutes(Router router) {
        // Fonctionne
        router.post("/api/importRealDataForCavity").handler(routingContext -> realDataHandlers.importRealDataForCavity(routingContext, listExploitant, database, realDataListTemporary));
        // Fonctionne
        router.get("/api/getConflictingValuesForRealData").handler(routingContext -> realDataHandlers.getConflictingValuesForRealData(routingContext, database, realDataListTemporary));
        // Fonctionne
        router.get("/api/clickOnAnnule").handler(routingContext -> realDataHandlers.clickOnAnnule(routingContext, realDataListTemporary));
        // Fonctionne
        router.get("/api/confirmForRealDataForCavity").handler(routingContext -> realDataHandlers.confirmForRealDataForCavity(routingContext, realDataListTemporary));
        // Fonctionne
        router.post("/api/correctRealDataForCavity").handler(routingContext -> realDataHandlers.correctRealDataForCavity(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/getRealPressionForCavity").handler(routingContext -> realDataHandlers.getRealPressionForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/getRealTemperatureForCavity").handler(routingContext -> realDataHandlers.getRealTemperatureForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/getRealVolumeForCavity").handler(routingContext -> realDataHandlers.getRealVolumeForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/getRealDebitForCavity").handler(routingContext -> realDataHandlers.getRealDebitForCavity(routingContext, database));
        // Fonctionne
        router.post("/api/getRealPressionForCavityDate").handler(routingContext -> realDataHandlers.getRealPressionForCavityDate(routingContext, database));
        // Fonctionne
        router.post("/api/getRealTemperatureForCavityDate").handler(routingContext -> realDataHandlers.getRealTemperatureForCavityDate(routingContext, database));
        // Fonctionne
        router.post("/api/getRealVolumeForCavityDate").handler(routingContext -> realDataHandlers.getRealVolumeForCavityDate(routingContext, database));
        // Fonctionne
        router.post("/api/getRealDebitForCavityDate").handler(routingContext -> realDataHandlers.getRealDebitForCavityDate(routingContext, database));
    }

    ////////////
    // Cavity //
    ////////////
    // Fichier CavityHandlers
    private void cavityRoutes(Router router) {
        // Fonctionne
        router.post("/api/addCavity").handler(routingContext -> cavityHandlers.addCavity(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/updateCavity").handler(routingContext -> cavityHandlers.updateCavity(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/removeCavity").handler(routingContext -> cavityHandlers.removeCavity(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/importCavity").handler(routingContext -> cavityHandlers.importCavity(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/exportCavity").handler(routingContext -> cavityHandlers.exportCavity(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/getListCavity").handler(routingContext -> cavityHandlers.getListCavity(routingContext, listExploitant, database));
    }

    /////////
    // Gaz //
    /////////
    // Fichier GazHandlers
    private void gazRoutes(Router router) {
        // Fonctionne
        router.get("/api/getListGaz").handler(routingContext -> gazHandlers.getListGaz(routingContext, database));
        // Fonctionne
        router.post("/api/addGaz").handler(routingContext -> gazHandlers.addGaz(routingContext, database, gazJsonFileName));
        // Fonctionne
        router.post("/api/updateGaz").handler(routingContext -> gazHandlers.updateGaz(routingContext, database, gazJsonFileName));
        // Fonctionne
        router.post("/api/removeGaz").handler(routingContext -> gazHandlers.removeGaz(routingContext, database, gazJsonFileName));

        //////////////////////////
        // List Composition Gaz //
        //////////////////////////

        // Fonctionne
        router.post("/api/getListCompositionGaz").handler(routingContext -> gazHandlers.getListCompositionGaz(routingContext, database));
        // Fonctionne
        router.post("/api/updateCompositionGazList").handler(routingContext -> gazHandlers.updateCompositionGazList(routingContext, listExploitant, database));
    }

    /////////////////
    // Exploitants //
    /////////////////
    // Fichier ExploitantHandlers
    private void exploitantRoutes(Router router) {
        // Fonctionne
        router.get("/api/getListExploitant").handler(routingContext -> exploitantHandlers.getListExploitant(routingContext, database));
        // Fonctionne
        router.post("/api/addExploitant").handler(routingContext -> exploitantHandlers.addExploitant(routingContext, listExploitant, database));
        // Fonctionne
        router.get("/api/getListNameExploitant").handler(routingContext -> exploitantHandlers.getListNameExploitant(routingContext, database));
        // Fonctionne
        router.post("/api/updateExploitant").handler(routingContext -> exploitantHandlers.updateExploitant(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/updateExploitantWithImage").handler(routingContext -> exploitantHandlers.updateExploitantWithImage(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/removeExploitant").handler(routingContext -> exploitantHandlers.removeExploitant(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/exportExploitant").handler(routingContext -> exploitantHandlers.exportExploitant(routingContext, listExploitant));
        // Fonctionne
        router.post("/api/importExploitant").handler(routingContext -> exploitantHandlers.importExploitant(routingContext, listExploitant, database));
    }

    //////////
    // Site //
    //////////
    // Fichier SiteHandlers
    private void siteRoutes(Router router) {
        // Fonctionne
        router.get("/api/getAllSite").handler(routingContext -> siteHandlers.getAllSites(routingContext, database));
        // Fonctionne
        router.post("/api/sendLoginForSite").handler(routingContext -> siteHandlers.sendLoginRequest(routingContext, database));
        // Fonctionne
        router.post("/api/updatePasswordSite").handler(routingContext -> siteHandlers.updatePasswordRequest(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/addSite").handler(routingContext -> siteHandlers.addSite(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/updateSite").handler(routingContext -> siteHandlers.updateSite(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/updateSiteWithImage").handler(routingContext -> siteHandlers.updateSiteWithImage(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/removeSite").handler(routingContext -> siteHandlers.removeSite(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/exportSite").handler(routingContext -> siteHandlers.exportSite(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/importSite").handler(routingContext -> siteHandlers.importSite(routingContext, listExploitant, database));
        // Fonctionne
        router.post("/api/getListSiteBySearch").handler(routingContext -> siteHandlers.getListSiteBySearch(routingContext, database));
        // Fonctionne
        router.post("/api/getSiteInfo").handler(routingContext -> siteHandlers.getSiteInfo(routingContext, database));
    }

}
