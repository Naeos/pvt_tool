package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.mail.Mailer;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.tools.Tools;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

import static com.noxdia.web.server.HandlersUtils.answerToRequest;

class MailHandlers {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    MailHandlers() {
    }

    /*##############################################################*/
    //////////////////// Export by mail Handler //////////////////////
    /*##############################################################*/

    /**
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Formats des adresses mail incorrect
     */
    void exportByMail(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In export by mail request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeExportByMailRequest(response, json, database);
        }
    }

    private void analyzeExportByMailRequest(HttpServerResponse response, JsonObject json, Database database) {
        String to = json.getString("Mails");
        String password = json.getString("Password");
        String siteName = json.getString("SiteName");
        JsonArray jsonArray = json.getJsonArray("Data"); // Changer le nom

        if (tools.verifyEmptyOrNull(to, password, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        // Construire la liste des mails
        List<String> mailAdresses = new ArrayList<>();
        String[] mails = to.split(";");

        // Verification du format des mail
        for (String mail : mails) {
            if (tools.checkMailFormat(mail).equals(mail)) {
                mailAdresses.add(mail);
            }
        }

        // Si jamais il n'y aucune adresse correct de rentrer
        if (mailAdresses.isEmpty()) {
            answerToRequest(response, 400, "No mail format are correct");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        List<String> listFileNames = new ArrayList<>();

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.getJsonObject(i);

            String fileName;
            String name = jsonObject.getString("Name");
            Optional<Site> optSiteInLoop = database.getSiteByName(name);
            if (optSiteInLoop.isPresent()) {
                Site siteToExport = optSiteInLoop.get();
                fileName = siteToExport.exportToZip(password);
                listFileNames.add(fileName);
            }

            Optional<Cavity> optCavity = database.getCavityByNameAndSite(name, site);
            if (optCavity.isPresent()) {
                Cavity cavity = optCavity.get();
                fileName = cavity.exportToZip(password);
                listFileNames.add(fileName);
            }
        }

        String subject = "Export des données SASP TOOL, le " + LocalDate.now();
        String bodyText = "Bonjour,\n" +
                "Un nouvel export vous a été transmis via SASP TOOL. Nous vous invitons à accéder " +
                "directement à l’archive ZIP en pièce jointe du présent mail via le mot de passe qui" +
                "vous a été communiqué par l’administrateur de l’application.\n\n" +
                "SASP TOOL.";

        Mailer mailer = new Mailer();
        for (String mail : mailAdresses) {
            boolean sendMail = mailer.sendMailTo(mail, subject, bodyText, listFileNames);
            if (!sendMail) {
                answerToRequest(response, 400, "Send mail failed, check internet connection");
                return;
            }
        }

        // Modif -> eviter de supprimer pour le moment , sinon ça peut supprimer des export effectué avant -> problème pour le user
        // Suppression des fichier après envoie par mail pour eviter de polluer le dossier
//        listFileNames.forEach(name -> {
//            Path path = Paths.get(name);
//            tools.deleteFileIfExists(path);
//        });
        answerToRequest(response, 200, "Mail correctly sent");
    }
}
