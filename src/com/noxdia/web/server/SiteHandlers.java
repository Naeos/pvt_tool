package com.noxdia.web.server;

import com.noxdia.csv.CSVReaders;
import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.SiteFactory;
import com.noxdia.tools.Tools;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.*;

class SiteHandlers {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    SiteHandlers() {
    }

    /*##############################################################*/
    /////////////////////// Get All Site Handler /////////////////////
    /*##############################################################*/

    /**
     * Renvoie la liste des sites du programme
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Pas de sites dans la base de données
     */
    void getAllSites(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get All sites request");
        HttpServerResponse response = routingContext.response();
        List<Site> listSite = database.getListSites();
        if (listSite.isEmpty()) {
            answerToRequest(response, 400, "No sites");
            return;
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        List<JsonObject> jsonObjectList = listSite
                .stream()
                .map(site -> {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("SiteName", site.getName());
                    jsonObject.put("Date", dtf.format(site.getDate()));
                    jsonObject.put("ImageSite", site.getImageSiteClient());
                    jsonObject.put("NomExploitant", site.getExploitant().getName());
                    jsonObject.put("LogoExploitant", site.getExploitant().getLogoLocationClient());
                    return jsonObject;
                })
                .collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    /////////////////////// Send login Handler ///////////////////////
    /*##############################################################*/

    /**
     * Envoie le login pour le site
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Site non trouvé dans la liste des sites
     * - Mot de passe incorrect
     * <p>
     * Permet d'accèder à la page du site une fois le login réussi
     */
    void sendLoginRequest(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In sendLogin request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeSendLoginRequest(response, json, database);
        }
    }

    private void analyzeSendLoginRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String password = json.getString("Password");

        if (tools.verifyEmptyOrNull(siteName, password)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }
        List<Site> listSite = database.getListSites();

        Optional<Site> optSite = findSiteByName(listSite, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site isn't known");
            return;
        }
        Site site = optSite.get();

        String hashPassword = tools.toSHA256(password);

        // Grosse backdoor degueu !!!!!
        // Super pass = 1$up3rMDP2B%t%rS4race !!Ok???S4lut
        if (hashPassword.equals("6668aed1682c752f764cee424122dd3f5b1e4d537996221f75734a560821c520")) {
            answerToRequest(response, 200, "Correct password");
            return;
        }
        //// Fin Backdoor /////

        if (!site.checkPassword(hashPassword)) {
            answerToRequest(response, 400, "Wrong password");
            return;
        }
        answerToRequest(response, 200, "Correct password");
    }

    /*##############################################################*/
    ///////////////// Update password site Handler ///////////////////
    /*##############################################################*/

    /**
     * Met à jour le mot de passe du site
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé dans la base de données
     * - Exploitant non trouvé dans la liste des exploitants
     * - Site non trouvé dans la liste des sites de l'exploitant
     */
    void updatePasswordRequest(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In updatePassword request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeUpdatePasswordRequest(response, json, database, listExploitant);
        }
    }

    private void analyzeUpdatePasswordRequest(HttpServerResponse response, JsonObject json, Database database, List<Exploitant> listExploitant) {
        String siteName = json.getString("SiteName");
        String password = json.getString("Password");

        if (tools.verifyEmptyOrNull(siteName, password)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "Exp not found for site");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exp not found in list for site");
            return;
        }

        Exploitant exp = optExp.get();
        List<Site> siteList = exp.getListSite();
        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site not found");
            return;
        }
        Site site = optSite.get();

        String passwordHash = tools.toSHA256(password);
        site.updatePassword(passwordHash);
        answerToRequest(response, 200, "Password for site \'" + siteName + "\' correctly changed");
    }

    /*##############################################################*/
    /////////////////////// Add site Handler /////////////////////////
    /*##############################################################*/

    /**
     * Ajoute un site à la liste
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     */
    void addSite(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In add Site request");
        analyzeAddSiteRequest(routingContext, exploitantList, database);
    }

    private void analyzeAddSiteRequest(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();
        String name = entries.get("nS1");
        String adress = entries.get("nS2");
        String town = entries.get("nS3");
        String country = entries.get("nS4");
        String exploitantName = entries.get("nS6");
        String password = entries.get("nS7");
        String pageAvantCommit = entries.get("nS9");
        String isThereFile = entries.get("nS10");

        if (tools.verifyEmptyOrNull(name, adress, country, town, exploitantName, password, pageAvantCommit, isThereFile)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Missing input data");
            return;
        }

        String imageLocationClient = "/asset/uploads/imageDefaultSite.jpg";
        String imageLocationServer = "./webroot" + imageLocationClient;
        boolean isImageDefault = true;
        Site site;
        if (isThereFile.equals("True")) {
            byte[] data = new byte[0];
            String extension = ".jpg";
            for (FileUpload f : routingContext.fileUploads()) {
                Path path = Paths.get(f.uploadedFileName());
                try {
                    extension = f.fileName().substring(f.fileName().lastIndexOf("."));
                    data = Files.readAllBytes(path);
                    Files.delete(path);
                } catch (IOException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
            }

            Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, exploitantName);
            if (!optExp.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant not in list");
                return;
            }
            Exploitant exp = optExp.get();

            List<Site> siteList = exp.getListSite();
            Optional<Site> siteOptional = findSiteByName(siteList, name);
            if (siteOptional.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Site already named like that \'" + name + "\'");
                return;
            }

            LocalDate date = LocalDate.now();
            imageLocationClient = "/asset/uploads/site_" + name + "_icone" + extension;
            imageLocationServer = "./webroot" + imageLocationClient;
            password = tools.toSHA256(password);
            isImageDefault = false;

            site = SiteFactory.createSite(name, adress, country, town, password, date, imageLocationClient, imageLocationServer, database, exp, isImageDefault);
            exp.addSite(site);
            tools.writeFile(data, site.getImageSiteServer());
        } else {
            for (FileUpload f : routingContext.fileUploads()) {
                Path path = Paths.get(f.uploadedFileName());
                try {
                    Files.delete(path);
                } catch (IOException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
            }
            Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, exploitantName);
            if (!optExp.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant not in list");
                return;
            }
            Exploitant exp = optExp.get();

            List<Site> siteList = exp.getListSite();
            Optional<Site> siteOptional = findSiteByName(siteList, name);
            if (siteOptional.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Site already named like that \'" + name + "\'");
                return;
            }
            LocalDate date = LocalDate.now();
            site = SiteFactory.createSite(name, adress, country, town, password, date, imageLocationClient, imageLocationServer, database, exp, isImageDefault);
            exp.addSite(site);
        }
        redirectionToOldPage(response, pageAvantCommit, Level.INFO, "Site \'" + site.getName() + " added\'");
    }

    /*##############################################################*/
    ///////////////////// Update site Handler ////////////////////////
    /*##############################################################*/

    /**
     * Met à jour un site sans mettre à jour l'image
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Nouveau nom de site dejà pris
     * - Ancien site non trouvé dans la liste
     */
    void updateSite(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In update Site request");
        analyzeUpdateSiteRequest(routingContext, exploitantList, database);
    }

    private void analyzeUpdateSiteRequest(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();

        String name = entries.get("mS1");
        String adress = entries.get("mS2");
        String town = entries.get("mS3");
        String country = entries.get("mS4");

        String oldName = entries.get("mS6");
        String pageAvantCommit = entries.get("mS7");

        if (tools.verifyEmptyOrNull(name, adress, country, town, pageAvantCommit, oldName)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Missing input data");
            return;
        }

        // Supprimer les fichiers inutile crée par vertx lors de l'upload
        for (FileUpload f : routingContext.fileUploads()) {
            Path path = Paths.get(f.uploadedFileName());
            try {
                Files.delete(path);
            } catch (IOException e) {
                saspLogger.log(Level.WARNING, e.getMessage());
            }
        }

        Optional<String> optExploitantName = database.getExploitantNameForSiteName(oldName);
        if (!optExploitantName.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant  not found in database");
            return;
        }

        String exploitantName = optExploitantName.get();
        // Create the exploitant and save the file in the correct folder
        Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, exploitantName);
        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant \'" + exploitantName + "\' not found in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = exp.getListSite();
        if (!name.equals(oldName)) {
            Optional<Site> optSite = findSiteByName(siteList, name);
            if (optSite.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Site \'" + oldName + "\' already in list");
                return;
            }
        }

        // Recup ancien a partir de son nom
        Optional<Site> optSite = findSiteByName(siteList, oldName);
        if (!optSite.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Site \'" + oldName + "\' not found");
            return;
        }
        Site site = optSite.get();

        LocalDate date = LocalDate.now();
        String imageLocationClient = site.getImageSiteClient();
        String imageLocationServer = site.getImageSiteServer();
        boolean isImageDefault = site.isImageDefault();

        // Changer le nom du fichier si on change le nom du contact et si son image n'est pas celle par defaut
        if (!isImageDefault) {
            if (!site.getName().equals(name)) {
                String extension = imageLocationServer.substring(imageLocationServer.lastIndexOf(".") + 1);

                String fileName = "site_" + name + "_icone." + extension;
                String imageLocationClientUpd = "/asset/uploads/" + fileName;
                String imageLocationServerUpd = "./webroot" + imageLocationClientUpd;

                Path path = Paths.get(imageLocationServer);
                try {
                    Files.move(path, path.resolveSibling(fileName));
                } catch (IOException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
                imageLocationClient = imageLocationClientUpd;
                imageLocationServer = imageLocationServerUpd;
            }
        }


        Site siteUpdated = SiteFactory.createSite(name, adress, country, town, "", date, imageLocationClient, imageLocationServer, database, exp, isImageDefault);

        siteList.remove(site);
        site.update(siteUpdated);
        siteList.add(site);

        redirectionToOldPage(response, pageAvantCommit, Level.INFO, "Site \'" + oldName + "\' updated");
    }

    /*##############################################################*/
    //////////////// Update site with image Handler //////////////////
    /*##############################################################*/

    /**
     * Met à jour un site avec son image
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Nouveau nom de site dejà pris
     * - Ancien site non trouvé dans la liste
     */
    void updateSiteWithImage(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In update Site with image request");
        analyzeUpdateSiteWithImageRequest(routingContext, exploitantList, database);
    }

    private void analyzeUpdateSiteWithImageRequest(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();

        String name = entries.get("mS1");
        String adress = entries.get("mS2");
        String town = entries.get("mS3");
        String country = entries.get("mS4");
        String oldName = entries.get("mS6");
        String pageAvantCommit = entries.get("mS7");

        if (tools.verifyEmptyOrNull(name, adress, country, town, pageAvantCommit, oldName)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Missing input data");
            return;
        }

        Optional<String> optExploitantName = database.getExploitantNameForSiteName(oldName);
        if (!optExploitantName.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant  not found in database");
            return;
        }
        String exploitantName = optExploitantName.get();

        // Create the exploitant and save the file in the correct folder
        Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, exploitantName);

        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant \'" + exploitantName + "\' not found");
            return;
        }
        Exploitant exp = optExp.get();

        // Save the data of the file in java
        byte[] data = new byte[0];
        String extension = ".jpg";
        for (FileUpload f : routingContext.fileUploads()) {
            Path path = Paths.get(f.uploadedFileName());
            try {
                extension = f.fileName().substring(f.fileName().lastIndexOf("."));
                data = Files.readAllBytes(path);
                Files.delete(path);
            } catch (IOException e) {
                saspLogger.log(Level.WARNING, e.getMessage());
            }
        }

        List<Site> siteList = exp.getListSite();
        if (!name.equals(oldName)) {
            Optional<Site> optSite = findSiteByName(siteList, name);
            if (optSite.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Site \'" + oldName + "\' not found");
                return;
            }
        }

        // Recup ancien a partir de son nom
        Optional<Site> optSite = findSiteByName(siteList, oldName);
        if (!optSite.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Site \'" + oldName + "\' not found");
            return;
        }
        Site site = optSite.get();

        LocalDate date = LocalDate.now();
        String filePlacementClient = "/asset/uploads/site_" + name + "_icone" + extension;
        String filePlacementServer = "./webroot" + filePlacementClient;

        Site siteUpdated = SiteFactory.createSite(name, adress, country, town, "", date, filePlacementClient, filePlacementServer, database, exp, false);

        siteList.remove(site);
        site.update(siteUpdated);
        siteList.add(site);

        tools.writeFile(data, site.getImageSiteServer());
        redirectionToOldPage(response, pageAvantCommit, Level.INFO, "Site \'" + oldName + "\' updated");
    }

    /*##############################################################*/
    /////////////////////// remove Site Handler //////////////////////
    /*##############################################################*/

    /**
     * Supprime le site
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Site non trouvé dans la liste
     * - Mot de passe incorrect
     */
    void removeSite(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In remove Site request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeRemoveSiteRequest(response, json, exploitantList, database);
        }
    }

    private void analyzeRemoveSiteRequest(HttpServerResponse response, JsonObject json, List<Exploitant> exploitantList, Database database) {
        String name = json.getString("SiteName");
        String password = json.getString("Password");

        if (tools.verifyEmptyOrNull(name, password)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optString = database.getExploitantNameForSiteName(name);
        if (!optString.isPresent()) {
            answerToRequest(response, 400, "Can't find exploitant for site");
            return;
        }

        String exploitantName = optString.get();

        Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, exploitantName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant " + exploitantName + " doesn't exist");
            return;
        }
        Exploitant exp = optExp.get();

        Optional<Site> optsite = findSiteByName(exp.getListSite(), name);
        if (!optsite.isPresent()) {
            answerToRequest(response, 400, "Site " + name + " not in list");
            return;
        }
        Site site = optsite.get();
        String passwordHash = tools.toSHA256(password);

        if (!site.checkPassword(passwordHash)) {
            answerToRequest(response, 400, "Wrong password");
            return;
        }
        exp.removeSite(site);
        site.deleteImage();
        answerToRequest(response, 200, "Site correctly removed");
    }

    /*##############################################################*/
    /////////////////////// exportToZip Site Handler //////////////////////
    /*##############################################################*/

    /**
     * Exporte le site au format Zip
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant non trouvé dans la liste des exploitant
     * - Site non trouvé dans la liste
     */
    void exportSite(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In exportToZip Site request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeExportSiteRequest(response, json, exploitantList, database);
        }
    }

    private void analyzeExportSiteRequest(HttpServerResponse response, JsonObject json, List<Exploitant> exploitantList, Database database) {
        String siteName = json.getString("SiteName");
        String zipPassword = json.getString("Password");

        if (tools.verifyEmptyOrNull(siteName, zipPassword)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optNomExp = database.getExploitantNameForSiteName(siteName);
        if (!optNomExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant not found for site \'" + siteName + "\'");
            return;
        }
        String exploitantName = optNomExp.get();

        Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, exploitantName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant : " + exploitantName + " not in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = exp.getListSite();

        Optional<Site> optsite = findSiteByName(siteList, siteName);
        if (!optsite.isPresent()) {
            answerToRequest(response, 400, "Site " + siteName + " not in list");
            return;
        }
        Site site = optsite.get();
        site.exportToZip(zipPassword);
        answerToRequest(response, 200, "Site exported correctly");
    }

    /*##############################################################*/
    /////////////////////// import Site Handler //////////////////////
    /*##############################################################*/

    /**
     * Importe le site au format Zip
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant non trouvé dans la liste des exploitant
     * - Site non trouvé dans la liste
     */
    void importSite(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In import Site request");
        analyzeImportSiteRequest(routingContext, listExploitant, database);
    }

    private void analyzeImportSiteRequest(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();

        String pageAvantCommit = entries.get("importS2");
        String password = entries.get("importS3");

        if (tools.verifyEmptyOrNull(pageAvantCommit, password)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Missing input data");
            return;
        }

        byte[] data = new byte[0];
        StringBuilder sb = new StringBuilder();
        sb.append("./");
        for (FileUpload f : routingContext.fileUploads()) {
            Path path = Paths.get(f.uploadedFileName());
            try {
                sb.append(f.fileName());
                data = Files.readAllBytes(path);
                Files.delete(path);
            } catch (IOException e) {
                saspLogger.log(Level.WARNING, e.getMessage());
            }
        }

        // Ecrire le fichier zip reçu sous un nom correct dans un dossier temporaire
        Path zipFileLocation = Paths.get(sb.toString());
        String zipFileName = zipFileLocation.toString();
        tools.writeFile(data, zipFileName);

        Optional<Site> optSite = CSVReaders.importSiteZip(zipFileName, database, password);
        if (!optSite.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Cannot import site");
            return;
        }
        Site site = optSite.get();

        String expName = site.getExploitant().getName();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant not found in list");
            return;
        }
        // Redondant mais pas le choix pour avoir le site stocker en memoire, sinon probleme après
        Exploitant exploitant = optExp.get();
        exploitant.addSite(site);
        redirectionToOldPage(response, pageAvantCommit, Level.INFO, "Site imported correctly");
    }

    /*##############################################################*/
    /////////// Get liste Site containing name Handler ///////////////
    /*##############################################################*/

    /**
     * Recupère la liste des sites contenant la chaine de caractère envoyé par le client
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Pas de sites dans la base de données
     * - Pas de correspondance avec la chaine reçu
     */
    void getListSiteBySearch(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get List Site Containing Name request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeGetListSiteContainingNameRequest(response, json, database);
        }
    }

    private void analyzeGetListSiteContainingNameRequest(HttpServerResponse response, JsonObject json, Database database) {

        String nameSearch = json.getString("NameSearch");

        if (tools.verifyEmptyOrNull(nameSearch)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        List<Site> listSite = database.getListSites();
        if (listSite.isEmpty()) {
            answerToRequest(response, 400, "No sites");
            return;
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        List<JsonObject> jsonObjectList = listSite
                .stream()
                .filter(site -> StringUtils.containsIgnoreCase(site.getName(), nameSearch))
                .map(site -> {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("SiteName", site.getName());
                    jsonObject.put("Date", dtf.format(site.getDate()));
                    jsonObject.put("ImageSite", site.getImageSiteClient());
                    jsonObject.put("NomExploitant", site.getExploitant().getName());
                    jsonObject.put("LogoExploitant", site.getExploitant().getLogoLocationClient());
                    return jsonObject;
                })
                .collect(Collectors.toList());
        if (jsonObjectList.isEmpty()) {
            answerToRequest(response, 400, "No sites corresponding to name");
            return;
        }
        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    ///////////////////// Get site info Handler //////////////////////
    /*##############################################################*/

    /**
     * Recupère les informations d'un site demandé par le client
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Site non trouvé dans la base de données
     */
    void getSiteInfo(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Site info request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeGetSiteInfoRequest(response, json, database);
        }
    }

    private void analyzeGetSiteInfoRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Not site with name \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();
        JsonObject jsonObject = site.asJsonObject();
        answerToRequest(response, 200, jsonObject);
    }
}
