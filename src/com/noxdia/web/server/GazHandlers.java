package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.gaz.CompositionGaz;
import com.noxdia.site.gaz.Gaz;
import com.noxdia.site.gaz.GazFactory;
import com.noxdia.tools.Tools;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.answerToRequest;
import static com.noxdia.web.server.HandlersUtils.findSiteByName;

class GazHandlers {
    private final Tools tools = Tools.getInstance();
    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Charset charset = Charset.forName("ISO-8859-1");
    GazHandlers() {
    }

    /*##############################################################*/
    /////////////////////// Get list CompoGaz Handler ////////////////
    /*##############################################################*/

    /**
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Site pas la liste
     * - Pas de composition de gaz pour le site en question
     */
    void getListCompositionGaz(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get list composition gaz request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeGetListCompositionGaz(response, json, database);
        }
    }

    private void analyzeGetListCompositionGaz(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }
        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \'" + siteName + "\' not in list");
            return;
        }
        Site site = optSite.get();

        List<CompositionGaz> compositionGazList = database.getListCompoGazForSite(site);
        if (compositionGazList.isEmpty()) {
            answerToRequest(response, 400, "No composition for site \'" + siteName + "\'");
            return;
        }

        List<JsonObject> jsonObjectList = compositionGazList.stream()
                .map(compositionGaz -> {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("Composant", compositionGaz.getGaz().getName());
                    jsonObject.put("Proportions", compositionGaz.getProportion());
                    return jsonObject;
                })
                .collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    ///////////////////// Update CompoGaz Handler ////////////////////
    /*##############################################################*/

    /**
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant pas la liste des exploitants
     * - Site pas la liste des sites de l'exploitant
     */
    void updateCompositionGazList(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In update Composition Gaz request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeUpdateCompositionGazList(response, json, listExploitant, database);
        }
    }

    private void analyzeUpdateCompositionGazList(HttpServerResponse response, JsonObject json, List<Exploitant> listExploitant, Database database) {
        String siteName = json.getString("SiteName");
        JsonArray jsonArray = json.getJsonArray("ListCompoGaz");

        if (tools.verifyEmptyOrNull(siteName) || jsonArray == null || jsonArray.isEmpty()) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "No exploitant for site \'" + siteName + "\'");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = HandlersUtils.findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> listSiteTmp = exp.getListSite();
        Optional<Site> optSite = findSiteByName(listSiteTmp, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \' " + siteName + "\' not in list");
            return;
        }
        Site site = optSite.get();

        List<CompositionGaz> listCompoGazSite = new ArrayList<>();
        listCompoGazSite.addAll(site.getListCompoGaz());
        listCompoGazSite.forEach(site::deleteCompositionGaz);

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonArray compoGazJsonArray = jsonArray.getJsonArray(i);
            String nomGaz = compoGazJsonArray.getString(0);
            double proportion = Double.parseDouble(compoGazJsonArray.getString(1));

            Optional<Gaz> optGaz = database.getGazByName(nomGaz);
            if (!optGaz.isPresent()) {
//                answerToRequest(response, 400, "Gaz \'" + nomGaz + "\' not in list");
                continue;
            }
            Gaz gaz = optGaz.get();
            CompositionGaz compositionGaz = GazFactory.createCompositionGaz(site, gaz, proportion);
            site.addCompositionGaz(compositionGaz);
        }
        answerToRequest(response, 200, "CompoGazListUpdated");
    }

    /*##############################################################*/
    /////////////////////// Get List Gaz Handler /////////////////////
    /*##############################################################*/

    /**
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Pas de gaz dans la liste
     */
    void getListGaz(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get List gaz request");

        HttpServerResponse response = routingContext.response();
        List<Gaz> gazList = database.getListGaz();

        if (gazList.isEmpty()) {
            answerToRequest(response, 400, "No gaz in list");
            return;
        }
        List<JsonObject> jsonObjectList = gazList.stream()
                .map(gaz -> {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("Name", gaz.getName());
                    jsonObject.put("Formule", gaz.getFormule());
                    jsonObject.put("MasseMolaire", gaz.getMasseMolaire());
                    jsonObject.put("Pression", gaz.getCriticalPression());
                    jsonObject.put("Temperature", gaz.getCriticalTemperature());
                    jsonObject.put("Densite", gaz.getDensite());
                    return jsonObject;
                })
                .collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    /////////////////////// Add Gaz Handler /////////////////////////
    /*##############################################################*/

    /**
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Gaz deja la liste des gaz
     */
    void addGaz(RoutingContext routingContext, Database database, String fileName) {
        saspLogger.log(Level.INFO, "In add Gaz request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeAddGazRequest(response, json, database, fileName);
        }
    }

    private void analyzeAddGazRequest(HttpServerResponse response, JsonObject json, Database database, String fileName) {
        String name = json.getString("GazName");
        String formule = json.getString("Formule");
        double masseMolaire = Double.parseDouble(json.getString("MasseMolaire"));
        double criticalPression = Double.parseDouble(json.getString("PressionCritique"));
        double criticalTemperature = Double.parseDouble(json.getString("TemperatureCritique"));
        double densite = Double.parseDouble(json.getString("Densite"));

        if (tools.verifyEmptyOrNull(name, formule)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        List<Gaz> gazList = database.getListGaz();
        if (findGazByName(gazList, name).isPresent()) {
            answerToRequest(response, 400, "Gaz \'" + name + "\' already in list");
            return;
        }

        Gaz gaz = GazFactory.createGaz(name, formule, masseMolaire, criticalTemperature, criticalPression, densite);
        database.insertGaz(gaz);
        updateJsonFile(database, fileName);
        answerToRequest(response, 200, "Gaz \'" + name + "\' inserted");
    }

    /*##############################################################*/
    ////////////////////// Update Gaz Handler ////////////////////////
    /*##############################################################*/

    /**
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Gaz pas dans la liste
     * - Un gaz avec le nouveau nom existe déjà
     */
    void updateGaz(RoutingContext routingContext, Database database, String fileName) {
        saspLogger.log(Level.INFO, "In update Gaz request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeUpdateGazRequest(response, json, database, fileName);
        }
    }

    private void analyzeUpdateGazRequest(HttpServerResponse response, JsonObject json, Database database, String fileName) {
        String name = json.getString("GazName");
        String newName = json.getString("NewGazName");
        String formule = json.getString("Formule");
        double masseMolaire = json.getDouble("MasseMolaire");
        double criticalPression = json.getDouble("PressionCritique");
        double criticalTemperature = json.getDouble("TemperatureCritique");
        double densite = json.getDouble("Densite");

        if (tools.verifyEmptyOrNull(name, formule, newName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        List<Gaz> gazList = database.getListGaz();
        if (!name.equals(newName)) {
            Optional<Gaz> newGazOptional = findGazByName(gazList, newName);
            if (newGazOptional.isPresent()) {
                answerToRequest(response, 400, "Can't rename gaz \'" + name + "\' to \'" + newName + "\' , there is already a gaz named like that ");
                return;
            }
        }

        Optional<Gaz> gazOptional = findGazByName(gazList, name);
        if (!gazOptional.isPresent()) {
            answerToRequest(response, 400, "Gaz \'" + name + "\' not in list");
            return;
        }
        Gaz gaz = gazOptional.get();

        Gaz newGaz = GazFactory.createGaz(newName, formule, masseMolaire, criticalTemperature, criticalPression, densite);
        gaz.update(newGaz);
        database.updateGaz(gaz);

        updateJsonFile(database, fileName);
        answerToRequest(response, 200, "Gaz \'" + name + "\' updated correctly");
    }

    /**
     * Met à jour le fichier json des gazs
     *
     * @param database Base de données du programme
     * @param fileName Nom du fichier json
     */
    private void updateJsonFile(Database database, String fileName) {
        String jsonString = generateJsonString(database);
        tools.writeFile(jsonString.getBytes(charset), fileName);
    }

    /**
     * Genère le string au format json de la liste des gaz
     *
     * @param database Base de données du programme
     * @return La string à ecrire dans le fichier
     */
    private String generateJsonString(Database database) {
        List<Gaz> gazList = database.getListGaz();
        StringBuilder sb = new StringBuilder();
        sb.append("[\n");
        gazList.stream()
                .map(gaz -> "\t{\n" +
                        "\t\t\"Name\":" + "\"" + gaz.getName() + "\"" + ",\n" +
                        "\t\t\"Formule\":" + "\"" + gaz.getFormule() + "\"" + ",\n" +
                        "\t\t\"MolarMass\":" + gaz.getMasseMolaire() + ",\n" +
                        "\t\t\"CriticalTemperature\":" + gaz.getCriticalTemperature() + ",\n" +
                        "\t\t\"CriticalPressure\":" + gaz.getCriticalPression() + ",\n" +
                        "\t\t\"Density\":" + gaz.getDensite() + "\n" +
                        "\t},\n")
                .collect(Collectors.toList())
                .forEach(sb::append);
        sb.replace(sb.lastIndexOf(","), sb.lastIndexOf(",") + 1, "");
        sb.append("\n]");
        return sb.toString();
    }

    /*##############################################################*/
    ////////////////////// remove Gaz Handler ////////////////////////
    /*##############################################################*/

    /**
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Gaz pas dans la liste
     * - Gaz utilisé par un site
     */
    void removeGaz(RoutingContext routingContext, Database database, String fileNameJson) {
        saspLogger.log(Level.INFO, "In remove Gaz request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeRemoveGazRequest(response, json, database, fileNameJson);
        }
    }

    private void analyzeRemoveGazRequest(HttpServerResponse response, JsonObject json, Database database, String fileName) {
        String gazName = json.getString("GazName");

        if (tools.verifyEmptyOrNull(gazName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }
        List<Gaz> gazList = database.getListGaz();
        Optional<Gaz> optGaz = findGazByName(gazList, gazName);
        if (!optGaz.isPresent()) {
            answerToRequest(response, 400, "Gaz not in list");
            return;
        }
        Gaz gaz = optGaz.get();

        if (database.isGazUsed(gaz)) {
            answerToRequest(response, 400, "Cannot remove a gaz that is used by a site");
            return;
        }
        database.deleteGaz(gaz);
        updateJsonFile(database, fileName);
        answerToRequest(response, 200, "Gaz removed");
    }

    /*##############################################################*/
    ///////////////////////// Usefull stuff //////////////////////////
    /*##############################################################*/

    /**
     * Trouve le gaz dans la liste par son nom
     *
     * @param gazList List des gaz
     * @param gazName Nom du gaz à trouver
     */
    private Optional<Gaz> findGazByName(List<Gaz> gazList, String gazName) {
        return gazList.stream()
                .filter(gaz -> gaz.getName().equals(gazName))
                .findFirst();
    }
}
