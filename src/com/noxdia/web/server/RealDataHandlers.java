package com.noxdia.web.server;

import com.noxdia.csv.CSVReaders;
import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.data.DataFactory;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.tools.Tools;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.*;

class RealDataHandlers {

    static final int nbDataBeforeCanCheckIncoherence = 35;
    private static final Tools tools = Tools.getInstance();
    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private final DecimalFormat df = new DecimalFormat("0.#######");

    RealDataHandlers() {
    }

    /**
     * True si "localDate" est dans la liste realDataList
     */
    static boolean checkIfDateIsInListOfRealData(List<RealData> realDataList, LocalDate localDate) {
        return realDataList
                .stream()
                .anyMatch(realData -> realData.getDateInsertion().isEqual(localDate));
    }

    /**
     * Verifie si la pression à une date T est incohérente ou non
     */
    static boolean checkIfPressionIsIncoherente(RealData realData, List<RealData> incoherenceDataList, List<RealData> realDataList) {
        // Si on a la bonne date , check si donnée cohérente ou non
        // Calcul de l'ecart de la valeur par rapport au 4 précédente , moyenne des ecarts entre les 4 valeurs précédente
        // Calcul ecart type total, si ecart > ecart type total * 10 ---> Erreur , mettre "null" dans "y" pour signifier erreur
        LocalDate currentDate = realData.getDateInsertion();
        double pression = realData.getPressionWelHead();
        double pressionPrevious = getPressionPreviousDate(currentDate, incoherenceDataList, realDataList);
        if (pressionPrevious == -1) {
            return true;
        }
        double ecartAtDate = Math.abs(pression - pressionPrevious);

        double ecartTypeTotalPreviousDates = calculEcartTypePressionPreviousDates(currentDate, incoherenceDataList, realDataList);
        // ecart type total = ecartType sur les valeurs precedente et pas sur totalité de la liste

        // Pour eviter les erreurs sur les constantes
        if (ecartTypeTotalPreviousDates < 0.001) {
            return false;
        }

        double coefMultiplePression = 9.5;
        double ecartTypeTotalMult = ecartTypeTotalPreviousDates * coefMultiplePression;
        return ecartAtDate >= ecartTypeTotalMult;
    }

    /**
     * Verifie si la temperature à une date T est incohérente ou non
     */
    static boolean checkIfTemperatureIsIncoherente(RealData realData, List<RealData> incoherenceDataList, List<RealData> realDataList) {
        // Si on a la bonne date , check si donnée cohérente ou non
        // Calcul de l'ecart de la valeur par rapport au 4 précédente , moyenne des ecarts entre les 4 valeurs précédente
        // Calcul ecart type total, si ecart > ecart type total * 10 ---> Erreur , mettre "null" dans "y" pour signifier erreur
        LocalDate currentDate = realData.getDateInsertion();
        double temperature = realData.getTemperatureWelHead();
        double temperaturePrevious = getTemperaturePreviousDate(currentDate, incoherenceDataList, realDataList);
        if (temperaturePrevious == -1) {
            return true;
        }
        double ecartAtDate = Math.abs(temperature - temperaturePrevious);

        double ecartTypeTotalPreviousDates = calculEcartTypeTemperaturePreviousDates(currentDate, incoherenceDataList, realDataList);

        // Pour eviter les erreurs sur les valeurs constantes
        if (ecartTypeTotalPreviousDates < 0.001) {
            return false;
        }
        // ecart type total = ecartType sur les valeurs precedente et pas sur totalité de la liste
        double coefMultipleTemperature = 10;
        double ecartTypeTotalMult = ecartTypeTotalPreviousDates * coefMultipleTemperature;

        return ecartAtDate >= ecartTypeTotalMult;
    }

    /**
     * Trouve les données réels à la date "currentDate" dans la liste "dataList" et renvoie un Optional
     */
    private static Optional<RealData> findDataAtDateInList(List<RealData> dataList, LocalDate currentDate) {
        return dataList.stream()
                .filter(realData -> realData.getDateInsertion().isEqual(currentDate))
                .findFirst();
    }

    /**
     * Récupère la temperature à la date précédente, si la valeur précédente est incohérente, on recule encore d'un jour,
     * jusqu'a à avoir un moment ou la temperature est cohérente.
     */
    private static double getTemperaturePreviousDate(LocalDate currentDate, List<RealData> incoherenceDataList, List<RealData> realDataList) {
        int nbDaysBefore = 1;
        double temperature;
        while (true) {
            Optional<RealData> optRealData = findDataAtDateInList(realDataList, currentDate.minusDays(nbDaysBefore));
            if (!optRealData.isPresent()) {
                return -1;
            }
            RealData realData = optRealData.get();
            temperature = realData.getTemperatureWelHead();
            if (realData.getDebit() != 0 || isRealDataTemperatureIncoherente(incoherenceDataList, realData)) {
                nbDaysBefore++;
            } else {
                break;
            }
        }
        return temperature;
    }

    /**
     * Calcul l'ecart type de la temperature sur les dates précédentes
     */
    private static double calculEcartTypeTemperaturePreviousDates(LocalDate currentDate, List<RealData> incoherenceDataList, List<RealData> realDataList) {
        LocalDate from = currentDate.minusDays(nbDataBeforeCanCheckIncoherence);
        LocalDate to = currentDate.minusDays(1);

        List<RealData> dataList = getAllRealDataBetweenDatesInList(realDataList, from, to);
        long countIncoherentData = dataList
                .stream()
                .filter(realData -> realData.getDebit() != 0 || isRealDataTemperatureIncoherente(incoherenceDataList, realData))
                .count();
        // Nombre de valeurs minimal pour detecter une incoherence + Nombre de jour avec le debit
        // Recup toutes ces valeurs
        // Garder que les valeurs avec debit = 0 et qui sont pas dans liste des incoherence
        long valuesToCheck = nbDataBeforeCanCheckIncoherence + countIncoherentData;
        from = currentDate.minusDays(valuesToCheck);
        List<Double> listValues = getAllRealDataBetweenDatesInList(realDataList, from, to)
                .stream()
                .filter(realData -> realData.getDebit() == 0 && !isRealDataTemperatureIncoherente(incoherenceDataList, realData))/*realData.getDebit() == 0 && !incoherenceDataList.contains(realData)*/
                .map(RealData::getTemperatureWelHead)
                .collect(Collectors.toList());
        return tools.calculEcartType(listValues);
    }

    /**
     * Indique si la temperature à la date T est incohérente
     */
    private static boolean isRealDataTemperatureIncoherente(List<RealData> incoherenceDataList, RealData realData) {
        return realData.getTemperatureWelHead() == -1 || incoherenceDataList.contains(realData);
    }

    /**
     * Récupère la liste de realData entre les dates "from" et "to" de manière inclusive
     * Renvoie la nouvelle liste
     */
    private static List<RealData> getAllRealDataBetweenDatesInList(List<RealData> realDataList, LocalDate from, LocalDate to) {
        return realDataList.stream()
                .filter(realData -> tools.dateBetweenOthersInclusive(realData.getDateInsertion(), from, to))
                .collect(Collectors.toList());
    }

    /**
     * Récupère la pression à la date précédente, si la valeur précédente est incohérente, on recule encore d'un jour,
     * jusqu'a à avoir un moment ou la pression est cohérente.
     */
    private static double getPressionPreviousDate(LocalDate currentDate, List<RealData> incoherenceDataList, List<RealData> realDataList) {
        int nbDaysBefore = 1;
        double pression;
        while (true) {
            Optional<RealData> optRealData = findDataAtDateInList(realDataList, currentDate.minusDays(nbDaysBefore));
            if (!optRealData.isPresent()) {
                return -1;
            }
            RealData realData = optRealData.get();
            pression = realData.getPressionWelHead();
            if (isRealDataPressionIncoherente(realData, incoherenceDataList)) {
                nbDaysBefore++;
            } else {
                break;
            }
        }
        return pression;
    }

    /**
     * Calcul de l'ecart type de la pression, en ne prenant pas en compte les données incohérentes
     */
    private static double calculEcartTypePressionPreviousDates(LocalDate currentDate, List<RealData> incoherenceDataList, List<RealData> realDataList) {
        LocalDate from = currentDate.minusDays(nbDataBeforeCanCheckIncoherence);
        LocalDate to = currentDate.minusDays(1);

        List<RealData> dataList = getAllRealDataBetweenDatesInList(realDataList, from, to);
        // Compte le nombre que l'on va eviter lors du calcul de l'ecart type
        long countIncoherenteData = dataList.stream()
                .filter(realData -> isRealDataPressionIncoherente(realData, incoherenceDataList))
                .count();
        from = currentDate.minusDays(nbDataBeforeCanCheckIncoherence + countIncoherenteData);

        List<Double> listValues = getAllRealDataBetweenDatesInList(realDataList, from, to)
                .stream()
                .filter(realData -> !isRealDataPressionIncoherente(realData, incoherenceDataList))
                .map(RealData::getPressionWelHead)
                .collect(Collectors.toList());
        return tools.calculEcartType(listValues);
    }

    /**
     * Indique si la pression à la date T est incohérente
     */
    private static boolean isRealDataPressionIncoherente(RealData realData, List<RealData> incoherenceDataList) {
        return realData.getPressionWelHead() == -1 || incoherenceDataList.contains(realData);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Importe le fichier csv contenant les données réel à insérer dans l'outil et stocke les valeurs dans une liste temporaire en cas de conflit
     * La confirmation se fera au travers d'une autre api
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant non trouvé dans la liste des exploitants
     * - Site non trouvé dans la liste des sites de l'exploitant
     * - Cavité non trouvé dans la liste des cavités du site
     * - Extension du fichier incorrect
     * <p>
     * Indique s'il y a des données incohérente
     */
    void importRealDataForCavity(RoutingContext routingContext, List<Exploitant> listExploitant, Database database, List<RealData> realDataListTemporary) {
        saspLogger.log(Level.INFO, "In importRealData For cavity request");
        analyzeImportRealDataForCavityRequest(routingContext, listExploitant, database, realDataListTemporary);
    }

    private void analyzeImportRealDataForCavityRequest(RoutingContext routingContext, List<Exploitant> listExploitant, Database database, List<RealData> realDataListTemporary) {
        HttpServerResponse response = routingContext.response();
        MultiMap entries = routingContext.request().formAttributes();
        String siteName = entries.get("importCSV2");
        String cavityName = entries.get("importCSV3");
        String pageAvantCommit = entries.get("importCSV4");
        String logModif = entries.get("importCSV5");

        if (tools.verifyEmptyOrNull(cavityName, siteName, pageAvantCommit)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant not found for site name");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant not found in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> listSite = exp.getListSite();

        Optional<Site> optSite = findSiteByName(listSite, siteName);
        if (!optSite.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Site \'" + siteName + "\' not found in list");
            return;
        }
        Site site = optSite.get();

        List<Cavity> cavityList = site.getListCavity();

        Optional<Cavity> optCavity = findCavityByName(cavityList, cavityName);
        if (!optCavity.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Cavity \'" + cavityName + "\' not found in list");
            return;
        }
        Cavity cavity = optCavity.get();

        byte[] data = new byte[0];
        String extension = ".csv";
        for (FileUpload f : routingContext.fileUploads()) {
            Path path = Paths.get(f.uploadedFileName());
            try {
                extension = f.fileName().substring(f.fileName().lastIndexOf("."));
                if (!extension.equals(".csv")) {
                    Files.delete(path);
                    redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Wrong file format !");
                    return;
                }
                data = Files.readAllBytes(path);
                Files.delete(path);
            } catch (IOException e) {
                saspLogger.log(Level.WARNING, e.getMessage());
            }
        }

        String uploadTemplocation = "./webroot/asset/uploads/realData_" + cavityName + extension;
        tools.writeFile(data, uploadTemplocation);

        Path path = Paths.get(uploadTemplocation);

        List<RealData> realDataList = CSVReaders.importCSVRealData(path, cavity, logModif);
        realDataList.sort(tools.dataRealComparator);

        List<RealData> cavityRealData = database.getRealDataForCavity(cavity);
        if (cavityRealData.isEmpty()) {
            updateRealDataInCavityFromList(cavity, realDataList);
        } else {
            boolean conflict = isThereConflict(cavityRealData, realDataList);
            if (conflict) {
                // Ajout dans une liste temporaire pour confirmer plus tard
                realDataListTemporary.addAll(realDataList);
                redirectionToOldPage(response, pageAvantCommit + "?confVal", Level.INFO, "Conflicting values for import need confirm");
                return;
            } else {
                // Pas de conflit -> Ajout dans la liste
                updateRealDataInCavityFromList(cavity, realDataList);
            }
        }
        // Suppression du fichier temporaire pour eviter de polluer le repertoire
        Path pathUploadTempLocation = Paths.get(uploadTemplocation);
        tools.deleteFileIfExists(pathUploadTempLocation);
        redirectionToOldPage(response, pageAvantCommit, Level.INFO, "Real data for dates added");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données en conflit lors de l'import d'un fichier
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Pas de données temporaire -> requete d'import non réussi avant
     */
    void getConflictingValuesForRealData(RoutingContext routingContext, Database database, List<RealData> realDataListTemporary) {
        saspLogger.log(Level.INFO, "In getConflictingValuesForRealData request");
        HttpServerResponse response = routingContext.response();
        // realDataListTemporary ne sera normalement jamais vide ici
        // Ne devrait jamais arriver
        if (realDataListTemporary.isEmpty()) {
            answerToRequest(response, 400, "No temporary data");
            return;
        }
        Cavity cavity = realDataListTemporary.get(0).getCavity();
        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        List<JsonObject> jsonObjectList = new ArrayList<>();

        realDataList.forEach(realData -> {
            LocalDate currentDate = realData.getDateInsertion();
            if (checkIfDateIsInListOfRealData(realDataListTemporary, currentDate)) {

                Optional<RealData> optRealDataTempo = findDataAtDateInList(realDataListTemporary, currentDate);
                RealData realDataTemp = optRealDataTempo.orElse(realData);

                double pressionOld = realData.getPressionWelHead();
                double pressionNew = realDataTemp.getPressionWelHead();

                double tempOld = realData.getTemperatureWelHead();
                double tempNew = realDataTemp.getTemperatureWelHead();

                double volumeOld = realData.getVolume();
                double volumeNew = realDataTemp.getVolume();

                double debitOld = realData.getDebit();
                double debitNew = realDataTemp.getDebit();

                JsonObject jsonObject = new JsonObject();
                jsonObject.put("Date", dtf.format(currentDate));

                jsonObject.put("PressionOld", df.format(pressionOld));
                jsonObject.put("PressionNew", df.format(pressionNew));

                jsonObject.put("TempOld", df.format(tempOld));
                jsonObject.put("TempNew", df.format(tempNew));

                jsonObject.put("VolumeOld", df.format(volumeOld));
                jsonObject.put("VolumeNew", df.format(volumeNew));

                jsonObject.put("DebitOld", df.format(debitOld));
                jsonObject.put("DebitNew", df.format(debitNew));

                jsonObjectList.add(jsonObject);
            }
        });
        answerToRequest(response, 200, jsonObjectList);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Vide la liste de données temporaires
     */
    void clickOnAnnule(RoutingContext routingContext, List<RealData> realDataListTemporary) {
        saspLogger.log(Level.INFO, "In clickOnAnnule request");
        HttpServerResponse response = routingContext.response();
        realDataListTemporary.clear();
        answerToRequest(response, 200, "Real data list temp cleared");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Confirme l'ajout des données malgrés les conflits dans les données
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Pas de données temporaire -> requete d'import non réussi avant
     */
    void confirmForRealDataForCavity(RoutingContext routingContext, List<RealData> realDataListTemporary) {
        saspLogger.log(Level.INFO, "In confirmForRealDataForCavity request");
        HttpServerResponse response = routingContext.response();

        if (realDataListTemporary.isEmpty()) {
            answerToRequest(response, 400, "No temporary data");
            return;
        }
        Cavity cavity = realDataListTemporary.get(0).getCavity();

        updateRealDataInCavityFromList(cavity, realDataListTemporary);
        realDataListTemporary.clear();
        answerToRequest(response, 200, "Real data updated");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données corrigés manuellement par l'utilisateur et corrige les données insérés dans la base de données
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant non trouvé dans la liste des exploitants
     * - Site non trouvé dans la liste des sites de l'exploitants
     * - Cavité non trouvé dans la liste des cavités du site
     */
    void correctRealDataForCavity(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In correctRealDataForCavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeCorrectRealDataForCavityRequest(response, json, database, listExploitant);
        }
    }

    private void analyzeCorrectRealDataForCavityRequest(HttpServerResponse response, JsonObject json, Database database, List<Exploitant> listExploitant) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        String logModif = json.getString("LogModif");
        JsonArray jsonArray = json.getJsonArray("TabRealData");

        if (tools.verifyEmptyOrNull(siteName, cavityName, logModif) || (null == jsonArray) || jsonArray.isEmpty()) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "Can't find exploitant name from site name");
            return;
        }

        String expName = optExpName.get();
        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Can't find exploitant in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> listSite = exp.getListSite();

        Optional<Site> optSite = findSiteByName(listSite, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Can't find site in list");
            return;
        }
        Site site = optSite.get();

        List<Cavity> cavityList = site.getListCavity();

        Optional<Cavity> optCavity = findCavityByName(cavityList, cavityName);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Can't find cavity for site");
            return;
        }
        Cavity cavity = optCavity.get();

        List<RealData> realDataList = new ArrayList<>();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject realDataJson = jsonArray.getJsonObject(i);

            String stringDate = realDataJson.getString("Date");
            LocalDate dateInsertion = LocalDate.from(dtf.parse(stringDate));
            double pression = Double.parseDouble(realDataJson.getString("Pression").trim());
            double temperature = Double.parseDouble(realDataJson.getString("Temperature").trim());
            double volume = Double.parseDouble(realDataJson.getString("Volume").trim());
            double debit = Double.parseDouble(realDataJson.getString("Debit").trim());

            LocalDate dateLastModif = LocalDate.now();

            RealData realData = DataFactory.createRealData(dateInsertion, cavity, temperature, pression, volume, debit, dateLastModif, logModif);
            realDataList.add(realData);
        }

        // eventuellement faire un update plutot
        updateRealDataInCavityFromList(cavity, realDataList);
        answerToRequest(response, 200, "Data corrected");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données réel pour la pression
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getRealPressionForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get real pression for cavity");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetRealPressionForCavityRequest(response, json, database);
        }
    }

    private void analyzeGetRealPressionForCavityRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");

        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No data");
            return;
        }

        realDataList.sort(tools.dataRealComparator);

        // Recup date debut et date fin , et faire une liste de dates comme ça .
        LocalDate from = realDataList.get(0).getDateInsertion();
        LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();
        createAndAnswerPression(response, cavity, realDataList, from, to);
    }

    private void createAndAnswerPression(HttpServerResponse response, Cavity cavity, List<RealData> realDataList, LocalDate from, LocalDate to) {
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);
        // S'il manque des dates -> ajoute des null dans la liste renvoyé
        final int[] count = {0, 0};

        // Ajout des données incohérente dans la liste de données incohérentes
        List<RealData> incoherenceDataList = new ArrayList<>();
        realDataList.forEach(realData -> {
            if (count[1] > nbDataBeforeCanCheckIncoherence) {
                if (checkIfPressionIsIncoherente(realData, incoherenceDataList, realDataList)) {
                    incoherenceDataList.add(realData);
                }
            }
            count[1]++;
        });

        List<JsonObject> jsonObjectList = realDataList
                .stream()
                .map(realData -> {
                    double pression = realData.getPressionWelHead();
                    JsonObject jsonObject = new JsonObject();
                    LocalDate currentDate = realData.getDateInsertion();
                    if (pression == -1) { // Check si manque date
                        jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                        jsonObject.put("isIncoherent", true);
                    } else {
                        // traitement incohérence
                        if (count[0] < nbDataBeforeCanCheckIncoherence) {
                            jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                            jsonObject.put("y", pression);
                            jsonObject.put("isIncoherent", false);
                        } else {
                            if (incoherenceDataList.contains(realData)) {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", pression);
                                jsonObject.put("isIncoherent", true);
                            } else {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", pression);
                                jsonObject.put("isIncoherent", false);
                            }
                        }
                        count[0]++;
                    }
                    return jsonObject;
                }).collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données réel pour la pression entre des dates donné
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getRealPressionForCavityDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get real pression for cavity Date");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetRealPressionForCavityDateRequest(response, json, database);
        }
    }

    private void analyzeGetRealPressionForCavityDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, dateFrom, dateTo)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));

        // Permet d'afficher uniquement les données existantes
        AvoidEmptyValues avoidEmptyValues = new AvoidEmptyValues(database, cavity, from, to).invoke();
        from = avoidEmptyValues.getFrom();
        to = avoidEmptyValues.getTo();

        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);

        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No data for given period");
            return;
        }
        createAndAnswerPression(response, cavity, realDataList, from, to);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données réel pour la temperature
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la temperature à afficher pour le graphe
     */
    void getRealTemperatureForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get real temperature for cavity");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetRealTemperatureForCavityRequest(response, json, database);
        }
    }

    private void analyzeGetRealTemperatureForCavityRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No data");
            return;
        }

        LocalDate from = realDataList.get(0).getDateInsertion();
        LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();
        createAndAnswerTemperature(response, cavity, realDataList, from, to);
    }

    private void createAndAnswerTemperature(HttpServerResponse response, Cavity cavity, List<RealData> realDataList, LocalDate from, LocalDate to) {
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        // S'il manque des dates -> ajoute des null dans la liste renvoyé
        final int[] count = {0, 0};

        // Ajout des données incohérente dans la liste de données incohérentes
        List<RealData> incoherenceDataList = new ArrayList<>();
        realDataList.forEach(realData -> {
            if (count[1] > nbDataBeforeCanCheckIncoherence) {
                if (realData.getDebit() == 0 && checkIfTemperatureIsIncoherente(realData, incoherenceDataList, realDataList)) {
                    incoherenceDataList.add(realData);
                }
            }
            count[1]++;
        });

        List<JsonObject> jsonObjectList = realDataList
                .stream()
                .map(realData -> {
                    double temperature = realData.getTemperatureWelHead();
                    JsonObject jsonObject = new JsonObject();
                    LocalDate currentDate = realData.getDateInsertion();
                    if (temperature < 0) { // Check si manque date
                        jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                        jsonObject.put("isIncoherent", true);
                    } else {
                        // traitement incohérence
                        if (count[0] < nbDataBeforeCanCheckIncoherence) {
                            jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                            jsonObject.put("y", temperature);
                            jsonObject.put("isIncoherent", false);
                        } else {
                            if (incoherenceDataList.contains(realData)) {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", temperature);
                                jsonObject.put("isIncoherent", true);
                            } else {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", temperature);
                                jsonObject.put("isIncoherent", false);
                            }
                        }
                        count[0]++;
                    }
                    return jsonObject;
                }).collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données réel pour la temperature entre des dates donné
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getRealTemperatureForCavityDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get real temperature for cavity date");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetRealTemperatureForCavityDateRequest(response, json, database);
        }
    }

    private void analyzeGetRealTemperatureForCavityDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        String siteName = json.getString("SiteName");

        if (tools.verifyEmptyOrNull(cavityName, dateFrom, dateTo, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));

        // Permet d'afficher uniquement les données existantes
        AvoidEmptyValues avoidEmptyValues = new AvoidEmptyValues(database, cavity, from, to).invoke();
        from = avoidEmptyValues.getFrom();
        to = avoidEmptyValues.getTo();

        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);

        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No data for given period");
            return;
        }
        createAndAnswerTemperature(response, cavity, realDataList, from, to);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données réel pour le volume
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la volume à afficher pour le graphe
     */
    void getRealVolumeForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get real volume for cavity");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetRealVolumeForCavityRequest(response, json, database);
        }
    }

    private void analyzeGetRealVolumeForCavityRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No data");
            return;
        }

        LocalDate from = realDataList.get(0).getDateInsertion();
        LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();

        createAndAnswerVolume(response, cavity, realDataList, from, to);
    }

    private void createAndAnswerVolume(HttpServerResponse response, Cavity cavity, List<RealData> realDataList, LocalDate from, LocalDate to) {
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);


        final int[] count = {0};

        // Ajout des données incohérente dans la liste de données incohérentes
        List<RealData> incoherenceDataList = new ArrayList<>();
        realDataList.forEach(realData -> {
            // Besoin de juste une date pour verifier le volume
            if (count[0] > 0) {
                if (checkIfVolumeIsIncoherente(realData, realDataList)) {
                    incoherenceDataList.add(realData);
                }
            }
            count[0]++;
        });

        List<JsonObject> jsonObjectList = realDataList
                .stream()
                .map(realData -> {
                    double volume = realData.getVolume();
                    JsonObject jsonObject = new JsonObject();
                    LocalDate currentDate = realData.getDateInsertion();
                    if (volume < 0) { // Check si manque date
                        jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                        jsonObject.put("isIncoherent", true);
                    } else {
                        if (incoherenceDataList.contains(realData)) {
                            jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                            jsonObject.put("y", volume);
                            jsonObject.put("isIncoherent", true);
                        } else {
                            jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                            jsonObject.put("y", volume);
                            jsonObject.put("isIncoherent", false);
                        }
                    }
                    return jsonObject;
                }).collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données réel pour le volume
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la volume à afficher pour le graphe
     */
    void getRealVolumeForCavityDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get real volume for cavity date");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetRealVolumeForCavityDateRequest(response, json, database);
        }
    }

    private void analyzeGetRealVolumeForCavityDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, dateFrom, dateTo, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));

        // Permet d'afficher uniquement les données existantes
        AvoidEmptyValues avoidEmptyValues = new AvoidEmptyValues(database, cavity, from, to).invoke();
        from = avoidEmptyValues.getFrom();
        to = avoidEmptyValues.getTo();

        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);

        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No data");
            return;
        }
        createAndAnswerVolume(response, cavity, realDataList, from, to);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données réel pour le débit
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la débit à afficher pour le graphe
     */
    void getRealDebitForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get real debit for cavity");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetRealDebitForCavityRequest(response, json, database);
        }
    }

    private void analyzeGetRealDebitForCavityRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No data");
            return;
        }
        // Ajouter quand dates manquantes
        LocalDate from = realDataList.get(0).getDateInsertion();
        LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();
        createAndAnswerDebit(response, cavity, realDataList, from, to);
    }

    private void createAndAnswerDebit(HttpServerResponse response, Cavity cavity, List<RealData> realDataList, LocalDate from, LocalDate to) {
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -123456789, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        List<JsonObject> jsonObjectList = realDataList.stream().map(realData -> {
            double debit = realData.getDebit();
            JsonObject jsonObject = new JsonObject();
            LocalDate currentDate = realData.getDateInsertion();

            if (debit == -123456789) { // Check si manque date
                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                jsonObject.put("y", "null");
                jsonObject.put("isIncoherent", true);
            } else {
                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                jsonObject.put("y", realData.getDebit());
                jsonObject.put("isIncoherent", false);
            }
            return jsonObject;
        }).collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Récupère la liste des données réel pour le débit entre des dates donné
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la débit à afficher pour le graphe
     */
    void getRealDebitForCavityDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get real debit for cavity date");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetRealDebitForCavityDateRequest(response, json, database);
        }
    }

    private void analyzeGetRealDebitForCavityDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, dateFrom, dateTo, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));

        AvoidEmptyValues avoidEmptyValues = new AvoidEmptyValues(database, cavity, from, to).invoke();
        from = avoidEmptyValues.getFrom();
        to = avoidEmptyValues.getTo();

        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);

        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No data for given period");
            return;
        }
        createAndAnswerDebit(response, cavity, realDataList, from, to);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Verifie si le volume à une date T est incohérente ou non
     */
    private boolean checkIfVolumeIsIncoherente(RealData realData, List<RealData> realDataList) {
        LocalDate currentDate = realData.getDateInsertion();

        Optional<RealData> optPreviousData = findDataAtDateInList(realDataList, currentDate.minusDays(1));
        if (!optPreviousData.isPresent()) {
            return false;
        }
        RealData previousRealData = optPreviousData.get();

        // Utilisation de la même taille de nombre pour comparer et réduire les erreurs liés aux doubles
        double volume = realData.getVolume();

        int precision = Double.toString(volume).length() - 1;

        BigDecimal volumeBd = new BigDecimal(volume);
        volumeBd = volumeBd.round(new MathContext(precision));
        double roundedVolume = volumeBd.doubleValue();

        double debit = realData.getDebit();
        BigDecimal debitBd = new BigDecimal(debit);
        debitBd = debitBd.round(new MathContext(precision));
        double roundedDebit = debitBd.doubleValue();

        double previousVolume = previousRealData.getVolume();
        BigDecimal previousVolumeBd = new BigDecimal(previousVolume);
        previousVolumeBd = previousVolumeBd.round(new MathContext(precision));
        double roundedPreviousVolume = previousVolumeBd.doubleValue();

        double previousVolumeDebit = roundedPreviousVolume + roundedDebit;
        BigDecimal bd = new BigDecimal(previousVolumeDebit);
        bd = bd.round(new MathContext(precision));
        double roundedPreviousVolumeDebit = bd.doubleValue();

        double ecart = Math.abs(roundedVolume - roundedPreviousVolumeDebit);
        // Pour eviter les erreurs lié aux approximation sur les doubles
//        return !(ecart < 0.000001);
        // todo -> verifier
        return !(ecart < 0.001);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Met à jour les données réels dans la liste de la cavité "cavity" et dans la base de données
     */
    private void updateRealDataInCavityFromList(Cavity cavity, List<RealData> realDataList) {
        realDataList.forEach(realData -> {
            // Enlever les anciennes valeurs à la même date
            cavity.removeRealData(realData);
            // Ajout des nouvels valeurs
            cavity.addRealData(realData);
        });
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Indique s'il y des conflits entre les 2 listes par rapport aux dates
     */
    private boolean isThereConflict(List<RealData> cavityRealDataList, List<RealData> realDataList) {
        for (RealData cavityRealData : cavityRealDataList) {
            LocalDate localDate = cavityRealData.getDateInsertion();
            if (checkIfDateIsInListOfRealData(realDataList, localDate)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Objet temporaire pour eviter d'avoir des valeurs vide à l'affichage
     */
    private static class AvoidEmptyValues {
        private Database database;
        private Cavity cavity;
        private LocalDate from;
        private LocalDate to;

        AvoidEmptyValues(Database database, Cavity cavity, LocalDate from, LocalDate to) {
            this.database = database;
            this.cavity = cavity;
            this.from = from;
            this.to = to;
        }

        public LocalDate getFrom() {
            return from;
        }

        public LocalDate getTo() {
            return to;
        }

        AvoidEmptyValues invoke() {
            List<RealData> realDataListTmp = database.getRealDataForCavity(cavity);
            realDataListTmp.sort(tools.dataRealComparator);

            LocalDate dateInsertionDebut = realDataListTmp.get(0).getDateInsertion();
            LocalDate dateInsertionFin = realDataListTmp.get(realDataListTmp.size() - 1).getDateInsertion();

            if (from.isBefore(dateInsertionDebut)) {
                from = dateInsertionDebut;
            }
            if (to.isAfter(dateInsertionFin)) {
                to = dateInsertionFin;
            }
            return this;
        }
    }
}
