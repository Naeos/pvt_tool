package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.docxGenerator.DocxGenerator;
import com.noxdia.docxGenerator.graphs.GraphsGenerator;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.tools.Tools;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.noxdia.web.server.HandlersUtils.answerToRequest;

class GenerateDocxHandlers {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    GenerateDocxHandlers() {
    }

    void generateDocx(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In generateDocx request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGenerateDocxRequest(response, json, database);
        }
    }

    /**
     * Genere le fichier docx à partir du template
     * Les cas d'erreurs sont les suivants :
     * - Données manquantes lors de la requete
     * - Mauvais nom de site
     * - Erreur lors de la generation des fichiers images des graph
     */
    private void analyzeGenerateDocxRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String dateGenerationString = json.getString("DateTo");

        boolean generateTemp = json.getBoolean("GenerateTemp");
        boolean generateCavern = json.getBoolean("GenerateCavern");
        boolean generateCasingShoe = json.getBoolean("GenerateCasingShoe");

        if (tools.verifyEmptyOrNull(siteName, dateGenerationString)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();

        LocalDate dateGeneration = LocalDate.from(dtf.parse(dateGenerationString));

        GraphsGenerator graphsGenerator = new GraphsGenerator(database, site, dateGeneration, generateTemp, generateCavern, generateCasingShoe);
        if (!graphsGenerator.generateAllGraphs()) {
            saspLogger.log(Level.WARNING, "GRAPHS GENERATOR NOT FINISHED !!!!!!!!!");
        } else {
            saspLogger.log(Level.INFO, "GRAPHS GENERATOR FINISHED !!");
        }

        // Cree la liste des nom des images à ajouter dans le document
        List<String> graphLocations = generateListNameFiles(response);
        if (graphLocations == null || graphLocations.isEmpty()) {
            answerToRequest(response, 400, "No screenshots");
            return;
        }

        DocxGenerator docxGenerator = new DocxGenerator("./templates/ReportTemplate.docx", database, generateTemp, generateCavern, generateCasingShoe);

        String dateReportFileName = dateGeneration.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH) + " " + dateGeneration.getYear();

        String newReportName = "SASP Survey Report - " + siteName + " - " + dateReportFileName;
        if (!docxGenerator.generate(newReportName, site, dateGeneration, graphLocations)) {
            cleanListScreenShots(graphLocations);
            answerToRequest(response, 400, "Error while generating docx");
            return;
        }
        cleanListScreenShots(graphLocations);

        answerToRequest(response, 200, "Docx generate correctly");
    }

    /**
     * Supprime tous les screenshots fait par l'appli lors de la creation du fichier docx, pour eviter de polluer le dossier
     *
     * @param graphLocations Liste des screenshots effectués
     */
    private void cleanListScreenShots(List<String> graphLocations) {
        for (String fileName : graphLocations) {
            Path path = Paths.get(fileName);
            tools.deleteFileIfExists(path);
        }
    }

    /**
     * Génère la liste des nom des images en scanner le dossier "screenshots"
     *
     * @return Liste des nom des images à inserer dans le docuement
     */
    private List<String> generateListNameFiles(HttpServerResponse response) {
        String baseLocation = "./webroot/asset/screenshots/";

        List<String> graphLocations;
        File[] files = new File(baseLocation).listFiles(File::isFile);
        if (files == null || files.length == 0) {
            answerToRequest(response, 400, "Error while generating list of screenshots to put in docx file");
            return null;
        }
        graphLocations = Stream.of(files)
                .filter(file -> file.getName().contains("png")) // format des fichiers images
                .map(file -> baseLocation + file.getName())
                .collect(Collectors.toList());
        return graphLocations;
    }
}
