package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.data.DataFactory;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.tools.Tools;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.answerToRequest;
import static com.noxdia.web.server.RealDataHandlers.*;
import static java.lang.Math.PI;

public class CasingShoeHandlers {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    private final Tools tools = Tools.getInstance();

    CasingShoeHandlers() {
    }

    /*#############################################*/
    /////////// Fonctions de calculs ///////////////
    /*#############################################*/

    /**
     * Calcul du PCasingShoe
     */
    public static double calculPCasingShoe(double masseMolaireTotal, double tubeLength, double diameter, double gasDensity, double tempWellHead, double pressionWellHead, double debit, double tempCritique, double pressionCritique) {
        if (tempWellHead == -1 || pressionWellHead == -1) {
            return -1;
        }
        double m = 1000000 * ((-debit)) * gasDensity / 24 / 3600;

        double temperatureKelv = tempWellHead + 273.15;

        double zFactor = calculZFactor(temperatureKelv / tempCritique, pressionWellHead * 0.1 / pressionCritique);

        double viscosity = calculGasViscosity(gasDensity, temperatureKelv * 9 / 5, pressionWellHead * 14.5, zFactor);

        double nre = diameter * ((4 * (Math.abs(m))) / ((PI * Math.pow(diameter, 2)))) / (viscosity * 0.001);

        double fricfac;
        if (nre == 0) {
            fricfac = 0;
        } else {
            fricfac = 1 / Math.pow(1.14 - 2 * Math.log10((21.25 / (Math.pow(nre, 0.9)) + (0.00025 / 0.18))), 2);
        }

        double ngp = masseMolaireTotal * 9.81 * tubeLength / (zFactor * temperatureKelv * 8314);

        double nfp = (4 * zFactor) * fricfac * 8314 * temperatureKelv * tubeLength * Math.pow(Math.abs(m), (1.1)) / ((Math.pow((pressionWellHead * Math.pow(10, 5)), 2) * diameter * ((Math.pow(PI, 2)) * Math.pow((Math.pow(diameter, 2) / 4), 2) * masseMolaireTotal)));

        return pressionWellHead * Math.pow((Math.exp(2 * ngp) + (nfp / (2 * ngp)) * (1 - Math.exp(2 * ngp))), 0.5);
    }

    /**
     * Calcul du zFactor
     */
    static double calculZFactor(double tr, double pr) {
        double del, zFactor = 1;
        double fun, dFun;
        double returnValue = 0;
        for (int i = 0; i < 500; i++) {
            SPVTValues spvtValues = calculSpvtDak(tr, pr, zFactor);
            fun = spvtValues.getFun();
            dFun = spvtValues.getDfun();
            del = -(fun / dFun);
            zFactor = zFactor + del;
            returnValue = zFactor;
            if (Math.abs(del) < 0.00000001) {
                break;
            }
        }
        return returnValue;
    }

    /**
     * Calcul Spvt Dak
     */
    private static SPVTValues calculSpvtDak(double tr, double pr, double zFactor) {

        double A1 = 0.3265,
                A2 = -1.07,
                A3 = -0.5339,
                A4 = 0.01569,
                A5 = -0.05165,
                A6 = 0.5475,
                A7 = -0.7361,
                A8 = 0.1844,
                A9 = 0.1056,
                A10 = 0.6134,
                A11 = 0.721;
        // C1 Term
        double C1 = A1 + A2 / tr + A3 / Math.pow(tr, 3) + A4 / Math.pow(tr, 4) + A5 / Math.pow(tr, 5);
        // C2 Term
        double C2 = A6 + A7 / tr + A8 / Math.pow(tr, 2);
        // C3 Term
        double C3 = A9 * (A7 / tr + A8 / Math.pow(tr, 2));

        //rhor Term
        double R_Z = 0.27 * (pr / (zFactor * tr));

        // Tr,rhor-Dependent Terms...//

        // C4 Term
        double T1 = A10 * (1.0 + A11 * Math.pow(R_Z, 2));
        double T2 = Math.pow(R_Z, 2) / Math.pow(tr, 3);
        double T3 = Math.exp(-A11 * Math.pow(R_Z, 2));
        double C4 = T1 * T2 * T3;

        // d(C4)/d(rho) Term
        T1 = 2.0 * A10 * R_Z / Math.pow(tr, 3);
        T2 = 1.0 + A11 * Math.pow(R_Z, 2) - (A11 * Math.pow(R_Z, 4));
        T3 = Math.exp(-A11 * Math.pow(R_Z, 2));
        double DC4DR = T1 * T2 * T3;

        // d(z)/d(rho) Term
//        T1 = C1;
//        T2 = 2 * C2 * R_Z;
//        T3 = -5 * C3 * Math.pow(R_Z, 4);
//        double T4 = DC4DR;
//        double DZDR = T1 + T2 + T3 + T4;

        // Dranchuk/Abou-Kassem Z-Factor EOS and Derivative

        // z Term
        T1 = 1;
        T2 = C1 * R_Z;
        T3 = C2 * Math.pow(R_Z, 2);
        double T4 = -C3 * Math.pow(R_Z, 5);
        double T5 = C4;
        double fun = zFactor - (T1 + T2 + T3 + T4 + T5);

        // d(z-fun)/dz Term
        T1 = 1;
        T2 = C1 * R_Z / zFactor;
        T3 = 2.0 * C2 * Math.pow(R_Z, 2) / zFactor;
        T4 = -5.0 * C3 * Math.pow(R_Z, 5) / zFactor;
        T5 = DC4DR * R_Z / zFactor;
        double dFun = T1 + T2 + T3 + T4 + T5;

        // cg from the Dranchuk/Abou-Kassem Z-Factor EOS

        // Reduced Compressibility Term
//        T1 = DZDR / (1 + (R_Z / zFactor) * DZDR);
//        double CGR = (1 / pr) - (0.27 / (Math.pow(zFactor, 2) * tr)) * T1;


//        '-- Reduced Pressure/Reduced Gas Compressibility Term (cg=Pr*Cgr/p)
//        double PR_CGR = pr * CGR;

        return new SPVTValues(dFun, fun);
    }

    /**
     * Calcul gas viscosity
     */
    private static double calculGasViscosity(double gasDensity, double tRank, double pPsia, double zFactor) {
        double DUM_MW = (28.969 * gasDensity);
        double RHO_DUM = (0.0014935 * pPsia * DUM_MW) / (zFactor * tRank);
        double TERM_K1 = (9.379 + (0.01607 * DUM_MW)) * Math.pow(tRank, 1.5);
        double TERM_K2 = (209.2 + (19.26 * DUM_MW) + tRank);
        double TERM_K = (TERM_K1 / TERM_K2);
        double TERM_X = (3.448 + (986.4 / tRank) + (0.01009 * DUM_MW));
        double TERM_Y = (2.447 - (0.2224 * TERM_X));
        return (0.0001 * TERM_K) * Math.exp(TERM_X * Math.pow(RHO_DUM, TERM_Y));
    }

    /**
     * Calcul de la temperature Casing Shoe
     */
    public static double calculTCasingShoe(double masseMolaireTotal, double tubeLength, double diameter, double densiteTotal, double tempWellHead, double pressionWellHead, double debit, double tempCritique, double pressionCritique) {
        if (tempWellHead == -1 || pressionWellHead == -1) {
            return -1;
        }
        double jtCoefficient = calculJTCoefficient(tempWellHead, pressionWellHead, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
        double pCasingShoe = calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
        double calculPertesThermiquesTube = calculPertesThermiquesTube(tempWellHead, pressionWellHead, debit, masseMolaireTotal, diameter, tubeLength, densiteTotal, tempCritique, pressionCritique);

        return tempWellHead + jtCoefficient * (pCasingShoe - pressionWellHead) + Math.abs(calculPertesThermiquesTube);
    }

    /**
     * Calcul de JT coefficient
     */
    private static double calculJTCoefficient(double temperature, double pressure, double densiteTotal, double masseMolaireTotal, double tempCritique, double pressionCritique) {
        double cp = ((1.1684 * Math.pow(1.0123, (temperature)) * Math.pow(temperature, (-0.6476))) + (2.1436 * Math.pow(1.0146, (pressure / 10)) * (Math.pow(pressure / 10, 0.0188))) * Math.pow(densiteTotal / 0.6, 0.025)) * masseMolaireTotal;
        double tempKelv = temperature + 273.15;
        double Tpr = tempKelv / tempCritique;
        double Ppr = (pressure * 0.1) / pressionCritique;
        double RhoPR = 0.27 * Ppr / ((calculZFactor(Tpr, pressure * 0.1 / pressionCritique)) * Tpr);
        double i = -(RhoPR / tempKelv) * (0.31506237 + (2 * (-1.0467099) / Tpr) + (4 * (-0.57832729) / Math.pow(Tpr, 3)) + RhoPR * ((-0.61232032 / Tpr) + 2 * (0.53530771 + (-0.61232032 / Tpr))));
        double ii = (-RhoPR / tempKelv) * ((6 * (-0.61232032) * (-0.10488813) * Math.pow(RhoPR, 4)) / Tpr - (RhoPR * (Math.exp(-0.68446549 * RhoPR * RhoPR))) * (0.68157001 * (0.68446549 * RhoPR * RhoPR * (2 * 0.68446549 * RhoPR * RhoPR - 5) - 5)) / Math.pow(Tpr, 3));
        return (8.314) * Math.pow(tempKelv, 2) * (i + ii) / (pressure * cp);
    }

    /**
     * Calcul des pertes thermique du tube
     */
    private static double calculPertesThermiquesTube(double temperature, double pressure, double debit, double masseMolaireTotal, double diameter, double tubeLength, double densiteTotal, double tempCritique, double pressionCritique) {
        double tempKelv = temperature + 273.15;
        double tr = tempKelv / tempCritique;
        double density = masseMolaireTotal * Math.pow(10, (-3)) * pressure * Math.pow(10, 5) / (8.314 * (calculZFactor(tr, pressure * 0.1 / pressionCritique)) * tempKelv);
        double M = 1000000 * ((Math.abs(debit))) * densiteTotal / 24 / 3600;
        double cp = (((1.1684 * Math.pow(1.0123, temperature) * Math.pow(temperature, (-0.6476))) + (2.1436 * Math.pow(1.0146, (pressure / 10)) * Math.pow((pressure / 10), 0.0188))) * Math.pow(densiteTotal / 0.6, 0.025) * masseMolaireTotal);
        double viscosity = calculGasViscosity(densiteTotal, tempKelv * 9 / 5, pressure * 14.5, calculZFactor(tr, pressure * 0.1 / pressionCritique));
        double Nre = 0.1 + diameter * ((4 * (Math.abs(M))) / ((PI * Math.pow(diameter, 2))) / (viscosity * 0.001));
        double fricfac;
        if (Nre == 0) {
            fricfac = 0;
        } else {
            fricfac = 1 / Math.pow(1.14 - 2 * Math.log10((21.25 / Math.pow(Nre, 0.9)) + (0.00025 / 0.18)), 2);
        }

        return ((masseMolaireTotal * 0.001 / (cp) * (fricfac * (Math.pow((4 * M / (density * (PI * Math.pow(diameter, 2)))), 2)) / (2 * diameter) - 9.81) * tubeLength));
    }

    /*########################################################*/
    /////////// Get casing shoe Pression Handler ///////////////
    /*########################################################*/

    /**
     * Récupère la liste de la pression simulé pour Casing Shoe
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getCasingShoePression(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get casing shoe pression request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetCasingShoePressionRequest(response, json, database);
        }
    }

    private void analyzeGetCasingShoePressionRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        if (tools.verifyEmptyOrNull(siteName, cavityName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "No cavity with name \'" + cavityName + "\' for site \'" + siteName + "\'");
            return;
        }
        Cavity cavity = optCavity.get();

        double densiteTotal = site.calculDensiteTotal();
        double masseMolaireTotal = site.calculMasseMolaireTotal();
        double tempCritique = site.calculTempCritiqueTotal();
        double pressionCritique = site.calculPressionCritiqueTotal();
        if (densiteTotal == -1 || masseMolaireTotal == -1 || tempCritique == -1 || pressionCritique == -1) {
            answerToRequest(response, 400, "No gaz for site \'" + siteName + "\'");
            return;
        }
        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No realData in cavity");
            return;
        }

        realDataList.sort(tools.dataRealComparator);
        LocalDate from = realDataList.get(0).getDateInsertion();
        LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();
        createAndAnswerPressionCasingShoe(response, realDataList, from, to, cavity, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
    }

    private void createAndAnswerPressionCasingShoe(HttpServerResponse response, List<RealData> realDataList, LocalDate from, LocalDate to, Cavity cavity, double densiteTotal, double masseMolaireTotal, double tempCrique, double pressionCritique) {
        double tubeLength = cavity.getHauteurTubage();
        double diameter = cavity.getDiametreTubage();

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        final int[] count = {0, 0};

        // Ajout des données incohérente dans la liste de données incohérentes
        List<RealData> incoherenceDataList = new ArrayList<>();
        realDataList.forEach(realData -> {
            if (count[1] > nbDataBeforeCanCheckIncoherence) {
                if (checkIfPressionIsIncoherente(realData, incoherenceDataList, realDataList)) {
                    incoherenceDataList.add(realData);
                }
            }
            count[1]++;
        });

        List<JsonObject> jsonObjectList = realDataList
                .stream()
                .map(realData -> {
                    JsonObject jsonObject = new JsonObject();
                    LocalDate currentDate = realData.getDateInsertion();
                    double tempWellHead = realData.getTemperatureWelHead();
                    double pressionWellHead = realData.getPressionWelHead();
                    double debit = realData.getDebit();
                    double pCasingShoe = calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCrique, pressionCritique);
                    if (pCasingShoe == -1) { // Check si manque date
                        jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                        jsonObject.put("isIncoherent", true);
                    } else {
                        // traitement incohérence
                        if (count[0] < nbDataBeforeCanCheckIncoherence) {
                            jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                            jsonObject.put("y", pCasingShoe);
                            jsonObject.put("isIncoherent", false);
                        } else {
                            if (incoherenceDataList.contains(realData)) {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", pCasingShoe);
                                jsonObject.put("isIncoherent", true);
                            } else {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", pCasingShoe);
                                jsonObject.put("isIncoherent", false);
                            }
                        }
                        count[0]++;
                    }
                    return jsonObject;
                }).collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    /////////// Get casing shoe Pression Date Handler ////////////////
    /*##############################################################*/

    /**
     * Récupère la liste de la pression simulé pour Casing Shoe entre les date 'from' et 'to'
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getCasingShoePressionDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get casing shoe pression Date request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetCasingShoePressionDateRequest(response, json, database);
        }
    }

    private void analyzeGetCasingShoePressionDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        if (tools.verifyEmptyOrNull(siteName, cavityName, dateFrom, dateTo)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "No cavity with name \'" + cavityName + "\' for site \'" + siteName + "\'");
            return;
        }
        Cavity cavity = optCavity.get();

        double densiteTotal = site.calculDensiteTotal();
        double masseMolaireTotal = site.calculMasseMolaireTotal();
        double tempCritique = site.calculTempCritiqueTotal();
        double pressionCritique = site.calculPressionCritiqueTotal();

        if (densiteTotal == -1 || masseMolaireTotal == -1 || tempCritique == -1 || pressionCritique == -1) {
            answerToRequest(response, 400, "No gaz for site \'" + siteName + "\'");
            return;
        }

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No realData in cavity");
            return;
        }

        realDataList.sort(tools.dataRealComparator);
        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));
        createAndAnswerPressionCasingShoe(response, realDataList, from, to, cavity, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
    }

    /*##############################################################*/
    //////////// Get casing shoe Temperature Handler /////////////////
    /*##############################################################*/

    /**
     * Récupère la liste de la temperature simulé pour Casing Shoe
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la temperature à afficher pour le graphe
     */
    void getCasingShoeTemperature(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get casing shoe temperature request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetCasingShoeTemperatureRequest(response, json, database);
        }
    }

    private void analyzeGetCasingShoeTemperatureRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        if (tools.verifyEmptyOrNull(siteName, cavityName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "No cavity with name \'" + cavityName + "\' for site \'" + siteName + "\'");
            return;
        }
        Cavity cavity = optCavity.get();

        double densiteTotal = site.calculDensiteTotal();
        double masseMolaireTotal = site.calculMasseMolaireTotal();
        double tempCritique = site.calculTempCritiqueTotal();
        double pressionCritique = site.calculPressionCritiqueTotal();

        if (densiteTotal == -1 || masseMolaireTotal == -1 || tempCritique == -1 || pressionCritique == -1) {
            answerToRequest(response, 400, "No gaz for site \'" + siteName + "\'");
            return;
        }

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No realData in cavity");
            return;
        }

        realDataList.sort(tools.dataRealComparator);
        LocalDate from = realDataList.get(0).getDateInsertion();
        LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();
        createAndAnswerTemperatureCasingShoe(response, realDataList, from, to, cavity, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
    }

    /*##############################################################*/
    ////////// Get casing shoe Temperature Date Handler //////////////
    /*##############################################################*/

    /**
     * Récupère la liste de la temperature simulé pour Casing Shoe entre les date 'from' et 'to'
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la temperature à afficher pour le graphe
     */
    void getCasingShoeTemperatureDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get casing shoe temperature request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetCasingShoeTemperatureDateRequest(response, json, database);
        }
    }

    private void analyzeGetCasingShoeTemperatureDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        if (tools.verifyEmptyOrNull(siteName, cavityName, dateFrom, dateTo)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "No site with name \'" + siteName + "\'");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "No cavity with name \'" + cavityName + "\' for site \'" + siteName + "\'");
            return;
        }
        Cavity cavity = optCavity.get();

        double densiteTotal = site.calculDensiteTotal();
        double masseMolaireTotal = site.calculMasseMolaireTotal();
        double tempCritique = site.calculTempCritiqueTotal();
        double pressionCritique = site.calculPressionCritiqueTotal();

        if (densiteTotal == -1 || masseMolaireTotal == -1 || tempCritique == -1 || pressionCritique == -1) {
            answerToRequest(response, 400, "No gaz for site \'" + siteName + "\'");
            return;
        }

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (realDataList.isEmpty()) {
            answerToRequest(response, 400, "No realData in cavity");
            return;
        }

        realDataList.sort(tools.dataRealComparator);
        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));
        createAndAnswerTemperatureCasingShoe(response, realDataList, from, to, cavity, densiteTotal, masseMolaireTotal, tempCritique, pressionCritique);
    }

    private void createAndAnswerTemperatureCasingShoe(HttpServerResponse response, List<RealData> realDataList, LocalDate from, LocalDate to, Cavity cavity, double densiteTotal, double masseMolaireTotal, double tempCritique, double pressionCritique) {
        double tubeLength = cavity.getHauteurTubage();
        double diameter = cavity.getDiametreTubage();

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        // S'il manque des dates -> ajoute des null dans la liste renvoyé
        final int[] count = {0, 0};

        // Ajout des données incohérente dans la liste de données incohérentes
        List<RealData> incoherenceDataList = new ArrayList<>();
        realDataList.forEach(realData -> {
            if (count[1] > nbDataBeforeCanCheckIncoherence) {
                if (realData.getDebit() == 0 && checkIfTemperatureIsIncoherente(realData, incoherenceDataList, realDataList)) {
                    incoherenceDataList.add(realData);
                }
            }
            count[1]++;
        });

        List<JsonObject> jsonObjectList = realDataList
                .stream()
                .map(realData -> {
                    JsonObject jsonObject = new JsonObject();
                    LocalDate currentDate = realData.getDateInsertion();
                    double tempWellHead = realData.getTemperatureWelHead();
                    double pressionWellHead = realData.getPressionWelHead();
                    double debit = realData.getDebit();
                    double tCasingShoe = calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
                    if (tCasingShoe == -1) { // Check si manque date
                        jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                        jsonObject.put("isIncoherent", true);
                    } else {
                        // traitement incohérence
                        if (count[0] < nbDataBeforeCanCheckIncoherence) {
                            jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                            jsonObject.put("y", tCasingShoe);
                            jsonObject.put("isIncoherent", false);
                        } else {
                            if (incoherenceDataList.contains(realData)) {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", tCasingShoe);
                                jsonObject.put("isIncoherent", true);
                            } else {
                                jsonObject.put("x", currentDate.getYear() + "," + currentDate.getMonthValue() + "," + currentDate.getDayOfMonth());
                                jsonObject.put("y", tCasingShoe);
                                jsonObject.put("isIncoherent", false);
                            }
                        }
                        count[0]++;
                    }
                    return jsonObject;
                }).collect(Collectors.toList());

        answerToRequest(response, 200, jsonObjectList);
    }

    private static class SPVTValues {
        private final double fun;
        private final double dfun;

        private SPVTValues(double dfun, double fun) {
            this.dfun = dfun;
            this.fun = fun;
        }

        double getFun() {
            return fun;
        }

        double getDfun() {
            return dfun;
        }
    }

}
