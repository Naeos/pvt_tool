package com.noxdia.web.server;

import com.noxdia.csv.CSVReaders;
import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.CavityFactory;
import com.noxdia.tools.Tools;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.*;

class CavityHandlers {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    CavityHandlers() {
    }

    /*##############################################################*/
    /////////////////////// add Cavity Handler //////////////////////
    /*##############################################################*/

    /**
     * Ajoute une cavité dans la base donnée et dans la liste des cavités d'un site, à partir des informations reçu en JSON
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site donné
     * - Nom d'exploitant incorrect -> Introuvable dans la liste
     * - Site non trouvé dans la liste
     */
    void addCavity(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In add cavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeAddCavityRequest(json, response, listExploitant, database);
        }
    }

    private void analyzeAddCavityRequest(JsonObject json, HttpServerResponse response, List<Exploitant> listExploitant, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        double height = Double.parseDouble(json.getString("Height"));
        double diameter = Double.parseDouble(json.getString("Diameter"));
        double depth = Double.parseDouble(json.getString("Depth"));
        double volume = Double.parseDouble(json.getString("Volume"));

        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "No exploitant found for site");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = exp.getListSite();

        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \'" + siteName + "\' not found in list exploitant");
            return;
        }
        Site site = optSite.get();
        LocalDate localDate = LocalDate.now();
        Cavity cavity = CavityFactory.createCavity(site, cavityName, height, diameter, depth, volume, localDate, database);
        if (!site.addCavity(cavity)) {
            answerToRequest(response, 400, "Cavity \'" + cavityName + "\' already exists for site");
            return;
        }
        answerToRequest(response, 200, "Cavity created");
    }

    /*##############################################################*/
    ////////////////////// update Cavity Handler /////////////////////
    /*##############################################################*/

    /**
     * Met à jour la cavité avec les informations envoyé par le client, au format JSON
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site donné
     * - Nom d'exploitant incorrect -> Introuvable dans la liste
     * - Site non trouvé dans la liste
     * - Nouveau nom de cavité déjà utilisé
     * - Ancien nom de cavité incorrect -> cavité introuvable
     */
    void updateCavity(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In update Cavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeUpdateCavityRequest(response, json, exploitantList, database);
        }
    }

    private void analyzeUpdateCavityRequest(HttpServerResponse response, JsonObject json, List<Exploitant> exploitantList, Database database) {
        String newCavityName = json.getString("NewCavityName");
        String oldCavityName = json.getString("OldCavityName");
        String siteName = json.getString("SiteName");
        double height = Double.parseDouble(json.getString("Height"));
        double diameter = Double.parseDouble(json.getString("Diameter"));
        double depth = Double.parseDouble(json.getString("Depth"));
        double volume = Double.parseDouble(json.getString("Volume"));

        if (tools.verifyEmptyOrNull(newCavityName, siteName, oldCavityName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "No exploitant found for site");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = exp.getListSite();

        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \'" + siteName + "\' not found in list exploitant");
            return;
        }
        Site site = optSite.get();

        List<Cavity> cavityList = site.getListCavity();

        if (!newCavityName.equals(oldCavityName)) {
            Optional<Cavity> newCavityOpt = findCavityByName(cavityList, newCavityName);
            if (newCavityOpt.isPresent()) {
                answerToRequest(response, 400, "Can't rename cavity to \'" + newCavityName + "\', already exists");
                return;
            }
        }

        Optional<Cavity> cavityOpt = findCavityByName(cavityList, oldCavityName);
        if (!cavityOpt.isPresent()) {
            answerToRequest(response, 400, "No cavity with name : \'" + oldCavityName + "\'");
            return;
        }
        Cavity cavity = cavityOpt.get();

        LocalDate localDate = LocalDate.now();
        Cavity cavityUpdated = CavityFactory.createCavity(site, newCavityName, height, diameter, depth, volume, localDate, database);
        cavity.update(cavityUpdated);

        answerToRequest(response, 200, "Cavity \'" + newCavityName + "\' updated");
    }

    /*##############################################################*/
    /////////////////////// remove Cavity Handler ////////////////////
    /*##############################################################*/

    /**
     * Supprime la cavité de la base de donnée et de la liste des cavité du site avec les informations envoyé par le client, au format JSON
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant pas la liste des exploitant
     * - Site non trouvé dans la liste des sites de l'exploitant
     * - Mot de passe incorrect
     * - Cavité non trouvé dans la liste des cavité du site
     */
    void removeCavity(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In remove cavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeRemoveCavityRequest(response, json, exploitantList, database);
        }
    }

    private void analyzeRemoveCavityRequest(HttpServerResponse response, JsonObject json, List<Exploitant> exploitantList, Database database) {
        String siteName = json.getString("SiteName");
        String cavityName = json.getString("CavityName");
        String password = json.getString("Password");

        if (tools.verifyEmptyOrNull(cavityName, siteName, password)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "No exploitant found for site");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = exp.getListSite();

        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \'" + siteName + "\' not found in list exploitant");
            return;
        }
        Site site = optSite.get();
        String passwordHash = tools.toSHA256(password);
        if (!site.checkPassword(passwordHash)) {
            answerToRequest(response, 400, "Wrong password");
            return;
        }

        Optional<Cavity> optCavity = findCavityByName(site.getListCavity(), cavityName);

        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity \'" + cavityName + "\' not found for site \'" + siteName + "\'");
            return;
        }

        Cavity cavity = optCavity.get();
        site.removeCavity(cavity);
        answerToRequest(response, 200, "Remove cavity \'" + cavityName + "\' worked");
    }

    /*##############################################################*/
    ////////////////////// Import Cavity Handler /////////////////////
    /*##############################################################*/

    /**
     * Importe les informations d'une cavité via un fichier zip au format défini dans le manuel utilisateur
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Erreur dans le format des dossiers / fichiers
     * - Exploitant non trouvé dans la liste des exploitants
     * - Site non trouvé dans la liste des sites de l'exploitant
     * <p>
     * Redirige sur la page ou l'utilisateur était
     */
    void importCavity(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In import cavity request");
        analyzeImportCavityRequest(routingContext, listExploitant, database);
    }

    private void analyzeImportCavityRequest(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();

        String pageAvantCommit = entries.get("importC2");
        String password = entries.get("importC3");

        if (tools.verifyEmptyOrNull(pageAvantCommit, password)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Missing input data");
            return;
        }

        byte[] data = new byte[0];
        StringBuilder sb = new StringBuilder();
        sb.append("./");
        for (FileUpload f : routingContext.fileUploads()) {
            Path path = Paths.get(f.uploadedFileName());
            try {
                sb.append(f.fileName());
                data = Files.readAllBytes(path);
                Files.delete(path);
            } catch (IOException e) {
                saspLogger.log(Level.WARNING, e.getMessage());
            }
        }

        // Ecrire le fichier zip reçu sous un nom correct dans un dossier temporaire
        Path zipFileLocation = Paths.get(sb.toString());
        String zipFileName = zipFileLocation.toString();
        tools.writeFile(data, zipFileName);

        Optional<Cavity> optCavity = CSVReaders.importCavityZip(zipFileName, database, password);
        if (!optCavity.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Cannot import cavity");
            return;
        }
        Cavity cavity = optCavity.get();

        Site site = cavity.getSite();
        String expName = site.getExploitant().getName();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);

        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant \'" + expName + "\' not found in list");
            return;
        }
        // Redondant mais pas le choix pour avoir le site stocker en memoire, sinon probleme après
        Exploitant exp = optExp.get();
        List<Site> siteList = exp.getListSite();
        String siteName = site.getName();
        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Site \'" + siteName + "\' not found in list");
            return;
        }

        Site siteInList = optSite.get();
        if (!siteInList.addCavity(cavity)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Site \'" + siteName + "\' not found in list");
            return;
        }

        redirectionToOldPage(response, pageAvantCommit, Level.INFO, "Cavity imported correctly");
    }

    /*##############################################################*/
    ////////////////////// exportToZip Cavity Handler /////////////////////
    /*##############################################################*/

    /**
     * Importe les informations d'une cavité dans un fichier zip protégé par un mot passe, au format défini dans le manuel utilisateur
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant pas la liste des exploitants
     * - Site non trouvé dans la liste des sites de l'exploitant
     * - Cavité non trouvé dans la liste des cavités du site
     */
    void exportCavity(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In exportToZip cavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeExportCavityRequest(response, json, exploitantList, database);
        }
    }

    private void analyzeExportCavityRequest(HttpServerResponse response, JsonObject json, List<Exploitant> exploitantList, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        String password = json.getString("Password");

        if (tools.verifyEmptyOrNull(cavityName, siteName, password)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "No exploitant found for site");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = exp.getListSite();

        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \'" + siteName + "\' not found in list exploitant");
            return;
        }
        Site site = optSite.get();

        List<Cavity> cavityList = site.getListCavity();

        Optional<Cavity> optCavity = findCavityByName(cavityList, cavityName);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not found");
            return;
        }
        Cavity cavity = optCavity.get();
        cavity.exportToZip(password);
        answerToRequest(response, 200, "Cavity exported");
    }

    /*##############################################################*/
    ////////////////////// get list Cavity Handler //////////////////
    /*##############################################################*/

    /**
     * Récupère la liste des cavités d'un site donné
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant introuvable pour le site
     * - Exploitant non renseigné dans la liste des exploitants
     * - Site non trouvé dans la liste des sites de l'exploitant
     * - Pas de cavités pour le site donné
     * <p>
     * Renvoie la liste des cavités du site si pas d'erreurs, rien sinon
     */
    void getListCavity(RoutingContext routingContext, List<Exploitant> exploitantList, Database database) {
        saspLogger.log(Level.INFO, "In get List cavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeGetListCavityRequest(response, json, exploitantList, database);
        }
    }

    private void analyzeGetListCavityRequest(HttpServerResponse response, JsonObject json, List<Exploitant> exploitantList, Database database) {
        String siteName = json.getString("SiteName");

        if (tools.verifyEmptyOrNull(siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "Exploit does not exists");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(exploitantList, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant for site \'" + siteName + "\' not found");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = database.getListSiteForExp(exp);

        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site " + siteName + " not found in exp list site");
            return;
        }
        Site site = optSite.get();

        List<Cavity> cavityList = database.getListCavityForSite(site);
        if (cavityList.isEmpty()) {
            answerToRequest(response, 400, "No cavity in site");
            return;
        }

        List<JsonObject> jsonObjectList = cavityList.stream().map(cavity -> {
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("CavityName", cavity.getName());
            jsonObject.put("Height", cavity.getHauteurTubage());
            jsonObject.put("Diameter", cavity.getDiametreTubage());
            jsonObject.put("Depth", cavity.getDepth());
            jsonObject.put("Volume", cavity.getVolume());
            jsonObject.put("LogoExp", exp.getLogoLocationClient());
            return jsonObject;
        }).collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

}
