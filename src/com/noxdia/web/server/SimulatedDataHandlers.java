package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.data.DataFactory;
import com.noxdia.site.cavite.data.SimulatedData;
import com.noxdia.tools.Tools;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.answerToRequest;

class SimulatedDataHandlers {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    SimulatedDataHandlers() {
    }

    /*##############################################################*/
    ////////// Get Simulated Pression For Cavity Handler /////////////
    /*##############################################################*/

    /**
     * Récupère la liste des données simulée pour la pression
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getSimulatedPressionForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get simulated pression for cavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetSimulatedPressionForCavityRequest(response, json, database);
        }
    }

    private void analyzeGetSimulatedPressionForCavityRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            answerToRequest(response, 400, "No data");
            return;
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        LocalDate from = simulatedDataList.get(0).getDateInsertion();
        LocalDate to = simulatedDataList.get(simulatedDataList.size() - 1).getDateInsertion();
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        List<JsonObject> jsonObjectList = simulatedDataList
                .stream()
                .map(simulatedData -> {
                    double pression = simulatedData.getPressionWelHead();
                    JsonObject jsonObject = new JsonObject();
                    LocalDate localDate = simulatedData.getDateInsertion();
                    if (pression < 0) { // Check si manque date
                        jsonObject.put("x", localDate.getYear() + "," + localDate.getMonthValue() + "," + localDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                    } else {
                        jsonObject.put("x", localDate.getYear() + "," + localDate.getMonthValue() + "," + localDate.getDayOfMonth());
                        jsonObject.put("y", simulatedData.getPressionWelHead());
                    }
                    return jsonObject;
                })
                .collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    //////// Get Simulated Pression For Cavity Date Handler //////////
    /*##############################################################*/

    /**
     * Récupère la liste des données simulée pour la pression entre des dates donné
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la pression à afficher pour le graphe
     */
    void getSimulatedPressionForCavityDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get simulated pression for cavity date request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetSimulatedPressionForCavityDateRequest(response, json, database);
        }
    }

    private void analyzeGetSimulatedPressionForCavityDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, dateFrom, dateTo, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }

        Cavity cavity = optCavity.get();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));

        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            answerToRequest(response, 400, "No data for given period");
            return;
        }

        simulatedDataList.sort(tools.simulatedDataComparator);
        // Ajouter toutes les données simulées , puis ne garder que celles dans la période donnée.
        List<SimulatedData> filteredSimulatedData = simulatedDataList
                .stream()
                .filter(simulatedData -> {
                    LocalDate date = simulatedData.getDateInsertion();
                    return tools.dateBetweenOthersInclusive(date, from, to);
                }).collect(Collectors.toList());

        if (filteredSimulatedData.isEmpty()) {
            answerToRequest(response, 400, "No data for given period");
            return;
        }
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfSimulatedData(filteredSimulatedData, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                filteredSimulatedData.add(fakeSimulatedData);
            }
        });
        filteredSimulatedData.sort(tools.simulatedDataComparator);

        // Devrait fonctionner
        List<JsonObject> jsonObjectList = filteredSimulatedData
                .stream()
                .map(simulatedData -> {
                    double pression = simulatedData.getPressionWelHead();
                    JsonObject jsonObject = new JsonObject();
                    LocalDate localDate = simulatedData.getDateInsertion();
                    if (pression < 0) { // Check si manque date
                        jsonObject.put("x", localDate.getYear() + "," + localDate.getMonthValue() + "," + localDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                    } else {
                        jsonObject.put("x", localDate.getYear() + "," + localDate.getMonthValue() + "," + localDate.getDayOfMonth());
                        jsonObject.put("y", simulatedData.getPressionWelHead());
                    }
                    return jsonObject;
                })
                .collect(Collectors.toList());

        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    ///////// Get Simulated Temperature For Cavity Handler ///////////
    /*##############################################################*/

    /**
     * Récupère la liste des données simulée pour la temperature
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la temperature à afficher pour le graphe
     */
    void getSimulatedTemperatureForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get simulated temperature for cavity request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetSimulatedTemperatureForCavityRequest(response, json, database);
        }
    }

    private void analyzeGetSimulatedTemperatureForCavityRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            answerToRequest(response, 400, "No data");
            return;
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        LocalDate from = simulatedDataList.get(0).getDateInsertion();
        LocalDate to = simulatedDataList.get(simulatedDataList.size() - 1).getDateInsertion();
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        // Devrait fonctionner
        List<JsonObject> jsonObjectList = simulatedDataList
                .stream()
                .map(simulatedData -> {
                    double temperature = simulatedData.getTemperatureWelHead();
                    JsonObject jsonObject = new JsonObject();
                    LocalDate localDate = simulatedData.getDateInsertion();
                    if (temperature < 0) { // Check si manque date
                        jsonObject.put("x", localDate.getYear() + "," + localDate.getMonthValue() + "," + localDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                    } else {
                        jsonObject.put("x", localDate.getYear() + "," + localDate.getMonthValue() + "," + localDate.getDayOfMonth());
                        jsonObject.put("y", temperature);
                    }
                    return jsonObject;
                })
                .collect(Collectors.toList());

        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    ////// Get Simulated Temperature For Cavity Date Handler /////////
    /*##############################################################*/

    /**
     * Récupère la liste des données simulée pour la temperature entre des dates donné
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base de données
     * - Pas de données insérée dans la base de données
     * <p>
     * Renvoie la liste de la temperature à afficher pour le graphe
     */
    void getSimulatedTemperatureForCavityDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get simulated temperature for cavity date request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetSimulatedTemperatureForCavityDateRequest(response, json, database);
        }
    }

    private void analyzeGetSimulatedTemperatureForCavityDateRequest(HttpServerResponse response, JsonObject json, Database database) {
        String cavityName = json.getString("CavityName");
        String dateFrom = json.getString("DateFrom");
        String dateTo = json.getString("DateTo");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, dateFrom, dateTo, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cavity not in database");
            return;
        }
        Cavity cavity = optCavity.get();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate from = LocalDate.from(dtf.parse(dateFrom));
        LocalDate to = LocalDate.from(dtf.parse(dateTo));

        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();

        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                // Simule pression et temperature
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            answerToRequest(response, 400, "No data for given period");
            return;
        }

        simulatedDataList.sort(tools.simulatedDataComparator);
        // Ajouter toutes les données simulées , puis ne garder que celles dans la période donnée.
        List<SimulatedData> filteredSimulatedData = simulatedDataList
                .stream()
                .filter(simulatedData -> {
                    LocalDate date = simulatedData.getDateInsertion();
                    return tools.dateBetweenOthersInclusive(date, from, to);
                }).collect(Collectors.toList());

        if (filteredSimulatedData.isEmpty()) {
            answerToRequest(response, 400, "No data for given period");
            return;
        }

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!checkIfDateIsInListOfSimulatedData(filteredSimulatedData, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                filteredSimulatedData.add(fakeSimulatedData);
            }
        });
        filteredSimulatedData.sort(tools.simulatedDataComparator);


        List<JsonObject> jsonObjectList = filteredSimulatedData
                .stream()
                .map(simulatedData -> {
                    double temperature = simulatedData.getTemperatureWelHead();
                    JsonObject jsonObject = new JsonObject();
                    LocalDate localDate = simulatedData.getDateInsertion();
                    if (temperature < 0) { // Check si manque date
                        jsonObject.put("x", localDate.getYear() + "," + localDate.getMonthValue() + "," + localDate.getDayOfMonth());
                        jsonObject.put("y", "null");
                    } else {
                        jsonObject.put("x", localDate.getYear() + "," + localDate.getMonthValue() + "," + localDate.getDayOfMonth());
                        jsonObject.put("y", temperature);
                    }
                    return jsonObject;
                })
                .collect(Collectors.toList());

        answerToRequest(response, 200, jsonObjectList);
    }

    /**
     * True si "localdDate" se trouve dans la liste "simulatedDataList"
     */
    private boolean checkIfDateIsInListOfSimulatedData(List<SimulatedData> simulatedDataList, LocalDate localDate) {
        return simulatedDataList
                .stream()
                .anyMatch(simulatedData -> simulatedData.getDateInsertion().isEqual(localDate));
    }
}
