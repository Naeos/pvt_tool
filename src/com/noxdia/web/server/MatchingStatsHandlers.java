package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.cavite.data.SimulatedData;
import com.noxdia.tools.Tools;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.answerToRequest;

class MatchingStatsHandlers {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private final DecimalFormat df = new DecimalFormat("0.##");
    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    MatchingStatsHandlers() {
    }

    /*##############################################################*/
    ////////////// Get Matching Stat Pression For Cavity /////////////
    /*##############################################################*/

    /**
     * Récupère les stats de matching pour la pression sur toute la période
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base donnée
     * - Pas assez de données pour simuler et donc récupérer les statistiques de matchings
     */
    void getMatchingStatPressionForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Matching Stat pression request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetMatchingStatPressionForCavityRequest(json, response, database);
        }
    }

    private void analyzeGetMatchingStatPressionForCavityRequest(JsonObject json, HttpServerResponse response, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cannot find cavity \'" + cavityName + "\' in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<Calage> calageList = database.getListCalageForCavity(cavity)
                .stream()
                .filter(Calage::isLocked)
                .collect(Collectors.toList());

        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final boolean[] noError = {true};
        calageList.forEach(calage -> {
            noError[0] = calage.simulateTemperature(); // Permet de simuler pression et temperature et remplie la liste des simulated data lié au calage
            simulatedDataList.addAll(calage.getSimulatedDataList());
        });

        if (!noError[0]) {
            answerToRequest(response, 400, "Not enough data to do matching stats");
            return;
        }
        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        List<Double> deviationPression = new ArrayList<>();

        realDataList.forEach(realData -> {
            LocalDate date = realData.getDateInsertion();
            Optional<Double> optPressionSimul = getPressionInSimulatedDataForDate(simulatedDataList, date);
            if (optPressionSimul.isPresent()) {
                double pressionSimul = optPressionSimul.get();
                double pressionReel = realData.getPressionWelHead();
                deviationPression.add(pressionSimul - pressionReel);
            }
        });

        if (deviationPression.isEmpty()) {
            answerToRequest(response, 400, "No deviation");
            return;
        }

        double ecartTypePression = tools.calculEcartType(deviationPression);
        double deviationMinPression = tools.calculMin(deviationPression);
        double deviationMaxPression = tools.calculMax(deviationPression);
        double sommeEcartsQuadratiquePression = tools.calculSommeEcartsQuadratique(deviationPression);
        double deviationMoyennePression = tools.calculDeviationMoyenne(deviationPression);

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("standardDeviationPression", df.format(ecartTypePression));
        jsonObject.put("minDeviationPression", df.format(deviationMinPression));
        jsonObject.put("maxDeviationPression", df.format(deviationMaxPression));
        jsonObject.put("sommeEcartsQuadratiquesPression", df.format(sommeEcartsQuadratiquePression));
        jsonObject.put("moyDeviationPression", df.format(deviationMoyennePression));

        answerToRequest(response, 200, jsonObject);
    }

    /*##############################################################*/
    /////////// Get Matching Stat Pression For Cavity Date ///////////
    /*##############################################################*/

    /**
     * Récupère les stats de matching pour la pression sur une période donnée
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base donnée
     * - Pas assez de données pour simuler et donc récupérer les statistiques de matchings
     */
    void getMatchingStatPressionForCavityDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Matching Stat pression for date request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetMatchingStatPressionForCavityDateRequest(json, response, database);
        }
    }

    private void analyzeGetMatchingStatPressionForCavityDateRequest(JsonObject json, HttpServerResponse response, Database database) {
        String cavityName = json.getString("CavityName");
        String dateFromString = json.getString("DateFrom");
        String dateToString = json.getString("DateTo");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, dateFromString, dateToString, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cannot find cavity \'" + cavityName + "\' in database");
            return;
        }

        Cavity cavity = optCavity.get();

        List<Calage> calageList = database.getListCalageForCavity(cavity)
                .stream()
                .filter(Calage::isLocked)
                .collect(Collectors.toList());

        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final boolean[] noError = {true};
        calageList.forEach(calage -> {
            noError[0] = calage.simulateTemperature();
            simulatedDataList.addAll(calage.getSimulatedDataList());
        });

        LocalDate from = LocalDate.from(dtf.parse(dateFromString));
        LocalDate to = LocalDate.from(dtf.parse(dateToString));

        List<SimulatedData> filteredSimulatedData = simulatedDataList.stream()
                .filter(simulatedData -> tools.dateBetweenOthersInclusive(simulatedData.getDateInsertion(), from, to))
                .collect(Collectors.toList());

        if (!noError[0]) {
            answerToRequest(response, 400, "Not enough data to do matching stats");
            return;
        }
        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        List<Double> deviationPression = new ArrayList<>();

        realDataList.forEach(realData -> {
            LocalDate date = realData.getDateInsertion();
            Optional<Double> optPressionSimul = getPressionInSimulatedDataForDate(filteredSimulatedData, date);
            if (optPressionSimul.isPresent()) {
                double pressionSimul = optPressionSimul.get();
                double pressionReel = realData.getPressionWelHead();
                deviationPression.add(pressionSimul - pressionReel);
            }
        });

        if (deviationPression.isEmpty()) {
            answerToRequest(response, 400, "No deviation");
            return;
        }
        double ecartTypePression = tools.calculEcartType(deviationPression);
        double deviationMinPression = tools.calculMin(deviationPression);
        double deviationMaxPression = tools.calculMax(deviationPression);
        double sommeEcartsQuadratiquePression = tools.calculSommeEcartsQuadratique(deviationPression);
        double deviationMoyennePression = tools.calculDeviationMoyenne(deviationPression);

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("standardDeviationPression", df.format(ecartTypePression));
        jsonObject.put("minDeviationPression", df.format(deviationMinPression));
        jsonObject.put("maxDeviationPression", df.format(deviationMaxPression));
        jsonObject.put("sommeEcartsQuadratiquesPression", df.format(sommeEcartsQuadratiquePression));
        jsonObject.put("moyDeviationPression", df.format(deviationMoyennePression));

        answerToRequest(response, 200, jsonObject);
    }

    /*##############################################################*/
    /////////// Get Matching Stat Temperature For Cavity /////////////
    /*##############################################################*/

    /**
     * Récupère les stats de matching pour la temperature sur toute la période
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base donnée
     * - Pas assez de données pour simuler et donc récupérer les statistiques de matchings
     */
    void getMatchingStatTemperatureForCavity(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Matching Stat temperature request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetMatchingStatTemperatureForCavityRequest(json, response, database);
        }
    }

    private void analyzeGetMatchingStatTemperatureForCavityRequest(JsonObject json, HttpServerResponse response, Database database) {
        String cavityName = json.getString("CavityName");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cannot find cavity \'" + cavityName + "\' in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<Calage> calageList = database.getListCalageForCavity(cavity)
                .stream()
                .filter(Calage::isLocked)
                .collect(Collectors.toList());

        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final boolean[] noError = {true};
        calageList.forEach(calage -> {
            noError[0] = calage.simulateTemperature();
            simulatedDataList.addAll(calage.getSimulatedDataList());
        });
        if (!noError[0]) {
            answerToRequest(response, 400, "Not enough data to do matching stats");
            return;
        }
        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        List<Double> deviationTemperature = new ArrayList<>();

        realDataList.forEach(realData -> {
            LocalDate date = realData.getDateInsertion();
            Optional<Double> optTempSimul = getTemperatureInSimulatedDataForDate(simulatedDataList, date);
            if (optTempSimul.isPresent()) {
                double tempSimul = optTempSimul.get();
                double tempReel = realData.getTemperatureWelHead();
                deviationTemperature.add(tempSimul - tempReel);
            }
        });

        if (deviationTemperature.isEmpty()) {
            answerToRequest(response, 400, "No deviation");
            return;
        }

        double ecartTypeTemperature = tools.calculEcartType(deviationTemperature);
        double deviationMinTemperature = tools.calculMin(deviationTemperature);
        double deviationMaxTemperature = tools.calculMax(deviationTemperature);
        double sommeEcartsQuadratiqueTemperature = tools.calculSommeEcartsQuadratique(deviationTemperature);
        double deviationMoyenneTemperature = tools.calculDeviationMoyenne(deviationTemperature);

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("standardDeviationTemp", df.format(ecartTypeTemperature));
        jsonObject.put("minDeviationTemp", df.format(deviationMinTemperature));
        jsonObject.put("maxDeviationTemp", df.format(deviationMaxTemperature));
        jsonObject.put("sommeEcartsQuadratiquesTemp", df.format(sommeEcartsQuadratiqueTemperature));
        jsonObject.put("moyDeviationTemp", df.format(deviationMoyenneTemperature));

        answerToRequest(response, 200, jsonObject);
    }

    /*##############################################################*/
    ///////// Get Matching Stat Temperature For Cavity Date //////////
    /*##############################################################*/

    /**
     * Récupère les stats de matching pour la temperature sur une période donnée
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Cavité non trouvé dans la base donnée
     * - Pas assez de données pour simuler et donc récupérer les statistiques de matchings
     */
    void getMatchingStatTemperatureForCavityDate(RoutingContext routingContext, Database database) {
        saspLogger.log(Level.INFO, "In get Matching Stat temperature for date request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong json input");
        } else {
            analyzeGetMatchingStatTemperatureForCavityDateRequest(json, response, database);
        }
    }

    private void analyzeGetMatchingStatTemperatureForCavityDateRequest(JsonObject json, HttpServerResponse response, Database database) {
        String cavityName = json.getString("CavityName");
        String dateFromString = json.getString("DateFrom");
        String dateToString = json.getString("DateTo");
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(cavityName, dateFromString, dateToString, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Site> optSite = database.getSiteByName(siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Cannot find site \'" + siteName + "\' in database");
            return;
        }
        Site site = optSite.get();

        Optional<Cavity> optCavity = database.getCavityByNameAndSite(cavityName, site);
        if (!optCavity.isPresent()) {
            answerToRequest(response, 400, "Cannot find cavity \'" + cavityName + "\' in database");
            return;
        }
        Cavity cavity = optCavity.get();

        List<Calage> calageList = database.getListCalageForCavity(cavity)
                .stream()
                .filter(Calage::isLocked)
                .collect(Collectors.toList());

        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final boolean[] noError = {true};
        calageList.forEach(calage -> {
            noError[0] = calage.simulateTemperature();
            simulatedDataList.addAll(calage.getSimulatedDataList());
        });

        LocalDate from = LocalDate.from(dtf.parse(dateFromString));
        LocalDate to = LocalDate.from(dtf.parse(dateToString));

        List<SimulatedData> filteredSimulatedData = simulatedDataList.stream()
                .filter(simulatedData -> tools.dateBetweenOthersInclusive(simulatedData.getDateInsertion(), from, to))
                .collect(Collectors.toList());

        if (!noError[0]) {
            answerToRequest(response, 400, "Not enough data to do matching stats");
            return;
        }
        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        List<Double> deviationTemperature = new ArrayList<>();

        realDataList.forEach(realData -> {
            LocalDate date = realData.getDateInsertion();
            Optional<Double> optTempSimul = getTemperatureInSimulatedDataForDate(filteredSimulatedData, date);
            if (optTempSimul.isPresent()) {
                double tempSimul = optTempSimul.get();
                double tempReel = realData.getTemperatureWelHead();
                deviationTemperature.add(tempSimul - tempReel);
            }
        });

        if (deviationTemperature.isEmpty()) {
            answerToRequest(response, 400, "No deviation");
            return;
        }

        double ecartTypeTemperature = tools.calculEcartType(deviationTemperature);
        double deviationMinTemperature = tools.calculMin(deviationTemperature);
        double deviationMaxTemperature = tools.calculMax(deviationTemperature);
        double sommeEcartsQuadratiqueTemperature = tools.calculSommeEcartsQuadratique(deviationTemperature);
        double deviationMoyenneTemperature = tools.calculDeviationMoyenne(deviationTemperature);

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("standardDeviationTemp", df.format(ecartTypeTemperature));
        jsonObject.put("minDeviationTemp", df.format(deviationMinTemperature));
        jsonObject.put("maxDeviationTemp", df.format(deviationMaxTemperature));
        jsonObject.put("sommeEcartsQuadratiquesTemp", df.format(sommeEcartsQuadratiqueTemperature));
        jsonObject.put("moyDeviationTemp", df.format(deviationMoyenneTemperature));

        answerToRequest(response, 200, jsonObject);
    }

    /*##############################################################*/
    //////////////////////// Usefull stuff ///////////////////////////
    /*##############################################################*/

    /**
     * Récupère la pression à la date "date" dans la liste de données simulées
     */
    private Optional<Double> getPressionInSimulatedDataForDate(List<SimulatedData> simulatedDataList, LocalDate date) {
        return simulatedDataList.stream()
                .filter(simulatedData -> simulatedData.getDateInsertion().isEqual(date))
                .map(SimulatedData::getPressionWelHead)
                .findFirst();
    }

    /**
     * Récupère la temperature à la date "date" dans la liste de données simulées
     */
    private Optional<Double> getTemperatureInSimulatedDataForDate(List<SimulatedData> simulatedDataList, LocalDate date) {
        return simulatedDataList.stream()
                .filter(simulatedData -> simulatedData.getDateInsertion().isEqual(date))
                .map(SimulatedData::getTemperatureWelHead)
                .findFirst();

    }


}
