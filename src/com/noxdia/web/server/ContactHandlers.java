package com.noxdia.web.server;

import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;
import com.noxdia.exploitant.contact.Contact;
import com.noxdia.exploitant.contact.ContactFactory;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.tools.Tools;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.noxdia.web.server.HandlersUtils.*;

class ContactHandlers {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();

    ContactHandlers() {
    }

    /*##############################################################*/
    //////////////// get list contact for Exp Handler ////////////////
    /*##############################################################*/

    /**
     * Récupère la liste des contact pour l'exploitant
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé dans liste
     * - Pas de contact pour l'exploitant
     * <p>
     * Renvoie la liste des contacts si pas d'erreurs, s'il n'y en a pas, la liste sera vide
     */
    void getListContactForExp(RoutingContext routingContext, Database database, List<Exploitant> listExploitant) {
        saspLogger.log(Level.INFO, "In get List contact for Exp request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeGetListContactForExpRequest(response, json, database, listExploitant);
        }
    }

    private void analyzeGetListContactForExpRequest(HttpServerResponse response, JsonObject json, Database database, List<Exploitant> listExploitant) {
        String exploitantName = json.getString("ExploitantName");

        if (tools.verifyEmptyOrNull(exploitantName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, exploitantName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Can't find exploitant in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Contact> contactList = database.getlistContactForExp(exp);

        if (contactList.isEmpty()) {
            answerToRequest(response, 400, "No contact for exploitant");
            return;
        }

        List<JsonObject> jsonObjectList = contactList.stream()
                .map(contact -> {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("Id", contact.getId());
                    jsonObject.put("LastName", contact.getLastName());
                    jsonObject.put("Name", contact.getName());
                    jsonObject.put("Function", contact.getFonction());
                    jsonObject.put("Service", contact.getService());
                    jsonObject.put("Phone", contact.getTelephone());
                    jsonObject.put("Email", contact.getMail());
                    jsonObject.put("IsAffected", contact.isAffected());
                    if (contact.isAffected()) {
                        jsonObject.put("SiteName", contact.getSite().getName());
                        jsonObject.put("SiteTown", contact.getSite().getTown());
                    }
                    jsonObject.put("ExploitantName", contact.getExploitant().getName());
                    jsonObject.put("Image", contact.getImageLocationClient());
                    return jsonObject;
                }).collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    /////////////// get list contact for Site Handler ////////////////
    /*##############################################################*/

    /**
     * Récupère la liste des contact pour un site donné
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant non trouvé dans la liste
     * - Site non trouvé dans la liste des sites de l'exploitant
     * <p>
     * Renvoie la liste des contacts associé à un site, s'il n'y en a pas, la liste sera vide
     */
    void getListContactForSite(RoutingContext routingContext, Database database, List<Exploitant> listExploitant) {
        saspLogger.log(Level.INFO, "In get List contact for site request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeGetListContactForSiteRequest(response, json, database, listExploitant);
        }
    }

    private void analyzeGetListContactForSiteRequest(HttpServerResponse response, JsonObject json, Database database, List<Exploitant> listExploitant) {
        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "Exp not found for site");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not found in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = exp.getListSite();

        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \'" + siteName + "\' not found in list");
            return;
        }
        Site site = optSite.get();

        List<Contact> contactList = exp.getListContactForSite(site);

        List<JsonObject> jsonObjectList = contactList.stream()
                .filter(Contact::isAffected)
                .map(contact -> {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("Id", contact.getId());
                    jsonObject.put("LastName", contact.getLastName());
                    jsonObject.put("Name", contact.getName());
                    jsonObject.put("Function", contact.getFonction());
                    jsonObject.put("Service", contact.getService());
                    jsonObject.put("Phone", contact.getTelephone());
                    jsonObject.put("Email", contact.getMail());
                    jsonObject.put("IsAffected", contact.isAffected());
                    if (contact.isAffected()) {
                        jsonObject.put("SiteName", contact.getSite().getName());
                        jsonObject.put("SiteTown", contact.getSite().getTown());
                    }
                    jsonObject.put("ExploitantName", contact.getExploitant().getName());
                    jsonObject.put("Image", contact.getImageLocationClient());
                    return jsonObject;
                }).collect(Collectors.toList());
        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    ///////// get list contact for exp without site Handler //////////
    /*##############################################################*/

    /**
     * Récupère la liste des contact non associé à un site
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant non trouvé dans la liste
     * - Site non trouvé dans la liste des sites de l'exploitant
     * <p>
     * Renvoie la liste des contacts non associé à un site, s'il n'y en a pas, la liste sera vide
     */
    void getListContactForExpWithoutSite(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In get List contact for exp without site request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeGetListContactForExpWithoutSiteRequest(response, json, listExploitant, database);
        }
    }

    private void analyzeGetListContactForExpWithoutSiteRequest(HttpServerResponse response, JsonObject json, List<Exploitant> listExploitant, Database database) {

        String siteName = json.getString("SiteName");
        if (tools.verifyEmptyOrNull(siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "Exp not found for site");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not found in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Contact> contactList = exp.getListContact();
        List<JsonObject> jsonObjectList = contactList.stream()
                .filter(contact -> !contact.isAffected())
                .map(contact -> {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("Id", contact.getId());
                    jsonObject.put("LastName", contact.getLastName());
                    jsonObject.put("Name", contact.getName());
                    jsonObject.put("Function", contact.getFonction());
                    jsonObject.put("Service", contact.getService());
                    jsonObject.put("Phone", contact.getTelephone());
                    jsonObject.put("Email", contact.getMail());
                    jsonObject.put("IsAffected", contact.isAffected());
                    if (contact.isAffected()) {
                        jsonObject.put("SiteName", contact.getSite().getName());
                        jsonObject.put("SiteTown", contact.getSite().getTown());
                    }
                    jsonObject.put("ExploitantName", contact.getExploitant().getName());
                    jsonObject.put("Image", contact.getImageLocationClient());
                    return jsonObject;
                }).collect(Collectors.toList());

        answerToRequest(response, 200, jsonObjectList);
    }

    /*##############################################################*/
    ///////////////////// Add contact Handler ////////////////////////
    /*##############################################################*/

    /**
     * Ajoute un contact à un exploitant
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le contact
     */
    void addContact(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In add contact request");
        analyzeAddContactRequest(routingContext, database, listExploitant);
    }

    private void analyzeAddContactRequest(RoutingContext routingContext, Database database, List<Exploitant> listExploitant) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();

        String lastName = entries.get("nC1");
        String name = entries.get("nC2");
        String exploitantName = entries.get("nC3");
        String fonction = entries.get("nC4");
        String service = entries.get("nC5");
        String phone = entries.get("nC6");
        String email = entries.get("nC7");
        String pageAvantCommit = entries.get("nC8");
        String isThereFile = entries.get("nC9");

        if (tools.verifyEmptyOrNull(exploitantName, lastName, name, fonction, service, email, phone, pageAvantCommit, isThereFile)) {
            redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Missing input data");
            return;
        }

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, exploitantName);
        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Exploitant not found for contact");
            return;
        }
        Exploitant exp = optExp.get();

        String imageLocationClient = "/asset/uploads/imageDefaultContact.jpg";
        String imageLocationServer = "./webroot" + imageLocationClient;
        boolean isImageDefault = true;
        if (isThereFile.equals("True")) {
            byte[] data = new byte[0];
            String extension = ".jpg";
            for (FileUpload f : routingContext.fileUploads()) {
                Path path = Paths.get(f.uploadedFileName());
                try {
                    extension = f.fileName().substring(f.fileName().lastIndexOf("."));
                    data = Files.readAllBytes(path);
                    Files.delete(path);
                } catch (IOException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
            }

            Optional<Contact> contactOptional = findContactByNameAndLastName(exp.getListContact(), name, lastName);
            if (contactOptional.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Contact name :\'" + name + "\' \'" + lastName + "\' already exists");
                return;
            }

            // Create the exploitant and save the file in the correct folder
            imageLocationClient = "/asset/uploads/contact_" + name + "_icone" + extension;
            imageLocationServer = "./webroot" + imageLocationClient;
            isImageDefault = false;
            tools.writeFile(data, imageLocationServer);
        } else {
            for (FileUpload f : routingContext.fileUploads()) {
                Path path = Paths.get(f.uploadedFileName());
                try {
                    Files.delete(path);
                } catch (IOException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
            }
            Optional<Contact> contactOptional = findContactByNameAndLastName(exp.getListContact(), name, lastName);
            if (contactOptional.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Contact name :\'" + name + "\' \'" + lastName + "\' already exists");
                return;
            }
        }

        Contact contact = ContactFactory.createContact(lastName, name, exp, fonction, service, email, phone, imageLocationClient, imageLocationServer, isImageDefault, database);
        exp.addContact(contact);
        redirectionToOldPage(response, pageAvantCommit, Level.INFO, " Contact \'" + name + " " + lastName + "\' added to exploitant \'" + exploitantName + "\'");
    }

    /*##############################################################*/
    ///////////////////// Remove contact Handler /////////////////////
    /*##############################################################*/

    /**
     * Supprime le contact d'un exploitant, et supprime le lien avec un site, s'il existe
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Contact non trouvé dans la base de données
     * - Exploitant non trouvé dans la liste des exploitants
     * - Site non trouvé dans la liste des sites de l'exploitant
     */
    void removeContact(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In remove contact request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeRemoveContactRequest(response, json, database, listExploitant);
        }
    }

    private void analyzeRemoveContactRequest(HttpServerResponse response, JsonObject json, Database database, List<Exploitant> listExploitant) {
        String idString = json.getString("IdContact");
        String exploitantName = json.getString("ExploitantName");
        String siteName = json.getString("SiteName");

        if (tools.verifyEmptyOrNull(idString, exploitantName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        int id = Integer.parseInt(idString);
        Optional<Contact> optContact = database.getContactById(id);
        if (!optContact.isPresent()) {
            answerToRequest(response, 400, "Contact not found");
            return;
        }
        Contact contact = optContact.get();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, exploitantName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant not found in list");
            return;
        }
        Exploitant exp = optExp.get();
        // Si contact associé à un site -> supprimer le lien avec le site puis supprimer le contact
        if (!siteName.isEmpty()) {
            List<Site> siteList = exp.getListSite();
            Optional<Site> optSite = findSiteByName(siteList, siteName);
            if (!optSite.isPresent()) {
                answerToRequest(response, 400, "Site not found in list");
                return;
            }
            Site site = optSite.get();
            exp.unAffectContactForSite(contact, site);
        }

        contact.deleteImage();
        exp.removeContact(contact);
        answerToRequest(response, 200, "Contact \'" + contact.getName() + " " + contact.getLastName() + "\' removed from exploitant \'" + exploitantName + "\'");
    }

    /*##############################################################*/
    //////////////////// update contact Handler //////////////////////
    /*##############################################################*/

    /**
     * Met à jour le contact sans modifier son image
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le contact
     * - Combinaison de nouveau nom / prénom déjà existant pour un contact de l'exploitant
     * - Contact à mettre à jour non trouvé dans la base de données
     */
    void updateContact(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In update contact request");
        analyzeUpdateContactRequest(routingContext, listExploitant, database);
    }

    private void analyzeUpdateContactRequest(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();

        String lastName = entries.get("prenomContact");
        String name = entries.get("nomContact");
        String exploitantName = entries.get("entrepriseContact");
        String fonction = entries.get("fonctionContact");
        String service = entries.get("serviceContact");
        String phone = entries.get("telContact");
        String email = entries.get("mailContact");
        String pageAvantCommit = entries.get("mC8");

        String oldName = entries.get("mC11");
        String oldLastName = entries.get("mC12");

        if (tools.verifyEmptyOrNull(exploitantName, lastName, name, fonction, service, email, phone, pageAvantCommit)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Missing input data");
            return;
        }

        for (FileUpload f : routingContext.fileUploads()) {
            Path path = Paths.get(f.uploadedFileName());
            try {
                Files.delete(path);
            } catch (IOException e) {
                saspLogger.log(Level.WARNING, e.getMessage());
            }
        }

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, exploitantName);
        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant not found for contact");
            return;
        }
        Exploitant exp = optExp.get();

        List<Contact> listContact = exp.getListContact();
        // Si nouveau nom / prenom en conflit avec déjà existant pour exploitant -> erreur
        if (!name.equals(oldName) && !lastName.equals(oldLastName)) {
            Optional<Contact> optContactNew = findContactByNameAndLastName(listContact, name, lastName);
            if (optContactNew.isPresent()) {
                redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Contact with \'" + name + " " + lastName + "\' already exist for exploitant");
                return;
            }
        }

        Optional<Contact> optContact = findContactByNameAndLastName(listContact, oldName, oldLastName);
        if (!optContact.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Contact \'" + oldName + " " + oldLastName + "\' not found ");
            return;
        }
        Contact contact = optContact.get();

        String imageLocationClient = contact.getImageLocationClient();
        String imageLocationServer = contact.getImageLocationServer();

        boolean isImageDefault = contact.isImageDefault();

        // Changer le nom du fichier si on change le nom du contact et si son image n'est pas celle par defaut
        if (!isImageDefault) {
            if (!contact.getLastName().equals(lastName) && !contact.getName().equals(name)) {
                String extension = imageLocationServer.substring(imageLocationServer.lastIndexOf(".") + 1);
                String fileName = "contact_" + name + "_icone." + extension;
                String imageLocationClientUpd = "/asset/uploads/" + fileName;
                String imageLocationServerUpd = "./webroot" + imageLocationClientUpd;

                Path path = Paths.get(imageLocationServer);
                try {
                    Files.move(path, path.resolveSibling(fileName));
                } catch (IOException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
                imageLocationClient = imageLocationClientUpd;
                imageLocationServer = imageLocationServerUpd;
            }
        }

        Contact contactUpdated = ContactFactory.createContact(lastName, name, exp, fonction, service, email, phone, imageLocationClient, imageLocationServer, isImageDefault, database);
        listContact.remove(contact);
        contact.update(contactUpdated);
        listContact.add(contact);
        redirectionToOldPage(response, pageAvantCommit, Level.INFO, " Contact \'" + name + " " + lastName + "\' updated");
    }

    /*##############################################################*/
    /////////////// update contact with image Handler ////////////////
    /*##############################################################*/

    /**
     * Met à jour le contact en modifiant son image
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le contact
     * - Combinaison de nouveau nom / prénom déjà existant pour un contact de l'exploitant
     * - Contact à mettre à jour non trouvé dans la base de données
     */
    void updateContactWithImage(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        saspLogger.log(Level.INFO, "In update contact with image request");
        analyzeUpdateContactWithImageRequest(routingContext, listExploitant, database);
    }

    private void analyzeUpdateContactWithImageRequest(RoutingContext routingContext, List<Exploitant> listExploitant, Database database) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();

        String lastName = entries.get("nomContact");
        String name = entries.get("prenomContact");
        String exploitantName = entries.get("entrepriseContact");
        String fonction = entries.get("fonctionContact");
        String service = entries.get("serviceContact");
        String phone = entries.get("telContact");
        String email = entries.get("mailContact");
        String pageAvantCommit = entries.get("mC8");

        String oldName = entries.get("mC11");
        String oldLastName = entries.get("mC12");

        if (tools.verifyEmptyOrNull(exploitantName, lastName, name, fonction, service, email, phone, pageAvantCommit, oldName, oldLastName)) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Missing input data");
            return;
        }

        byte[] data = new byte[0];
        String extension = ".jpg";
        for (FileUpload f : routingContext.fileUploads()) {
            Path path = Paths.get(f.uploadedFileName());
            try {
                extension = f.fileName().substring(f.fileName().lastIndexOf("."));
                data = Files.readAllBytes(path);
                Files.delete(path);
            } catch (IOException e) {
                saspLogger.log(Level.WARNING, e.getMessage());
            }
        }
        String imageLocationClient = "/asset/uploads/contact_" + name + "_" + lastName + "_icone" + extension;
        String imageLocationServer = "./webroot" + imageLocationClient;

        tools.writeFile(data, imageLocationServer);

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, exploitantName);
        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Exploitant not found for contact");
            return;
        }
        Exploitant exp = optExp.get();
        List<Contact> listContact = exp.getListContact();

        Optional<Contact> optContact = findContactByNameAndLastName(listContact, oldName, oldLastName);
        if (!optContact.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit + "?error", Level.WARNING, "Contact to update not found");
            return;
        }
        Contact contact = optContact.get();

        Contact contactUpdated = ContactFactory.createContact(lastName, name, exp, fonction, service, email, phone, imageLocationClient, imageLocationServer, false, database);
        listContact.remove(contact);
        contact.update(contactUpdated);
        listContact.add(contact);
        redirectionToOldPage(response, pageAvantCommit, Level.INFO, " Contact \'" + name + " " + lastName + "\' updated ");
    }

    /*##############################################################*/
    ///////////////// affect contact to site Handler /////////////////
    /*##############################################################*/

    /**
     * Affecte le contact a un site
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     * - Exploitant non trouvé pour le site
     * - Exploitant non trouvé dans la liste des exploitants
     */
    void affectContactToSite(RoutingContext routingContext, Database database, List<Exploitant> listExploitant) {
        saspLogger.log(Level.INFO, "In affect contact to site request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeAffectContactToSiteRequest(response, json, database, listExploitant);
        }
    }

    private void analyzeAffectContactToSiteRequest(HttpServerResponse response, JsonObject json, Database database, List<Exploitant> listExploitant) {
        JsonArray jsonArray = json.getJsonArray("TabContact");
        String siteName = json.getString("SiteName");

        if (tools.verifyEmptyOrNull(siteName) || jsonArray == null || jsonArray.isEmpty()) {
            answerToRequest(response, 400, "Missing Input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "Exp not found for site");
            return;
        }
        String expName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not found in list");
            return;
        }
        Exploitant exp = optExp.get();

        List<Site> siteList = exp.getListSite();
        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \'" + siteName + "\' not found in list");
            return;
        }
        Site site = optSite.get();

        // Permet d'ajouter liste de contact a un site donné
        for (int i = 0; i < jsonArray.size(); i++) {
            String idString = jsonArray.getString(i);
            int idContact = Integer.parseInt(idString);
            Optional<Contact> optContact = database.getContactById(idContact);
            if (!optContact.isPresent()) {
                answerToRequest(response, 400, "Contact not found in db");
                return;
            }
            Contact contact = optContact.get();
            // Ne devrait jamais arriver -> Securite au cas ou
            if (contact.isAffected()) {
                answerToRequest(response, 400, "Contact already affected to a site");
                return;
            }
            exp.affectContactToSite(contact, site);
        }
        answerToRequest(response, 200, "Contact affected to site");
    }

    /*##############################################################*/
    ///////////////// add Contact With Site Handler //////////////////
    /*##############################################################*/

    /**
     * Ajoute un contact en l'associant à site directement
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     */
    void addContactWithSite(RoutingContext routingContext, Database database, List<Exploitant> listExploitant) {
        saspLogger.log(Level.INFO, "In add contact with site request");
        analyzeAddContactWithSiteRequest(routingContext, database, listExploitant);
    }

    private void analyzeAddContactWithSiteRequest(RoutingContext routingContext, Database database, List<Exploitant> listExploitant) {
        HttpServerResponse response = routingContext.response();
        response.setChunked(true);
        MultiMap entries = routingContext.request().formAttributes();

        String lastName = entries.get("nC1");
        String name = entries.get("nC2");
        String fonction = entries.get("nC4");
        String service = entries.get("nC5");
        String phone = entries.get("nC6");
        String email = entries.get("nC7");
        String pageAvantCommit = entries.get("nC8");
        String isThereFile = entries.get("nC9");
        String siteName = entries.get("nC11");

        if (tools.verifyEmptyOrNull(lastName, name, fonction, service, email, phone, pageAvantCommit, isThereFile, siteName)) {
            redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Missing input data");
            return;
        }

        String imageLocationClient = "/asset/uploads/imageDefaultContact.jpg";
        String imageLocationServer = "./webroot" + imageLocationClient;
        boolean isImageDefault = true;
        if (isThereFile.equals("True")) {
            byte[] data = new byte[0];
            String extension = ".jpg";
            for (FileUpload f : routingContext.fileUploads()) {
                Path path = Paths.get(f.uploadedFileName());
                try {
                    extension = f.fileName().substring(f.fileName().lastIndexOf("."));
                    data = Files.readAllBytes(path);
                    Files.delete(path);
                } catch (IOException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
            }
            // Create the exploitant and save the file in the correct folder
            imageLocationClient = "/asset/uploads/contact_" + name + "_icone" + extension;
            imageLocationServer = "./webroot" + imageLocationClient;
            isImageDefault = false;
            tools.writeFile(data, imageLocationServer);
        } else {
            for (FileUpload f : routingContext.fileUploads()) {
                Path path = Paths.get(f.uploadedFileName());
                try {
                    Files.delete(path);
                } catch (IOException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
            }
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Exploitant not found for contact");
            return;
        }
        String exploitantName = optExpName.get();

        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, exploitantName);
        if (!optExp.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Exploitant not found for contact");
            return;
        }
        Exploitant exp = optExp.get();

        Contact contact = ContactFactory.createContact(lastName, name, exp, fonction, service, email, phone, imageLocationClient, imageLocationServer, isImageDefault, database);

        List<Site> siteList = exp.getListSite();
        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            redirectionToOldPage(response, pageAvantCommit, Level.WARNING, "Site not found in list");
            return;
        }
        Site site = optSite.get();

        exp.addContact(contact);
        exp.affectContactToSite(contact, site);
        redirectionToOldPage(response, pageAvantCommit, Level.INFO, " Contact \'" + name + " " + lastName + "\' added to exploitant \'" + exploitantName + "\'");
    }

    /*##############################################################*/
    ////////////// unAffect Contact from Site Handler ////////////////
    /*##############################################################*/

    /**
     * Desaffecte le contact d'un site
     * Il peut y avoir des erreurs dans les cas suivant :
     * - Données manquante à l'envoie par le client
     */
    void unAffectContactFromSite(RoutingContext routingContext, Database database, List<Exploitant> listExploitant) {
        saspLogger.log(Level.INFO, "In unAffect Contact From Site request");
        HttpServerResponse response = routingContext.response();
        JsonObject json = routingContext.getBodyAsJson();
        if (null == json) {
            answerToRequest(response, 400, "Wrong JSON input");
        } else {
            analyzeUnAffectContactFromSiteRequest(response, json, database, listExploitant);
        }
    }

    private void analyzeUnAffectContactFromSiteRequest(HttpServerResponse response, JsonObject json, Database database, List<Exploitant> listExploitant) {
        String contacIdString = json.getString("IdContact");
        String siteName = json.getString("SiteName");

        if (tools.verifyEmptyOrNull(contacIdString, siteName)) {
            answerToRequest(response, 400, "Missing input data");
            return;
        }

        Optional<String> optExpName = database.getExploitantNameForSiteName(siteName);
        if (!optExpName.isPresent()) {
            answerToRequest(response, 400, "Exploitant not found for site \'" + siteName + "\'");
            return;
        }

        String expName = optExpName.get();
        Optional<Exploitant> optExp = findExploitantInListByName(listExploitant, expName);
        if (!optExp.isPresent()) {
            answerToRequest(response, 400, "Exploitant \'" + expName + "\' not found in list ");
            return;
        }
        Exploitant exp = optExp.get();

        int contactId = Integer.parseInt(contacIdString);
        Optional<Contact> optContact = database.getContactById(contactId);
        if (!optContact.isPresent()) {
            answerToRequest(response, 400, "Contact not in db");
            return;
        }
        Contact contact = optContact.get();

        List<Site> siteList = exp.getListSite();
        Optional<Site> optSite = findSiteByName(siteList, siteName);
        if (!optSite.isPresent()) {
            answerToRequest(response, 400, "Site \'" + siteName + "\' not found in list");
            return;
        }
        Site site = optSite.get();

        exp.unAffectContactForSite(contact, site);
        answerToRequest(response, 200, "Contact \'" + contact.getName() + " " + contact.getLastName() + "\' unAffected from site \'" + siteName + "\'");
    }

    /*##############################################################*/
    ///////////////////////// Usefull stuff //////////////////////////
    /*##############################################################*/

    /**
     * Trouve un contact avec nom et prenom
     */
    private Optional<Contact> findContactByNameAndLastName(List<Contact> contactList, String contactName, String contactLastName) {
        return contactList.stream()
                .filter(contact -> contact.compareNameAndLastName(contactName, contactLastName))
                .findFirst();
    }
}
