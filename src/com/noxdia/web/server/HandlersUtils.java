package com.noxdia.web.server;

import com.noxdia.exploitant.Exploitant;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

class HandlersUtils {

    private static final SaspLogger SASP_LOGGER = SaspLogger.getInstance();
    /*##############################################################*/
    ////////////// Usefull stuff for every handler ///////////////////
    /*##############################################################*/

    /**
     * Répond à la requête du serveur avec le code "code" et la réponse "answer"
     * Un code entre >= 200 et < 303 indique une reussite / redirection
     * Un code >= 303 indique une erreur
     * Generalement pour les erreurs on utilise le code "400"
     */
    static void answerToRequest(HttpServerResponse response, int code, Object answer) {
        String tmp = Json.encodePrettily(answer);
        if (code >= 200 && code < 303) {
            SASP_LOGGER.log(Level.INFO, "code: " + code + "\nanswer: " + tmp);
        } else {
            SASP_LOGGER.log(Level.WARNING, "code: " + code + "\nanswer: " + tmp);
        }
        response.setStatusCode(code)
                .putHeader("content-type", "application/json")
                .end(tmp);
    }

    /**
     * Redirige vers la page "pageAvantCommit" en réponse à la requête au serveur
     */
    static void redirectionToOldPage(HttpServerResponse response, String pageAvantCommit, Level level, String logMessage) {
        int code = 301;
        SASP_LOGGER.log(level, logMessage);
        SASP_LOGGER.log(Level.INFO, "code: " + code + "\nredirection ancienne page : " + pageAvantCommit);
        response.setStatusCode(code)
                .putHeader("Location", pageAvantCommit)
                .end();
    }

    /**
     * Trouve le site dans la liste par son nom
     */
    static Optional<Site> findSiteByName(List<Site> listSite, String siteName) {
        return listSite.stream()
                .filter(s -> s.getName().equals(siteName))
                .findFirst();
    }

    /**
     * Trouve une cavité par son nom
     */
    static Optional<Cavity> findCavityByName(List<Cavity> cavityList, String cavityName) {
        return cavityList.stream()
                .filter(cavity -> cavity.checkName(cavityName))
                .findFirst();
    }

    /**
     * Trouve un exploitant par son nom
     */
    static Optional<Exploitant> findExploitantInListByName(List<Exploitant> listExploitant, String name) {
        return listExploitant.stream()
                .filter(exploitant -> exploitant.getName().equals(name))
                .findFirst();
    }

}
