package com.noxdia.web.browser;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class Browser extends Region {

    private final WebView webView = new WebView();

    public Browser() {
        webView.setContextMenuEnabled(true);
        webView.setCache(false);
        WebEngine webEngine = webView.getEngine();
        webEngine.load("http://localhost:8080");
//        webEngine.load("https://danstonchat.com");
        getChildren().add(webView);
    }


    @Override
    protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(webView, 0, 0, w, h, 0, HPos.CENTER, VPos.CENTER);
    }

    @Override
    protected double computePrefWidth(double height) {
        return 750;
    }

    @Override
    protected double computePrefHeight(double width) {
        return 500;
    }
}
