package com.noxdia.tools;


import com.noxdia.logger.SaspLogger;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.cavite.data.SimulatedData;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class represent some of our tools
 * that will be used across our classes.
 */
public class Tools {

    private static final SaspLogger SASP_LOGGER = SaspLogger.getInstance();
    private static Tools self;

    static {
        try {
            self = new Tools();
        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * Comparateur pour trier les liste de real data
     */
    public Comparator<RealData> dataRealComparator = (o1, o2) -> {
        if (o1.getDateInsertion().isBefore(o2.getDateInsertion())) {
            return -1;
        }
        return 1;
    };
    /**
     * Comparateur pour trier les liste de simulated data
     */
    public Comparator<SimulatedData> simulatedDataComparator = (o1, o2) -> {
        if (o1.getDateInsertion().isBefore(o2.getDateInsertion())) {
            return -1;
        }
        return 1;
    };

    /**
     * Singleton de l'objet
     *
     * @return L'instance unique de Tools
     */
    public static Tools getInstance() {
        return self;
    }

    /**
     * This function take a String representation of a password and
     * encrypt it with the SHA256 algorithm.
     *
     * @param password the password to encrypt
     * @return the password encrypted with SHA256 algorithm
     */
    public String toSHA256(String password) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
        md.update(new byte[]{5, 3, 3, 'd'});
        byte[] data = md.digest(password.getBytes(StandardCharsets.UTF_8));
        return IntStream.range(0, data.length)
                .mapToObj(i -> String.format("%02x", data[i]))
                .collect(Collectors.joining());
    }

    /**
     * Teste si "value" est positif
     *
     * @param value Valeur à tester
     * @return La valeur testé si positif, throw IllegalArgumentException sinon
     */
    public double checkNotNegative(double value) {
        if (value < 0) {
            throw new IllegalArgumentException("Value must be positive !");
        }
        return value;
    }

    /**
     * Teste si une chaine de caractère respecte le formail "mail"
     *
     * @param email Adresse mail à tester
     * @return L'adresse mail si c'est bon, "Wrong" sinon
     */
    public String checkMailFormat(String email) {
        String pattern = "(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])";
        Pattern pat = Pattern.compile(pattern);
        Matcher matcher = pat.matcher(email);
        if (!matcher.matches()) {
            SASP_LOGGER.log(Level.WARNING, "Mail : \'" + email + "\' does not match pattern");
            return "Wrong";
        }
        return email;
    }

    /**
     * Affiche la liste des strings passer en arguments
     *
     * @param args Les strings que l'ont veut afficher
     */
    public void printVarargsStrings(String... args) {
        StringBuilder sb = new StringBuilder();
        for (String s : args) {
            sb.append(s).append(" ; ");
        }
        System.out.println(sb.toString());
    }

    /**
     * Supprime un dossier, ainsi que tout son contenu
     *
     * @param folderPath Dossier à supprimer
     */
    public void deleteDirectoryRecurs(Path folderPath) {
        SaspLogger saspLogger = SaspLogger.getInstance();
        saspLogger.log(Level.INFO, "Deleting recursivey : " + folderPath);
        //browsing the file directory and delete recursively using java nio
        try {
            Files.walkFileTree(folderPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    saspLogger.log(Level.INFO, "Deleting file : " + file);
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    saspLogger.log(Level.INFO, "Deleting directory : " + dir);
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            saspLogger.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Teste si la date "date" est entre la date "from" et la date "to"
     *
     * @param date Date à tester
     * @param from Borne inférieur de test
     * @param to   Borne supérieur de test
     * @return True si date est inclus entre from et to
     */
    public boolean dateBetweenOthersInclusive(LocalDate date, LocalDate from, LocalDate to) {
        return date.isAfter(from.minusDays(1)) && date.isBefore(to.plusDays(1));
    }

    /**
     * Génère une liste de date entre la date "from" et la date "to"
     *
     * @param from Date de debut
     * @param to   Date de fin
     * @return Liste de dates entre "from" et "to"
     */
    public List<LocalDate> generateListDateFromTo(LocalDate from, LocalDate to) {
        if (from.isAfter(to)) {
            return Collections.emptyList();
        }
        List<LocalDate> localDates = new ArrayList<>();
        for (LocalDate date = from; date.isBefore(to.plusDays(1)); date = date.plusDays(1)) {
            localDates.add(date);
        }
        return localDates;
    }

    /**
     * Crée le dossier "folder" s'il n'existe pas
     *
     * @param folder Dossier à créer
     */
    public void createFolder(Path folder) {
        if (Files.notExists(folder)) {
            try {
                Files.createDirectory(folder);
            } catch (IOException e) {
                SASP_LOGGER.log(Level.WARNING, e.getMessage());
            }
        }
    }

    /**
     * Ecrit les données "data" dans un le fichier "location"
     *
     * @param data     Données à écrire
     * @param location Localisation du fichier
     */
    public void writeFile(byte[] data, String location) {
        Path path = Paths.get(location);
        try {
            Files.write(path, data);
        } catch (IOException e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Supprime le fichier ayant pour path "path"
     *
     * @param path Fichier à supprimer
     */
    public void deleteFileIfExists(Path path) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Indique si une des string des varargs est nul ou vide
     *
     * @param strings Liste des strings à affiché
     * @return True si une des strings est null ou vide
     */
    public boolean verifyEmptyOrNull(String... strings) {
        for (String s : strings) {
            if (s == null || s.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Calcul de la moyenne d'une liste de double
     *
     * @param listDouble La liste de double dont on veut calculer la moyenne
     * @return moyenne de la liste
     */
    public Double calculDeviationMoyenne(List<Double> listDouble) {
        return listDouble
                .stream()
                .mapToDouble(Double::doubleValue)
                .sum() / listDouble.size();
    }

    /**
     * Calcul la variance de la liste de double en parametre
     *
     * @param listDouble La liste de double dont on veut calculer la variance
     * @return Variance de la liste de double en parametre
     */
    private Double calculVariance(List<Double> listDouble) {
        double moyenne = calculDeviationMoyenne(listDouble);
        if (moyenne == 0) {
            return 0.;
        }
        return listDouble.stream()
                .map(value -> Math.pow(value - moyenne, 2))
                .mapToDouble(Double::doubleValue)
                .sum() / listDouble.size();
    }

    /**
     * Calcul l'ecart type de la liste de double passé en paramètre
     *
     * @param listDouble Liste de double ou il faut calculer l'écart type
     * @return L'ecart type de la liste
     */
    public Double calculEcartType(List<Double> listDouble) {
        return Math.sqrt(calculVariance(listDouble));
    }

    /**
     * Trouve le minimum d'une liste, renvoie 0 si la liste est vide
     *
     * @param listDouble Liste dans laquel trouver le minimum
     * @return Le minimum de la liste, si liste vide , renvoie 0
     */
    public Double calculMin(List<Double> listDouble) {
        return listDouble
                .stream()
                .min(Double::compare)
                .orElse(0.);
    }

    /**
     * Trouve le maximum d'une liste, renvoie 0 si la liste est vide
     *
     * @param listDouble Liste dans laquel trouver le maximum
     * @return Le maximum de la liste, si liste vide , renvoie 0
     */
    public Double calculMax(List<Double> listDouble) {
        return listDouble
                .stream()
                .max(Double::compare)
                .orElse(0.);
    }

    /**
     * Calcul la somme des carré de la liste
     *
     * @param listDoubleEcart Liste à partir de laquel calcul la somme des carré
     * @return La somme des ecarts quadratique
     */
    public Double calculSommeEcartsQuadratique(List<Double> listDoubleEcart) {
        return listDoubleEcart
                .stream()
                .mapToDouble(ecart -> ecart * ecart)
                .sum();
    }

    /**
     * Deplacer le fichier "toMove" à "newLocation"
     *
     * @param toMove      Path du fichier à deplacer
     * @param newLocation Path de l'endroit où le déplacer
     */
    public void moveFileToLocation(Path toMove, Path newLocation) {
        try {
            deleteFileIfExists(newLocation);
            Files.move(toMove, newLocation);
        } catch (IOException e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Copy la source a l'emplacement target si la target n'existe pas
     *
     * @param source Fichier source à copier
     * @param target Emplacement ou copier le fichier
     */
    public void copySourceToTargetIfnotExists(Path source, Path target) {
        try {
            if (Files.notExists(target)) {
                Files.copy(source, target);
            }
        } catch (IOException e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * @param date Date à partir de laquelle on veut la fin du mois
     * @return La fin du mois de la date courante
     */
    public LocalDate calculEndOfMonthFromDate(LocalDate date) {
        return date.plusDays(date.getMonth().length(date.isLeapYear()) - date.getDayOfMonth());
    }
}
