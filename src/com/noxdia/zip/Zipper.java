package com.noxdia.zip;

import com.noxdia.logger.SaspLogger;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.util.logging.Level;

public class Zipper {
    private final SaspLogger saspLogger = SaspLogger.getInstance();
    public Zipper() {

    }

    /**
     * Compresse le dossier "folderToAdd" dans une archive zip avec le nom "zipFileName"
     *
     * @param zipFileName Nom du fichier zip
     * @param folderToAdd Dossier à ajouter
     */
    public void pack(String zipFileName, String folderToAdd) {

        try {
            ZipFile zipFile = new ZipFile(zipFileName);
            // Initiate Zip Parameters which define various properties such
            // as compression method, etc.
            ZipParameters parameters = new ZipParameters();

            // set compression method to store compression
            parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

            // Set the compression level. This value has to be in between 0 to 9
            // Several predefined compression levels are available
            // DEFLATE_LEVEL_FASTEST - Lowest compression level but higher speed of compression
            // DEFLATE_LEVEL_FAST - Low compression level but higher speed of compression
            // DEFLATE_LEVEL_NORMAL - Optimal balance between compression level/speed
            // DEFLATE_LEVEL_MAXIMUM - High compression level with a compromise of speed
            // DEFLATE_LEVEL_ULTRA - Highest compression level but low speed
            parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

            // Add folder to the zip file
            zipFile.addFolder(folderToAdd, parameters);

        } catch (ZipException e) {
            saspLogger.log(Level.WARNING, e.getMessage());
        }

    }

    /**
     * Compresse le dossier "folderToAdd" dans une archive zip avec le nom "zipFileName" et protégé par le mot de passe "password"
     * @param zipFileName Nom du fichier zip
     * @param folderToAdd Dossier à ajouter
     */
    public void packWithPassword(String zipFileName, String folderToAdd, String password) {

        try {
            ZipFile zipFile = new ZipFile(zipFileName);
            // Initiate Zip Parameters which define various properties such
            // as compression method, etc.
            ZipParameters parameters = new ZipParameters();

            // set compression method to store compression
            parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

            // Set the compression level. This value has to be in between 0 to 9
            // Several predefined compression levels are available
            // DEFLATE_LEVEL_FASTEST - Lowest compression level but higher speed of compression
            // DEFLATE_LEVEL_FAST - Low compression level but higher speed of compression
            // DEFLATE_LEVEL_NORMAL - Optimal balance between compression level/speed
            // DEFLATE_LEVEL_MAXIMUM - High compression level with a compromise of speed
            // DEFLATE_LEVEL_ULTRA - Highest compression level but low speed
            parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

            // Set the encryption flag to true
            // If this is set to false, then the rest of encryption properties are ignored
            parameters.setEncryptFiles(true);

            // Set the encryption method to AES Zip Encryption
            parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);

            // Set AES Key strength. Key strengths available for AES encryption are:
            // AES_STRENGTH_128 - For both encryption and decryption
            // AES_STRENGTH_192 - For decryption only
            // AES_STRENGTH_256 - For both encryption and decryption
            // Key strength 192 cannot be used for encryption. But if a zip file already has a
            // file encrypted with key strength of 192, then Zip4j can decrypt this file
            parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);

            // Set password
            parameters.setPassword(password);

            // Add folder to the zip file
            zipFile.addFolder(folderToAdd, parameters);

        } catch (ZipException e) {
            saspLogger.log(Level.WARNING, e.getMessage());
        }

    }

    /**
     * Extrait le dossier zip "sourceZipFilePath" dans le repertoire "extractedZipFilePath" en utilisant le mot de passe "password"
     * @param sourceZipFilePath Dossier zip a extraire
     * @param extractedZipFilePath Endroit ou extraire le dossier zip
     * @param password Mot de passe de l'archive
     */
    public void unpackWithPassword(String sourceZipFilePath, String extractedZipFilePath, String password) {
        try {
            ZipFile zipFile = new ZipFile(sourceZipFilePath);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword(password);
            }
            zipFile.extractAll(extractedZipFilePath);
        } catch (ZipException e) {
            saspLogger.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Extrait le dossier zip "sourceZipFilePath" dans le repertoire "extractedZipFilePath"
     * @param sourceZipFilePath Dossier zip a extraire
     * @param extractedZipFilePath Endroit ou extraire le dossier zip
     */
    public void unpack(String sourceZipFilePath, String extractedZipFilePath) {
        try {
            ZipFile zipFile = new ZipFile(sourceZipFilePath);
            zipFile.extractAll(extractedZipFilePath);
        } catch (ZipException e) {
            saspLogger.log(Level.WARNING, e.getMessage());
        }
    }

}
