package com.noxdia.docxGenerator;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.cavite.data.SimulatedData;
import com.noxdia.tools.Tools;
import org.docx4j.dml.wordprocessingDrawing.Inline;
import org.docx4j.jaxb.Context;
import org.docx4j.jaxb.XPathBinderAssociationIsPartialException;
import org.docx4j.model.structure.HeaderFooterPolicy;
import org.docx4j.model.structure.SectionWrapper;
import org.docx4j.model.table.TblFactory;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.docx4j.openpackaging.parts.WordprocessingML.HeaderPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.*;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Classe permettant la génération d'un fichier docx à partir du template donné en paramètre
 */
public class DocxGenerator {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final List<DataForDocx> dataForDocxList = new ArrayList<>();
    private final String templateName;
    private final Database database;
    private final DecimalFormat df = new DecimalFormat("0.##");
    private final ObjectFactory factory = Context.getWmlObjectFactory();
    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();
    private final boolean generateTemp;
    private final boolean generateCavern;
    private final boolean generateCasingShoe;


    public DocxGenerator(String templateName, Database database, boolean generateTemp, boolean generateCavern, boolean generateCasingShoe) {
        this.templateName = Objects.requireNonNull(templateName);
        this.database = Objects.requireNonNull(database);
        this.generateTemp = generateTemp;
        this.generateCavern = generateCavern;
        this.generateCasingShoe = generateCasingShoe;
    }

    /**
     * Permet de généer le fichier docx à partir du template et des différents paramètres
     *
     * @param newReportName  Nom du fichier
     * @param site           Site à partir duquel il faut les informations
     * @param reportDate     Date de fin du rapport
     * @param graphLocations Liste des emplacement des images
     * @return True si pas d'erreur lors de la génération, false sinon
     */
    public boolean generate(String newReportName, Site site, LocalDate reportDate, List<String> graphLocations) {
        saspLogger.log(Level.INFO, "Entering generate docx function");

        // Creer le dossier si jamais il n'existe pas
        Path folderExportPath = Paths.get("./export");
        if (Files.notExists(folderExportPath)) {
            tools.createFolder(folderExportPath);
        }

        folderExportPath = Paths.get("./export/reports");
        if (Files.notExists(folderExportPath)) {
            tools.createFolder(folderExportPath);
        }

        newReportName = "./export/reports/" + newReportName + ".docx";
        try {
            List<Cavity> listCavityForSite = database.getListCavityForSite(site);
            listCavityForSite.forEach(cavity -> {
                List<Calage> calageList = database.getListCalageForCavity(cavity);
                List<RealData> realDataList = database.getRealDataForCavity(cavity);
                if (!realDataList.isEmpty()) {
                    if (!calageList.isEmpty()) {
                        Calage calage = calageList.get(0);
                        LocalDate debut = calage.getFrom();
                        LocalDate fin = calage.getTo();

                        Optional<DataForDocx> dataForDocxOpt = populateDataForDocx(cavity, debut, fin);
                        if (dataForDocxOpt.isPresent()) {
                            DataForDocx dataForDocx = dataForDocxOpt.get();
                            dataForDocxList.add(dataForDocx);
                        }
                    }
                }
            });

            if (dataForDocxList.isEmpty()) {
                saspLogger.log(Level.INFO, "Could not load data to generate, no calage in database");
            }

            WordprocessingMLPackage template = getTemplate(templateName);

            replaceDateAtStart(template, reportDate);

            createMatchingStatsPart2(template);

            if (!graphLocations.isEmpty()) {
                writeListGraphsPart2(graphLocations, template);
                writeListGraphsPart3(graphLocations, template);
            }

            createMatchingStatsPart4(reportDate, listCavityForSite, template);

            if (!graphLocations.isEmpty()) {
                writeListGraphsPart4(graphLocations, template);
            }

            writeDocxToStream(template, newReportName);
            saspLogger.log(Level.INFO, "Docx finished generated without error");
            return true;
        } catch (Exception e) {
            saspLogger.log(Level.WARNING, "Error while generating docx file : " + e.getMessage());
            return false;
        } finally {
            dataForDocxList.clear();
        }
    }

    /**
     * Calcul et ajoute toutes les données necessaire à ecrire dans le document
     *
     * @param cavity Cavité à partir de laquel recupéré les données
     * @param debut  Date de debut
     * @param fin    Date de fin
     * @return Objet dataForDocx si pas d'erreur, Optional.empty() sinon
     */
    private Optional<DataForDocx> populateDataForDocx(Cavity cavity, LocalDate debut, LocalDate fin) {

        List<Calage> calageList = database.getListCalageForCavity(cavity)
                .stream()
                .filter(Calage::isLocked)
                .collect(Collectors.toList());

        // S'il y a des calages
        if (!calageList.isEmpty()) {
            // Recupère le dernier calage en date et recopie les valeurs dans un nouveau calage temporaire
            final Calage[] calageCopy = new Calage[1];
            calageList.stream().skip(calageList.size() - 1).forEach(calage -> {
                // Ajoute un calage temporaire à la fin pour generer les données necessaire
                LocalDate endCalage = calage.getTo();
                LocalDate startNewTemp = endCalage.plusDays(1);
                if (startNewTemp.isBefore(fin)) {
                    calageCopy[0] = calage.copyWithNewPeriod(startNewTemp, fin);
                    calageCopy[0].setLockStatus(true);
                }
            });
            if (calageCopy[0] != null) {
                calageList.add(calageCopy[0]);
            }
        }

        if (calageList.isEmpty()) {
            return Optional.empty();
        }

        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final boolean[] noError = {true};
        calageList.forEach(calage -> {

            noError[0] = calage.simulateTemperature(); // Permet de simuler pression et temperature et remplie la liste des simulated data lié au calage
            simulatedDataList.addAll(calage.getSimulatedDataList());
        });

        if (!noError[0]) {
            return Optional.empty();
        }

        List<SimulatedData> filteredSimulatedData = simulatedDataList.stream()
                .filter(simulatedData -> tools.dateBetweenOthersInclusive(simulatedData.getDateInsertion(), debut, fin))
                .collect(Collectors.toList());

        List<RealData> realDataList = database.getRealDataForCavity(cavity);

        List<Double> deviationPression = new ArrayList<>();
        List<Double> deviationTemperature = new ArrayList<>();

        realDataList.forEach(realData -> {
            addPressionDevToList(filteredSimulatedData, deviationPression, realData);
            addTempDevToList(filteredSimulatedData, deviationTemperature, realData);
        });

        Double sumSquareDevPression = tools.calculSommeEcartsQuadratique(deviationPression);
        Double ecartTypePression = tools.calculEcartType(deviationPression);
        Double minDevPression = tools.calculMin(deviationPression);
        Double maxDevPression = tools.calculMax(deviationPression);
        Double moyDevPression = tools.calculDeviationMoyenne(deviationPression);

        Double sumSquareDevTemp = tools.calculSommeEcartsQuadratique(deviationTemperature);
        Double ecartTypeTemp = tools.calculEcartType(deviationTemperature);
        Double minDevTemp = tools.calculMin(deviationTemperature);
        Double maxDevTemp = tools.calculMax(deviationTemperature);
        Double moyDevTemp = tools.calculDeviationMoyenne(deviationTemperature);

        if (testIfDoubleAreNaN(sumSquareDevPression, ecartTypePression, minDevPression, maxDevPression, moyDevPression,
                sumSquareDevTemp, ecartTypeTemp, minDevTemp, maxDevTemp, moyDevTemp)) {
            return Optional.empty();
        }

        // Faire calcul et ajouter à la liste
        String cavityName = cavity.getName();
        DataForDocx dataForDocx = new DataForDocx(cavityName, sumSquareDevPression, ecartTypePression, minDevPression, maxDevPression, moyDevPression, sumSquareDevTemp, ecartTypeTemp, minDevTemp, maxDevTemp, moyDevTemp);

        return Optional.of(dataForDocx);
    }

    /**
     * Test s'il y a des NaN dans la liste de double en parametre
     *
     * @param doubles List des doubles
     * @return True s'il y a un NaN, false sinon
     */
    private boolean testIfDoubleAreNaN(Double... doubles) {
        for (Double d : doubles) {
            if (d.isNaN()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Crée l'objet "WordprocessingMLPackage" à partir du fichier à l'emplacer "location"
     *
     * @param location Emplacement du fichier template
     * @return Le WordprocessingMLPackage à partir du fichier
     * @throws Docx4JException       : Problème dans la biliothèque
     * @throws FileNotFoundException : Fichier non trouver
     */
    private WordprocessingMLPackage getTemplate(String location) throws Docx4JException, IOException {
        FileInputStream fileInputStream = new FileInputStream(new File(location));
        WordprocessingMLPackage template = WordprocessingMLPackage.load(fileInputStream);
        fileInputStream.close();
        return template;

    }

    /**
     * Remplace les dates au debut du document et dans le header
     *
     * @param template   Template dans lequel remplacer les dates
     * @param dateReport Date de fin du rapport
     */
    private void replaceDateAtStart(WordprocessingMLPackage template, LocalDate dateReport) {
        saspLogger.log(Level.INFO, "Remplace date debut");
        String dateActuelPlaceHolder = "0_DATEA";
        LocalDate dateNow = LocalDate.now();
        String newText = dtf.format(dateNow);
        replacePlaceholder(template.getMainDocumentPart(), newText, dateActuelPlaceHolder);

        saspLogger.log(Level.INFO, "Remplace date debut terminé");

        saspLogger.log(Level.INFO, "Remplace date dans titre");

        // Mois actuel
        String dateMoiAnneePlaceHolder = "0_DATEB";

        newText = dateReport.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH).toUpperCase() + " " + dateReport.getYear();
        replacePlaceholder(template.getMainDocumentPart(), newText, dateMoiAnneePlaceHolder);

        saspLogger.log(Level.INFO, "Remplace date dans titre terminé");

        saspLogger.log(Level.INFO, "Remplace date dans en-tête");
        // Date au debut du fichier, changer par la date actuelle
        String dateMoiAnneeEnTetePlaceHolder = "0_DATEC";
        HeaderPart headerPart = null;
        List<SectionWrapper> sectionWrappers = template.getDocumentModel().getSections();
        for (SectionWrapper sw : sectionWrappers) {
            HeaderFooterPolicy hfp = sw.getHeaderFooterPolicy();
            if (hfp.getDefaultHeader() != null) {
                headerPart = hfp.getDefaultHeader();
            }
        }
        replacePlaceholder(headerPart, newText, dateMoiAnneeEnTetePlaceHolder);
        saspLogger.log(Level.INFO, "Remplace date dans en-tête terminé");
    }

    /**
     * Crée les tableau de matching stats pour la période global
     *
     * @param template Template ou modifier les informations
     * @throws JAXBException                            ?
     * @throws XPathBinderAssociationIsPartialException Placeholder non trouvé dans le fichier
     */
    private void createMatchingStatsPart2(WordprocessingMLPackage template) throws JAXBException, XPathBinderAssociationIsPartialException {
        saspLogger.log(Level.INFO, "Creating matchings stats tabs on part 2");
        // 2 tableaux de la partie 2
        String pressionTabP2placeholder = "2_PMSC";
        String tempTabP2placeholder = "2_TMSC";
        MainDocumentPart mainDocumentPart = template.getMainDocumentPart();
        if (dataForDocxList.isEmpty()) {
            saspLogger.log(Level.WARNING, "Could not rebuild for previous month, missing data");
            replacePlaceholder(mainDocumentPart, "No calage, could not generate data", pressionTabP2placeholder);
            if (generateTemp) {
                replacePlaceholder(mainDocumentPart, "No calage, could not generate data", tempTabP2placeholder);
            } else {
                replacePlaceholder(mainDocumentPart, "", tempTabP2placeholder);
            }
        } else {
            createTablePressionAtPlaceHolder(template, pressionTabP2placeholder, true);
            if (generateTemp) {
                createTableTemperatureAtPlaceHolder(template, tempTabP2placeholder, false);
            } else {
                replacePlaceholder(mainDocumentPart, "", tempTabP2placeholder);
            }
        }
        saspLogger.log(Level.INFO, "Creating matchings stats tabs part 2 finished");
    }

    /**
     * Ecrit la liste des graphs pour la partie 2 du template
     *
     * @param graphLocations Liste des emplacement des images
     * @param template       Template ou modifier les information
     * @throws JAXBException                            Exception du à la libraire
     * @throws XPathBinderAssociationIsPartialException Placeholder non trouvé dans le fichier
     */
    private void writeListGraphsPart2(List<String> graphLocations, WordprocessingMLPackage template) throws JAXBException, XPathBinderAssociationIsPartialException {
        saspLogger.log(Level.INFO, "Creating graphs on part 2 for all dates");

        MainDocumentPart mainDocumentPart = template.getMainDocumentPart();
        // Pression et temperature reel + simulé
        String graph1APressionPlaceholder = "2_G1P";
        createListImageAtPlaceHolder(template, graph1APressionPlaceholder, "P2Pression", graphLocations);

        String graph1BTempPlaceholder = "2_G1T";
        if (generateTemp) {
            createListImageAtPlaceHolder(template, graph1BTempPlaceholder, "P2Temperature", graphLocations);
        } else {
            replacePlaceholder(mainDocumentPart, "", graph1BTempPlaceholder);
        }

        // Pression et temperature sur casing shoe
        String graph1EPressionPlaceholder = "2_G1PCS";
        String graph1FTempPlaceholder = "2_G1TCS";
        if (generateCasingShoe) {
            createListImageAtPlaceHolder(template, graph1EPressionPlaceholder, "P2PCasingShoe", graphLocations);
            if (generateTemp) {
                createListImageAtPlaceHolder(template, graph1FTempPlaceholder, "P2TCasingShoe", graphLocations);
            } else {
                replacePlaceholder(mainDocumentPart, "", graph1FTempPlaceholder);
            }
        } else {
            replacePlaceholder(mainDocumentPart, "", graph1EPressionPlaceholder);
            replacePlaceholder(mainDocumentPart, "", graph1FTempPlaceholder);
        }

        String graph1CPressionPlaceholder = "2_G1PS";
        String graph1DTempPlaceholder = "2_G1TS";
        if (generateCavern) {
            // Pression  et temperature sur Cavern
            createListImageAtPlaceHolder(template, graph1CPressionPlaceholder, "P2PCavern", graphLocations);

            if (generateTemp) {
                createListImageAtPlaceHolder(template, graph1DTempPlaceholder, "P2TCavern", graphLocations);
            } else {
                replacePlaceholder(mainDocumentPart, "", graph1DTempPlaceholder);
            }
        } else {
            replacePlaceholder(mainDocumentPart, "", graph1CPressionPlaceholder);
            replacePlaceholder(mainDocumentPart, "", graph1DTempPlaceholder);
        }
        saspLogger.log(Level.INFO, "Creating graphs on part 2 finished");
    }

    /**
     * Ecrit la liste des graphs pour la partie 3 du template
     *
     * @param graphLocations Liste des emplacement des images
     * @param template       Template ou modifier les information
     * @throws JAXBException                            Exception du à la libraire
     * @throws XPathBinderAssociationIsPartialException Placeholder non trouvé dans le fichier
     */
    private void writeListGraphsPart3(List<String> graphLocations, WordprocessingMLPackage template) throws JAXBException, XPathBinderAssociationIsPartialException {
        saspLogger.log(Level.INFO, "Creating graphs on part 3 for specific dates");

        MainDocumentPart mainDocumentPart = template.getMainDocumentPart();
        // Pression et temperature reel + simulé
        String graph3APressionPlaceholder = "3_G1P";
        String graph3BTempPlaceholder = "3_G1T";

        createListImageAtPlaceHolder(template, graph3APressionPlaceholder, "P3Pression", graphLocations);
        if (generateTemp) {
            createListImageAtPlaceHolder(template, graph3BTempPlaceholder, "P3Temperature", graphLocations);
        } else {
            replacePlaceholder(mainDocumentPart, "", graph3BTempPlaceholder);
        }

        // Pression et temperature sur casing shoe
        String graph1EPressionPlaceholder = "3_G1PCS";
        String graph1FTempPlaceholder = "3_G1TCS";
        if (generateCasingShoe) {
            createListImageAtPlaceHolder(template, graph1EPressionPlaceholder, "P3PCasingShoe", graphLocations);
            if (generateTemp) {
                createListImageAtPlaceHolder(template, graph1FTempPlaceholder, "P3TCasingShoe", graphLocations);
            } else {
                replacePlaceholder(mainDocumentPart, "", graph1FTempPlaceholder);
            }
        } else {
            replacePlaceholder(mainDocumentPart, "", graph1EPressionPlaceholder);
            replacePlaceholder(mainDocumentPart, "", graph1FTempPlaceholder);
        }

        // Pression  et temperature sur Cavern
        String graph3CPressionPlaceholder = "3_G1PS";
        String graph3DTempPlaceholder = "3_G1TS";
        if (generateCavern) {
            createListImageAtPlaceHolder(template, graph3CPressionPlaceholder, "P3PCavern", graphLocations);
            if (generateTemp) {
                createListImageAtPlaceHolder(template, graph3DTempPlaceholder, "P3TCavern", graphLocations);
            } else {
                replacePlaceholder(mainDocumentPart, "", graph3DTempPlaceholder);
            }
        } else {
            replacePlaceholder(mainDocumentPart, "", graph3CPressionPlaceholder);
            replacePlaceholder(mainDocumentPart, "", graph3DTempPlaceholder);

        }
        saspLogger.log(Level.INFO, "Creating graphs on part 3 finished");
    }

    /**
     * Cree les tableaux de matching stats pour la partie 4 du template
     *
     * @param reportDate        Fin de la periode pour faire les stats
     * @param listCavityForSite Liste des cavité du site
     * @param template          Template ou modifier les information
     * @throws JAXBException                            Exception du à la libraire
     * @throws XPathBinderAssociationIsPartialException Placeholder non trouvé dans le fichier
     */
    private void createMatchingStatsPart4(LocalDate reportDate, List<Cavity> listCavityForSite, WordprocessingMLPackage template) throws JAXBException, XPathBinderAssociationIsPartialException {

        LocalDate monthFin = tools.calculEndOfMonthFromDate(reportDate);

        LocalDate previousMonthFin = monthFin.minusMonths(1);

        // On reconstruit la liste pour les dates specifiques
        rebuildListForSpecificDate(previousMonthFin, listCavityForSite);

        saspLogger.log(Level.INFO, "Creating matchings stats tabs on part 4");
        String pressionTabP4placeholder = "4_PMSC1";
        String tempTabP4placeholder = "4_TMSC1";
        MainDocumentPart mainDocumentPart = template.getMainDocumentPart();
        if (dataForDocxList.isEmpty()) {
            saspLogger.log(Level.WARNING, "Could not rebuild for previous month, missing data");
            replacePlaceholder(mainDocumentPart, "No calage, could not generate data", pressionTabP4placeholder);
            if (generateTemp) {
                replacePlaceholder(mainDocumentPart, "No calage, could not generate data", tempTabP4placeholder);
            } else {
                replacePlaceholder(mainDocumentPart, "", tempTabP4placeholder);
            }
            // On passe sur le mois courant
            rebuildListForSpecificDate(monthFin, listCavityForSite);

            String pressionTabP4CurrentMonthplaceholder = "4_PMSC2";
            String tempTabP4CurrentMonthplaceholder = "4_TMSC2";
            if (dataForDocxList.isEmpty()) {
                saspLogger.log(Level.WARNING, "Could not rebuild for month, missing data");
                replacePlaceholder(mainDocumentPart, "No calage, could not generate data", pressionTabP4CurrentMonthplaceholder);
                if (generateTemp) {
                    replacePlaceholder(mainDocumentPart, "No calage, could not generate data", tempTabP4CurrentMonthplaceholder);
                } else {
                    replacePlaceholder(mainDocumentPart, "", tempTabP4CurrentMonthplaceholder);
                }
            } else {
                createTablePressionAtPlaceHolder(template, pressionTabP4CurrentMonthplaceholder, false);
                if (generateTemp) {
                    createTableTemperatureAtPlaceHolder(template, tempTabP4CurrentMonthplaceholder, false);
                } else {
                    replacePlaceholder(mainDocumentPart, "", tempTabP4CurrentMonthplaceholder);
                }
            }

        } else {
            createTablePressionAtPlaceHolder(template, pressionTabP4placeholder, false);
            if (generateTemp) {
                createTableTemperatureAtPlaceHolder(template, tempTabP4placeholder, false);
            } else {
                replacePlaceholder(template.getMainDocumentPart(), "", tempTabP4placeholder);
            }

            // On passe sur le mois courant
            rebuildListForSpecificDate(monthFin, listCavityForSite);

            String pressionTabP4CurrentMonthplaceholder = "4_PMSC2";
            String tempTabP4CurrentMonthplaceholder = "4_TMSC2";
            if (dataForDocxList.isEmpty()) {
                saspLogger.log(Level.WARNING, "Could not rebuild for month, missing data");
                replacePlaceholder(mainDocumentPart, "No calage, could not generate data", pressionTabP4CurrentMonthplaceholder);
                if (generateTemp) {
                    replacePlaceholder(mainDocumentPart, "No calage, could not generate data", tempTabP4CurrentMonthplaceholder);
                } else {
                    replacePlaceholder(mainDocumentPart, "", tempTabP4CurrentMonthplaceholder);
                }
            } else {
                createTablePressionAtPlaceHolder(template, pressionTabP4CurrentMonthplaceholder, false);
                if (generateTemp) {
                    createTableTemperatureAtPlaceHolder(template, tempTabP4CurrentMonthplaceholder, false);
                } else {
                    replacePlaceholder(mainDocumentPart, "", tempTabP4CurrentMonthplaceholder);
                }
            }

        }
        saspLogger.log(Level.INFO, "Creating matchings stats tabs part 4 finished");
    }

    /**
     * Ecrit la liste des graphs pour la partie 4 du template
     *
     * @param graphLocations Liste des emplacement des images
     * @param template       Template où modifier les information
     * @throws JAXBException                            Exception du à la libraire
     * @throws XPathBinderAssociationIsPartialException Placeholder non trouvé dans le fichier
     */
    private void writeListGraphsPart4(List<String> graphLocations, WordprocessingMLPackage template) throws JAXBException, XPathBinderAssociationIsPartialException {
        saspLogger.log(Level.INFO, "Creating graphs on part 4");

        // Graph des deviations pression
        String graph4APressionPlaceholder = "4_DP1";
        createListImageAtPlaceHolder(template, graph4APressionPlaceholder, "P4PressDeviation", graphLocations);

        // Histogramme des deviation pression
        String graph4CPressionPlaceHolder = "4_DPG1";
        createListImageAtPlaceHolder(template, graph4CPressionPlaceHolder, "P4PressDevHistogramme", graphLocations);

        String graph4BTempPlaceholder = "4_DP2";
        String graph4DTempPlaceHolder = "4_DPG2";
        if (generateTemp) {
            // Graph des deviations temperature
            createListImageAtPlaceHolder(template, graph4BTempPlaceholder, "P4TempDeviation", graphLocations);

            // Histogramme des deviation temperature
            createListImageAtPlaceHolder(template, graph4DTempPlaceHolder, "P4TempDevHistogramme", graphLocations);
        } else {
            MainDocumentPart mainDocumentPart = template.getMainDocumentPart();
            replacePlaceholder(mainDocumentPart, "", graph4BTempPlaceholder);
            replacePlaceholder(mainDocumentPart, "", graph4DTempPlaceHolder);
        }

        saspLogger.log(Level.INFO, "Creating graphs on part 4 finished");
    }

    /**
     * Insère la liste des images a l'emplacement voulu dans le fichier
     *
     * @param template       Document charé en mémoire
     * @param placeholder    Texte à remplacer dans le document
     * @param typeRegexp     Nom du fichier à retrouver
     * @param graphLocations Liste des emplacement des images des graphes
     * @throws JAXBException                            Exception du à la libraire
     * @throws XPathBinderAssociationIsPartialException Placeholder non trouvé dans le fichier
     */
    private void createListImageAtPlaceHolder(WordprocessingMLPackage template, String placeholder, String typeRegexp, List<String> graphLocations) throws JAXBException, XPathBinderAssociationIsPartialException {
        String xpath = createXPathWithPlaceholder(placeholder);
        MainDocumentPart mainDocumentPart = template.getMainDocumentPart();

        final List<Object> jaxbNodes = mainDocumentPart.getJAXBNodesViaXPath(xpath, false);
        if (jaxbNodes.isEmpty()) {
            saspLogger.log(Level.WARNING, "No nodes : " + placeholder);
            return;
        }
        R r = (R) jaxbNodes.get(0);
        P parent = (P) r.getParent();

        int index = mainDocumentPart.getContent().indexOf(parent);

        List<String> graphLocationsFilteredList = graphLocations.stream()
                .filter(s -> s.contains(typeRegexp))
                .collect(Collectors.toList());

        if (graphLocationsFilteredList.isEmpty()) {
            saspLogger.log(Level.WARNING, "Graphe location list empty");
            return;
        }
        mainDocumentPart.getContent().remove(index);
        for (String graphLocation : graphLocationsFilteredList) {
            // create image

            Optional<org.docx4j.wml.P> imageOpt = createImage(template, graphLocation);
            if (imageOpt.isPresent()) {
                org.docx4j.wml.P image = imageOpt.get();
                mainDocumentPart.getContent().add(index, image);
                index++;

                P paragraph = createText("\n");
                mainDocumentPart.getContent().add(index, paragraph);

                index++;
            }
        }

    }

    /**
     * Cree le text avec la string S et renvoie le paragraphe associé
     *
     * @param stringToWrite String a ecrire
     * @return Paragraphe avec le texte
     */
    private P createText(String stringToWrite) {
        Text text = factory.createText();
        text.setValue(stringToWrite);
        P paragraph = factory.createP();
        R run = factory.createR();
        run.getContent().add(text);
        paragraph.getContent().add(run);
        return paragraph;
    }

    /**
     * Cree l'image et l'associe a un paragraphe
     *
     * @param template      Template où modifier les information
     * @param graphLocation Localisation du fichier image
     * @return Paragraphe avec l'image ou Optional.empty() sinon
     */
    private Optional<P> createImage(WordprocessingMLPackage template, String graphLocation) {
        saspLogger.log(Level.INFO, "In create image");
        Path path = Paths.get(graphLocation);
        if (path == null) {
            saspLogger.log(Level.WARNING, "Null path");
            return Optional.empty();
        }
        if (Files.notExists(path)) {
            saspLogger.log(Level.WARNING, "Path not exist");
            return Optional.empty();
        }
        try {
            byte[] bytes = Files.readAllBytes(path);

            String filenameHint = null;
            String altText = null;
            int id1 = 0;
            int id2 = 1;

            int size = 9750;

            org.docx4j.wml.P image = newImage(template, bytes,
                    filenameHint, altText,
                    id1, id2, size);
            saspLogger.log(Level.INFO, "End of create image");
            return Optional.of(image);
        } catch (Exception e) {
            saspLogger.log(Level.WARNING, e.getMessage());
            return Optional.empty();
        }
    }

    /**
     * Cree le tableau temperature au placeholder voulu
     *
     * @param template    Template où modifier les information
     * @param placeholder Texte à remplacer
     * @param refreshXml  Indique si on doit rafraichir le fichier xml avant d'effectuer la recherche
     * @throws JAXBException                            Exception du à la libraire
     * @throws XPathBinderAssociationIsPartialException Placeholder non trouvé dans le fichier
     */
    private void createTableTemperatureAtPlaceHolder(WordprocessingMLPackage template, String placeholder, boolean refreshXml) throws JAXBException, XPathBinderAssociationIsPartialException {
        String xpath = createXPathWithPlaceholder(placeholder);
        MainDocumentPart mainDocumentPart = template.getMainDocumentPart();

        // false si fait après le 1er getJAXBNodesViaXPath
        final List<Object> jaxbNodes = mainDocumentPart.getJAXBNodesViaXPath(xpath, refreshXml);
        R r = (R) jaxbNodes.get(0);
        P parent = (P) r.getParent();

        int index = mainDocumentPart.getContent().indexOf(parent);
        Tbl tbl = createTableMatchingStat(template, false);

        mainDocumentPart.getContent().remove(index);
        mainDocumentPart.getContent().add(index, tbl);
    }

    /**
     * Cree le tableau pression au placeholder voulu
     *
     * @param template    Template où modifier les information
     * @param placeholder Texte à remplacer
     * @param refreshXml  Indique si on doit rafraichir le fichier xml avant d'effectuer la recherche
     * @throws JAXBException                            Exception du à la libraire
     * @throws XPathBinderAssociationIsPartialException Placeholder non trouvé dans le fichier
     */
    private void createTablePressionAtPlaceHolder(WordprocessingMLPackage template, String placeholder, boolean refreshXml) throws JAXBException, XPathBinderAssociationIsPartialException {
        String xpath = createXPathWithPlaceholder(placeholder);
        MainDocumentPart mainDocumentPart = template.getMainDocumentPart();

        // true car 1er getJAXBNodesViaXPath
        final List<Object> jaxbNodes = mainDocumentPart.getJAXBNodesViaXPath(xpath, refreshXml);
        R r = (R) jaxbNodes.get(0);
        P parent = (P) r.getParent();

        int index = mainDocumentPart.getContent().indexOf(parent);
        Tbl tbl = createTableMatchingStat(template, true);

        mainDocumentPart.getContent().remove(index);
        mainDocumentPart.getContent().add(index, tbl);
    }

    /**
     * Permet de creer le XPath à partir de la string "placeholder"
     */
    private String createXPathWithPlaceholder(String placeholder) {
        return "//w:r[w:t[contains(text(), \'" + placeholder + "\')]]";
    }

    /**
     * Ajoute le style à la table
     *
     * @param table      Table à laquelle ajouter le style
     * @param tableWidth Largeur à appliqué à la table
     */
    private void setStyleToTable(Tbl table, int tableWidth) {
        // Affiche la grille du tableau
        showTableGrid(table);

        // Definie la taille maximal du tableau
        TblWidth tblwidth = factory.createTblWidth();
        tblwidth.setType("dxa");
        tblwidth.setW(BigInteger.valueOf(tableWidth));
        table.getTblPr().setTblW(tblwidth);

        CTTblOverlap tbloverlap = factory.createCTTblOverlap();
        tbloverlap.setVal(STTblOverlap.NEVER);
        table.getTblPr().setTblOverlap(tbloverlap);

        // Aligne par rapport au text au dessus
        CTTblPPr tblppr = factory.createCTTblPPr();
        tblppr.setLeftFromText(BigInteger.valueOf(141));
        tblppr.setRightFromText(BigInteger.valueOf(141));
        tblppr.setVertAnchor(STVAnchor.TEXT);
        tblppr.setTblpX(BigInteger.valueOf(567));
        tblppr.setTblpY(BigInteger.valueOf(1));
        table.getTblPr().setTblpPr(tblppr);
    }

    /**
     * Permet d'afficher la grille du tableau "table"
     *
     * @param table table sur laquel afficher la grille
     */
    private void showTableGrid(Tbl table) {
        CTTblPrBase.TblStyle tblprbasetblstyle = factory.createCTTblPrBaseTblStyle();
        tblprbasetblstyle.setVal("Grilledutableau");
        table.getTblPr().setTblStyle(tblprbasetblstyle);
    }

    /**
     * @param filteredSimulatedDataList Liste de simulatedData ou chercher le simulated data à la date voulu
     * @param deviationTemperatureList  Liste des deviation de la temperature
     * @param realData                  Real data avec lequel calculé la deviation
     */
    private void addTempDevToList(List<SimulatedData> filteredSimulatedDataList, List<Double> deviationTemperatureList, RealData realData) {
        LocalDate date = realData.getDateInsertion();
        Optional<Double> optTempSimul = getTemperatureInSimulatedDataForDate(filteredSimulatedDataList, date);
        if (optTempSimul.isPresent()) {
            double tempSimul = optTempSimul.get();
            double tempReel = realData.getTemperatureWelHead();
            double deviation = tempSimul - tempReel;
            deviationTemperatureList.add(deviation);
        }
    }

    /**
     * @param filteredSimulatedDataList Liste de simulatedData ou chercher le simulated data à la date voulu
     * @param deviationPressionList     Liste des deviation de la pression
     * @param realData                  Real data avec lequel calculé la deviation
     */
    private void addPressionDevToList(List<SimulatedData> filteredSimulatedDataList, List<Double> deviationPressionList, RealData realData) {
        LocalDate date = realData.getDateInsertion();
        Optional<Double> optPressionSimul = getPressionInSimulatedDataForDate(filteredSimulatedDataList, date);
        if (optPressionSimul.isPresent()) {
            double pressionSimul = optPressionSimul.get();
            double pressionReel = realData.getPressionWelHead();
            deviationPressionList.add(pressionSimul - pressionReel);
        }
    }

    /**
     * Creer la table des stats de matching pour la pression ou la temperature, si "pression" == true -> tableau pression
     * Sinon tableau temperature
     *
     * @param template   Template où modifier les elements
     * @param isPression Indique si on fait le tableau de la pression ou celui de la temperature
     * @return Le tableau de matching stat créé
     */
    private Tbl createTableMatchingStat(WordprocessingMLPackage template, boolean isPression) {

        int writableWidthTwips = template.getDocumentModel().getSections()
                .get(0).getPageDimensions()
                .getWritableWidthTwips();

        int countCavitys = dataForDocxList.size();
        writableWidthTwips -= 567;
        int cellWidthTwips = (writableWidthTwips - 567) / countCavitys;
        int rows = 6;
        Tbl table = TblFactory.createTable(rows, 1 + countCavitys, cellWidthTwips);

        setStyleToTable(table, writableWidthTwips);

        // pression
        if (isPression) {
            setTextForColumnRow(table, 0, 0, "Cavern", true);
            setTextForColumnRow(table, 1, 0, "Sum of square deviations (bar²)", false);
            setTextForColumnRow(table, 2, 0, "Standard deviation (bar)", false);
            setTextForColumnRow(table, 3, 0, "Minimum deviation (bar)", false);
            setTextForColumnRow(table, 4, 0, "Maximum deviation (bar)", false);
            setTextForColumnRow(table, 5, 0, "Average deviation (bar)", false);
            final int[] col = {1};
            dataForDocxList.forEach(dataForDocx -> {
                String name = dataForDocx.cavityName;
                double sumSquareDev = dataForDocx.sumSquareDevPression;
                double ecartType = dataForDocx.ecartTypePression;
                double minDev = dataForDocx.minDevPression;
                double maxDev = dataForDocx.maxDevPression;
                double moyDev = dataForDocx.moyDevPression;

                setTextForColumnRow(table, 0, col[0], name, true);
                setTextForColumnRow(table, 1, col[0], df.format(sumSquareDev), true);
                setTextForColumnRow(table, 2, col[0], df.format(ecartType), true);
                setTextForColumnRow(table, 3, col[0], df.format(minDev), true);
                setTextForColumnRow(table, 4, col[0], df.format(maxDev), true);
                setTextForColumnRow(table, 5, col[0], df.format(moyDev), true);

                col[0]++;
            });
        } else { // Temperature sinon
            setTextForColumnRow(table, 0, 0, "Cavern", true);
            setTextForColumnRow(table, 1, 0, "Sum of square deviations (°C)", false);
            setTextForColumnRow(table, 2, 0, "Standard deviation (°C)", false);
            setTextForColumnRow(table, 3, 0, "Minimum deviation (°C)", false);
            setTextForColumnRow(table, 4, 0, "Maximum deviation (°C)", false);
            setTextForColumnRow(table, 5, 0, "Average deviation (°C)", false);
            final int[] col = {1};
            dataForDocxList.forEach(dataForDocx -> {
                String name = dataForDocx.cavityName;
                double sumSquareDev = dataForDocx.sumSquareDevTemp;
                double ecartType = dataForDocx.ecartTypeTemp;
                double minDev = dataForDocx.minDevTemp;
                double maxDev = dataForDocx.maxDevTemp;
                double moyDev = dataForDocx.moyDevTemp;

                setTextForColumnRow(table, 0, col[0], name, true);
                setTextForColumnRow(table, 1, col[0], df.format(sumSquareDev), true);
                setTextForColumnRow(table, 2, col[0], df.format(ecartType), true);
                setTextForColumnRow(table, 3, col[0], df.format(minDev), true);
                setTextForColumnRow(table, 4, col[0], df.format(maxDev), true);
                setTextForColumnRow(table, 5, col[0], df.format(moyDev), true);

                col[0]++;
            });
        }
        return table;
    }

    /**
     * Ecrit le texte "stringToWrite" dans le tableau "table" a la position rowNumber / colNumber
     *
     * @param table         Table où écrire
     * @param rowNumber     Numero de ligne ou écrire
     * @param colNumber     Colonne ou écrire
     * @param stringToWrite Texte à ecrire dans la cellule
     * @param toCenter      Indique s'il faut centrer ou non le texte dans la celleule
     */
    private void setTextForColumnRow(Tbl table, int rowNumber, int colNumber, String stringToWrite, boolean toCenter) {
        Text text = factory.createText();

        text.setValue(stringToWrite);
        R run = factory.createR();
        run.getContent().add(text);

        Tr headRow = (Tr) table.getContent().get(rowNumber);
        Tc headColum = (Tc) headRow.getContent().get(colNumber);
        P columPara = (P) headColum.getContent().get(0);

        // Center text in paragraph
        if (toCenter) {
            centerTextInPara(columPara);
        } else {
            PPrBase.Ind pprbaseind = factory.createPPrBaseInd();
            pprbaseind.setLeft(BigInteger.valueOf(0));
            pprbaseind.setRight(BigInteger.valueOf(0));

            PPr paragraphProperties = factory.createPPr();
            paragraphProperties.setInd(pprbaseind);
            columPara.setPPr(paragraphProperties);
        }

        columPara.getContent().add(run);
    }

    /**
     * Centre le texte du paragraphe
     *
     * @param para Paragraphe où centré le texte
     */
    private void centerTextInPara(P para) {
        PPr paragraphProperties = factory.createPPr();
        Jc justification = factory.createJc();
        justification.setVal(JcEnumeration.CENTER);
        paragraphProperties.setJc(justification);
        para.setPPr(paragraphProperties);

    }

    /**
     * Remplace le placeholder par le texte "newValue" dans la partie du document volu
     *
     * @param documentPart Partie du document avec le placeholder
     * @param newValue     Nouveau texte a inserer
     * @param placeholder  Texte à remplacer dans le document
     */
    private void replacePlaceholder(Object documentPart, String newValue, String placeholder) {
        if (documentPart != null) {
            List<Object> texts = getAllElementFromObject(documentPart, Text.class);
            placeholder = "{" + placeholder + "}";
            for (Object text : texts) {
                Text textElement = (Text) text;
                if (textElement.getValue().equals(placeholder)) {
                    textElement.setValue(newValue);
                }
            }
        }
    }

    /**
     * Ecrit toutes les données du template dans le nouveau fichier
     *
     * @param template Template à écrire
     * @param target   Nom du fichier cible
     * @throws IOException     Fichier cible non trouvable
     * @throws Docx4JException ?
     */
    private void writeDocxToStream(WordprocessingMLPackage template, String target) throws IOException, Docx4JException {
        File f = new File(target);
        template.save(f);
    }

    /**
     * @param wordMLPackage Document dans lequel on ecrit
     * @param bytes         Données à écrire
     * @param filenameHint  ?
     * @param altText       Texte alternatif à la place de l'image
     * @param id1           ?
     * @param id2           ?
     * @param size          Largeur de l'image sur le document
     * @return Le paragraphe avec l'image
     * @throws Exception Si une exception pete sur createImagePart ou createImageInLine
     */
    private P newImage(WordprocessingMLPackage wordMLPackage,
                       byte[] bytes,
                       String filenameHint, String altText,
                       int id1, int id2, long size) throws Exception {

        BinaryPartAbstractImage imagePart = BinaryPartAbstractImage.createImagePart(wordMLPackage, bytes);

        Inline inline = imagePart.createImageInline(filenameHint, altText,
                id1, id2, size, false);

        // Now add the inline in w:p/w:r/w:drawing
        P paragraph = factory.createP();

        R run = factory.createR();
        paragraph.getContent().add(run);
        Drawing drawing = factory.createDrawing();
        run.getContent().add(drawing);
        drawing.getAnchorOrInline().add(inline);

        return paragraph;
    }

    /*############################## */
    ////////// Usefull stuff //////////
    /*############################## */

    /**
     * Récupère la pression à la date "date" dans la liste de données simulées
     *
     * @param simulatedDataList Liste de données simulées
     * @param date              Date à laquelle récupérer la valeur
     * @return La valeur de la pression à la date "date"
     */
    private Optional<Double> getPressionInSimulatedDataForDate(List<SimulatedData> simulatedDataList, LocalDate date) {
        return simulatedDataList.stream()
                .filter(simulatedData -> simulatedData.getDateInsertion().isEqual(date))
                .map(SimulatedData::getPressionWelHead)
                .findFirst();
    }

    /**
     * Récupère la temperature à la date "date" dans la liste de données simulées
     *
     * @param simulatedDataList Liste de données simulées
     * @param date              Date à laquelle récupérer la valeur
     * @return La valeur de la pression à la date "date"
     */
    private Optional<Double> getTemperatureInSimulatedDataForDate(List<SimulatedData> simulatedDataList, LocalDate date) {
        return simulatedDataList.stream()
                .filter(simulatedData -> simulatedData.getDateInsertion().isEqual(date))
                .map(SimulatedData::getTemperatureWelHead)
                .findFirst();
    }

    /**
     * Reconstruit la liste "dataForDocxList" avec les nouveaux éléments necessaire.
     *
     * @param finSpec           Date de fin de la reconstruction
     * @param listCavityForSite Liste des cavité du site
     */
    private void rebuildListForSpecificDate(LocalDate finSpec, List<Cavity> listCavityForSite) {
        dataForDocxList.clear();

        final LocalDate[] copyDebutSpec = new LocalDate[1];
        final LocalDate[] copyFinSpec = {finSpec};

        listCavityForSite.forEach(cavity -> {

            List<Calage> calageList = database.getListCalageForCavity(cavity);

            // S'il y a des calages
            if (!calageList.isEmpty()) {
                // Recupère le dernier calage en date et recopie les valeurs dans un nouveau calage temporaire
                calageList.stream().skip(calageList.size() - 1).forEach(calage -> {

                    // Ajoute un calage temporaire à la fin pour generer les données necessaire
                    LocalDate endCalage = calage.getTo();
                    LocalDate startNewTemp = endCalage.plusDays(1);
                    copyDebutSpec[0] = startNewTemp;

                });
            }

            List<RealData> realDataList = database.getRealDataForCavity(cavity);
            if (!realDataList.isEmpty()) {
                LocalDate debutGlobal = realDataList.get(0).getDateInsertion();
                LocalDate finGlobal = realDataList.get(realDataList.size() - 1).getDateInsertion();
                if (copyDebutSpec[0].isBefore(debutGlobal)) {
                    copyDebutSpec[0] = debutGlobal;
                }
                if (copyFinSpec[0].isAfter(finGlobal)) {
                    copyFinSpec[0] = finGlobal;
                }
            }

            Optional<DataForDocx> dataForDocxOpt = populateDataForDocx(cavity, copyDebutSpec[0], copyFinSpec[0]);
            if (dataForDocxOpt.isPresent()) {
                DataForDocx dataForDocx = dataForDocxOpt.get();
                dataForDocxList.add(dataForDocx);
            }
        });
    }

    /**
     * Recupère tous les éléments d'une classe d'un objet
     *
     * @param obj      Objet dans lequel chercher
     * @param toSearch Classe à rechercher dans l'objet
     * @param <T>      Type à chercher
     * @return La liste des tous les elements de l'objet ayant pour classe "toSearh"
     */
    @SuppressWarnings("unchecked")
    private <T> List<T> getAllElementFromObject(Object obj, Class<?> toSearch) {
        List<T> result = new ArrayList<>();
        if (obj instanceof JAXBElement) {
            obj = ((JAXBElement<?>) obj).getValue();
        }
        if (obj.getClass().equals(toSearch)) {
            result.add((T) obj);
        } else if (obj instanceof ContentAccessor) {
            List<?> children = ((ContentAccessor) obj).getContent();
            for (Object child : children) {
                result.addAll(getAllElementFromObject(child, toSearch));
            }

        }
        return result;
    }

    /**
     * Pour stocker les informations calculer une seule fois -> plus optimal
     */
    private static class DataForDocx {
        private final String cavityName;
        private final double sumSquareDevPression;
        private final double ecartTypePression;
        private final double minDevPression;
        private final double maxDevPression;
        private final double moyDevPression;

        private final double sumSquareDevTemp;
        private final double ecartTypeTemp;
        private final double minDevTemp;
        private final double maxDevTemp;
        private final double moyDevTemp;

        private DataForDocx(String cavityName, double sumSquareDevPression, double ecartTypePression, double minDevPression, double maxDevPression, double moyDevPression, double sumSquareDevTemp, double ecartTypeTemp, double minDevTemp, double maxDevTemp, double moyDevTemp) {
            this.cavityName = cavityName;
            this.sumSquareDevPression = sumSquareDevPression;
            this.ecartTypePression = ecartTypePression;
            this.minDevPression = minDevPression;
            this.maxDevPression = maxDevPression;
            this.moyDevPression = moyDevPression;
            this.sumSquareDevTemp = sumSquareDevTemp;
            this.ecartTypeTemp = ecartTypeTemp;
            this.minDevTemp = minDevTemp;
            this.maxDevTemp = maxDevTemp;
            this.moyDevTemp = moyDevTemp;
        }

        @Override
        public String toString() {
            return "DataForDocx{" +
                    "cavityName='" + cavityName + '\'' +
                    ", sumSquareDevPression=" + sumSquareDevPression +
                    ", ecartTypePression=" + ecartTypePression +
                    ", minDevPression=" + minDevPression +
                    ", maxDevPression=" + maxDevPression +
                    ", moyDevPression=" + moyDevPression +
                    ", sumSquareDevTemp=" + sumSquareDevTemp +
                    ", ecartTypeTemp=" + ecartTypeTemp +
                    ", minDevTemp=" + minDevTemp +
                    ", maxDevTemp=" + maxDevTemp +
                    ", moyDevTemp=" + moyDevTemp +
                    '}';
        }
    }


}
