package com.noxdia.docxGenerator.graphs;

import com.noxdia.logger.SaspLogger;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.cavite.data.SimulatedData;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;

class GraphsGeneratorUtils {
    private SaspLogger saspLogger = SaspLogger.getInstance();

    GraphsGeneratorUtils() {
    }

    /**
     * Génère la tache à effectuer plus tard
     *
     * @param runnable Code à executer plus tard
     * @return La tache avec le code à executer
     */
    Task<Void> generateTask(Runnable runnable) {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Platform.runLater(runnable);
                return null;
            }
        };
    }

    /**
     * Permet de creer un graphe avec 1 courbe et d'exporter la capture d'ecran sous format png
     *
     * @param graphTitle Titre du graphe
     * @param xLabel     Etiquette des abscisses
     * @param yLabel     Etiquette des ordonnées
     * @param courbeName Nom de la courbe
     * @param fileName   Nom du fichier ou exporter la capture d'ecran
     * @param dataFirst  Données de la courbe
     */
    void createSingleLineChart(String graphTitle, String xLabel, String yLabel, String courbeName, String fileName, TreeMap<Date, Double> dataFirst, int width, int height) {
        //defining the axes
        final DateAxis xAxis = new DateAxis();
        final NumberAxis yAxis = new NumberAxis();

        xAxis.setLabel(xLabel);
        yAxis.setLabel(yLabel);

        //creating the chart
        LineChart<Date, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setTitle(graphTitle);
        lineChart.setLegendSide(Side.TOP);

        //defining a series
        XYChart.Series<Date, Number> series = new XYChart.Series<>();
        series.setName(courbeName);

        dataFirst.forEach((date, value) -> {
            if (!value.isNaN()) {
                XYChart.Data<Date, Number> input = new XYChart.Data<>(date, value);
                series.getData().add(input);
            }
        });

        Scene scene = new Scene(lineChart, width, height);
        // to add CSS
        scene.getStylesheets().add(GraphsGenerator.class.getResource("graphs.css").toExternalForm());

        lineChart.setAnimated(false);
        lineChart.applyCss();
        lineChart.layout();

        lineChart.getData().add(series);

        // Supprime les points sur la courbe pour plus de lisibilité
        for (XYChart.Series<Date, Number> seriesTmp : lineChart.getData()) {
            //for all series, take date, each data has Node (symbol) for representing point
            for (XYChart.Data<Date, Number> data : seriesTmp.getData()) {
                // this node is StackPane
                StackPane stackPane = (StackPane) data.getNode();
                stackPane.setVisible(false);
            }
        }
        saveAsPng(scene, fileName);
    }

    /**
     * Permet de creer un graphe avec 2 courbes et d'exporter la capture d'ecran sous format png
     *
     * @param graphTitle       Titre du graphe
     * @param xLabel           Etiquette des abscisses
     * @param yLabel           Etiquette des ordonnées
     * @param courbeName       Nom de la 1ere courbe
     * @param courbeNameSecond Nom de la 2e courbe
     * @param fileName         Nom du fichier ou exporter la capture d'ecran
     * @param dataFirstCourbe  Données de la 1ere courbe
     * @param dataSecondCourbe Données de la 2e courbe
     */
    @SuppressWarnings("unchecked")
    void createDoubleLineChart(String graphTitle, String xLabel, String yLabel, String courbeName, String courbeNameSecond, String fileName, TreeMap<Date, Double> dataFirstCourbe, TreeMap<Date, Double> dataSecondCourbe, int width, int height) {

        //defining the axes
        final DateAxis xAxis = new DateAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel(xLabel);
        yAxis.setLabel(yLabel);

        //creating the chart
        LineChart<Date, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setTitle(graphTitle);
        lineChart.setLegendSide(Side.TOP);

        //defining a series
        XYChart.Series<Date, Number> series = new XYChart.Series<>();
        series.setName(courbeName);

        XYChart.Series<Date, Number> seriesSecond = new XYChart.Series<>();
        seriesSecond.setName(courbeNameSecond);

        dataFirstCourbe.forEach((date, value) -> {
            if (!value.isNaN()) {
                XYChart.Data<Date, Number> input = new XYChart.Data<>(date, value);
                series.getData().add(input);
            }
        });

        dataSecondCourbe.forEach((date, value) -> {
            if (!value.isNaN()) {
                XYChart.Data<Date, Number> input = new XYChart.Data<>(date, value);
                seriesSecond.getData().add(input);
            }
        });

        Scene scene = new Scene(lineChart, width, height);
        // to add CSS
        scene.getStylesheets().add(GraphsGenerator.class.getResource("graphs.css").toExternalForm());

        lineChart.setAnimated(false);

        lineChart.getData().addAll(series, seriesSecond);

        // Supprime les points sur la courbe pour plus de lisibilité
        for (XYChart.Series<Date, Number> seriesTmp : lineChart.getData()) {
            //for all series, take date, each data has Node (symbol) for representing point
            for (XYChart.Data<Date, Number> data : seriesTmp.getData()) {
                // this node is StackPane
                StackPane stackPane = (StackPane) data.getNode();
                stackPane.setVisible(false);
            }
        }
        saveAsPng(scene, fileName);
    }

    /**
     * Permet de creer un graphe avec 1 courbe et d'exporter la capture d'ecran sous format png
     *
     * @param graphTitle Titre du graphe
     * @param xLabel     Etiquette des abscisses
     * @param yLabel     Etiquette des ordonnées
     * @param histoName  Nom de l'histogramme
     * @param fileName   Nom du fichier ou exporter la capture d'ecran
     * @param dataHisto  Données de l'histogramme
     */
    @SuppressWarnings("unchecked")
    void createBarChart(String graphTitle, String xLabel, String yLabel, String histoName, String fileName, TreeMap<Double, Integer> dataHisto, int width, int height) {
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart barChart = new BarChart<>(xAxis, yAxis);
        barChart.setTitle(graphTitle);
        barChart.setCategoryGap(0);
        barChart.setBarGap(1);

        xAxis.setLabel(xLabel);
        yAxis.setLabel(yLabel);

        XYChart.Series<String, Number> series = new XYChart.Series<>();
        series.setName(histoName);

        dataHisto.forEach((valeur, nbOccurenceVal) -> {
            if (!valeur.isNaN()) {
                XYChart.Data<String, Number> input = new XYChart.Data<>(valeur.toString(), nbOccurenceVal);
                series.getData().add(input);
            }
        });

        Scene scene = new Scene(barChart, width, height);
        // to add CSS
        scene.getStylesheets().add(GraphsGenerator.class.getResource("graphs.css").toExternalForm());

        barChart.setAnimated(false);
        barChart.applyCss();
        barChart.layout();

        barChart.getData().add(series);

        saveAsPng(scene, fileName);
    }

    /**
     * Sauvegarde la capture d'ecran de la scene à l'emplacement "location"
     *
     * @param scene    Scene ou prendre le screenshot
     * @param fileName Nom du fichier
     */
    private void saveAsPng(Scene scene, String fileName) {
        String location = "./webroot/asset/screenshots/" + fileName + ".png";
        WritableImage image = scene.snapshot(null);
        File file = new File(location);
        try {
            RenderedImage fxImage = SwingFXUtils.fromFXImage(image, null);
            ImageIO.write(fxImage, "png", file);
        } catch (IOException e) {
            saspLogger.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * True si "localdDate" se trouve dans la liste "simulatedDataList"
     */
    boolean checkIfDateIsInListOfSimulatedData(List<SimulatedData> simulatedDataList, LocalDate localDate) {
        return simulatedDataList
                .stream()
                .anyMatch(simulatedData -> simulatedData.getDateInsertion().isEqual(localDate));
    }

    /**
     * True si "localdDate" se trouve dans la liste "realDataList"
     */
    boolean checkIfDateIsInListOfRealData(List<RealData> realDataList, LocalDate localDate) {
        return realDataList
                .stream()
                .anyMatch(realData -> realData.getDateInsertion().isEqual(localDate));
    }


}
