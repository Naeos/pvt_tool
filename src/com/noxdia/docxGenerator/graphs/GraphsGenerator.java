package com.noxdia.docxGenerator.graphs;

import com.noxdia.database.Database;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.tools.Tools;
import javafx.concurrent.Task;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;


/**
 * Classe permettant de générer les graphs à mettre dans le fichier docx
 */

public class GraphsGenerator {

    private final SaspLogger saspLogger = SaspLogger.getInstance();
    private final Tools tools = Tools.getInstance();
    private final int width = 1000;
    private final int height = 550;
    private final Site site;
    //    private final LocalDate from;
    private final DataPopulator dataPopulator;
    private final GraphsGeneratorUtils graphsGeneratorUtils;
    private final Database database;

    private final LocalDate dateGeneration;
    private final boolean generateTemp;
    private final boolean generateCavern;
    private final boolean generateCasingShoe;

    private CountDownLatch executionCompleted;

    public GraphsGenerator(Database database, Site site, LocalDate dateGeneration, boolean generateTemp, boolean generateCavern, boolean generateCasingShoe) {
        this.database = database;
        this.site = site;
        this.dateGeneration = dateGeneration;
        this.graphsGeneratorUtils = new GraphsGeneratorUtils();
        this.dataPopulator = new DataPopulator(database, graphsGeneratorUtils);
        this.generateTemp = generateTemp;
        this.generateCavern = generateCavern;
        this.generateCasingShoe = generateCasingShoe;
    }

    // Permet de générer tous les screenshots des graphs à mettre dans le fichier docx généré
    public boolean generateAllGraphs() {

        // for chaque cavity -> creer chacun des graphs à sauvegarder -> generer les images

        List<Cavity> cavityList = database.getListCavityForSite(site);

        List<Thread> threads = new ArrayList<>();

        // Pour chaque cavités
        cavityList.forEach(cavity -> {
            List<RealData> realDataList = database.getRealDataForCavity(cavity);
            if (!realDataList.isEmpty() && realDataList.size() > 4) {
                // Graphe sur periode calage
                generateGraphsPeriodCalage(threads, cavity);

                // Graphe sur periode calage + reste des données
                generateGraphsPeriodCalageToEndOfData(threads, cavity);

                generateGraphsDeviationPeriodCalageToEndOfData(threads, cavity);

            }
        });
        // On lance toutes les threads à la suite et on attend la fin de chacune avant de continuer le programme
        // Il faut que tous les graphs soit généré avant de continuer
        executionCompleted = new CountDownLatch(threads.size());

        threads.forEach(Thread::start);
        while (executionCompleted.getCount() > 0) {
            try {
                executionCompleted.await();
            } catch (InterruptedException e) {
                saspLogger.log(Level.WARNING, e.getMessage());
            }
        }

        threads.forEach(thread -> {
            if (thread.isAlive()) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    saspLogger.log(Level.WARNING, e.getMessage());
                }
            }
        });
        threads.clear();
        return executionCompleted.getCount() == 0;
    }

    // ############################### //
    // Graphe sur la période de calage //
    // ############################### //

    /**
     * Génère tous les graphs pour la période du calage
     *
     * @param threads Liste de threads dans laquelle ajouter les taches à faire
     * @param cavity  Cavity à partir de laquelle recuperer les données
     */
    private void generateGraphsPeriodCalage(List<Thread> threads, Cavity cavity) {

        List<Calage> calageList = database.getListCalageForCavity(cavity);
        Optional<Calage> optCalage = calageList.stream().filter(Calage::isLocked).findFirst();
        String prefix = "P2";
        if (optCalage.isPresent()) {
            Calage calage = optCalage.get();
            LocalDate from = calage.getFrom();
            LocalDate to = calage.getTo();

            generateDoubleGraphPression(threads, cavity, from, to, prefix);
            if (generateTemp) {
                generateDoubleGraphTemperature(threads, cavity, from, to, prefix);
            }

            if (generateCasingShoe) {
                generateGraphePCasingShoe(threads, cavity, from, to, prefix);
                if (generateTemp) {
                    generateGrapheTCasingShoe(threads, cavity, from, to, prefix);
                }
            }

            if (generateCavern) {
                generateGraphePCavern(threads, cavity, from, to, prefix);
                if (generateTemp) {
                    generateGrapheTCavern(threads, cavity, from, to, prefix);
                }
            }
        } else {
            List<RealData> realDataList = database.getRealDataForCavity(cavity);
            if (!realDataList.isEmpty()) {
                LocalDate from = realDataList.get(0).getDateInsertion();
                LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();

                if (generateCasingShoe) {
                    generateGraphePCasingShoe(threads, cavity, from, to, prefix);
                    if (generateTemp) {
                        generateGrapheTCasingShoe(threads, cavity, from, to, prefix);
                    }
                }

                if (generateCavern) {
                    generateGraphePCavern(threads, cavity, from, to, prefix);
                    if (generateTemp) {
                        generateGrapheTCavern(threads, cavity, from, to, prefix);
                    }
                }
            }
        }

    }

    /**
     * Génère tous les graphs pour la periode de calage + jusqu'a le fin de la periode
     *
     * @param threads Liste de threads dans laquelle ajouter les taches à faire
     * @param cavity  Cavity à partir de laquelle recuperer les données
     */
    private void generateGraphsPeriodCalageToEndOfData(List<Thread> threads, Cavity cavity) {
        List<Calage> calageList = database.getListCalageForCavity(cavity);

        Optional<Calage> optCalage = calageList.stream().filter(Calage::isLocked).findFirst();

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        String prefix = "P3";
        if (optCalage.isPresent()) {
            if (!realDataList.isEmpty()) {

                Calage calage = optCalage.get();

                LocalDate from = calage.getFrom();
                LocalDate to = tools.calculEndOfMonthFromDate(dateGeneration);
                generateDoubleGraphPression(threads, cavity, from, to, prefix);

                if (generateTemp) {
                    generateDoubleGraphTemperature(threads, cavity, from, to, prefix);
                }

                if (generateCasingShoe) {
                    generateGraphePCasingShoe(threads, cavity, from, to, prefix);
                    if (generateTemp) {
                        generateGrapheTCasingShoe(threads, cavity, from, to, prefix);
                    }
                }

                if (generateCavern) {
                    generateGraphePCavern(threads, cavity, from, to, prefix);
                    if (generateTemp) {
                        generateGrapheTCavern(threads, cavity, from, to, prefix);
                    }
                }
            }
        } else {
            if (!realDataList.isEmpty()) {
                LocalDate from = realDataList.get(0).getDateInsertion();
                LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();

                if (generateCasingShoe) {
                    generateGraphePCasingShoe(threads, cavity, from, to, prefix);
                    if (generateTemp) {
                        generateGrapheTCasingShoe(threads, cavity, from, to, prefix);
                    }
                }

                if (generateCavern) {
                    generateGraphePCavern(threads, cavity, from, to, prefix);
                    if (generateTemp) {
                        generateGrapheTCavern(threads, cavity, from, to, prefix);
                    }
                }
            }
        }
    }

    /**
     * Génère tous les graphs de deviation pour la periode de calage + jusqu'a le fin de la periode
     *
     * @param threads Liste de threads dans laquelle ajouter les taches à faire
     * @param cavity  Cavity à partir de laquelle recuperer les données
     */
    private void generateGraphsDeviationPeriodCalageToEndOfData(List<Thread> threads, Cavity cavity) {
        List<Calage> calageList = database.getListCalageForCavity(cavity);

        Optional<Calage> optCalage = calageList.stream().filter(Calage::isLocked).findFirst();

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        if (optCalage.isPresent()) {
            if (!realDataList.isEmpty()) {
                Calage calage = optCalage.get();
                LocalDate from = calage.getFrom();
                LocalDate to = realDataList.get(realDataList.size() - 1).getDateInsertion();
                String prefix = "P4";
                generateGraphDeviationPression(threads, cavity, from, to, prefix);
                generateGraphHistoDeviationPression(threads, cavity, from, to, prefix);
                // Double graphe temperature
                if (generateTemp) {
                    generateGraphDeviationTemperature(threads, cavity, from, to, prefix);
                    generateGraphHistoDeviationTemperature(threads, cavity, from, to, prefix);
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère le double graph pression reel / Simulée pour la période voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateDoubleGraphPression(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {
        String cavityName = cavity.getName();
        String graphTitle = "Pressure_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");

            TreeMap<Date, Double> dataFirstCourbe = dataPopulator.populateDataRealPression(cavity, from, to);
            TreeMap<Date, Double> dataSecondCourbe = dataPopulator.populateDataSimulatedPression(cavity, from, to);

            String xLabel = "Time";
            String yLabel = "Pressure";
            String courbeNameFirst = "Real Pressure";
            String courbeNameSecond = "Simulated Pressure";
            String fileNameToGenerate = prefix + "Pression_" + cavityName;
            graphsGeneratorUtils.createDoubleLineChart(graphTitle, xLabel, yLabel, courbeNameFirst, courbeNameSecond, fileNameToGenerate, dataFirstCourbe, dataSecondCourbe, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");

            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);
        threads.add(new Thread(task));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère le double graph temperature reel / Simulée pour la période voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateDoubleGraphTemperature(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {
        String cavityName = cavity.getName();
        String graphTitle = "Temperature_" + cavityName;

        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");
            TreeMap<Date, Double> dataFirstCourbe = dataPopulator.populateDataRealTemperature(cavity, from, to);
            TreeMap<Date, Double> dataSecondCourbe = dataPopulator.populateDataSimulatedTemperature(cavity, from, to);

            String xLabel = "Time";
            String yLabel = "Temperature";
            String courbeNameFirst = "Real Temperature";
            String courbeNameSecond = "Simulated Temperature";
            String fileNameToGenerate = prefix + "Temperature_" + cavityName;
            graphsGeneratorUtils.createDoubleLineChart(graphTitle, xLabel, yLabel, courbeNameFirst, courbeNameSecond, fileNameToGenerate, dataFirstCourbe, dataSecondCourbe, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();

        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);
        threads.add(new Thread(task));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère le graph PCasingShoe pour la période voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateGraphePCasingShoe(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {

        String cavityName = cavity.getName();
        String graphTitle = "PressureCasingShoe_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");

            TreeMap<Date, Double> dataCourbe = dataPopulator.populateDataPCasingShoeFromData(cavity, from, to, site);
            TreeMap<Date, Double> dataCourbe2 = dataPopulator.populateDataPCasingShoeFromSimulated(cavity, from, to, site);

            String xLabel = "Time";
            String yLabel = "Pressure";
            String courbeName = "Pressure Casing Shoe From Real Data";
            String courbeName2 = "Pressure Casing Shoe From Simulated";

            String fileName = prefix + "PCasingShoe_" + cavityName;

            graphsGeneratorUtils.createDoubleLineChart(graphTitle, xLabel, yLabel, courbeName, courbeName2, fileName, dataCourbe, dataCourbe2, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);

        threads.add(new Thread(task));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère le graph TCasingShoe pour la période voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateGrapheTCasingShoe(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {

        String cavityName = cavity.getName();
        String graphTitle = "TemperatureCasingShoe_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");

            TreeMap<Date, Double> dataCourbe = dataPopulator.populateDataTCasingShoeFromData(cavity, from, to, site);
            TreeMap<Date, Double> dataCourbe2 = dataPopulator.populateDataTCasingShoeFromSimulated(cavity, from, to, site);

            String xLabel = "Time";
            String yLabel = "Temperature";
            String courbeName = "Temperature Casing Shoe From Real Data";
            String courbeName2 = "Temperature Casing Shoe From Simulated";
            String fileName = prefix + "TCasingShoe_" + cavityName;

//            graphsGeneratorUtils.createSingleLineChart(graphTitle, xLabel, yLabel, courbeName, fileName, dataCourbe, width, height);
            graphsGeneratorUtils.createDoubleLineChart(graphTitle, xLabel, yLabel, courbeName, courbeName2, fileName, dataCourbe, dataCourbe2, width, height);

            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);

        threads.add(new Thread(task));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère le graph PCavern pour la période voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateGraphePCavern(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {
        String cavityName = cavity.getName();
        String graphTitle = "PressureCavern_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");
            TreeMap<Date, Double> dataCourbe = dataPopulator.populateDataPCavernFromData(cavity, from, to, site);
            TreeMap<Date, Double> dataCourbe2 = dataPopulator.populateDataPCavernFromSimulated(cavity, from, to, site);

            String xLabel = "Time";
            String yLabel = "Pressure";
            String courbeName = "Pressure Cavern From Real Data";
            String courbeName2 = "Pressure Cavern From Simulated";
            String fileName = prefix + "PCavern_" + cavityName;

            graphsGeneratorUtils.createDoubleLineChart(graphTitle, xLabel, yLabel, courbeName, courbeName2, fileName, dataCourbe, dataCourbe2, width, height);

//            graphsGeneratorUtils.createSingleLineChart(graphTitle, xLabel, yLabel, courbeName, fileName, dataCourbe, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);
        threads.add(new Thread(task));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère le graph TCavern pour la période voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateGrapheTCavern(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {
        String cavityName = cavity.getName();
        String graphTitle = "TemperatureCavern_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");
            TreeMap<Date, Double> dataCourbe = dataPopulator.populateDataTCavernFromData(cavity, from, to, site);
            TreeMap<Date, Double> dataCourbe2 = dataPopulator.populateDataTCavernFromSimulated(cavity, from, to, site);

            String xLabel = "Time";
            String yLabel = "Temperature";
            String courbeName = "Temperature Cavern From Real Data";
            String courbeName2 = "Temperature Cavern From Simulated";
            String fileName = prefix + "TCavern_" + cavityName;

            graphsGeneratorUtils.createDoubleLineChart(graphTitle, xLabel, yLabel, courbeName, courbeName2, fileName, dataCourbe, dataCourbe2, width, height);

//            graphsGeneratorUtils.createSingleLineChart(graphTitle, xLabel, yLabel, courbeName, fileName, dataCourbe, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);

        threads.add(new Thread(task));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère les graphs de deviation de la pression sur la période voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateGraphDeviationPression(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {
        String cavityName = cavity.getName();
        String graphTitle = "Pressure_Deviation_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");

            TreeMap<Date, Double> dataFirstCourbe = dataPopulator.populateDataDeviationPression(cavity, from, to);

            String xLabel = "Time";
            String yLabel = "Deviation";
            String courbeNameFirst = "Deviation Pressure";
            String fileNameToGenerate = prefix + "PressDeviation_" + cavityName;
            graphsGeneratorUtils.createSingleLineChart(graphTitle, xLabel, yLabel, courbeNameFirst, fileNameToGenerate, dataFirstCourbe, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);
        threads.add(new Thread(task));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère les histogramme de deviation de la pression sur la période voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateGraphHistoDeviationPression(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {
        String cavityName = cavity.getName();
        String graphTitle = "Pressure_Deviation_Histogramme_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");

            TreeMap<Double, Integer> dataFirstCourbe = dataPopulator.populateDataHistogrammeDeviationPression(cavity, from, to);

            String xLabel = "Time";
            String yLabel = "Deviation";
            String histoName = "Deviation Pressure";
            String fileNameToGenerate = prefix + "PressDevHistogramme_" + cavityName;
            graphsGeneratorUtils.createBarChart(graphTitle, xLabel, yLabel, histoName, fileNameToGenerate, dataFirstCourbe, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);
        threads.add(new Thread(task));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère les graphs de deviation de la temperature sur la periode voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateGraphDeviationTemperature(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {
        String cavityName = cavity.getName();
        String graphTitle = "Temperature_Deviation_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");

            TreeMap<Date, Double> dataFirstCourbe = dataPopulator.populateDataDeviationTemperature(cavity, from, to);

            String xLabel = "Time";
            String yLabel = "Deviation";
            String courbeNameFirst = "Deviation Temperature";
            String fileNameToGenerate = prefix + "TempDeviation_" + cavityName;
            graphsGeneratorUtils.createSingleLineChart(graphTitle, xLabel, yLabel, courbeNameFirst, fileNameToGenerate, dataFirstCourbe, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);
        threads.add(new Thread(task));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Génère les histogrammes de deviation de la temperature sur la periode voulu
     *
     * @param threads La liste dans laquelle ajouter la nouvelle tache
     * @param cavity  Cavité à partir de laquelle on veut les informations
     * @param from    Date de debut du graphe
     * @param to      Date de fin du graphe
     * @param prefix  Prefixe du nom du fichier
     */
    private void generateGraphHistoDeviationTemperature(List<Thread> threads, Cavity cavity, LocalDate from, LocalDate to, String prefix) {
        String cavityName = cavity.getName();
        String graphTitle = "Temperature_Deviation_Histogramme_" + cavityName;
        Runnable runnable = () -> {
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' started loading");

            TreeMap<Double, Integer> dataFirstCourbe = dataPopulator.populateDataHistogrammeDeviationTemperature(cavity, from, to);

            String xLabel = "Time";
            String yLabel = "Deviation";
            String histoName = "Temperature Deviation";
            String fileNameToGenerate = prefix + "TempDevHistogramme_" + cavityName;
            graphsGeneratorUtils.createBarChart(graphTitle, xLabel, yLabel, histoName, fileNameToGenerate, dataFirstCourbe, width, height);
            saspLogger.log(Level.INFO, "Graph \'" + graphTitle + "\' on cavity \'" + cavityName + "\' finished loading");
            executionCompleted.countDown();
        };
        Task<Void> task = graphsGeneratorUtils.generateTask(runnable);
        threads.add(new Thread(task));
    }


}
