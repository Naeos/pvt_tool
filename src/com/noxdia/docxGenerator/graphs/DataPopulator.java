package com.noxdia.docxGenerator.graphs;

import com.noxdia.database.Database;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.data.DataFactory;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.cavite.data.SimulatedData;
import com.noxdia.tools.Tools;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static com.noxdia.web.server.CasingShoeHandlers.calculPCasingShoe;
import static com.noxdia.web.server.CasingShoeHandlers.calculTCasingShoe;
import static com.noxdia.web.server.CavernHandlers.calculPCavern;
import static com.noxdia.web.server.CavernHandlers.calculTCavern;

class DataPopulator {

    private final Database database;
    private final GraphsGeneratorUtils graphsGeneratorUtils;
    private final Tools tools = Tools.getInstance();

    DataPopulator(Database database, GraphsGeneratorUtils graphsGeneratorUtils) {
        this.database = Objects.requireNonNull(database);
        this.graphsGeneratorUtils = Objects.requireNonNull(graphsGeneratorUtils);
    }

    // ############################################## //
    // Fonctions d'ajout des données pour les graphes //
    // ############################################## //

    /**
     * Ajoute les données reel de la pression pour pouvoir les afficher sur le graphe
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataRealPression(Cavity cavity, LocalDate from, LocalDate to) {
        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        realDataList.sort(tools.dataRealComparator);
        TreeMap<Date, Double> treeMap = new TreeMap<>();
        realDataList.forEach(realData -> {
            LocalDate localDate = realData.getDateInsertion();
            Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
            Double value = realData.getPressionWelHead();
            treeMap.put(key, value);
        });
        return treeMap;
    }

    /**
     * Ajoute les données simulée de la pression pour pouvoir les afficher sur le graphe
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataSimulatedPression(Cavity cavity, LocalDate from, LocalDate to) {
        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();

        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);

        if (calageList.isEmpty()) {
            return new TreeMap<>();
        }
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new TreeMap<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        simulatedDataList.forEach(simulatedData -> {
            if (simulatedData.getPressionWelHead() != -1) {
                LocalDate localDate = simulatedData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = simulatedData.getPressionWelHead();
                treeMap.put(key, value);
            }
        });
        return treeMap;
    }

    /**
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date de fin
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataDeviationPression(Cavity cavity, LocalDate from, LocalDate to) {

        List<Calage> calageList = database.getListCalageForCavity(cavity);

        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);

        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new TreeMap<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        TreeMap<Date, Double> treeMap = new TreeMap<>();
        simulatedDataList.forEach(simulatedData -> {
            if (simulatedData.getPressionWelHead() != -1) {

                LocalDate localDate = simulatedData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Optional<Double> optPressionAtDate = getPressionInRealDataForDate(realDataList, localDate);
                if (optPressionAtDate.isPresent()) {
                    Double value = simulatedData.getPressionWelHead() - optPressionAtDate.get();
                    treeMap.put(key, value);
                }
            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données reel de la temperature pour pouvoir les afficher sur le graphe
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataRealTemperature(Cavity cavity, LocalDate from, LocalDate to) {
        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        realDataList.sort(tools.dataRealComparator);

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        realDataList.forEach(realData -> {
            LocalDate localDate = realData.getDateInsertion();
            Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
            Double value = realData.getTemperatureWelHead();
            treeMap.put(key, value);
        });
        return treeMap;
    }

    /**
     * Ajoute les données simulée de la temperature pour pouvoir les afficher sur le graphe
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataSimulatedTemperature(Cavity cavity, LocalDate from, LocalDate to) {
        List<Calage> calageList = database.getListCalageForCavity(cavity);
        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);
        if (calageList.isEmpty()) {
            return new TreeMap<>();
        }
        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new TreeMap<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        simulatedDataList.forEach(simulatedData -> {
            Double value = simulatedData.getTemperatureWelHead();
            if (value != -1) {
                LocalDate localDate = simulatedData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                treeMap.put(key, value);
            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données de deviation pour la temperature sur une periode donnée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataDeviationTemperature(Cavity cavity, LocalDate from, LocalDate to) {
        List<Calage> calageList = database.getListCalageForCavity(cavity);

        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);

        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new TreeMap<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        TreeMap<Date, Double> treeMap = new TreeMap<>();

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        simulatedDataList.forEach(simulatedData -> {
            if (simulatedData.getTemperatureWelHead() != -1) {
                LocalDate localDate = simulatedData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Optional<Double> optTemperatureAtDate = getTemperatureInRealDataForDate(realDataList, localDate);
                if (optTemperatureAtDate.isPresent()) {
                    Double value = simulatedData.getTemperatureWelHead() - optTemperatureAtDate.get();
                    treeMap.put(key, value);
                }

            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données simulée de la pression Casing shoe pour pouvoir les afficher sur le graphe
     * Prend les données réel en entrée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @param site   Site ou recupéré les informations sur le gaz
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataPCasingShoeFromData(Cavity cavity, LocalDate from, LocalDate to, Site site) {

        CavityData cavityData = new CavityData(cavity).invoke();
        double tubeLength = cavityData.getTubeLength();
        double diameter = cavityData.getDiameter();

        SiteData siteData = new SiteData(site).invoke();
        double masseMolaireTotal = siteData.getMasseMolaireTotal();
        double densiteTotal = siteData.getDensiteTotal();
        double tempCritique = siteData.getTempCritique();
        double pressionCritique = siteData.getPressionCritique();


        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        realDataList.forEach(realData -> {
            double tempWellHead = realData.getTemperatureWelHead();
            double pressionWellHead = realData.getPressionWelHead();
            double debit = realData.getDebit();
            double pCasingShoe = calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            if (pCasingShoe != -1) {
                LocalDate localDate = realData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = pCasingShoe;
                if (!value.isNaN())
                    treeMap.put(key, value);
            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données simulée de la pression Casing shoe pour pouvoir les afficher sur le graphe
     * Prend les données simulée en entrée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @param site   Site ou recupéré les informations sur le gaz
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataPCasingShoeFromSimulated(Cavity cavity, LocalDate from, LocalDate to, Site site) {

        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();

        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);

        if (calageList.isEmpty()) {
            return new TreeMap<>();
        }
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new TreeMap<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        CavityData cavityData = new CavityData(cavity).invoke();
        double tubeLength = cavityData.getTubeLength();
        double diameter = cavityData.getDiameter();

        SiteData siteData = new SiteData(site).invoke();
        double masseMolaireTotal = siteData.getMasseMolaireTotal();
        double densiteTotal = siteData.getDensiteTotal();
        double tempCritique = siteData.getTempCritique();
        double pressionCritique = siteData.getPressionCritique();


        TreeMap<Date, Double> treeMap = new TreeMap<>();
        simulatedDataList.forEach(simulatedData -> {
            double tempWellHead = simulatedData.getTemperatureWelHead();
            double pressionWellHead = simulatedData.getPressionWelHead();
            double debit = simulatedData.getDebit();
            double pCasingShoe = calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            if (pCasingShoe != -1) {
                LocalDate localDate = simulatedData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = pCasingShoe;
                if (!value.isNaN())
                    treeMap.put(key, value);
            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données simulée de la temperature Casing shoe pour pouvoir les afficher sur le graphe
     * Prend les données réel en entrée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @param site   Site ou recupéré les informations sur le gaz
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataTCasingShoeFromData(Cavity cavity, LocalDate from, LocalDate to, Site site) {
        CavityData cavityData = new CavityData(cavity).invoke();
        double tubeLength = cavityData.getTubeLength();
        double diameter = cavityData.getDiameter();

        SiteData siteData = new SiteData(site).invoke();
        double masseMolaireTotal = siteData.getMasseMolaireTotal();
        double densiteTotal = siteData.getDensiteTotal();
        double tempCritique = siteData.getTempCritique();
        double pressionCritique = siteData.getPressionCritique();


        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        realDataList.forEach(realData -> {
            double tempWellHead = realData.getTemperatureWelHead();
            double pressionWellHead = realData.getPressionWelHead();
            double debit = realData.getDebit();
            double tCasingShoe = calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            if (tCasingShoe != -1) {
                LocalDate localDate = realData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = tCasingShoe;
                if (!value.isNaN())
                    treeMap.put(key, value);
            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données simulée de la temperature Casing shoe pour pouvoir les afficher sur le graphe
     * Prend les données simulée en entrée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @param site   Site ou recupéré les informations sur le gaz
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataTCasingShoeFromSimulated(Cavity cavity, LocalDate from, LocalDate to, Site site) {
        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();

        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);

        if (calageList.isEmpty()) {
            return new TreeMap<>();
        }
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new TreeMap<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        CavityData cavityData = new CavityData(cavity).invoke();
        double tubeLength = cavityData.getTubeLength();
        double diameter = cavityData.getDiameter();

        SiteData siteData = new SiteData(site).invoke();
        double masseMolaireTotal = siteData.getMasseMolaireTotal();
        double densiteTotal = siteData.getDensiteTotal();
        double tempCritique = siteData.getTempCritique();
        double pressionCritique = siteData.getPressionCritique();


        TreeMap<Date, Double> treeMap = new TreeMap<>();
        simulatedDataList.forEach(simulatedData -> {
            double tempWellHead = simulatedData.getTemperatureWelHead();
            double pressionWellHead = simulatedData.getPressionWelHead();
            double debit = simulatedData.getDebit();
            double pCasingShoe = calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            if (pCasingShoe != -1) {
                LocalDate localDate = simulatedData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = pCasingShoe;
                if (!value.isNaN())
                    treeMap.put(key, value);
            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données simulée de la pression Cavern pour pouvoir les afficher sur le graphe
     * Prend les données réel en entrée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @param site   Site ou recupéré les informations sur le gaz
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataPCavernFromData(Cavity cavity, LocalDate from, LocalDate to, Site site) {

        CavityData cavityData = new CavityData(cavity).invoke();
        double tubeLength = cavityData.getTubeLength();
        double diameter = cavityData.getDiameter();
        double profondeurCavity = cavityData.getProfondeurCavity();

        SiteData siteData = new SiteData(site).invoke();
        double masseMolaireTotal = siteData.getMasseMolaireTotal();
        double densiteTotal = siteData.getDensiteTotal();
        double tempCritique = siteData.getTempCritique();
        double pressionCritique = siteData.getPressionCritique();


        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        realDataList.forEach(realData -> {

            double tempWellHead = realData.getTemperatureWelHead();
            double pressionWellHead = realData.getPressionWelHead();
            double debit = realData.getDebit();
            double pressionCasingShoe = calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            double temperatureCasingShoe = calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            double pCavern = calculPCavern(pressionCasingShoe, temperatureCasingShoe, tubeLength, profondeurCavity, densiteTotal, tempCritique, pressionCritique);
            if (pCavern != -1) {
                LocalDate localDate = realData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = pCavern;
                if (!value.isNaN()) {
                    treeMap.put(key, value);
                }
            }

        });

        return treeMap;
    }

    /**
     * Ajoute les données simulée de la pression Cavern pour pouvoir les afficher sur le graphe
     * Prend les données simulée en entrée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @param site   Site ou recupéré les informations sur le gaz
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataPCavernFromSimulated(Cavity cavity, LocalDate from, LocalDate to, Site site) {
        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();

        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);

        if (calageList.isEmpty()) {
            return new TreeMap<>();
        }
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new TreeMap<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        CavityData cavityData = new CavityData(cavity).invoke();
        double tubeLength = cavityData.getTubeLength();
        double diameter = cavityData.getDiameter();
        double profondeurCavity = cavityData.getProfondeurCavity();

        SiteData siteData = new SiteData(site).invoke();
        double masseMolaireTotal = siteData.getMasseMolaireTotal();
        double densiteTotal = siteData.getDensiteTotal();
        double tempCritique = siteData.getTempCritique();
        double pressionCritique = siteData.getPressionCritique();

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        simulatedDataList.forEach(simulatedData -> {
            double tempWellHead = simulatedData.getTemperatureWelHead();
            double pressionWellHead = simulatedData.getPressionWelHead();
            double debit = simulatedData.getDebit();
            double pressionCasingShoe = calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            double temperatureCasingShoe = calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            double pCavern = calculPCavern(pressionCasingShoe, temperatureCasingShoe, tubeLength, profondeurCavity, densiteTotal, tempCritique, pressionCritique);
            if (pCavern != -1) {
                LocalDate localDate = simulatedData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = pCavern;
                if (!value.isNaN()) {
                    treeMap.put(key, value);
                }
            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données simulée de la temperature Cavern pour pouvoir les afficher sur le graphe
     * Prend les données réel en entrée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @param site   Site ou recupéré les informations sur le gaz
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataTCavernFromData(Cavity cavity, LocalDate from, LocalDate to, Site site) {
        CavityData cavityData = new CavityData(cavity).invoke();
        double tubeLength = cavityData.getTubeLength();
        double diameter = cavityData.getDiameter();
        double profondeurCavity = cavityData.getProfondeurCavity();
        double volumeCavity = cavityData.getVolumeCavity();

        SiteData siteData = new SiteData(site).invoke();
        double masseMolaireTotal = siteData.getMasseMolaireTotal();
        double densiteTotal = siteData.getDensiteTotal();
        double tempCritique = siteData.getTempCritique();
        double pressionCritique = siteData.getPressionCritique();

        List<RealData> realDataList = database.getRealDataForCavityFromTo(cavity, from, to);
        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);
        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfRealData(realDataList, localDate)) {
                RealData fakeRealData = DataFactory.createRealData(localDate, cavity, -1, -1, -1, -1, localDate, "fakeValue");
                realDataList.add(fakeRealData);
            }
        });

        realDataList.sort(tools.dataRealComparator);

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        realDataList.forEach(realData -> {

            double tempWellHead = realData.getTemperatureWelHead();
            double pressionWellHead = realData.getPressionWelHead();
            double debit = realData.getDebit();
            double pressionCasingShoe = calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            double temperatureCasingShoe = calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            double pressionCavern = calculPCavern(pressionCasingShoe, temperatureCasingShoe, tubeLength, profondeurCavity, densiteTotal, tempCritique, pressionCritique);

            double volumeGaz = realData.getVolume();
            double tCavern = calculTCavern(pressionCavern, pressionCasingShoe, temperatureCasingShoe, volumeGaz, volumeCavity, tempCritique, pressionCritique);
            if (tCavern != -1) {
                LocalDate localDate = realData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = tCavern;
                if (!value.isNaN()) {
                    treeMap.put(key, value);
                }
            }

        });

        return treeMap;
    }

    /**
     * Ajoute les données simulée de la temperature Cavern pour pouvoir les afficher sur le graphe
     * Prend les données simulée en entrée
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @param site   Site ou recupéré les informations sur le gaz
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Date, Double> populateDataTCavernFromSimulated(Cavity cavity, LocalDate from, LocalDate to, Site site) {
        List<Calage> calageList = database.getListCalageForCavity(cavity);
        List<SimulatedData> simulatedDataList = new ArrayList<>();

        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);

        if (calageList.isEmpty()) {
            return new TreeMap<>();
        }
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new TreeMap<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                simulatedDataList.add(fakeSimulatedData);
            }
        });
        simulatedDataList.sort(tools.simulatedDataComparator);

        CavityData cavityData = new CavityData(cavity).invoke();
        double tubeLength = cavityData.getTubeLength();
        double diameter = cavityData.getDiameter();
        double profondeurCavity = cavityData.getProfondeurCavity();

        SiteData siteData = new SiteData(site).invoke();
        double masseMolaireTotal = siteData.getMasseMolaireTotal();
        double densiteTotal = siteData.getDensiteTotal();
        double tempCritique = siteData.getTempCritique();
        double pressionCritique = siteData.getPressionCritique();

        TreeMap<Date, Double> treeMap = new TreeMap<>();
        simulatedDataList.forEach(simulatedData -> {
            double tempWellHead = simulatedData.getTemperatureWelHead();
            double pressionWellHead = simulatedData.getPressionWelHead();
            double debit = simulatedData.getDebit();
            double pressionCasingShoe = calculPCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            double temperatureCasingShoe = calculTCasingShoe(masseMolaireTotal, tubeLength, diameter, densiteTotal, tempWellHead, pressionWellHead, debit, tempCritique, pressionCritique);
            double pCavern = calculTCavern(pressionCasingShoe, temperatureCasingShoe, tubeLength, profondeurCavity, densiteTotal, tempCritique, pressionCritique);
            if (pCavern != -1) {
                LocalDate localDate = simulatedData.getDateInsertion();
                Date key = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                Double value = pCavern;
                if (!value.isNaN()) {
                    treeMap.put(key, value);
                }
            }
        });
        return treeMap;
    }

    /**
     * Ajoute les données pour l'histogramme de deviation de la pression
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Double, Integer> populateDataHistogrammeDeviationPression(Cavity cavity, LocalDate from, LocalDate to) {

        List<Double> deviationList = generateDeviationListPression(cavity, from, to);
        TreeMap<Double, Integer> hashMap = new TreeMap<>();

        int precision = 10; // precision voulu, en 10e dans notre cas
        deviationList.forEach(deviation -> {
            double roundedValue = (double) Math.round(deviation * precision) / precision;
            if (!hashMap.containsKey(roundedValue)) {
                hashMap.put(roundedValue, 1);
            } else {
                int currentVal = hashMap.get(roundedValue);
                hashMap.put(roundedValue, currentVal + 1);
            }
        });

        return hashMap;
    }

    /**
     * Ajoute les données pour l'histogramme de deviation de la temperature
     *
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une treeMap de "Date,Double" , avec la date et la valeur correspondante de la donnée à ce moment
     */
    TreeMap<Double, Integer> populateDataHistogrammeDeviationTemperature(Cavity cavity, LocalDate from, LocalDate to) {

        List<Double> deviationList = generateDeviationListTemperature(cavity, from, to);
        TreeMap<Double, Integer> hashMap = new TreeMap<>();

        int precision = 10; // precision voulu, en 10e dans notre cas
        deviationList.forEach(deviation -> {
            double roundedValue = (double) Math.round(deviation * precision) / precision;
            if (!hashMap.containsKey(roundedValue)) {
                hashMap.put(roundedValue, 1);
            } else {
                int currentVal = hashMap.get(roundedValue);
                hashMap.put(roundedValue, currentVal + 1);
            }
        });

        return hashMap;
    }

    /**
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une liste de double avec toutes les deviation de la pression qui ont été calculer
     */
    private List<Double> generateDeviationListPression(Cavity cavity, LocalDate from, LocalDate to) {

        List<Calage> calageList = database.getListCalageForCavity(cavity);

        // S'il y a des calages
        addCalageTmpToEndOfPeriod(to, calageList);

        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;

            }
        });

        if (simulatedDataList.isEmpty()) {
            return Collections.emptyList();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<SimulatedData> filteredSimulatedData = simulatedDataList
                .stream()
                .filter(simulatedData -> {
                    LocalDate date = simulatedData.getDateInsertion();
                    return tools.dateBetweenOthersInclusive(date, from, to);
                }).collect(Collectors.toList());

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                filteredSimulatedData.add(fakeSimulatedData);
            }
        });
        filteredSimulatedData.sort(tools.simulatedDataComparator);
        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        List<Double> deviationList = new ArrayList<>();
        filteredSimulatedData.forEach(simulatedData -> {
            if (simulatedData.getPressionWelHead() != -1) {

                LocalDate localDate = simulatedData.getDateInsertion();
                Optional<Double> optPressionAtDate = getPressionInRealDataForDate(realDataList, localDate);
                if (optPressionAtDate.isPresent()) {
                    Double value = simulatedData.getPressionWelHead() - optPressionAtDate.get();
                    deviationList.add(value);
                }
            }
        });

        return deviationList;
    }

    /**
     * @param cavity Cavité ou sont stocké les données
     * @param from   Date de debut
     * @param to     Date ce fin
     * @return Une liste de double avec toutes les deviation de la temperature qui ont été calculer
     */
    private List<Double> generateDeviationListTemperature(Cavity cavity, LocalDate from, LocalDate to) {

        List<Double> deviationList = new ArrayList<>();

        List<Calage> calageList = database.getListCalageForCavity(cavity);
        addCalageTmpToEndOfPeriod(to, calageList);


        List<SimulatedData> simulatedDataList = new ArrayList<>();
        final Calage[] calageTemp = new Calage[1];
        calageList.forEach(calage -> {
            if (calage.isLocked()) {
                calage.simulateTemperature();
                simulatedDataList.addAll(calage.getSimulatedDataList());
                calageTemp[0] = calage;
            }
        });

        if (simulatedDataList.isEmpty()) {
            return new ArrayList<>();
        }

        simulatedDataList.sort(tools.simulatedDataComparator);

        List<SimulatedData> filteredSimulatedData = simulatedDataList
                .stream()
                .filter(simulatedData -> {
                    LocalDate date = simulatedData.getDateInsertion();
                    return tools.dateBetweenOthersInclusive(date, from, to);
                }).collect(Collectors.toList());

        List<LocalDate> allDatesInPeriod = tools.generateListDateFromTo(from, to);

        allDatesInPeriod.forEach(localDate -> {
            if (!graphsGeneratorUtils.checkIfDateIsInListOfSimulatedData(simulatedDataList, localDate)) {
                // calageTemp -> Juste un temporaire pour creer le simulated data
                SimulatedData fakeSimulatedData = DataFactory.createSimulatedData(localDate, cavity, -1, -1, -1, -1, calageTemp[0]);
                filteredSimulatedData.add(fakeSimulatedData);
            }
        });
        filteredSimulatedData.sort(tools.simulatedDataComparator);

        List<RealData> realDataList = database.getRealDataForCavity(cavity);
        filteredSimulatedData.forEach(simulatedData -> {
            if (simulatedData.getTemperatureWelHead() != -1) {
                LocalDate localDate = simulatedData.getDateInsertion();
                Optional<Double> optTemperatureAtDate = getTemperatureInRealDataForDate(realDataList, localDate);
                if (optTemperatureAtDate.isPresent()) {
                    Double value = simulatedData.getTemperatureWelHead() - optTemperatureAtDate.get();
                    deviationList.add(value);
                }
            }
        });

        return deviationList;
    }

    /**
     * @param to         Date de fin du nouveau calage à creer
     * @param calageList Liste des calages ou ajouter l'élément
     */
    private void addCalageTmpToEndOfPeriod(LocalDate to, List<Calage> calageList) {
        // S'il y a des calages
        if (!calageList.isEmpty()) {
            // Recupère le dernier calage en date et recopie les valeurs dans un nouveau calage temporaire
            final Calage[] calageCopy = new Calage[1];
            calageList.stream().skip(calageList.size() - 1).forEach(calage -> {
                LocalDate end = calage.getTo();
                LocalDate startNewTemp = end.plusDays(1);
                if (startNewTemp.isBefore(to)) {
                    calageCopy[0] = calage.copyWithNewPeriod(startNewTemp, to);
                    calageCopy[0].setLockStatus(true);
                }
            });
            if (null != calageCopy[0]) {
                calageList.add(calageCopy[0]);
            }
        }
    }

    /**
     * Récupère la pression à la date "date" dans la liste de données réel
     */
    private Optional<Double> getPressionInRealDataForDate(List<RealData> realDataList, LocalDate date) {
        return realDataList.stream()
                .filter(realData -> realData.getDateInsertion().isEqual(date))
                .map(RealData::getPressionWelHead)
                .findFirst();
    }

    /**
     * Récupère la temperature à la date "date" dans la liste de données réel
     */
    private Optional<Double> getTemperatureInRealDataForDate(List<RealData> realDataList, LocalDate date) {
        return realDataList.stream()
                .filter(simulatedData -> simulatedData.getDateInsertion().isEqual(date))
                .map(RealData::getTemperatureWelHead)
                .findFirst();

    }

    /**
     * Contient les données lié aux sites
     * Permet de centraliser les appels au calcul pour les sites
     */
    private static class SiteData {
        private Site site;
        private double densiteTotal;
        private double masseMolaireTotal;
        private double tempCritique;
        private double pressionCritique;

        SiteData(Site site) {
            this.site = site;
        }

        private double getDensiteTotal() {
            return densiteTotal;
        }

        private double getMasseMolaireTotal() {
            return masseMolaireTotal;
        }

        private double getTempCritique() {
            return tempCritique;
        }

        private double getPressionCritique() {
            return pressionCritique;
        }

        private SiteData invoke() {
            densiteTotal = site.calculDensiteTotal();
            masseMolaireTotal = site.calculMasseMolaireTotal();
            tempCritique = site.calculTempCritiqueTotal();
            pressionCritique = site.calculPressionCritiqueTotal();
            return this;
        }
    }


    /**
     * Contient les données lié aux Cavités
     * Permet de centraliser les appels au calcul pour les sites
     */
    private static class CavityData {
        private Cavity cavity;
        private double profondeurCavity;
        private double tubeLength;
        private double diameter;
        private double volumeCavity;

        private CavityData(Cavity cavity) {
            this.cavity = cavity;
        }

        private double getProfondeurCavity() {
            return profondeurCavity;
        }

        private double getTubeLength() {
            return tubeLength;
        }

        private double getDiameter() {
            return diameter;
        }

        private double getVolumeCavity() {
            return volumeCavity;
        }

        private CavityData invoke() {
            profondeurCavity = cavity.getDepth();
            tubeLength = cavity.getHauteurTubage();
            diameter = cavity.getDiametreTubage();
            volumeCavity = cavity.getVolume();
            return this;
        }
    }
}
