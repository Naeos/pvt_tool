package com.noxdia.logger;

import com.noxdia.tools.Tools;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class SaspLogger {

    private static SaspLogger self;

    static {
        try {
            // todo -> mettre a false une fois le programme fini, sert uniquement pour le debug
            // Permet d'aider le futur dev
            self = new SaspLogger(true);
        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    private final Logger logger = Logger.getLogger("SaspLogger");
    private final boolean enabled;

    /**
     * Constructor for our logger.
     *
     * @param enabled true if the logger can write log, false otherwise.
     * @throws IOException If the folder logs/ don't exist
     */
    private SaspLogger(boolean enabled) throws IOException {
        String fileName = Objects.requireNonNull(setNameWithCurrentDate());
        FileHandler fileHandler = new FileHandler(fileName, true);
        Logger logger = Logger.getLogger("");
        fileHandler.setFormatter(new SimpleFormatter());
        logger.addHandler(fileHandler);
        logger.setLevel(Level.CONFIG);
        this.enabled = enabled;
    }

    public static SaspLogger getInstance() {
        return self;
    }

    private static String setNameWithCurrentDate() {
        return "./logs/log_" + Date.from(Instant.now()).toString().replaceAll(" ", "__").replaceAll(":", "_") + ".log";
    }

    /**
     * This method logs the current message with the Level given in parameter.
     * If enabled is turned to false, we won't write anything.
     *
     * @param level   The level of the message (INFO, WARNING, etc) see Level for more details
     * @param message The message to write
     */
    public void log(Level level, String message) {
        Objects.requireNonNull(message);
        Objects.requireNonNull(level);
        Path logsDir = Paths.get("./logs");
        Tools.getInstance().createFolder(logsDir);
        if (enabled) {
//            Runtime rt = Runtime.getRuntime();
//            long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
//            logger.log(level, message + "\n" + "Memory usage : " + usedMB + "Mo\n");
            logger.log(level, message + "\n");
        }
    }
}
