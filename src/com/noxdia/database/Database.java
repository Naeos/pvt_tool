package com.noxdia.database;

import com.noxdia.exploitant.Exploitant;
import com.noxdia.exploitant.ExploitantFactory;
import com.noxdia.exploitant.contact.Contact;
import com.noxdia.exploitant.contact.ContactFactory;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.SiteFactory;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.CavityFactory;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.calage.CalageFactory;
import com.noxdia.site.cavite.data.DataFactory;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.gaz.CompositionGaz;
import com.noxdia.site.gaz.Gaz;
import com.noxdia.site.gaz.GazFactory;
import com.noxdia.tools.Tools;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.sql.*;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;

import static com.noxdia.database.DatabaseCreationRequests.*;

public class Database {

    private final Connection co;
    private final Tools tools = Tools.getInstance();

    public Database(Path pathToDB, String dbName) throws ClassNotFoundException, SQLException, IOException {
        Objects.requireNonNull(pathToDB);
        Objects.requireNonNull(dbName);
        Class.forName("org.sqlite.JDBC");
        tools.createFolder(pathToDB);
        String forGetConnection = "jdbc:sqlite:" + pathToDB + FileSystems.getDefault().getSeparator() + dbName + ".db";
        co = DriverManager.getConnection(forGetConnection);
        Objects.requireNonNull(co);
        initializeDB();
    }

    /**
     * Execute la requete passer en parametre
     *
     * @param request   Requete a executer
     * @param statement Statement sur lequel executer la requete
     * @throws SQLException S'il y a une erreur SQL
     */
    static void executeUpdate(String request, Statement statement) throws SQLException {
        statement.executeUpdate(request);
        statement.closeOnCompletion();
    }

    /**
     * Initialise la base de données
     */
    private void initializeDB() {
        SaspLogger.getInstance().log(Level.INFO, "Initializing database");
        createExploitantTable(co);
        createContactTable(co);
        createSiteTable(co);
        createCavityTable(co);
        createGazTable(co);
        createCompoGazTable(co);
        createCalageNarmaTable(co);
        createRealDataTable(co);
        createLienContactTable(co);
        SaspLogger.getInstance().log(Level.INFO, "Initializing database finished");
    }

    /////////////////////////////////////
    //######## Insert Requests ########//
    /////////////////////////////////////

    // Implementation dans classe DatabaseInsertRequests

    /**
     * Insere le site 'site' dans la base de données
     *
     * @param site Site à insérer dans la base de données
     */
    public void insertSite(Site site) {
        DatabaseInsertRequests.insertSite(site, co);
    }

    /**
     * Insere l'exploitant 'exp' dans la base de données
     *
     * @param exp Exploitant a insérer dans la base de données
     */
    public void insertExploitant(Exploitant exp) {
        DatabaseInsertRequests.insertExploitant(exp, co);
    }

    /**
     * Insere le contact 'contact' dans la base de données
     *
     * @param contact Contact à insérer dans la base de données
     */
    public void insertContact(Contact contact) {
        DatabaseInsertRequests.insertContact(contact, co);
    }

    /**
     * Insere le gaz 'gaz' dans la base de données
     *
     * @param gaz Gaz à insérer dans la base de données
     */
    public void insertGaz(Gaz gaz) {
        DatabaseInsertRequests.insertGaz(gaz, co);
    }

    /**
     * Insere la CompositionGaz 'compositionGaz' dans la base de données
     *
     * @param compositionGaz CompositionGaz à insérer dans la base de données
     */
    public void insertCompoGaz(CompositionGaz compositionGaz) {
        DatabaseInsertRequests.insertCompoGaz(compositionGaz, co);
    }

    /**
     * Insere la Cavity 'cavity' dans la base de données
     *
     * @param cavity Cavity à insérer dans la base de données
     */
    public void insertCavity(Cavity cavity) {
        DatabaseInsertRequests.insertCavity(cavity, co);
    }

    /**
     * Insere le RealData 'realData' dans la base de données
     *
     * @param realData RealData à insérer dans la base de données
     */
    public void insertRealData(RealData realData) {
        DatabaseInsertRequests.insertRealData(realData, co);
    }

    /**
     * Insere le Calage 'calage' dans la base de données
     *
     * @param calage Calage à insérer dans la base de données
     */
    public void insertCalageNarma(Calage calage) {
        DatabaseInsertRequests.insertCalageNarma(calage, co);
    }

    /**
     * Insert un lienContactSite dans la base de données, crée un lien entre un site 'site' et un contact 'contact'
     *
     * @param contact Contact à associer au site
     * @param site    Site à associer au contact
     */
    public void insertLienContactSite(Contact contact, Site site) {
        DatabaseInsertRequests.insertLienContactSite(contact, site, co);
    }

    /////////////////////////////////////
    //######## Update Requests ########//
    /////////////////////////////////////

    /**
     * Met à jour le site 'site' dans la base de données
     *
     * @param site Site mis à jour dans le programme et devant etre mis à jour dans la base de données
     */
    public void updateSite(Site site) {
        DatabaseUpdateRequests.updateSite(site, co);
    }

    /**
     * Met à jour le mot de passe du site 'site' dans la base de données
     *
     * @param site         Site dans lequel mettre le mot de passe à jour
     * @param passwordHash Nouveau mot de passe hasher
     */
    public void updateSitePassword(Site site, String passwordHash) {
        DatabaseUpdateRequests.updateSitePassword(site, passwordHash, co);
    }

    /**
     * Met à jour l'exploitant 'exp' dans la base de données
     *
     * @param exp Exploitant mis à jour dans le programme et devant etre mis à jour dans la base de données
     */
    public void updateExploitant(Exploitant exp) {
        DatabaseUpdateRequests.updateExploitant(exp, co);
    }

    /**
     * Met à jour le contact 'contact' dans la base de données
     *
     * @param contact Contact mis à jour dans le programme et devant etre mis à jour dans la base de données
     */
    public void updateContact(Contact contact) {
        DatabaseUpdateRequests.updateContact(contact, co);
    }

    /**
     * Met à jour le gaz 'gaz' dans la base de données
     *
     * @param gaz Gaz mis à jour dans le programme et devant etre mis à jour dans la base de données
     */
    public void updateGaz(Gaz gaz) {
        DatabaseUpdateRequests.updateGaz(gaz, co);
    }

    /**
     * Met à jour la cavité 'cavity' dans la base de données
     *
     * @param cavity Cavité mise à jour dans le programme et devant etre mis à jour dans la base de données
     */
    public void updateCavity(Cavity cavity) {
        DatabaseUpdateRequests.updateCavity(cavity, co);
    }

    /**
     * Met à jour le calage 'calage' dans la base de données
     *
     * @param calage Calage mis à jour dans le programme et devant etre mis à jour dans la base de données
     */
    public void updateCalageNarma(Calage calage) {
        DatabaseUpdateRequests.updateCalageNarma(calage, co);
    }

    /**
     * Change le status de verrouillage du calage
     *
     * @param calage       Calage dans lequel mettre à jour le statut de verrouillage
     * @param lockedStatus Nouveau status de verrouillage
     */
    public void updateCalageNarmaStatus(Calage calage, boolean lockedStatus) {
        DatabaseUpdateRequests.updateCalageNarmaLockedStatus(calage, lockedStatus, co);
    }

    /////////////////////////////////////
    //######## Delete Requests ########//
    /////////////////////////////////////

    /**
     * Supprime le site "site" de la base de données ainsi que toutes les cavités en dépendant
     *
     * @param site Site à supprimer
     */
    public void deleteSite(Site site) {
        DatabaseDeleteRequests.deleteSite(site, co);
    }

    /**
     * Supprime l'exploitant 'exp' de la base de données, ainsi que tous les sites en dépendant
     *
     * @param exp Exploitant à supprimer
     */
    public void deleteExploitant(Exploitant exp) {
        DatabaseDeleteRequests.deleteExploitant(exp, co);
    }

    /**
     * Supprime le contact de la base de données ainsi que les lienContactSite en dépendant
     *
     * @param contact Contact à supprimer
     */
    public void deleteContact(Contact contact) {
        DatabaseDeleteRequests.deleteContact(contact, co);
    }

    /**
     * Supprime le gaz 'gaz' de la base de données
     *
     * @param gaz Gaz à supprimer
     */
    public void deleteGaz(Gaz gaz) {
        DatabaseDeleteRequests.deleteGaz(gaz, co);
    }

    /**
     * @param compositionGaz Composition gaz à supprimer
     */
    public void deleteCompoGaz(CompositionGaz compositionGaz) {
        DatabaseDeleteRequests.deleteCompoGaz(compositionGaz, co);
    }

    /**
     * Supprime la cavité "cavity" de la base de données, ainsi que toutes les données et calages en dépendant
     *
     * @param cavity Cavité à supprimer
     */
    public void deleteCavity(Cavity cavity) {
        DatabaseDeleteRequests.deleteCavity(cavity, co);
    }

    /**
     * Supprime une donnée réel de la base de donnée
     *
     * @param realData Donnée réel à supprimer
     */
    public void deleteRealData(RealData realData) {
        DatabaseDeleteRequests.deleteRealData(realData, co);
    }

    /**
     * Supprimer le calage ayant pour id "calageid"
     *
     * @param calageId ID du calage à supprimer
     */
    public void deleteCalageNarma(int calageId) {
        DatabaseDeleteRequests.deleteCalageNarma(calageId, co);
    }

    /**
     * Supprime le lien entre un contact et un site
     *
     * @param contact Contact auquel supprimer le lien
     * @param site    Site auquel supprimer le lien
     */
    public void deleteLienContactSite(Contact contact, Site site) {
        DatabaseDeleteRequests.deleteLienContactSite(contact, site, co);
    }

    /////////////////////////////////////////
    ////######## Select Requests ########////
    /////////////////////////////////////////

    /**
     * @return La liste des sites de la base de données
     */
    public List<Site> getListSites() {
        try (Statement statement = co.createStatement()) {
            String request = "select * from site order by nom;";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<Site> siteList = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    String adress = rs.getString("adresse");
                    String country = rs.getString("pays");
                    String town = rs.getString("ville");
                    String password = rs.getString("password");
                    LocalDate date = LocalDate.parse(rs.getString("date_creation"));
                    String imageLocationClient = rs.getString("imageLocationClient");
                    String imageLocationServer = rs.getString("imageLocationServer");
                    int idExp = rs.getInt("exploitant");
                    Optional<Exploitant> optExp = getExploitantByID(idExp);
                    if (!optExp.isPresent()) {
                        continue;
                    }
                    Exploitant exp = optExp.get();

                    boolean isImageDefault = imageLocationClient.equals("/asset/uploads/imageDefaultSite.jpg");
                    Site site = SiteFactory.createSite(name, adress, country, town, password, date, imageLocationClient, imageLocationServer, this, exp, isImageDefault);
                    site.setId(id);
                    siteList.add(site);
                }
                if (siteList.isEmpty()) {
                    return Collections.emptyList();
                }
                return siteList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * @return La liste des exploitants de la base de données
     */
    public List<Exploitant> getListExploitant() {
        try (Statement statement = co.createStatement()) {
            String request = "select * from exploitant order by nom;";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<Exploitant> exploitantList = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    String adress = rs.getString("adresse");
                    String town = rs.getString("ville");
                    String country = rs.getString("pays");
                    String email = rs.getString("email");
                    String imageLocationClient = rs.getString("imageLocationClient");
                    String imageLocationServer = rs.getString("imageLocationServer");
                    boolean isImageDefault = imageLocationClient.equals("/asset/uploads/imageDefaultExploitant.jpg");
                    Exploitant exp = ExploitantFactory.createExploitant(name, adress, country, town, email, imageLocationClient, imageLocationServer, this, isImageDefault);
                    exp.setId(id);
                    exploitantList.add(exp);
                }
                if (exploitantList.isEmpty()) {
                    return Collections.emptyList();
                }
                return exploitantList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * @return La liste des gaz de la base de données
     */
    public List<Gaz> getListGaz() {
        try (Statement statement = co.createStatement()) {
            String request = "select * from gaz order by nom;";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<Gaz> gazList = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    String formule = rs.getString("formule");
                    double masseMolaire = rs.getDouble("masse_molaire");
                    double criticalTemperature = rs.getDouble("temperature_critique");
                    double criticalPression = rs.getDouble("pression_critique");
                    double densite = rs.getDouble("densite");
                    Gaz gaz = GazFactory.createGaz(name, formule, masseMolaire, criticalTemperature, criticalPression, densite);
                    gaz.setId(id);
                    gazList.add(gaz);
                }
                if (gazList.isEmpty()) {
                    return Collections.emptyList();
                }
                return gazList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * @param site Site dans lequel recuperer la composition du gaz
     * @return La liste des elements du site avec leur proportions
     */
    public List<CompositionGaz> getListCompoGazForSite(Site site) {
        try (Statement statement = co.createStatement()) {
            int idSite = site.getId();
            String request = "select * from compositionGaz where site = " + idSite + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<CompositionGaz> compositionGazList = new ArrayList<>();
                while (rs.next()) {
                    int idGaz = rs.getInt("element");
                    double taux = rs.getDouble("taux");

                    Optional<Gaz> optGaz = getGazByID(idGaz);
                    // Ne devrait jamais arriver
                    if (!optGaz.isPresent()) {
                        continue;
                    }

                    Gaz gaz = optGaz.get();
                    CompositionGaz compositionGaz = GazFactory.createCompositionGaz(site, gaz, taux);
                    compositionGaz.setId(getCompositionGazIDBySiteIDAndGazID(idSite, idGaz));
                    compositionGazList.add(compositionGaz);
                }
                if (compositionGazList.isEmpty()) {
                    return Collections.emptyList();
                }
                return compositionGazList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * @param exploitant Exploitant ou l'on veut
     * @return La liste des sites lié à un exploitant
     */
    public List<Site> getListSiteForExp(Exploitant exploitant) {
        try (Statement statement = co.createStatement()) {
            int idExp = exploitant.getId();
            String request = "select * from site where exploitant = " + idExp + " order by nom;";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<Site> siteList = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    String adress = rs.getString("adresse");
                    String country = rs.getString("pays");
                    String town = rs.getString("ville");
                    String password = rs.getString("password");
                    LocalDate date = LocalDate.parse(rs.getString("date_creation"));
                    String imageLocationClient = rs.getString("imageLocationClient");
                    String imageLocationServer = rs.getString("imageLocationServer");

                    boolean isImageDefault = imageLocationClient.equals("/asset/uploads/imageDefaultSite.jpg");
                    Site site = SiteFactory.createSite(name, adress, country, town, password, date, imageLocationClient, imageLocationServer, this, exploitant, isImageDefault);
                    site.setId(id);
                    exploitant.addSite(site);
                    siteList.add(site);
                }
                if (siteList.isEmpty()) {
                    return Collections.emptyList();
                }
                return siteList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * @param site Site dans lequel recuperer la liste des cavités
     * @return La liste des cavités lié à un site
     */
    public List<Cavity> getListCavityForSite(Site site) {
        try (Statement statement = co.createStatement()) {
            int idSite = site.getId();
            String request = "select * from cavity where site = " + idSite + " order by nom;";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<Cavity> cavityList = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    double heigthTubage = rs.getDouble("hauteur");
                    double diametreTubage = rs.getDouble("diametre");
                    double depth = rs.getDouble("profondeur");
                    double freeVolume = rs.getDouble("volume");
                    LocalDate dateCreation = LocalDate.parse(rs.getString("date_creation"));
                    Cavity cav = CavityFactory.createCavity(site, name, heigthTubage, diametreTubage, depth, freeVolume, dateCreation, this);
                    cav.setId(id);
                    cavityList.add(cav);
                }

                if (cavityList.isEmpty()) {
                    return Collections.emptyList();
                }
                return cavityList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    //###########//
    // Real Data//
    //##########//

    /**
     * @param cavity Cavité dans laquel recuperer les données reel
     * @return La liste des RealData lié à une cavité
     */
    public List<RealData> getRealDataForCavity(Cavity cavity) {
        try (Statement statement = co.createStatement()) {
            int cavityId = cavity.getId();
            String request = "select * from realData where idcavity = " + cavityId + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<RealData> realDataList = new ArrayList<>();
                while (rs.next()) {
                    LocalDate dateInsertion = LocalDate.parse(rs.getString("date"));
                    double twelHead = rs.getDouble("twelHead");
                    double pwelHead = rs.getDouble("pwelHead");
                    double volume = rs.getDouble("volume");
                    double debit = rs.getDouble("debit");
                    LocalDate dateLastModif = LocalDate.parse(rs.getString("last_modif"));
                    String logModif = rs.getString("log_modif");
                    RealData realData = DataFactory.createRealData(dateInsertion, cavity, twelHead, pwelHead, volume, debit, dateLastModif, logModif);
                    realDataList.add(realData);
                }
                if (realDataList.isEmpty()) {
                    return Collections.emptyList();
                }
                return realDataList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * @param cavity Cavité dans laquel recuperer les données reel
     * @param at     Date a laquel recupérer l'information
     * @return Le RealData lié à une cavité à la date "at", Optional.empty() sinon
     */
    public Optional<RealData> getRealDataForCavityAt(Cavity cavity, LocalDate at) {
        try (Statement statement = co.createStatement()) {
            int cavityId = cavity.getId();
            String request = "select * from realData where idcavity = " + cavityId + " AND date = \"" + at + "\";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    LocalDate dateInsertion = LocalDate.parse(rs.getString("date"));
                    double twelHead = rs.getDouble("twelHead");
                    double pwelHead = rs.getDouble("pwelHead");
                    double volume = rs.getDouble("volume");
                    double debit = rs.getDouble("debit");
                    LocalDate dateLastModif = LocalDate.parse(rs.getString("last_modif"));
                    String logModif = rs.getString("log_modif");
                    RealData realData = DataFactory.createRealData(dateInsertion, cavity, twelHead, pwelHead, volume, debit, dateLastModif, logModif);
                    return Optional.of(realData);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
    }

    /**
     * @param cavity Cavité dans laquel recuperer les données reel
     * @param from   Date de debut de la selection
     * @param to     Date de fin de la selection
     * @return La liste des RealData lié à une cavité
     */
    public List<RealData> getRealDataForCavityFromTo(Cavity cavity, LocalDate from, LocalDate to) {
        try (Statement statement = co.createStatement()) {
            int cavityId = cavity.getId();
            String request = "select * from realData where idcavity = " + cavityId + " AND date BETWEEN \'" + from + "\' AND \'" + to + "\';";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<RealData> realDataList = new ArrayList<>();
                while (rs.next()) {
                    LocalDate dateInsertion = LocalDate.parse(rs.getString("date"));
                    double twelHead = rs.getDouble("twelHead");
                    double pwelHead = rs.getDouble("pwelHead");
                    double volume = rs.getDouble("volume");
                    double debit = rs.getDouble("debit");
                    LocalDate dateLastModif = LocalDate.parse(rs.getString("last_modif"));
                    String logModif = rs.getString("log_modif");
                    RealData realData = DataFactory.createRealData(dateInsertion, cavity, twelHead, pwelHead, volume, debit, dateLastModif, logModif);
                    realDataList.add(realData);
                }
                if (realDataList.isEmpty()) {
                    return Collections.emptyList();
                }
                return realDataList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * @param idCalage Id du calage à recuperer dans la base de donnée
     * @return Le calage trouvé par l'Id, Optional.empty() sinon
     */
    public Optional<Calage> getCalageById(double idCalage) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from calageNarma where id = " + idCalage;
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    int cavityId = rs.getInt("idcavity");
                    LocalDate from = LocalDate.parse(rs.getString("dateFrom"));
                    LocalDate to = LocalDate.parse(rs.getString("dateTo"));
                    double p_3_volumeStock = rs.getDouble("P_3_VolumeStock");
                    double p_2_volumeStock = rs.getDouble("P_2_VolumeStock");
                    double p_1_volumeStock = rs.getDouble("P_1_VolumeStock");
                    double p_0_volumeStock = rs.getDouble("P_0_VolumeStock");

                    double p_3_debit = rs.getDouble("P_3_Debit");
                    double p_2_debit = rs.getDouble("P_2_Debit");
                    double p_1_debit = rs.getDouble("P_1_Debit");
                    double p_0_debit = rs.getDouble("P_0_Debit");

                    double p_3_PSimul = rs.getDouble("P_3_Psimul");
                    double p_2_PSimul = rs.getDouble("P_2_Psimul");
                    double p_1_PSimul = rs.getDouble("P_1_Psimul");
                    double p_0_PSimul = rs.getDouble("P_0_Psimul");

                    double p_2_dp_arma = rs.getDouble("P_2_DP_Arma");
                    double p_1_dp_arma = rs.getDouble("P_1_DP_Arma");
                    double p_0_dp_arma = rs.getDouble("P_0_DP_Arma");

                    double t_3_pArma = rs.getDouble("T_3_PArma");
                    double t_2_pArma = rs.getDouble("T_2_PArma");
                    double t_1_pArma = rs.getDouble("T_1_PArma");
                    double t_0_pArma = rs.getDouble("T_0_PArma");

                    double t_3_volumeStock = rs.getDouble("T_3_VolumeStock");
                    double t_2_volumeStock = rs.getDouble("T_2_VolumeStock");
                    double t_1_volumeStock = rs.getDouble("T_1_VolumeStock");
                    double t_0_volumeStock = rs.getDouble("T_0_VolumeStock");

                    double t_3_debit = rs.getDouble("T_3_Debit");
                    double t_2_debit = rs.getDouble("T_2_Debit");
                    double t_1_debit = rs.getDouble("T_1_Debit");
                    double t_0_debit = rs.getDouble("T_0_Debit");

                    double t_3_Tsimul = rs.getDouble("T_3_Tsimul");
                    double t_2_Tsimul = rs.getDouble("T_2_Tsimul");
                    double t_1_Tsimul = rs.getDouble("T_1_Tsimul");
                    double t_0_Tsimul = rs.getDouble("T_0_Tsimul");

                    double t_2_dpArma = rs.getDouble("T_2_DP_Arma");

                    String isLocked = rs.getString("isLocked");
                    Optional<Cavity> optCavity = getCavityById(cavityId);
                    if (!optCavity.isPresent()) {
                        return Optional.empty();
                    }
                    Cavity cavity = optCavity.get();

                    Calage calage = CalageFactory.createCalage(from, to, cavity, this,
                            p_3_volumeStock, p_3_debit, p_3_PSimul,
                            p_2_volumeStock, p_2_debit, p_2_PSimul, p_2_dp_arma,
                            p_1_volumeStock, p_1_debit, p_1_PSimul, p_1_dp_arma,
                            p_0_volumeStock, p_0_debit, p_0_PSimul, p_0_dp_arma,
                            t_3_pArma, t_3_volumeStock, t_3_debit, t_3_Tsimul,
                            t_2_pArma, t_2_volumeStock, t_2_debit, t_2_Tsimul, t_2_dpArma,
                            t_1_pArma, t_1_volumeStock, t_1_debit, t_1_Tsimul,
                            t_0_pArma, t_0_volumeStock, t_0_debit, t_0_Tsimul);
                    calage.setId(id);
                    if (isLocked.equals("TRUE")) {
                        calage.lock();
                        // default calage is unlocked
                    }
                    return Optional.of(calage);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
    }

    /**
     * @param cavityId Id de la cavité à recuperer dans la base de donnée
     * @return Le cavité trouvé par l'Id, Optional.empty() sinon
     */
    private Optional<Cavity> getCavityById(int cavityId) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from cavity where id = " + cavityId;
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    double heigthTubage = rs.getDouble("hauteur");
                    double diametreTubage = rs.getDouble("diametre");
                    double depth = rs.getDouble("profondeur");
                    double freeVolume = rs.getDouble("volume");
                    LocalDate dateCreation = LocalDate.parse(rs.getString("date_creation"));
                    int siteId = rs.getInt("site");

                    Optional<Site> optSite = getSiteByID(siteId);
                    if (!optSite.isPresent()) {
                        return Optional.empty();
                    }
                    Site site = optSite.get();
                    Cavity cavity = CavityFactory.createCavity(site, name, heigthTubage, diametreTubage, depth, freeVolume, dateCreation, this);
                    cavity.setId(id);
                    return Optional.of(cavity);
                }
            }
            return Optional.empty();
        } catch (SQLException e) {
            return Optional.empty();
        }
    }

    /**
     * @param siteName Le nom du site à chercher dans la base de donnée
     * @return Le site avec le nom "siteName", Optional.empty() sinon
     */
    public Optional<Site> getSiteByName(String siteName) {
        int idSite = getSiteIdByName(siteName);
        return getSiteByID(idSite);
    }

    /**
     * @param exp Exploitant ou recuperer les contacts
     * @return La liste de contacts lié à l'exploitant
     */
    public List<Contact> getlistContactForExp(Exploitant exp) {
        try (Statement statement = co.createStatement()) {
            String request = " select * from contact where exploitant = " + exp.getId() + " order by nom;";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<Contact> contactList = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String lastName = rs.getString("nom");
                    String name = rs.getString("prenom");
                    String fonction = rs.getString("fonction");
                    String service = rs.getString("service");
                    String mail = rs.getString("mail");
                    String phone = rs.getString("telephone");
                    String imageLocationClient = rs.getString("imageLocationClient");
                    String imageLocationServer = rs.getString("imageLocationServer");

                    // Recuperer le site grace LienContactSite s'il existe et faire l'affectation dans ce cas.
                    int siteId = getSiteIdFromLienContact(id);
                    Site site = null;
                    if (siteId > 0) {
                        Optional<Site> optSite = getSiteByID(siteId);
                        // Ne devrait jamais arriver
                        // Si arrive -> problème dans la BDD
                        if (!optSite.isPresent()) {
                            continue;
                        }
                        site = optSite.get();
                    }

                    boolean isImageDefault = imageLocationClient.equals("/asset/uploads/imageDefaultContact.jpg");
                    Contact contact = ContactFactory.createContact(lastName, name, exp, fonction, service, mail, phone, imageLocationClient, imageLocationServer, isImageDefault, this);
                    contact.setId(id);
                    if (siteId > 0) {
                        exp.affectContactToSite(contact, site);
                    }
                    contactList.add(contact);
                }
                if (contactList.isEmpty()) {
                    return Collections.emptyList();
                }
                return contactList;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * @param cavity Cavité ou récuperer les calages
     * @return La liste des calages lié à la cavité "cavity"
     */
    public List<Calage> getListCalageForCavity(Cavity cavity) {
        try (Statement statement = co.createStatement()) {
            int cavityId = cavity.getId();
            String request = "select * from calageNarma where idcavity = " + cavityId;
            try (ResultSet rs = statement.executeQuery(request)) {
                List<Calage> calageList = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt("id");

                    LocalDate from = LocalDate.parse(rs.getString("dateFrom"));
                    LocalDate to = LocalDate.parse(rs.getString("dateTo"));
                    double p_3_volumeStock = rs.getDouble("P_3_VolumeStock");
                    double p_2_volumeStock = rs.getDouble("P_2_VolumeStock");
                    double p_1_volumeStock = rs.getDouble("P_1_VolumeStock");
                    double p_0_volumeStock = rs.getDouble("P_0_VolumeStock");

                    double p_3_debit = rs.getDouble("P_3_Debit");
                    double p_2_debit = rs.getDouble("P_2_Debit");
                    double p_1_debit = rs.getDouble("P_1_Debit");
                    double p_0_debit = rs.getDouble("P_0_Debit");

                    double p_3_PSimul = rs.getDouble("P_3_Psimul");
                    double p_2_PSimul = rs.getDouble("P_2_Psimul");
                    double p_1_PSimul = rs.getDouble("P_1_Psimul");
                    double p_0_PSimul = rs.getDouble("P_0_Psimul");

                    double p_2_dp_arma = rs.getDouble("P_2_DP_Arma");
                    double p_1_dp_arma = rs.getDouble("P_1_DP_Arma");
                    double p_0_dp_arma = rs.getDouble("P_0_DP_Arma");

                    double t_3_pArma = rs.getDouble("T_3_PArma");
                    double t_2_pArma = rs.getDouble("T_2_PArma");
                    double t_1_pArma = rs.getDouble("T_1_PArma");
                    double t_0_pArma = rs.getDouble("T_0_PArma");

                    double t_3_volumeStock = rs.getDouble("T_3_VolumeStock");
                    double t_2_volumeStock = rs.getDouble("T_2_VolumeStock");
                    double t_1_volumeStock = rs.getDouble("T_1_VolumeStock");
                    double t_0_volumeStock = rs.getDouble("T_0_VolumeStock");

                    double t_3_debit = rs.getDouble("T_3_Debit");
                    double t_2_debit = rs.getDouble("T_2_Debit");
                    double t_1_debit = rs.getDouble("T_1_Debit");
                    double t_0_debit = rs.getDouble("T_0_Debit");

                    double t_3_Tsimul = rs.getDouble("T_3_Tsimul");
                    double t_2_Tsimul = rs.getDouble("T_2_Tsimul");
                    double t_1_Tsimul = rs.getDouble("T_1_Tsimul");
                    double t_0_Tsimul = rs.getDouble("T_0_Tsimul");

                    double t_2_dpArma = rs.getDouble("T_2_DP_Arma");

                    String isLocked = rs.getString("isLocked");

                    Calage calage = CalageFactory.createCalage(from, to, cavity, this,
                            p_3_volumeStock, p_3_debit, p_3_PSimul,
                            p_2_volumeStock, p_2_debit, p_2_PSimul, p_2_dp_arma,
                            p_1_volumeStock, p_1_debit, p_1_PSimul, p_1_dp_arma,
                            p_0_volumeStock, p_0_debit, p_0_PSimul, p_0_dp_arma,
                            t_3_pArma, t_3_volumeStock, t_3_debit, t_3_Tsimul,
                            t_2_pArma, t_2_volumeStock, t_2_debit, t_2_Tsimul, t_2_dpArma,
                            t_1_pArma, t_1_volumeStock, t_1_debit, t_1_Tsimul,
                            t_0_pArma, t_0_volumeStock, t_0_debit, t_0_Tsimul);
                    calage.setId(id);
                    if (isLocked.equals("TRUE")) {
                        calage.lock();
                        // default calage is unlocked
                    }
                    calageList.add(calage);
                }
                if (calageList.isEmpty()) {
                    return Collections.emptyList();
                }
                return calageList;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * @param id ID du calage a regarder
     * @return True si un calage avec l'ID "id" existe , false sinon
     */
    public boolean calageExistForId(int id) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from calageNarma where id = " + id;
            try (ResultSet rs = statement.executeQuery(request)) {
                return rs.next();
            }
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * @param idGaz ID du gaz à recuperer
     * @return Le gaz trouvé à l'id, Optional.empty() sinon
     */
    private Optional<Gaz> getGazByID(int idGaz) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from gaz where id = " + idGaz + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    String name = rs.getString("nom");
                    String formule = rs.getString("formule");
                    double masseMolaire = rs.getDouble("masse_molaire");
                    double criticalTemperature = rs.getDouble("temperature_critique");
                    double criticalPression = rs.getDouble("pression_critique");
                    double densite = rs.getDouble("densite");
                    Gaz gaz = GazFactory.createGaz(name, formule, masseMolaire, criticalTemperature, criticalPression, densite);
                    gaz.setId(idGaz);
                    return Optional.of(gaz);
                }
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
        return Optional.empty();
    }

    /**
     * @param idSite ID du site à retrouver dans la base de données
     * @return Le site trouvé à l'id, Optional.empty() sinon
     */
    private Optional<Site> getSiteByID(int idSite) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from site where id = " + idSite + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    String adress = rs.getString("adresse");
                    String country = rs.getString("pays");
                    String town = rs.getString("ville");
                    String password = rs.getString("password");
                    LocalDate date = LocalDate.parse(rs.getString("date_creation"));
                    String imageLocationClient = rs.getString("imageLocationClient");
                    String imageLocationServer = rs.getString("imageLocationServer");
                    int idExp = rs.getInt("exploitant");
                    Optional<Exploitant> optExp = getExploitantByID(idExp);
                    if (!optExp.isPresent()) {
                        return Optional.empty();
                    }
                    Exploitant exploitant = optExp.get();

                    boolean isImageDefault = imageLocationClient.equals("/asset/uploads/imageDefaultSite.jpg");
                    Site site = SiteFactory.createSite(name, adress, country, town, password, date, imageLocationClient, imageLocationServer, this, exploitant, isImageDefault);
                    site.setId(id);
                    List<CompositionGaz> compositionGazList = this.getListCompoGazForSite(site);
                    compositionGazList.forEach(site::addCompositionGaz);

                    return Optional.of(site);
                }
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
        return Optional.empty();
    }

    /**
     * @param idExp ID de l'exploitant à récupérer
     * @return L'exploitant, Optional.empty() sinon
     */
    private Optional<Exploitant> getExploitantByID(int idExp) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from exploitant where id = " + idExp + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    String adress = rs.getString("adresse");
                    String town = rs.getString("ville");
                    String country = rs.getString("pays");
                    String email = rs.getString("email");
                    String imageLocationClient = rs.getString("imageLocationClient");
                    String imageLocationServer = rs.getString("imageLocationServer");
                    boolean isImageDefault = imageLocationClient.equals("/asset/uploads/imageDefaultExploitant.jpg");
                    Exploitant exp = ExploitantFactory.createExploitant(name, adress, country, town, email, imageLocationClient, imageLocationServer, this, isImageDefault);
                    exp.setId(id);
                    return Optional.of(exp);
                }
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
        return Optional.empty();
    }

    /**
     * @param nomExploitant Nom de l'exploitant à chercher dans la base de données
     * @return L'exploitant ayant pour nom "nomExploitant", Optional.empty() sinon
     */
    public Optional<Exploitant> getExploitantByName(String nomExploitant) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from exploitant where nom like \"" + nomExploitant + "\";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    String adress = rs.getString("adresse");
                    String town = rs.getString("ville");
                    String country = rs.getString("pays");
                    String email = rs.getString("email");
                    String imageLocationClient = rs.getString("imageLocationClient");
                    String imageLocationServer = rs.getString("imageLocationServer");
                    boolean isImageDefault = imageLocationClient.equals("/asset/uploads/imageDefaultExploitant.jpg");
                    Exploitant exp = ExploitantFactory.createExploitant(name, adress, country, town, email, imageLocationClient, imageLocationServer, this, isImageDefault);
                    exp.setId(id);
                    return Optional.of(exp);
                }
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
        return Optional.empty();
    }

    /**
     * @param siteName Nom du site ou il faut retrouver le nom de l'exploitant
     * @return Le nom de l'exploitant par rapport au nom du site
     */
    public Optional<String> getExploitantNameForSiteName(String siteName) {
        try (Statement statement = co.createStatement()) {
            String request = "select nom from exploitant where id = " + getExploitantIdBySiteName(siteName);
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    String name = rs.getString("nom");
                    return Optional.of(name);
                }
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
        return Optional.empty();
    }

    /**
     * @param gazName Le nom du gaz à récupérer
     * @return Le gaz ayant le nom "gazName", Optional.empty() sinon
     */
    public Optional<Gaz> getGazByName(String gazName) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from gaz where nom like \'" + gazName + "\';";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("nom");
                    String formule = rs.getString("formule");
                    double masseMolaire = rs.getDouble("masse_molaire");
                    double criticalTemperature = rs.getDouble("temperature_critique");
                    double criticalPression = rs.getDouble("pression_critique");
                    double densite = rs.getDouble("densite");
                    Gaz gaz = GazFactory.createGaz(name, formule, masseMolaire, criticalTemperature, criticalPression, densite);
                    gaz.setId(id);
                    return Optional.of(gaz);
                }
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
        return Optional.empty();
    }

    /**
     * @param cavityName Nom de la cavité à trouver dans le site
     * @param site       Site dans lequel se trouve la cavité
     * @return La cavité trouvé, Optional.empty() sinon
     */
    public Optional<Cavity> getCavityByNameAndSite(String cavityName, Site site) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from cavity where nom like \'" + cavityName + "\' AND site = " + site.getId() + " ;";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    double height = rs.getDouble("hauteur");
                    double diameter = rs.getDouble("diametre");
                    double depth = rs.getDouble("profondeur");
                    double volume = rs.getDouble("volume");
                    LocalDate dateCreation = LocalDate.parse(rs.getString("date_creation"));
                    Cavity cavity = CavityFactory.createCavity(site, cavityName, height, diameter, depth, volume, dateCreation, this);
                    cavity.setId(id);
                    return Optional.of(cavity);
                }
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
        return Optional.empty();

    }

    /**
     * @param siteName Nom du site pour lequel retrouver l'ID de l'exploitant
     * @return L'id de l'exploitant associé au site ayant pour nom "siteName"
     */
    private int getExploitantIdBySiteName(String siteName) {
        try (Statement statement = co.createStatement()) {
            String request = " select exploitant from site where nom like \'" + siteName + "\';";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("exploitant");
                }
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * @param siteName Nom du site pour lequel on veut l'ID
     * @return l'id du site si on le trouve, -1 sinon
     */
    private int getSiteIdByName(String siteName) {
        try (Statement statement = co.createStatement()) {
            String request = " select id from site where nom like \'" + siteName + "\';";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * @param siteid Id du site dans lequel chercher
     * @param gazid  Id du gaz ou l'on veut retrouver la proportion
     * @return l'id de la compositionGaz si on le trouve, -1 sinon
     */
    private int getCompositionGazIDBySiteIDAndGazID(int siteid, int gazid) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from compositionGaz where site = " + siteid + " AND element = " + gazid + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    rs.close();
                    return rs.getInt("id");
                }
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * @param gaz Gaz à verifier
     * @return True si le gaz est utilisé par un site, false sinon
     */
    public boolean isGazUsed(Gaz gaz) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from compositionGaz where element = " + gaz.getId();
            try (ResultSet rs = statement.executeQuery(request)) {
                return rs.next();
            }
        } catch (SQLException e) {
            return true;
        }
    }

    /**
     * @param id ID du contact à chercher
     * @return Le contact, Optional.empty() sinon
     */
    public Optional<Contact> getContactById(int id) {
        try (Statement statement = co.createStatement()) {
            String request = "select * from contact where id = " + id + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    String lastName = rs.getString("nom");
                    String name = rs.getString("prenom");
                    String fonction = rs.getString("fonction");
                    String service = rs.getString("service");
                    String email = rs.getString("mail");
                    String phone = rs.getString("telephone");
                    String imageLocationClient = rs.getString("imageLocationClient");
                    String imageLocationServer = rs.getString("imageLocationServer");
                    int idExp = rs.getInt("exploitant");

                    // Recuperer le site grace LienContactSite s'il existe et faire l'affectation dans ce cas.
                    int siteId = getSiteIdFromLienContact(id);
                    Site site = null;
                    if (siteId != -1) {
                        Optional<Site> optSite = getSiteByID(siteId);
                        if (!optSite.isPresent()) {
                            return Optional.empty();
                        }
                        site = optSite.get();
                    }
                    Optional<Exploitant> optExp = getExploitantByID(idExp);
                    if (!optExp.isPresent()) {
                        return Optional.empty();
                    }
                    Exploitant exp = optExp.get();
                    boolean isImageDefault = imageLocationClient.equals("/asset/uploads/imageDefaultContact.jpg");
                    Contact contact = ContactFactory.createContact(lastName, name, exp, fonction, service, email, phone, imageLocationClient, imageLocationServer, isImageDefault, this);
                    contact.setId(id);
                    if (siteId != -1) {
                        contact.affectToSite(site);
                    }
                    return Optional.of(contact);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            return Optional.empty();
        }
    }

    /**
     * @param id Id du lienContact à trouver
     * @return L'id du site trouvé , -1 sinon
     */
    private int getSiteIdFromLienContact(int id) {
        try (Statement statement = co.createStatement()) {
            String request = "select site from lienContactSite where contact = " + id + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("site");
                }
                return -1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Charge en mémoire les éléments de la base de données
     *
     * @param exploitantList Liste des exploitants
     * @param saspLogger     Logger de l'application
     */
    public void loadAll(List<Exploitant> exploitantList, SaspLogger saspLogger) {
        saspLogger.log(Level.INFO, "Starting loading program");

        saspLogger.log(Level.INFO, "Loading list exploitant");
        exploitantList.addAll(getListExploitant());

        saspLogger.log(Level.INFO, "Loading list contact pour chaque exploitant");

        // Chargement de la liste de contact de chaque exploitant
        exploitantList.forEach(exp -> {

            saspLogger.log(Level.INFO, "Loading list site for exploitant : " + exp.getName());
            List<Site> siteList = new ArrayList<>();
            siteList.addAll(getListSiteForExp(exp));

            siteList.forEach(exp::addSite);

            saspLogger.log(Level.INFO, "Loading list contact for exploitants : " + exp.getName());
            loadContactsForExp(exp);
            saspLogger.log(Level.INFO, "Loading list contact for exploitant : " + exp.getName() + " finished");

            // Chargement des données relatives aux differents sites
            siteList.forEach(site -> {
                saspLogger.log(Level.INFO, "Loading list compoGaz for site : " + site.getName());
                loadCompoGaz(site);
                saspLogger.log(Level.INFO, "Loading list compoGaz for site : " + site.getName() + " finished");
                saspLogger.log(Level.INFO, "Loading list cavity for site : " + site.getName());
                loadCavity(site);
                saspLogger.log(Level.INFO, "Loading list cavity for site : " + site.getName() + " finished");
            });
            saspLogger.log(Level.INFO, "Loading list site for exploitant : " + exp.getName() + " finished");
        });
        saspLogger.log(Level.INFO, "Loading list contact pour chaque exploitant finished");
        saspLogger.log(Level.INFO, "Loading list exploitant finished");
    }

    /**
     * Charge en memoire les contacts de l'exploitant "exp"
     *
     * @param exp Exploitant sur lequel chargé les contacts
     */
    private void loadContactsForExp(Exploitant exp) {
        List<Contact> contactList = getlistContactForExp(exp);
        contactList.forEach(exp::addContact);
    }

    /**
     * Charge en mémoire les cavités pour le site "site"
     *
     * @param site Site où charger les cavités
     */
    private void loadCavity(Site site) {
        List<Cavity> cavityList = getListCavityForSite(site);
        cavityList.forEach(site::addCavity);
        loadDataForCavity(cavityList);
    }

    /**
     * Charge en mémoire les real data pour chacune des cavités de la liste
     *
     * @param cavityList Liste de toutes les cavités d'un site
     */
    private void loadDataForCavity(List<Cavity> cavityList) {
        cavityList.forEach(cavity -> {
            List<RealData> realDataList = getRealDataForCavity(cavity);
            realDataList.forEach(cavity::addRealData);
        });
    }

    /**
     * Charge les composition gaz du site en mémoire
     *
     * @param site Site où charger les composition gaz
     */
    private void loadCompoGaz(Site site) {
        List<CompositionGaz> compositionGazList = getListCompoGazForSite(site);
        compositionGazList.forEach(site::addCompositionGaz);
    }

}
