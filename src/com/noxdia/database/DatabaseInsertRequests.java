package com.noxdia.database;

import com.noxdia.exploitant.Exploitant;
import com.noxdia.exploitant.contact.Contact;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.gaz.CompositionGaz;
import com.noxdia.site.gaz.Gaz;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

/**
 * Classe pour toutes les requetes d'insertion dans la base de données
 */
class DatabaseInsertRequests {


    /**
     * Insert le site "site" dans la base de données
     *
     * @param site Site à insérer dans la base de données
     * @param co   Connection à la base de données
     */
    static void insertSite(Site site, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "insert into site values(NULL," +
                    "\"" + site.getName() + "\"," +
                    "\"" + site.getAdress() + "\"," +
                    "\"" + site.getTown() + "\"," +
                    "\"" + site.getCountry() + "\"," +
                    "\"" + site.getPasswordHash() + "\"," +
                    "\"" + site.getDateCreation() + "\"," +
                    "\"" + site.getExploitant().getId() + "\"," +
                    "\"" + site.getImageSiteClient() + "\"," +
                    "\"" + site.getImageSiteServer() + "\"" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        } finally {
            site.setId(getSiteIDByName(site.getName(), co));
        }
    }

    /**
     * Insert l'exploitant "exploitant" dans la base de données
     *
     * @param exp Exploitant à inserer dans la base de données
     * @param co  Connection à la base de données
     */
    static void insertExploitant(Exploitant exp, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "insert into exploitant values (NULL," +
                    "\"" + exp.getName() + "\"," +
                    "\"" + exp.getAdresse() + "\"," +
                    "\"" + exp.getTown() + "\"," +
                    "\"" + exp.getCountry() + "\"," +
                    "\"" + exp.getLogoLocationClient() + "\"," +
                    "\"" + exp.getLogoLocationServer() + "\"," +
                    "\"" + exp.getEmail() + "\"" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        } finally {
            exp.setId(getExploitantIDByName(exp.getName(), co));
        }
    }

    /**
     * Insert le contact "contact" dans la base de données
     *
     * @param contact Contact à insérer dans la base de données
     * @param co      Connection à la base de données
     */
    static void insertContact(Contact contact, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "insert into contact values(NULL," +
                    "\"" + contact.getExploitant().getId() + "\"," +
                    "\"" + contact.getLastName() + "\"," +
                    "\"" + contact.getName() + "\"," +
                    "\"" + contact.getMail() + "\"," +
                    "\"" + contact.getFonction() + "\"," +
                    "\"" + contact.getService() + "\"," +
                    "\"" + contact.getTelephone() + "\"," +
                    "\"" + contact.getImageLocationClient() + "\"," +
                    "\"" + contact.getImageLocationServer() + "\"" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        } finally {
            contact.setId(getContactIDByNameAndLastName(contact.getLastName(), contact.getName(), co));
        }
    }

    /**
     * Insert le gaz "gaz" dans la base de données
     *
     * @param gaz Gaz à insérer dans la base de données
     * @param co  Connection à la base de données
     */
    static void insertGaz(Gaz gaz, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "insert into gaz values (NULL," +
                    "\"" + gaz.getName() + "\"," +
                    "\"" + gaz.getFormule() + "\"," +
                    "\"" + gaz.getCriticalTemperature() + "\"," +
                    "\"" + gaz.getCriticalPression() + "\"," +
                    "\"" + gaz.getMasseMolaire() + "\"," +
                    "\"" + gaz.getDensite() + "\"" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        } finally {
            gaz.setId(getGazIDByName(gaz.getName(), co));
        }
    }

    /**
     * Insert la composition gaz "compositionGaz" dans la base de données
     *
     * @param compositionGaz Composition gaz à insérer dans la base de données
     * @param co             Connection à la base de données
     */
    static void insertCompoGaz(CompositionGaz compositionGaz, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "insert into compositionGaz values (NULL," +
                    "\"" + compositionGaz.getSite().getId() + "\"," +
                    "\"" + compositionGaz.getGaz().getId() + "\"," +
                    "\"" + compositionGaz.getProportion() + "\"" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        } finally {
            compositionGaz.setId(getCompositionGazIDBySiteIDAndGazID(compositionGaz.getSite().getId(), compositionGaz.getGaz().getId(), co));
        }
    }

    /**
     * Insert la cavité "cavity" dans la base de données
     *
     * @param cavity Cavité à insérer dans la base de données
     * @param co     Connection à la base de données
     */
    static void insertCavity(Cavity cavity, Connection co) {

        String cavityName = cavity.getName();
        int idSite = cavity.getSite().getId();
        try (Statement statement = co.createStatement()) {
            String request = "insert into cavity values (NULL," +
                    "\"" + idSite + "\"," +
                    "\"" + cavityName + "\"," +
                    "\"" + cavity.getHauteurTubage() + "\"," +
                    "\"" + cavity.getDiametreTubage() + "\"," +
                    "\"" + cavity.getDepth() + "\"," +
                    "\"" + cavity.getVolume() + "\"," +
                    "\"" + cavity.getDateCreation() + "\"" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        } finally {
            cavity.setId(getCavityIDByNameAndSiteID(cavityName, idSite, co));
        }
    }

    /**
     * Insert le RealData "realData" dans la base de données
     *
     * @param realData Real data à insérer dans la base de données
     * @param co       Connection à la base de données
     */
    static void insertRealData(RealData realData, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "insert into realData values(NULL," +
                    "\"" + realData.getDateInsertion() + "\"," +
                    "\"" + realData.getCavity().getId() + "\"," +
                    "\"" + realData.getTemperatureWelHead() + "\"," +
                    "\"" + realData.getPressionWelHead() + "\"," +
                    "\"" + realData.getVolume() + "\"," +
                    "\"" + realData.getDebit() + "\"," +
                    "\"" + realData.getDateLastModif() + "\"," +
                    "\"" + realData.getLogLastModif() + "\"" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Insert le calage "calage" dans la base de données
     *
     * @param calage Calage à insérer dans la base de données
     * @param co     Connection à la base de données
     */
    static void insertCalageNarma(Calage calage, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "insert into calageNarma values(" +
                    "NULL," +
                    "\"" + calage.getCavity().getId() + "\"," +
                    "\"" + calage.getFrom() + "\"," +
                    "\"" + calage.getTo() + "\"," +
                    "\"" + calage.isLocked().toString().toUpperCase() + "\"," +

                    "\"" + calage.getP_3_VolumeStock() + "\"," +
                    "\"" + calage.getP_3_Debit() + "\"," +
                    "\"" + calage.getP_3_Psimul() + "\"," +

                    "\"" + calage.getP_2_VolumeStock() + "\"," +
                    "\"" + calage.getP_2_Debit() + "\"," +
                    "\"" + calage.getP_2_Psimul() + "\"," +
                    "\"" + calage.getP_2_DP_Arma() + "\"," +

                    "\"" + calage.getP_1_VolumeStock() + "\"," +
                    "\"" + calage.getP_1_Debit() + "\"," +
                    "\"" + calage.getP_1_Psimul() + "\"," +
                    "\"" + calage.getP_1_DP_Arma() + "\"," +

                    "\"" + calage.getP_0_VolumeStock() + "\"," +
                    "\"" + calage.getP_0_Debit() + "\"," +
                    "\"" + calage.getP_0_Psimul() + "\"," +
                    "\"" + calage.getP_0_DP_Arma() + "\"," +

                    "\"" + calage.getT_3_PArma() + "\"," +
                    "\"" + calage.getT_3_VolumeStock() + "\"," +
                    "\"" + calage.getT_3_Debit() + "\"," +
                    "\"" + calage.getT_3_Tsimul() + "\"," +

                    "\"" + calage.getT_2_PArma() + "\"," +
                    "\"" + calage.getT_2_VolumeStock() + "\"," +
                    "\"" + calage.getT_2_Debit() + "\"," +
                    "\"" + calage.getT_2_Tsimul() + "\"," +
                    "\"" + calage.getT_2_DP_Arma() + "\"," +

                    "\"" + calage.getT_1_PArma() + "\"," +
                    "\"" + calage.getT_1_VolumeStock() + "\"," +
                    "\"" + calage.getT_1_Debit() + "\"," +
                    "\"" + calage.getT_1_Tsimul() + "\"," +

                    "\"" + calage.getT_0_PArma() + "\"," +
                    "\"" + calage.getT_0_VolumeStock() + "\"," +
                    "\"" + calage.getT_0_Debit() + "\"," +
                    "\"" + calage.getT_0_Tsimul() + "\"" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        } finally {
            int id = getCalageIdByDateFromToAndCavity(calage.getFrom(), calage.getTo(), calage.getCavity().getId(), co);
            calage.setId(id);
        }
    }

    /**
     * Insert le lien entre on contact "contact" et un site "site" dans la base de données
     *
     * @param contact Contact avec lequel etablir le lien
     * @param site    Site avec lequel etablir le lien
     * @param co      Connection à la base de données
     */
    static void insertLienContactSite(Contact contact, Site site, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "insert into lienContactSite values(NULL," +
                    contact.getId() + "," +
                    site.getId() +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Retrouve l'id d'un calage en fonction de differents paramètres
     *
     * @param from     Date de debut du calage
     * @param to       Date de fin du calage
     * @param idcavity Id de la cavité ou est situé le calage
     * @param co       Connection à la base de données
     * @return Id du calage trouvé, -1 sinon
     */
    private static int getCalageIdByDateFromToAndCavity(LocalDate from, LocalDate to, int idcavity, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from calageNarma " +
                    "where dateFrom like \"" + from + "\" AND dateTo like \"" + to +
                    "\" AND idcavity = " + idcavity + " ;";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * Trouve l'ID d'un site par son nom, chaque nom de site est unique.
     *
     * @param name Nom du site
     * @param co   Connection à la base de données
     * @return ID du site si trouvé, -1 sinon
     */
    private static int getSiteIDByName(String name, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from site where nom like \"" + name + "\";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * Trouve l'ID d'un gaz par son nom, chaque nom étant unique
     *
     * @param name Nom du gaz à trouver
     * @param co   Connection à la base de données
     * @return ID du gaz si trouvé, -1 sinon
     */
    private static int getGazIDByName(String name, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from gaz where nom like \"" + name + "\";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * Trouve l'ID de l'exploitant par son nom
     *
     * @param name Nom de l'exploitant
     * @param co   Connection à la base de données
     * @return ID de l'exploitant si trouvé, -1 sinon
     */
    private static int getExploitantIDByName(String name, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from exploitant where nom like \"" + name + "\";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * Trouve l'ID de la cavité avec le nom de la cavité de l'ID du site associé
     *
     * @param name   Nom de la cavité
     * @param idSite ID du site ou est la cavité
     * @param co     Connection à la base de données
     * @return ID de la cavité si trouvé, -1 sinon
     */
    private static int getCavityIDByNameAndSiteID(String name, int idSite, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from cavity where nom like \"" + name + "\" AND site = " + idSite + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * Trouve l'ID de la compositionGaz avec l'ID du site et du l'ID du gaz utilisé
     *
     * @param siteid Id du site
     * @param gazid  Id du gaz
     * @param co     Connection à la base de données
     * @return Id de composition gaz si trouvé, -1 sinon
     */
    private static int getCompositionGazIDBySiteIDAndGazID(int siteid, int gazid, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from compositionGaz where site = " + siteid + " AND element = " + gazid + ";";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                statement.closeOnCompletion();
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }

    /**
     * Trouve l'ID d'un contact avec son nom / prenom, les couples de nom/ prenom etant unique
     *
     * @param lastName Nom de famille du contact
     * @param name     Prenom du contact
     * @param co       Connection à la base de données
     * @return ID du contact si trouvé, -1 sinon
     */
    private static int getContactIDByNameAndLastName(String lastName, String name, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from contact where prenom like \"" + name + "\" AND nom like \"" + lastName + "\"";
            try (ResultSet rs = statement.executeQuery(request)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                statement.closeOnCompletion();
                return -1;
            }
        } catch (SQLException e) {
            return -1;
        }
    }


}
