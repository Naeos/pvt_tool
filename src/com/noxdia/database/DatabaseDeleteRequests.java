package com.noxdia.database;

import com.noxdia.exploitant.Exploitant;
import com.noxdia.exploitant.contact.Contact;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.gaz.CompositionGaz;
import com.noxdia.site.gaz.Gaz;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contient toutes les requetes pour la suppression d'element des tables
 * <p>
 * /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\/!\ /!\
 * /!\ Le "on cascade" ne fonctionnant pas dans base de données, il faut faire les requetes à la main /!\
 * /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\/!\ /!\
 */
class DatabaseDeleteRequests {

    /**
     * Supprime le site "site" de la base de données
     *
     * @param site Site à supprimer de la base de données
     * @param co   Connection à la base de données
     */
    static void deleteSite(Site site, Connection co) {
        try (Statement statement = co.createStatement()) {
            int siteId = site.getId();
            String request = "delete from site where id = " + siteId;
            deleteCompoGazAssociatedWithSite(siteId, co);
            deleteCavityAssociatedWithSite(siteId, co);
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Supprime les cavités associé au siteId donné en paramètre
     *
     * @param siteId Id du site ou les cavités vont être supprimer
     * @param co     Connection à la base de données
     */
    private static void deleteCavityAssociatedWithSite(int siteId, Connection co) {
        List<Integer> listId = getListIdOfCavityForSite(siteId, co);
        listId.forEach(idInteger -> {
            deleteCalageNarmaAssociatedWithCavity(idInteger, co);
            deleteRealDataAssociatedWithCavity(idInteger, co);
            deleteCavityWithId(idInteger, co);
        });
    }

    /**
     * Supprime la cavité avec l'id "idInteger" de la base de données
     *
     * @param idInteger Id de la cavité à supprimer
     * @param co        Connection à la base de données
     */
    private static void deleteCavityWithId(Integer idInteger, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from cavity where id = \'" + idInteger + "\';";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Recupère la liste des id des cavités pour le site "siteId"
     *
     * @param siteId Site ou chercher la liste des cavités
     * @param co     Connection à la base de données
     * @return List d'integer, qui est la liste des id des cavités associé au site
     */
    private static List<Integer> getListIdOfCavityForSite(int siteId, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "select id from cavity where site = \'" + siteId + "\';";
            try (ResultSet rs = statement.executeQuery(request)) {
                List<Integer> integers = new ArrayList<>();
                while (rs.next()) {
                    int idInt = rs.getInt("id");
                    integers.add(idInt);
                }
                if (integers.isEmpty()) {
                    return Collections.emptyList();
                }
                return integers;
            }
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * Supprime la composition de gaz associé au site
     *
     * @param siteId Site dans lequel supprimer la composition de gaz
     * @param co     Connection à la base de données
     */
    private static void deleteCompoGazAssociatedWithSite(int siteId, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from compoGaz where site = " + siteId + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Supprime l'exploitant "exp" de la base de données
     *
     * @param exp Exploitant à supprimer de la base de donnée
     * @param co  Connection à la base de données
     */
    static void deleteExploitant(Exploitant exp, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from exploitant where id =  " + exp.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Supprime le contact "contact" de la base de données
     *
     * @param contact Contact à supprimer de la base de données
     * @param co      Connection à la base de données
     */
    static void deleteContact(Contact contact, Connection co) {
        try (Statement statement = co.createStatement()) {
            int contactId = contact.getId();
            String request = "delete from contact where id = " + contactId + ";";
            if (contact.isAffected()) {
                deleteLienContactSiteAssociatedWith(contactId, co);
            }
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Supprime le lien entre un site et contact si le contact est supprimé
     *
     * @param contactId Id du contact
     * @param co        Connection à la base de données
     */
    private static void deleteLienContactSiteAssociatedWith(int contactId, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from lienContactSite where contact = " + contactId + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Supprime le gaz "Gaz" de la base de données
     *
     * @param gaz Gaz à supprimer de la base de données
     * @param co  Connection à la base de données
     */
    static void deleteGaz(Gaz gaz, Connection co) {
        try (Statement statement = co.createStatement()) {
//            String request = "delete from gaz where id = " + gaz.getId() + ";";
            String request = "delete from gaz where nom like \"" + gaz.getName() + "\";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Supprime la composition de gaz "compositionGaz" de la base de donnée
     *
     * @param compositionGaz Composition de gaz à supprimer de la base de données
     * @param co             Connection à la base de données
     */
    static void deleteCompoGaz(CompositionGaz compositionGaz, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from compositionGaz where id = " + compositionGaz.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Supprime la cavité "cavity" ainsi que toutes les données associées
     *
     * @param cavity Cavité à supprimer de la base de données
     * @param co     Connection à la base de données
     */
    static void deleteCavity(Cavity cavity, Connection co) {
        try (Statement statement = co.createStatement()) {
            int cavityId = cavity.getId();
            String request = "delete from cavity where id = " + cavityId + ";";
            deleteRealDataAssociatedWithCavity(cavityId, co);
            deleteCalageNarmaAssociatedWithCavity(cavityId, co);
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Supprime les realData associé avec la cavité ayant l'id "cavityId"
     *
     * @param cavityId Id de la cavité où supprimer les realData
     * @param co       Connection à la base de données
     */
    private static void deleteRealDataAssociatedWithCavity(int cavityId, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from realData where idcavity = " + cavityId + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Supprime les calageNarma associé à la cavité ayant l'ID "cavityId"
     *
     * @param cavityId Id de la cavité où supprimer les calages
     * @param co       Connection à la base de données
     */
    private static void deleteCalageNarmaAssociatedWithCavity(int cavityId, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from calageNarma where idcavity = " + cavityId + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Supprime le Real data "realData" de la base de données
     *
     * @param realData Real data à supprimer de la table
     * @param co       Connection à la base de données
     */
    static void deleteRealData(RealData realData, Connection co) {
        try (Statement statement = co.createStatement()) {
            int idCavity = realData.getCavity().getId();
            String request = "delete from realData where date = \"" + realData.getDateInsertion() + "\" AND idcavity = " + idCavity + " ;";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Supprime le calage ayant pour ID "calageId" de la base de données
     *
     * @param calageId Id du calage à supprimer
     * @param co       Connection à la base de données
     */
    static void deleteCalageNarma(int calageId, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from calageNarma where id =  " + calageId;
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Supprime le lien entre un contact et un site
     *
     * @param contact Contact où récupérer l'id
     * @param site    Site où récupérer l'id
     * @param co      Connection à la base de données
     */
    static void deleteLienContactSite(Contact contact, Site site, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "delete from lienContactSite where contact = " + contact.getId() + " AND site = " + site.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
