package com.noxdia.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class DatabaseCreationRequests {

    /**
     * Crée la table "lienContactSite"
     *
     * @param co Connection à la base de données
     */
    static void createLienContactTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS lienContactSite (" +
                    "  id INTEGER PRIMARY KEY," +
                    "  contact INT NULL," +
                    "  site INT NULL," +
                    "  CONSTRAINT uniq UNIQUE(contact,site)," +
                    "  CONSTRAINT LienContact" +
                    "    FOREIGN KEY (contact)" +
                    "    REFERENCES contact (id)" +
                    "    ON DELETE NO ACTION" +
                    "    ON UPDATE NO ACTION," +
                    "  CONSTRAINT LienSite" +
                    "    FOREIGN KEY (site)" +
                    "    REFERENCES site (id)" +
                    "    ON DELETE NO ACTION" +
                    "    ON UPDATE NO ACTION" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Crée la table "realData"
     * @param co Connection à la base de données
     */
    static void createRealDataTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS realData (" +
                    "  id INTEGER PRIMARY KEY," +
                    "  date DATE NULL," +
                    "  idcavity INT NOT NULL," +
                    "  twelHead DECIMAL(5,2) NULL," +
                    "  pwelHead DECIMAL(5,2) NULL," +
                    "  volume DECIMAL(5,2) NULL," +
                    "  debit DECIMAL(5,2) NULL," +
                    "  last_modif DATE NULL," +
                    "  log_modif TEXT NULL," +
                    "  CONSTRAINT uniq UNIQUE(date,idcavity)," +
                    "  CONSTRAINT ReleveCavite" +
                    "    FOREIGN KEY (idcavity)" +
                    "    REFERENCES cavity (id)" +
                    "    ON DELETE CASCADE" +
                    "    ON UPDATE NO ACTION" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Crée la table "calageNarma"
     * @param co Connection à la base de données
     */
    static void createCalageNarmaTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS calageNarma (" +
                    "  id INTEGER PRIMARY KEY," +
                    "  idcavity INT NOT NULL," +
                    "  dateFrom DATE NULL," +
                    "  dateTo DATE NULL," +
                    "  isLocked BOOLEAN NULL," +

                    "  P_3_VolumeStock DECIMAL NULL," +
                    "  P_3_Debit DECIMAL NULL," +
                    "  P_3_Psimul DECIMAL NULL," +

                    "  P_2_VolumeStock DECIMAL NULL," +
                    "  P_2_Debit DECIMAL NULL," +
                    "  P_2_Psimul DECIMAL NULL," +
                    "  P_2_DP_Arma DECIMAL NULL," +

                    "  P_1_VolumeStock DECIMAL NULL," +
                    "  P_1_Debit DECIMAL NULL," +
                    "  P_1_Psimul DECIMAL NULL," +
                    "  P_1_DP_Arma DECIMAL NULL," +

                    "  P_0_VolumeStock DECIMAL NULL," +
                    "  P_0_Debit DECIMAL NULL," +
                    "  P_0_Psimul DECIMAL NULL," +
                    "  P_0_DP_Arma DECIMAL NULL," +

                    "  T_3_PArma DECIMAL NULL," +
                    "  T_3_VolumeStock DECIMAL NULL," +
                    "  T_3_Debit DECIMAL NULL," +
                    "  T_3_Tsimul DECIMAL NULL," +

                    "  T_2_PArma DECIMAL NULL," +
                    "  T_2_VolumeStock DECIMAL NULL," +
                    "  T_2_Debit DECIMAL NULL," +
                    "  T_2_Tsimul DECIMAL NULL," +
                    "  T_2_DP_Arma DECIMAL NULL," +

                    "  T_1_PArma DECIMAL NULL," +
                    "  T_1_VolumeStock DECIMAL NULL," +
                    "  T_1_Debit DECIMAL NULL," +
                    "  T_1_Tsimul DECIMAL NULL," +

                    "  T_0_PArma DECIMAL NULL," +
                    "  T_0_VolumeStock DECIMAL NULL," +
                    "  T_0_Debit DECIMAL NULL," +
                    "  T_0_Tsimul DECIMAL NULL," +
                    "  CONSTRAINT uniq UNIQUE(dateFrom,dateTo,idcavity)," +
                    "  CONSTRAINT CalageCavite" +
                    "    FOREIGN KEY (idcavity)" +
                    "    REFERENCES cavity (id)" +
                    "    ON DELETE CASCADE" +
                    "    ON UPDATE NO ACTION" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Crée la table "compositionGaz"
     * @param co Connection à la base de données
     */
    static void createCompoGazTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS compositionGaz (" +
                    "  id INTEGER PRIMARY KEY," +
                    "  site INT NULL," +
                    "  element INT NULL," +
                    "  taux DECIMAL(5,2) NULL," +
                    "  CONSTRAINT uniq UNIQUE (site,element)," +
                    "  CONSTRAINT CompositionSite" +
                    "    FOREIGN KEY (site)" +
                    "    REFERENCES site (id)" +
                    "    ON DELETE CASCADE" +
                    "    ON UPDATE NO ACTION," +
                    "  CONSTRAINT CompositionElement" +
                    "    FOREIGN KEY (element)" +
                    "    REFERENCES element_gaz (id)" +
                    "    ON DELETE CASCADE" +
                    "    ON UPDATE NO ACTION" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Crée la table "gaz"
     * @param co Connection à la base de données
     */
    static void createGazTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS gaz (" +
                    "  id INTEGER PRIMARY KEY," +
                    "  nom TEXT NULL," +
                    "  formule TEXT NULL," +
                    "  temperature_critique DECIMAL NULL," +
                    "  pression_critique DECIMAL NULL," +
                    "  masse_molaire DECIMAL NULL," +
                    "  densite DECIMAL NULL," +
                    "  CONSTRAINT uniq UNIQUE(nom)" +
                    "  );";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Crée la table "cavity"
     * @param co Connection à la base de données
     */
    static void createCavityTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS cavity(" +
                    "  id INTEGER PRIMARY KEY," +
                    "  site INT NOT NULL," +
                    "  nom TEXT NOT NULL," +
                    "  hauteur DECIMAL NULL," +
                    "  diametre DECIMAL NULL," +
                    "  profondeur DECIMAL NULL," +
                    "  volume DECIMAL NULL," +
                    "  date_creation DATE NULL," +
                    "  CONSTRAINT uniq UNIQUE(nom)," +
                    "  CONSTRAINT CaviteSite" +
                    "    FOREIGN KEY (site)" +
                    "    REFERENCES site (id)" +
                    "    ON DELETE CASCADE" +
                    "    ON UPDATE CASCADE" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Crée la table "site"
     * @param co Connection à la base de données
     */
    static void createSiteTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS site (" +
                    "  id INTEGER PRIMARY KEY," +
                    "  nom TEXT NOT NULL," +
                    "  adresse TEXT NULL," +
                    "  ville TEXT NULL," +
                    "  pays TEXT NULL," +
                    "  password TEXT NOT NULL," +
                    "  date_creation DATE NULL," +
                    "  exploitant INT NOT NULL," +
                    "  imageLocationClient MEDIUMTEXT NOT NULL," +
                    "  imageLocationServer MEDIUMTEXT NOT NULL," +
                    "  CONSTRAINT uniq UNIQUE(nom)," +
                    "  CONSTRAINT ExploitantSite" +
                    "    FOREIGN KEY (exploitant)" +
                    "    REFERENCES exploitant (id)" +
                    "    ON DELETE NO ACTION" +
                    "    ON UPDATE NO ACTION" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Crée la table "contact"
     * @param co Connection à la base de données
     */
    static void createContactTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS contact (" +
                    "  id INTEGER PRIMARY KEY," +
                    "  exploitant INT NOT NULL," +
                    "  nom TEXT NULL," +
                    "  prenom TEXT NULL," +
                    "  mail TEXT NULL," +
                    "  fonction TEXT NULL," +
                    "  service TEXT NULL," +
                    "  telephone TEXT NULL," +
                    "  imageLocationClient MEDIUMTEXT NOT NULL," +
                    "  imageLocationServer MEDIUMTEXT NOT NULL," +
                    "  CONSTRAINT uniq UNIQUE(nom,prenom,exploitant)," +
                    "  CONSTRAINT ContactExploitant" +
                    "    FOREIGN KEY (exploitant)" +
                    "    REFERENCES exploitant (id)" +
                    "    ON DELETE CASCADE" +
                    "    ON UPDATE NO ACTION" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Crée la table "exploitant"
     * @param co Connection à la base de données
     */
    static void createExploitantTable(Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "CREATE TABLE IF NOT EXISTS exploitant (" +
                    "id INTEGER PRIMARY KEY," +
                    "nom TEXT NOT NULL," +
                    "adresse TEXT NOT NULL," +
                    "ville TEXT NOT NULL," +
                    "pays TEXT NOT NULL," +
                    "imageLocationClient MEDIUMTEXT NOT NULL," +
                    "imageLocationServer MEDIUMTEXT NOT NULL," +
                    "email TEXT NOT NULL," +
                    "CONSTRAINT uniq UNIQUE(nom)" +
                    ");";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }
}
