package com.noxdia.database;

import com.noxdia.exploitant.Exploitant;
import com.noxdia.exploitant.contact.Contact;
import com.noxdia.site.Site;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.gaz.Gaz;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Contient toutes les requetes pour la mise à jour d'element des tables
 */
class DatabaseUpdateRequests {

    /**
     * Met à jour le site "site" dans la base de données
     *
     * @param site Site à mettre à jour dans la base de données
     * @param co   Connection à la base de données
     */
    static void updateSite(Site site, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "UPDATE site " +
                    "SET nom = \"" + site.getName() + "\"," +
                    "adresse = \"" + site.getAdress() + "\"," +
                    "ville = \"" + site.getTown() + "\"," +
                    "pays = \"" + site.getCountry() + "\"," +
                    "imageLocationClient = \"" + site.getImageSiteClient() + "\"," +
                    "imageLocationServer = \"" + site.getImageSiteServer() + "\" " +
                    "where id =  " + site.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Met à jour le mot du site "site" avec le hash "passwordHash"
     *
     * @param site         Site dans lequel mettre le mot de passe à jour
     * @param passwordHash Nouveau mot de passe hashée
     * @param co           Connection à la base de données
     */
    static void updateSitePassword(Site site, String passwordHash, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "UPDATE site " +
                    "SET password = \"" + passwordHash + "\" " +
                    "where id = " + site.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Met à jour l'exploitant "exp" dans la base de données
     *
     * @param exp Exploitant à mettre à jour dans la base de données
     * @param co  Connection à la base de données
     */
    static void updateExploitant(Exploitant exp, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "UPDATE exploitant " +
                    "SET nom = \"" + exp.getName() + "\"," +
                    "adresse = \"" + exp.getAdresse() + "\"," +
                    "ville = \"" + exp.getTown() + "\"," +
                    "pays = \"" + exp.getCountry() + "\"," +
                    "imageLocationClient = \"" + exp.getLogoLocationClient() + "\"," +
                    "imageLocationServer = \"" + exp.getLogoLocationServer() + "\"," +
                    "email = \"" + exp.getEmail() + "\" " +
                    "where id = " + exp.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Met à jour le contact "contact" dans la base de données
     *
     * @param contact Contact à mettre à jour
     * @param co      Connection à la base de données
     */
    static void updateContact(Contact contact, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "UPDATE contact " +
                    "SET nom = \"" + contact.getLastName() + "\"," +
                    "prenom = \"" + contact.getName() + "\"," +
                    "mail = \"" + contact.getMail() + "\"," +
                    "fonction = \"" + contact.getFonction() + "\"," +
                    "service = \"" + contact.getService() + "\"," +
                    "telephone = \"" + contact.getTelephone() + "\"," +
                    "imageLocationClient = \"" + contact.getImageLocationClient() + "\"," +
                    "imageLocationServer = \"" + contact.getImageLocationServer() + "\" " +
                    "where id = " + contact.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Met à jour le gaz "Gaz" dans la base de données
     *
     * @param gaz Gaz à mettre à jour dans la base de données
     * @param co  Connection à la base de données
     */
    static void updateGaz(Gaz gaz, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "UPDATE gaz " +
                    "SET " +
                    "formule = \"" + gaz.getFormule() + "\"," +
                    "temperature_critique = \"" + gaz.getCriticalTemperature() + "\"," +
                    "pression_critique = \"" + gaz.getCriticalPression() + "\"," +
                    "masse_molaire = \"" + gaz.getMasseMolaire() + "\"," +
                    "densite = \"" + gaz.getDensite() + "\" " +
                    "where nom like \"" + gaz.getName() + "\";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
        }
    }

    /**
     * Met à jour la cavité "cavity" dans la base de données
     *
     * @param cavity Cavité à mettre à jour dans la base de données
     * @param co     Connection à la base de données
     */
    static void updateCavity(Cavity cavity, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "UPDATE cavity " +
                    "SET nom = \"" + cavity.getName() + "\"," +
                    "hauteur = \"" + cavity.getHauteurTubage() + "\"," +
                    "diametre = \"" + cavity.getDiametreTubage() + "\"," +
                    "profondeur = \"" + cavity.getDepth() + "\"," +
                    "volume = \"" + cavity.getVolume() + "\" " +
                    "where id = " + cavity.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Met à jour le calage "calage" dans la base de données
     *
     * @param calage Calage à mettre à jour dans la base de données
     * @param co     Connection à la base de données
     */
    static void updateCalageNarma(Calage calage, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "UPDATE calageNarma " +
                    "SET dateFrom = \"" + calage.getFrom() + "\"," +
                    "dateTo = \"" + calage.getTo() + "\"," +

                    "P_3_VolumeStock = \"" + calage.getP_3_VolumeStock() + "\"," +
                    "P_3_Debit = \"" + calage.getP_3_Debit() + "\"," +
                    "P_3_Psimul = \"" + calage.getP_3_Psimul() + "\"," +

                    "P_2_VolumeStock = \"" + calage.getP_2_VolumeStock() + "\"," +
                    "P_2_Debit = \"" + calage.getP_2_Debit() + "\"," +
                    "P_2_Psimul = \"" + calage.getP_2_Psimul() + "\"," +
                    "P_2_DP_Arma = \"" + calage.getP_2_DP_Arma() + "\"," +

                    "P_1_VolumeStock = \"" + calage.getP_1_VolumeStock() + "\"," +
                    "P_1_Debit = \"" + calage.getP_1_Debit() + "\"," +
                    "P_1_Psimul = \"" + calage.getP_1_Psimul() + "\"," +
                    "P_1_DP_Arma = \"" + calage.getP_1_DP_Arma() + "\"," +

                    "P_0_VolumeStock = \"" + calage.getP_0_VolumeStock() + "\"," +
                    "P_0_Debit = \"" + calage.getP_0_Debit() + "\"," +
                    "P_0_Psimul = \"" + calage.getP_0_Psimul() + "\"," +
                    "P_0_DP_Arma = \"" + calage.getP_0_DP_Arma() + "\"," +

                    "T_3_PArma = \"" + calage.getT_3_PArma() + "\"," +
                    "T_3_VolumeStock = \"" + calage.getT_3_VolumeStock() + "\"," +
                    "T_3_Debit = \"" + calage.getT_3_Debit() + "\"," +
                    "T_3_Tsimul = \"" + calage.getT_3_Tsimul() + "\"," +

                    "T_2_PArma = \"" + calage.getT_2_PArma() + "\"," +
                    "T_2_VolumeStock = \"" + calage.getT_2_VolumeStock() + "\"," +
                    "T_2_Debit = \"" + calage.getT_2_Debit() + "\"," +
                    "T_2_Tsimul = \"" + calage.getT_2_Tsimul() + "\"," +
                    "T_2_DP_Arma = \"" + calage.getT_2_DP_Arma() + "\"," +

                    "T_1_PArma = \"" + calage.getT_1_PArma() + "\"," +
                    "T_1_VolumeStock = \"" + calage.getT_1_VolumeStock() + "\"," +
                    "T_1_Debit = \"" + calage.getT_1_Debit() + "\"," +
                    "T_1_Tsimul = \"" + calage.getT_1_Tsimul() + "\"," +

                    "T_0_PArma = \"" + calage.getT_0_PArma() + "\"," +
                    "T_0_VolumeStock = \"" + calage.getT_0_VolumeStock() + "\"," +
                    "T_0_Debit = \"" + calage.getT_0_Debit() + "\"," +
                    "T_0_Tsimul = \"" + calage.getT_0_Tsimul() + "\"" +
                    "where id =  " + calage.getId();
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }

    /**
     * Met à jour le status de verrouillage d'un calage
     *
     * @param calage       Calage ou changer le statut
     * @param lockedStatus Nouveau status à mettre dans la base de données
     * @param co           Connection à la base de données
     */
    static void updateCalageNarmaLockedStatus(Calage calage, Boolean lockedStatus, Connection co) {
        try (Statement statement = co.createStatement()) {
            String request = "UPDATE calageNarma " +
                    "SET isLocked = \"" + lockedStatus.toString().toUpperCase() + "\" " +
                    "where id = " + calage.getId() + ";";
            Database.executeUpdate(request, statement);
        } catch (SQLException e) {
            //
        }
    }
}
