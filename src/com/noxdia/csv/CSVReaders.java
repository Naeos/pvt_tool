package com.noxdia.csv;


import com.noxdia.database.Database;
import com.noxdia.exploitant.Exploitant;
import com.noxdia.exploitant.ExploitantFactory;
import com.noxdia.exploitant.contact.Contact;
import com.noxdia.exploitant.contact.ContactFactory;
import com.noxdia.logger.SaspLogger;
import com.noxdia.site.Site;
import com.noxdia.site.SiteFactory;
import com.noxdia.site.cavite.Cavity;
import com.noxdia.site.cavite.CavityFactory;
import com.noxdia.site.cavite.calage.Calage;
import com.noxdia.site.cavite.calage.CalageFactory;
import com.noxdia.site.cavite.data.DataFactory;
import com.noxdia.site.cavite.data.RealData;
import com.noxdia.site.gaz.CompositionGaz;
import com.noxdia.site.gaz.Gaz;
import com.noxdia.site.gaz.GazFactory;
import com.noxdia.tools.Tools;
import com.noxdia.zip.Zipper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CSVReaders {

    private static final SaspLogger SASP_LOGGER = SaspLogger.getInstance();
    private static final Tools tools = Tools.getInstance();
    // Formatage de la date dans les fichier lu
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static Comparator<RealData> dataRealComparator = (o1, o2) -> {
        if (o1.getDateInsertion().isBefore(o2.getDateInsertion())) {
            return -1;
        }
        return 1;
    };

    private CSVReaders() {
    }

    /**
     * Import de l'exploitant et de ses contacts
     *
     * @param zipFileLocation Localisation du fichier zip
     * @param database        La base de données du programme
     * @return Un exploitant si l'import s'est correctement effectué, un Optional.empty();
     */
    public static Optional<Exploitant> importExploitant(String zipFileLocation, Database database) {

        Zipper zipper = new Zipper();
        Path zipLocation = Paths.get(zipFileLocation);
        zipper.unpack(zipFileLocation, "./");
        Path zipLocationFileName = zipLocation.getFileName();
        if (null == zipLocationFileName) {
            return Optional.empty();
        }
        String zipFileName = zipLocationFileName.toString();
        String folderName = zipFileName.replace(".zip", "");
        Path extractedZipLocation = Paths.get("./" + folderName);

        Optional<Exploitant> optExp = importCSVInfoExploitant(extractedZipLocation, database);
        if (!optExp.isPresent()) {
            SASP_LOGGER.log(Level.WARNING, "Error while reading csv info exploitant");
            return Optional.empty();
        }

        Exploitant exploitant = optExp.get();

        // insertion de l'exploitant dans la bdd
        exploitant.insertInDB();
        List<Contact> contactList = importCSVContact(exploitant, extractedZipLocation, database);

        contactList.forEach(exploitant::addContact);

        // Once finished reading the data , delete the extracted files
        tools.deleteDirectoryRecurs(extractedZipLocation);
        return Optional.of(exploitant);
    }

    /**
     * Importe l'exploitant à partir de son fichier csv
     *
     * @param extractedZipLocation Localisation du fichier zip extrait
     * @param database             La base de données du programme
     * @return Un exploitant si l'import s'est correctement effectué, un Optional.empty();
     */
    private static Optional<Exploitant> importCSVInfoExploitant(Path extractedZipLocation, Database database) {
        Path expInfoCsvPath = Paths.get(extractedZipLocation.toString() + "/info.csv");
        try (Stream<String> lines = Files.lines(expInfoCsvPath)) {
            String[] linesArray = lines.toArray(String[]::new);
            String nomExploitant = linesArray[0].split(";")[1];
            String adress = linesArray[1].split(";")[1];
            String ville = linesArray[2].split(";")[1];
            String pays = linesArray[3].split(";")[1];
            String email = linesArray[4].split(";")[1];

            String logoFileLocation = linesArray[5].split(";")[1];

            String logoFileName = logoFileLocation.substring(logoFileLocation.lastIndexOf("/") + 1);

            String logoContactDefaultName = "imageDefaultExploitant.jpg";

            boolean isImageDefault = logoFileName.equals(logoContactDefaultName);

            Path source = Paths.get(extractedZipLocation.toString() + logoFileLocation);
            String imageLocationClient = "/asset/uploads/" + logoFileName;
            String imageLocationServer = "./webroot" + imageLocationClient;

            // Copie l'image au bon endroit pour le bon fonctionnement du programme
            Path target = Paths.get(imageLocationServer);
            tools.copySourceToTargetIfnotExists(source, target);

            Exploitant exploitant = ExploitantFactory.createExploitant(nomExploitant, adress, pays, ville, email, imageLocationClient, imageLocationServer, database, isImageDefault);
            return Optional.of(exploitant);
        } catch (Exception e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
        return Optional.empty();
    }

    /**
     * Importe la liste de contact contenu dans le fichier CSV
     *
     * @param exploitant           Exploitant associé aux contacts
     * @param extractedZipLocation Localisation du fichier zip extrait
     * @param database             La base de données du programme
     * @return Une liste de contact associé a l'exploitant, la liste sera vide s'il le fichier n'a pas pu etre lu correctement
     */
    private static List<Contact> importCSVContact(Exploitant exploitant, Path extractedZipLocation, Database database) {
        Path csvContactPath = Paths.get(extractedZipLocation.toString() + "/contact.csv");
        try (Stream<String> lines = Files.lines(csvContactPath)) {
            // Skip 1
            return lines.skip(1).map(line -> {
                String[] tokens = line.split(";");

                String imageContactDefaultName = "imageDefaultContact.jpg";

                String lastName = tokens[0];
                String name = tokens[1];
                // = tokens[2] -> Nom de l'exploitant -> Pas utile dans notre cas -> Connais deja exploitant au momment de l'appel
                String fonction = tokens[3];
                String service = tokens[4];
                String email = tokens[5];
                String phone = tokens[6];

                String iconeFileLocation = tokens[7];
                String iconeFileName = iconeFileLocation.substring(iconeFileLocation.lastIndexOf("/") + 1);

                String imageLocationClient = "/asset/uploads/" + iconeFileName;
                String imageLocationServer = "./webroot" + imageLocationClient;
                boolean isImageDefault = iconeFileName.equals(imageContactDefaultName);

                Path source = Paths.get(extractedZipLocation.toString() + iconeFileLocation);
                // We need to copy the image to the correct location when importing
                Path target = Paths.get(imageLocationServer);
                tools.copySourceToTargetIfnotExists(source, target);

                return ContactFactory.createContact(lastName, name, exploitant, fonction, service, email, phone, imageLocationClient, imageLocationServer, isImageDefault, database);
            }).collect(Collectors.toList());
        } catch (Exception e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
        return Collections.emptyList();
    }

    /**
     * Importe un site complet ( composition de gaz , cavité ) à partir du fichier zip
     *
     * @param zipFileLocation Localisation du fichier zip
     * @param database        La base de données du programme
     * @param password        Mot de passe du fichier zip a importé
     * @return Le site si tout s'est bien deroulé , Optional.empty() sinon
     */
    public static Optional<Site> importSiteZip(String zipFileLocation, Database database, String password) {
        // Extraire fichier zip comme pour exploitant
        Zipper zipper = new Zipper();
        Path zipLocation = Paths.get(zipFileLocation);
        zipper.unpackWithPassword(zipFileLocation, "./", password);
        Path fileName = zipLocation.getFileName();
        if (null == fileName) {
            return Optional.empty();
        }
        String zipFileName = fileName.toString();
        String folderName = zipFileName.replace(".zip", "");
        Path extractedZipLocation = Paths.get("./" + folderName);

        return importSite(extractedZipLocation, database);
    }

    /**
     * Importe un site complet (composition de gaz , cavité) à partir du fichier zip extrait
     *
     * @param folderLocation Emplacement du dossier a importer
     * @param database       La base de données du programme
     * @return Le site si tout s'est bien deroulé , Optional.empty() sinon
     */
    private static Optional<Site> importSite(Path folderLocation, Database database) {
        String currentFolder = folderLocation.toString();
        SaspLogger saspLogger = SaspLogger.getInstance();
        Optional<Site> optSite = importCSVSiteInfo(database, folderLocation);
        if (!optSite.isPresent()) {
            saspLogger.log(Level.WARNING, "Error lors de la recuperation des informations du fichier csv");
            return Optional.empty();
        }
        Site site = optSite.get();

        List<CompositionGaz> listCompoGaz = importCSVCompoGaz(currentFolder, database, site);
        listCompoGaz.forEach(site::addCompositionGaz);

        // ImportAllCavity
        String cavityFolder = currentFolder + "/cavites";
        List<Cavity> listCavity = importAllCavity(cavityFolder, database);
        if (listCavity.isEmpty()) {
            saspLogger.log(Level.INFO, "No cavity for site");
        } else {
            saspLogger.log(Level.INFO, "All cavity imported");
        }
        listCavity.forEach(site::addCavity);

        // Once finished reading the data , delete the extracted files
        tools.deleteDirectoryRecurs(folderLocation);

        return Optional.of(site);
    }

    /**
     * Importe un site en lisant les informations contenu dans le fichier csv
     *
     * @param database     La base de données du programme
     * @param parentFolder Dossier ou se situe le fichier .csv
     * @return Le site si l'import s'est bien passé, Optional.empty() sinon
     */
    private static Optional<Site> importCSVSiteInfo(Database database, Path parentFolder) {
        Path siteInfoCsvPath = Paths.get(parentFolder.toString() + "/info.csv");
        try (Stream<String> lines = Files.lines(siteInfoCsvPath)) {

            String[] linesArray = lines.toArray(String[]::new);

            String nomSite = linesArray[0].split(";")[1];
            String nomExploitant = linesArray[1].split(";")[1];
            String adresse = linesArray[2].split(";")[1];
            String ville = linesArray[3].split(";")[1];
            String pays = linesArray[4].split(";")[1];
            LocalDate date = LocalDate.from(dtf.parse(linesArray[5].split(";")[1]));
            String passwordHash = linesArray[6].split(";")[1];

            String imageFileLocation = linesArray[7].split(";")[1];
            String imageFileName = imageFileLocation.substring(imageFileLocation.lastIndexOf("/") + 1);

            Path source = Paths.get(parentFolder.toString() + imageFileLocation);
            String imageLocationClient = "/asset/uploads/" + imageFileName;
            String imageLocationServer = "./webroot" + imageLocationClient;

            // We need to copy the image to the correct location when importing
            Path target = Paths.get(imageLocationServer);

            tools.copySourceToTargetIfnotExists(source, target);

            Optional<Exploitant> optExp = database.getExploitantByName(nomExploitant);
            if (!optExp.isPresent()) {
                SASP_LOGGER.log(Level.WARNING, "Exploitant " + nomExploitant + " does not exists");
                return Optional.empty();
            }

            Exploitant exp = optExp.get();

            String imageSiteDefaultName = "imageDefaultSite.jpg";
            boolean isImageDefault = imageSiteDefaultName.equals(imageFileName);
            Site site = SiteFactory.createSite(nomSite, adresse, pays, ville, passwordHash, date, imageLocationClient, imageLocationServer, database, exp, isImageDefault);
            // Ajoute le site dans la BDD avec l'id de l'exploitant
            exp.addSite(site);


            return Optional.of(site);
        } catch (Exception e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
            return Optional.empty();
        }
    }

    /**
     * Importe la liste des compositionGaz à partir du fichier csv
     *
     * @param folder   Dossier ou se situe le fichier compo_gaz.csv
     * @param database La base de données du programme
     * @param site     Site auxquel associer la liste de compositionGaz
     * @return La liste des compositionGaz associé à un site, renvoie une liste vide s'il n'y en a pas
     */
    private static List<CompositionGaz> importCSVCompoGaz(String folder, Database database, Site site) {
        Path compoGazCsvPath = Paths.get(folder + "/compo_gaz.csv");
        try (Stream<String> lines = Files.lines(compoGazCsvPath)) {
            return lines.skip(2)
                    .map(line -> {
                        String[] tokens = line.split(";");
                        String gazName = tokens[0];
                        double proportion = Double.parseDouble(tokens[1].replace(",", "."));
                        String formule = tokens[2];

                        double masseMolaire = Double.parseDouble(tokens[3].replace(",", "."));
                        double criticalTemperature = Double.parseDouble(tokens[4].replace(",", "."));
                        double criticalPression = Double.parseDouble(tokens[5].replace(",", "."));
                        double densite = Double.parseDouble(tokens[6].replace(",", "."));
                        Gaz gaz = GazFactory.createGaz(gazName, formule, masseMolaire, criticalTemperature, criticalPression, densite);
                        database.insertGaz(gaz);

                        return GazFactory.createCompositionGaz(site, gaz, proportion);
                    })
                    .collect(Collectors.toList());

        } catch (Exception e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
        return Collections.emptyList();
    }

    /**
     * Importe la liste des cavité se trouvant dans le dossier
     *
     * @param rootFolder Dossier ou se situe toutes les cavity
     * @param database   La base de données du programme
     * @return La liste des cavity importé, renvoie une liste vide si rien n'est trouvé
     */
    private static List<Cavity> importAllCavity(String rootFolder, Database database) {
        List<Cavity> cavityList = new ArrayList<>();
        File[] directories = new File(rootFolder).listFiles(File::isDirectory);
        if (directories == null) {
            SASP_LOGGER.log(Level.WARNING, "No cavity in folder");
            return Collections.emptyList();
        }
        for (File f : directories) {
            String folder = f.toString();
            Optional<Cavity> cavityOpt = importCavity(database, folder);
            if (!cavityOpt.isPresent()) {
                continue;
            }
            Cavity cavity = cavityOpt.get();
            cavityList.add(cavity);
        }
        return cavityList;
    }

    /**
     * Importe une cavité en lisant les informations des fichiers
     *
     * @param database La base de données du programme
     * @param folder   Dossier ou se situe la cavité à importer, ce dossier doit contenir "CAVITE" dans son nom
     * @return La cavity si l'importation réussi, Optional.empty() sinon
     */
    private static Optional<Cavity> importCavity(Database database, String folder) {
        Optional<Cavity> optCav = importCSVInfoCavity(folder, database);
        if (!optCav.isPresent()) {
            SASP_LOGGER.log(Level.WARNING, "Cannot import csv info cavity");
            return Optional.empty();
        }
        Cavity cavity = optCav.get();
        cavity.insertInDb();

        Path realDataCSV = Paths.get(folder + "/real_data.csv");
        List<RealData> realDataList = importCSVRealData(realDataCSV, cavity, "");
        realDataList.sort(dataRealComparator);
        // Ajouter les real data dans la cavity
        realDataList.forEach(cavity::addRealData);

        String calagesFolder = folder + "/calages";

        // Chaque calage est ajouté à la bdd lors de sa création dans la fonction
        importAllCalage(calagesFolder, database, cavity);

        return Optional.of(cavity);
    }

    /**
     * Importe la liste des calages à partir du dossier "calages" dans fichier zip au moment de l'import
     * En lisant le contenu de chacun des dossiers
     *
     * @param calagesFolder Dossier ou se situe les dossier de calages
     * @param database      La base de données du programme
     * @param cavity        Cavity a laquelle associer la liste de calages
     */
    private static void importAllCalage(String calagesFolder, Database database, Cavity cavity) {
        File[] directories = new File(calagesFolder).listFiles(File::isDirectory);
        if (null == directories) {
            SASP_LOGGER.log(Level.INFO, "No calage for cavity : " + cavity.getName());
            return;
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        for (File directory : directories) {
            String folder = directory.toString();
            String folderName = directory.getName();
            String[] dates = folderName.split("_");
            String dateDebutString = dates[0];
            String dateFinString = dates[1];
            LocalDate from = LocalDate.from(dtf.parse(dateDebutString));
            LocalDate to = LocalDate.from(dtf.parse(dateFinString));
            Map<String, Double> coefPressions = importCSVPression(folder);
            if (coefPressions.isEmpty()) {
                continue;
            }
            Map<String, Double> coefTemperature = importCSVTemperature(folder);
            if (coefTemperature.isEmpty()) {
                continue;
            }
            double p_3_volumeStock = coefPressions.get("p_3_volumeStock");
            double p_3_debit = coefPressions.get("p_3_debit");
            double p_3_Psimul = coefPressions.get("p_3_psimul");

            double p_2_volumeStock = coefPressions.get("p_2_volumeStock");
            double p_2_debit = coefPressions.get("p_2_debit");
            double p_2_Psimul = coefPressions.get("p_2_psimul");
            double p_2_dp_arma = coefPressions.get("p_2_dp_arma");

            double p_1_volumeStock = coefPressions.get("p_1_volumeStock");
            double p_1_debit = coefPressions.get("p_1_debit");
            double p_1_Psimul = coefPressions.get("p_1_psimul");
            double p_1_dp_arma = coefPressions.get("p_1_dp_arma");

            double p_0_volumeStock = coefPressions.get("p_0_volumeStock");
            double p_0_debit = coefPressions.get("p_0_debit");
            double p_0_Psimul = coefPressions.get("p_0_psimul");
            double p_0_dp_arma = coefPressions.get("p_0_dp_arma");

            double t_3_pArma = coefTemperature.get("t_3_PArma");
            double t_3_volumeStock = coefTemperature.get("t_3_volumeStock");
            double t_3_debit = coefTemperature.get("t_3_debit");
            double t_3_Tsimul = coefTemperature.get("t_3_Tsimul");

            double t_2_pArma = coefTemperature.get("t_2_PArma");
            double t_2_volumeStock = coefTemperature.get("t_2_volumeStock");
            double t_2_debit = coefTemperature.get("t_2_debit");
            double t_2_Tsimul = coefTemperature.get("t_2_Tsimul");
            double t_2_dpArma = coefTemperature.get("t_2_dp_arma");

            double t_1_pArma = coefTemperature.get("t_1_PArma");
            double t_1_volumeStock = coefTemperature.get("t_1_volumeStock");
            double t_1_debit = coefTemperature.get("t_1_debit");
            double t_1_Tsimul = coefTemperature.get("t_1_Tsimul");

            double t_0_pArma = coefTemperature.get("t_0_PArma");
            double t_0_volumeStock = coefTemperature.get("t_0_volumeStock");
            double t_0_debit = coefTemperature.get("t_0_debit");
            double t_0_Tsimul = coefTemperature.get("t_0_Tsimul");

            Calage calage = CalageFactory.createCalage(from, to, cavity, database,
                    p_3_volumeStock, p_3_debit, p_3_Psimul,
                    p_2_volumeStock, p_2_debit, p_2_Psimul, p_2_dp_arma,
                    p_1_volumeStock, p_1_debit, p_1_Psimul, p_1_dp_arma,
                    p_0_volumeStock, p_0_debit, p_0_Psimul, p_0_dp_arma,
                    t_3_pArma, t_3_volumeStock, t_3_debit, t_3_Tsimul,
                    t_2_pArma, t_2_volumeStock, t_2_debit, t_2_Tsimul, t_2_dpArma,
                    t_1_pArma, t_1_volumeStock, t_1_debit, t_1_Tsimul,
                    t_0_pArma, t_0_volumeStock, t_0_debit, t_0_Tsimul);
            calage.insertInDb();

            calage.lock();
            List<Calage> calageList = database.getListCalageForCavity(cavity);
            calageList.forEach(calageToTest -> {
                // Verifier les dates pour chaques calage et si date coincide avec celle qu'on veut inserer
                // Passer le locked ( des dates qui coincide ) à false
                if (!calage.equals(calageToTest) && calage.checkIfPeriodOverlap(calageToTest)) {
                    calageToTest.unlock();
                }
            });
        }
    }

    /**
     * Importe le fichier pression.csv
     *
     * @param folder Dossier ou se situe le fichier pression.csv d'un calage
     * @return Une hashmap avec les informations sur les parametre de la pression pour ce calage
     */
    private static HashMap<String, Double> importCSVPression(String folder) {
        String csvFileLocation = folder + "/pression.csv";
        Path pressionCsv = Paths.get(csvFileLocation);
        HashMap<String, Double> coefPression = new HashMap<>();
        try (Stream<String> lines = Files.lines(pressionCsv)) {
            String[] linesArray = lines.skip(1).limit(4).toArray(String[]::new);

            String lineP_3 = linesArray[0];
            String lineP_2 = linesArray[1];
            String lineP_1 = linesArray[2];
            String lineP_0 = linesArray[3];

            int indiceVolumeStock = 2;
            int indiceDebit = 3;
            int indicePsimul = 4;
            int indiceDPArma = 5;

            double p_3_volumeStock = Double.parseDouble(lineP_3.split(";")[indiceVolumeStock]);
            double p_3_debit = Double.parseDouble(lineP_3.split(";")[indiceDebit]);
            double p_3_psimul = Double.parseDouble(lineP_3.split(";")[indicePsimul]);

            double p_2_volumeStock = Double.parseDouble(lineP_2.split(";")[indiceVolumeStock]);
            double p_2_debit = Double.parseDouble(lineP_2.split(";")[indiceDebit]);
            double p_2_psimul = Double.parseDouble(lineP_2.split(";")[indicePsimul]);
            double p_2_dp_arma = Double.parseDouble(lineP_2.split(";")[indiceDPArma]);

            double p_1_volumeStock = Double.parseDouble(lineP_1.split(";")[indiceVolumeStock]);
            double p_1_debit = Double.parseDouble(lineP_1.split(";")[indiceDebit]);
            double p_1_psimul = Double.parseDouble(lineP_1.split(";")[indicePsimul]);
            double p_1_dp_arma = Double.parseDouble(lineP_1.split(";")[indiceDPArma]);

            double p_0_volumeStock = Double.parseDouble(lineP_0.split(";")[indiceVolumeStock]);
            double p_0_debit = Double.parseDouble(lineP_0.split(";")[indiceDebit]);
            double p_0_psimul = Double.parseDouble(lineP_0.split(";")[indicePsimul]);
            double p_0_dp_arma = Double.parseDouble(lineP_0.split(";")[indiceDPArma]);

            coefPression.put("p_3_volumeStock", p_3_volumeStock);
            coefPression.put("p_3_debit", p_3_debit);
            coefPression.put("p_3_psimul", p_3_psimul);

            coefPression.put("p_2_volumeStock", p_2_volumeStock);
            coefPression.put("p_2_debit", p_2_debit);
            coefPression.put("p_2_psimul", p_2_psimul);
            coefPression.put("p_2_dp_arma", p_2_dp_arma);

            coefPression.put("p_1_volumeStock", p_1_volumeStock);
            coefPression.put("p_1_debit", p_1_debit);
            coefPression.put("p_1_psimul", p_1_psimul);
            coefPression.put("p_1_dp_arma", p_1_dp_arma);

            coefPression.put("p_0_volumeStock", p_0_volumeStock);
            coefPression.put("p_0_debit", p_0_debit);
            coefPression.put("p_0_psimul", p_0_psimul);
            coefPression.put("p_0_dp_arma", p_0_dp_arma);


        } catch (IOException e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
        return coefPression;
    }

    /**
     * Importe le fichier temperature.csv
     *
     * @param folder Dossier ou se situe le fichier temperature.csv d'un calage
     * @return Une hashmap avec les informations sur les parametre de la temperature pour ce calage
     */
    private static HashMap<String, Double> importCSVTemperature(String folder) {
        String csvFileLocation = folder + "/temperature.csv";
        Path temperatureCsv = Paths.get(csvFileLocation);
        HashMap<String, Double> coefTemp = new HashMap<>();
        try (Stream<String> lines = Files.lines(temperatureCsv)) {
            String[] linesArray = lines.skip(1).limit(4).toArray(String[]::new);

            String lineT_3 = linesArray[0];
            String lineT_2 = linesArray[1];
            String lineT_1 = linesArray[2];
            String lineT_0 = linesArray[3];

            int indiceParma = 2;
            int indiceVolumeStock = 3;
            int indiceDebit = 4;
            int indiceTsimul = 5;
            int indiceDPArma = 6;

            double t_3_PArma = Double.parseDouble(lineT_3.split(";")[indiceParma]);
            double t_3_volumeStock = Double.parseDouble(lineT_3.split(";")[indiceVolumeStock]);
            double t_3_debit = Double.parseDouble(lineT_3.split(";")[indiceDebit]);
            double t_3_Tsimul = Double.parseDouble(lineT_3.split(";")[indiceTsimul]);

            double t_2_PArma = Double.parseDouble(lineT_2.split(";")[indiceParma]);
            double t_2_volumeStock = Double.parseDouble(lineT_2.split(";")[indiceVolumeStock]);
            double t_2_debit = Double.parseDouble(lineT_2.split(";")[indiceDebit]);
            double t_2_Tsimul = Double.parseDouble(lineT_2.split(";")[indiceTsimul]);
            double t_2_dp_arma = Double.parseDouble(lineT_2.split(";")[indiceDPArma]);

            double t_1_PArma = Double.parseDouble(lineT_1.split(";")[indiceParma]);
            double t_1_volumeStock = Double.parseDouble(lineT_1.split(";")[indiceVolumeStock]);
            double t_1_debit = Double.parseDouble(lineT_1.split(";")[indiceDebit]);
            double t_1_Tsimul = Double.parseDouble(lineT_1.split(";")[indiceTsimul]);

            double t_0_PArma = Double.parseDouble(lineT_0.split(";")[indiceParma]);
            double t_0_volumeStock = Double.parseDouble(lineT_0.split(";")[indiceVolumeStock]);
            double t_0_debit = Double.parseDouble(lineT_0.split(";")[indiceDebit]);
            double t_0_Tsimul = Double.parseDouble(lineT_0.split(";")[indiceTsimul]);

            coefTemp.put("t_3_PArma", t_3_PArma);
            coefTemp.put("t_3_volumeStock", t_3_volumeStock);
            coefTemp.put("t_3_debit", t_3_debit);
            coefTemp.put("t_3_Tsimul", t_3_Tsimul);

            coefTemp.put("t_2_PArma", t_2_PArma);
            coefTemp.put("t_2_volumeStock", t_2_volumeStock);
            coefTemp.put("t_2_debit", t_2_debit);
            coefTemp.put("t_2_Tsimul", t_2_Tsimul);
            coefTemp.put("t_2_dp_arma", t_2_dp_arma);

            coefTemp.put("t_1_PArma", t_1_PArma);
            coefTemp.put("t_1_volumeStock", t_1_volumeStock);
            coefTemp.put("t_1_debit", t_1_debit);
            coefTemp.put("t_1_Tsimul", t_1_Tsimul);

            coefTemp.put("t_0_PArma", t_0_PArma);
            coefTemp.put("t_0_volumeStock", t_0_volumeStock);
            coefTemp.put("t_0_debit", t_0_debit);
            coefTemp.put("t_0_Tsimul", t_0_Tsimul);

        } catch (IOException e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
        return coefTemp;
    }

    /**
     * Importe une cavité par rapport à son fichier csv
     *
     * @param folder   dossier ou se situe le fichier info.csv de cavity
     * @param database La base de données du programme
     * @return La cavity créé à partir du fichier info.csv, s'il y a une erreur, renvoie Optional.empty()
     */
    private static Optional<Cavity> importCSVInfoCavity(String folder, Database database) {
        Path cavityCSVInfoPath = Paths.get(folder + "/info.csv");
        try (Stream<String> lines = Files.lines(cavityCSVInfoPath)) {
            String[] linesArray = lines.toArray(String[]::new);

            String nomCavity = linesArray[0].split(";")[1];
            String nomSite = linesArray[1].split(";")[1];
            double hauteurTube = Double.parseDouble(linesArray[3].split(";")[1]);
            double diametreTube = Double.parseDouble(linesArray[4].split(";")[1]);
            double profondeurCavity = Double.parseDouble(linesArray[5].split(";")[1]);
            double volumeCavity = Double.parseDouble(linesArray[6].split(";")[1]);
            LocalDate dateCreation = LocalDate.from(dtf.parse(linesArray[7].split(";")[1]));

            Optional<Site> optSite = database.getSiteByName(nomSite);
            if (!optSite.isPresent()) {
                SASP_LOGGER.log(Level.WARNING, "No site with name \'" + nomSite + "\' in database");
                return Optional.empty();
            }
            Site site = optSite.get();

            Optional<Cavity> cavityOptional = database.getCavityByNameAndSite(nomCavity, site);
            if (cavityOptional.isPresent()) {
                SASP_LOGGER.log(Level.WARNING, "Cavity already exists with that name \'" + nomCavity + "\'");
                return Optional.empty();
            }

            Cavity cavity = CavityFactory.createCavity(site, nomCavity, hauteurTube, diametreTube, profondeurCavity, volumeCavity, dateCreation, database);
            return Optional.of(cavity);

        } catch (Exception e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
        return Optional.empty();
    }

    /**
     * Importe la liste des donnéess réel à partir du fichier CSV
     *
     * @param path     Emplacement du fichier CSV contenant les donnéess à importer
     * @param cavity   Cavity a laquel associé les donnéess
     * @param logModif Un court texte contenant la raison des modification ( voir si cela sert vraiment dans le cas de l'import d'un fichier )
     * @return La liste des RealData lu à partir du fichier, renvoie Optional.empty() en cas de problème
     */
    public static List<RealData> importCSVRealData(Path path, Cavity cavity, String logModif) {
        try (Stream<String> lines = Files.lines(path)) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            return lines.skip(1)
                    .filter(line -> line.length() > 4) // evite les lignes avec que des ; dedans
                    .map(line -> {
                        String[] tokens = line.split(";");
                        LocalDate dateLastModif = LocalDate.now();
                        String dateString = tokens[0];
                        LocalDate dateInsertion = LocalDate.from(dtf.parse(dateString));
                        double twelHead = Double.parseDouble(tokens[1].replace(",", "."));
                        double pwelHead = Double.parseDouble(tokens[2].replace(",", "."));
                        double volume = Double.parseDouble(tokens[3].replace(",", "."));
                        double debit = Double.parseDouble(tokens[4].replace(",", "."));
                        return DataFactory.createRealData(dateInsertion, cavity, twelHead, pwelHead, volume, debit, dateLastModif, logModif);
                    })
                    .collect(Collectors.toList());
        } catch (Exception e) {
            SASP_LOGGER.log(Level.WARNING, e.getMessage());
        }
        return Collections.emptyList();
    }

    /**
     * Importe une cavité à partir du fichier zip
     *
     * @param zipFileLocation Localisation du fichier zip
     * @param database        La base de données du programme
     * @param password        Mot de passe du fichier zip passé en paramètre
     * @return La cavity extraite du zip , Optional.empty() sinon
     */
    public static Optional<Cavity> importCavityZip(String zipFileLocation, Database database, String password) {
        Zipper zipper = new Zipper();
        Path zipLocation = Paths.get(zipFileLocation);
        zipper.unpackWithPassword(zipFileLocation, "./", password);

        Path fileName = zipLocation.getFileName();
        if (null == fileName) {
            return Optional.empty();
        }
        String zipFileName = fileName.toString();
        String folderName = zipFileName.replace(".zip", "");
        Path extractedZipLocation = Paths.get("./" + folderName);
        Optional<Cavity> optCav = importCavity(database, extractedZipLocation.toString());

        // Once finished reading the data , delete the extracted files
        tools.deleteDirectoryRecurs(extractedZipLocation);

        return optCav;
    }

}
