var t =
    {
        "Importer un site": {
            en: "Import a site"
        },
        "Nouveau": {
            en: "New",
        },
        "LISTE DES SITES": {
            en : "SITES LIST"
        },
        "Site": {
            en : "Site"
        },
        "Sites": {
            en : "Sites"
        },
        "Cavité": {
            en : "Cavity"
        },
        "Cavités": {
            en : "Cavities"
        },
        "Exploitants": {
            en : "Owner"
        },
        "Créé le": {
            en : "Created the"
        },
        "Connexion": {
            en : "Log in"
        },
        
        
        "Du": {
            en : "From"
        },
        "Au": {
            en : "To"
        },
        "Exporter cavité": {
            en : "Export Cavity"
        },
        "Importer une cavité": {
            en : "Import a cavity"
        },
        "Paramètres": {
            en : "Settings"
        },
        "Exporter site": {
            en : "Export site"
        },
        "Voir les contacts": {
            en : "See the contacts"
        },
        "Générer rapport": {
            en : "Generate report"
        },
        "Liste des cavités" : {
            en : "Cavities list"
        },
        "Nouvelle cavité" : {
            en : "New cavity"
        },
        "Nom de la cavité" : {
            en : "Cavity name"
        },
        "Hauteur tube": {
            en : "Height of tube"
        },
        "Diamètre" : {
            en : "Diameter"
        },
        "Profondeur" : {
            en : "Depth"
        },
        "Volume" : {
            en : "Volume"
        },
        "Ajouter Cavité" : {
            en : "Add cavity"
        },
        "Enregistrer Cavité" : {
            en : "Save cavity"
        },
        "Veuillez vérifier les champs." : {
            en : "Please check the fields."
        },
        "Une cavité du même nom existe déjà pour ce site." : {
            en : "A cavity with the same name already exists for this site."
        },
        "Pression" : {
            en : "Pressure"
        },
        "Température" : {
            en : "Temperature"
        },
        "Volume - Débit" : {
            en : "Volume - Flow"
        },
        "Pression Casing Shoe" : {
            en : "Pressure Casing Shoe"
        },
        "Température Casing Shoe" : {
            en : "Temperature Casing Shoe"
        },
        "Pression cavern" : {
            en : "Pressure cavern"
        },
        "Température cavern" : {
            en : "Temperature cavern"
        },
        "Gérer données relevées" : {
            en : "Manage measured data"
        },
        "Gérer calages et simulations" : {
            en : "Manage calibrations and simulations"
        },
        "Une ou plusieurs données incohérentes ont été détectées, cela peut faussé les simulations." : {
            en : "One or more inconsistent data were detected, this may distort simulations."
        },
        "Une ou plusieurs données manquantes ont été détectées, vous ne pouvez alors pas aller sur la page des calages." : {
            en : "One or more missing data were detected, you can't go to the calibration page."
        },
        "Modifier le site" : {
            en : "Modify the site"
        },
        "Modifier le mot de passe" : {
            en : "Change the password"
        },
        "Supprimer le site" : {
            en : "Remove the site"
        },
        "Suppression du site" : {
            en : "Site removal"
        },
        "Entrez votre mot de passe :" : {
            en : "Enter your password"
        },
        "Supprimer" : {
            en : "Remove"
        },
        "Annuler" : {
            en : "Cancel"
        },
        "Veuillez fournir le mot de passe associé au site." : {
            en : "Please provide the password associated with the site."
        },
        "Mot de passe incorrect." : {
            en : "Wrong password"
        },
        "Modification du mot de passe" : {
            en : "Changing the password"
        },
        "Entrez le nouveau mot de passe :" : {
            en : "Enter the new password :"
        },
        "Recopier le nouveau mot de passe :" : {
            en : "Re-enter the new password :"
        },
        "Modifier" : {
            en : "Modify"
        },
        "Les deux mots de passe sont différents." : {
            en : "The two passwords are different."
        },
        "Suppression d'une cavité" : {
            en : "Removing a cavity"
        },
        "Entrez votre mot de passe :" : {
            en : "Enter your password :"
        },
        "Supprimer" : {
            en : "Remove"
        },
        "Importer une cavité ( *.zip )" : {
            en : "Import a cavity (*.zip)"
        },
        "Sélectionner le dossier :" : {
            en : "Select the folder :"
        },
        "Entrez le mot de passe du dossier :" : {
            en : "Enter the folder password :"
        },
        "Fermer" : {
            en : "Close"
        },
        "Nom" : {
            en : "Name"
        },
        "Adresse" : {
            en : "Adress"
        },
        "Ville" : {
            en : "City"
        },
        "Pays" : {
            en : "Country"
        },
        "Export des cavités par email" : {
            en : "Export cavity by emails"
        },
        "Emails des destinataires :" : {
            en : "Recipients e-mail :"
        },
        "Séparer chaque email par un ;" : {
            en : "Separate each email by a ;"
        },
        "Mot de passe du fichier zip créé :" : {
            en : "Created zip file password : "
        },
        "Choississez ce que vous souhaitez envoyer :" : {
            en : "Choose what you want to send:"
        },
        "Envoie effectué." : {
            en : "Send completed"
        },
        "Echec de l'envoi du mail. Veuillez vérifiez le champs mail." : {
            en : "Failed to send mail. Please check email field."
        },
        "Vous n'avez rien choisi." : {
            en : "You have chosen anything."
        },
        "Envoyer" : {
            en : "Send"
        },
        "Export du site" : {
            en : "Export of the site"
        },
        "Exporter" : {
            en : "Export"
        },
        "Veuillez définir un mot de passe." : {
            en : "Please set a password."
        },
        "Veuillez choisir le mot de passe du fichier zip créé :" : {
            en : "Please choose the created zip file password :"
        },
        "Ajouter un composant" : {
            en : "Add a component"
        },
        "Nouvel élément chimique" : {
            en : "New chemical element"
        },
        "Liste des éléments chimiques" : {
            en : "Chemical elements list"
        },
        "Masse molaire totale" : {
            en : "Total molar mass"
        },
        "Pression critique totale" : {
            en : "Total critical pressure"
        },
        "Température critique totale" : {
            en : "Total critical temperature"
        },
        "Densité totale" : {
            en : "Total density"
        },
        "Enregistrer" : {
            en : "Save"
        },
        "Nom de l'élément" : {
            en : "Element name"
        },
        "Formule" : {
            en : "Formula"
        },
        "Masse molaire" : {
            en : "Molar mass"
        },
        "Pression critique" : {
            en : "Critical pressure"
        },
        "Température critique" : {
            en : "Critical temperature"
        },
        "Densité" : {
            en : "Density"
        },
        "La valeur entrée est incorrecte." : {
            en : "The value entered is incorrect."
        },
        "Voulez-vous enregistrer les modifications ?" : {
            en : "Do you want to save the changes?"
        },
        "Un des champs n'est pas valide." : {
            en : "One of the fields is invalid."
        },
        "Modification annulée. Le nouveau nom de l'élément correspond déjà un élément existant." : {
            en : "Modification canceled. The new name of the element already corresponds to an existing element."
        },
        "Voulez-vous supprimer l'élément ?" : {
            en : "Do you want to remove the element ?"
        },
        "Composant chimique" : {
            en : "Chemical component"
        },
        "Gestion des données" : {
            en : "Data managment"
        },
        "Générer" : {
            en : "Generate"
        },
        "Générer un rapport (*.docx)" : {
            en : "Generate a report (* .docx)"
        },
        "Une ou plusieurs données incohérentes et/ou manquantes ont été détectées." : {
            en : "One or more inconsistent and/or missing data has been detected."
        },
        "Les champs dates ne sont pas remplis ou incorrects." : {
            en : "The date fields are not filled or incorrect."
        },
        "Options de corrections" : {
            en : "Correction options"
        },
        "Importer un fichier CSV" : {
            en : "Import a CSV file"
        },
        "Corriger manuellement" : {
            en : "Correct manually"
        },
        "Débit" : {
            en : "Flow"
        },
        "Incohérent" : {
            en : "Incoherent"
        },
        "Commentaire" : {
            en : "Comment"
        },
        "Pour une date où les valeurs étaient manquantes, vous devez soit ne remplir aucun champs, soit remplir chaque champs de cette même date." : {
            en : "For a date where values were missing, you must either not fill in any fields, or fill in each field for that date."
        },
        "Anc. données" : {
            en : "Old data"
        },
        "lles" : {
            en : " "
        },
        "Nv" : {
            en : "New"
        },
        "données" : {
            en : "data"
        },
        "Importer des données ( *.csv )" : {
            en : "Import data ( *.csv )"
        },
        "Sélectionner le fichier :" : {
            en : "Select file :"
        },
        "Entrez un commentaire" : {
            en : "Enter a comment"
        },
        "Gestion des calages" : {
            en : "Calibration management"
        },
        "Ajouter un calage" : {
            en : "Add a calibration"
        },
        "Enregistrez le nouveau calage avant d'en ajouter un nouveau." : {
            en : "Save the new calibration before adding a new one."
        },
        "Retour" : {
            en : "Back"
        },
        "dév min" : {
            en : "min dev"
        },
        "dév max" : {
            en : "max dev"
        },
        "seq" : {
            en : "sqd"
        },
        "V stocké" : {
            en : "stored V"
        },
        "P simul" : {
            en : "simul P"
        },
        "T simul" : {
            en : "simul T"
        },
        "Déviation min" : {
            en : "Min deviation"
        },
        "Déviation max" : {
            en : "Max deviation"
        },
        "Somme des écarts quadratiques" : {
            en : "Sum of the quadratic deviations"
        },
        "Déviations moyennes" : {
            en : "Average deviations"
        },
        "Afficher sur le graphe" : {
            en : "Show on the graph"
        },
        "Simuler" : {
            en : "Simulate"
        },
        "ds" : {
            en : "sd"
        },
        "Déviation standard" : {
            en : "Standard deviation"
        },
        "Pas assez de données pour pouvoir simuler entre ces dates" : {
            en : "Not enough data to simulate between these dates"
        },
        "Des champs coefficients ne sont pas remplis ou incorrects." : {
            en : "Coefficient fields are not filled or incorrect."
        },
        "Ajouter" : {
            en : "Add"
        },
        "Une erreur est survenue, veuillez recommencer." : {
            en : "An error occured, please try again."
        },
        "NOUVEAU SITE" : {
            en : "New site"
        },
        "Nouvel exploitant" : {
            en : "New owner"
        },
        "Adresse du siège social" : {
            en : "Head Office Address"
        },
        "Ajout d'un exploitant" : {
            en : "Adding an owner"
        },
        "Mot de passe" : {
            en : "Password"
        },
        "Confirmation du mot de passe" : {
            en : "Password Confirmation"
        },
        "Veuillez remplir tous les champs." : {
            en : "Please complete all fields."
        },
        "Enregistrement effectué" : {
            en : "Registration done"
        },
        "Exploitant" : {
            en : "Owner"
        },
        "Importer un exploitant" : {
            en : "Import a owner"
        },
        "Ajouter un exploitant" : {
            en : "Add an owner"
        },
        "Le format du mail est invalide." : {
            en : "The email format is invalid."
        },
        "Suppression de l'exploitant" : {
            en : "Removing the owner"
        },
        "Voulez-vous vraiment supprimer l'exploitant ?" : {
            en : "Do you really want to remove the owner ?"
        },
        "Vous ne pouve pas supprimer un exploitant déjà associé à un site." : {
            en : "You can't remove an owner already associated with a site."
        },
        "Importer un exploitant ( *.zip )" : {
            en : "Import owner ( *.zip )"
        },
        "Voir les contact" : {
            en : "View contacts"
        },
        "Retour à la liste des exploitants" : {
            en : "Back to the owners list"
        },
        "Fonction/Service" : {
            en : "Function/Service"
        },
        "Téléphone/Email" : {
            en : "Phone/Email"
        },
        "Ajouter un contact" : {
            en : "Add a contact"
        },
        "Suppression du contact" : {
            en : "Removin owner"
        },
        "Voulez-vous vraiment supprimer le contact ?" : {
            en : "Do you really want to remove the contact ?"
        },
        "Retour au site" : {
            en : "Back to the site"
        },
        "Désassignation du contact" : {
            en : "Contact Disassignment"
        },
        "Voulez-vous vraiment désassigner le contact du site ?" : {
            en : "Do you really want to disassociate the contact from the site?"
        },
        "Désassigner" : {
            en : "Unassign"
        },
        "Ajout de contact" : {
            en : "Adding contact"
        },
        "De quelle façon voulez vous ajouter un contact ?" : {
            en : "How would you like to add a contact ?"
        },
        "Assigner un contact" : {
            en : "Assign a contact"
        },
        "Créer un contact" : {
            en : "Create a contact"
        },
        "Sélectionnez des contacts dans la liste pour les assigner au site" : {
            en : "Select contacts from the list to assign them to the site"
        },
        "Générer graphe température" : {
            en : "Generate temperature graph"
        },
        "Générer graphes : " : {
            en : "Generate graphs : "
        },
        "Assigner" : {
            en : "Assign"
        },
        
    };

$(function() {

  $("#drapeaux div:nth-child(1) img").click(function(ev) {
   

      traductionFrançaise();
  });

  $("#drapeaux div:nth-child(2) img").click(function(ev) {
    


      traductionAnglaise();
  });



});




function traductionAnglaise()
{
    sessionStorage.setItem("Langue", "en");
    
    var _t = $('body').translate({lang: "fr", t: t});
    var str = _t.g("translate");
    
    var lang = "en"
    _t.lang(lang);

    
    
    $('input[name="champsSearch"]').attr('placeholder','Search');
    $('input[name="s2"]').attr('placeholder','Password');
    
    // OVERLAY EXPORT SITE
    $('input[name="mdpExport"]').attr('placeholder', 'Password');
    
    // MODIF PASSWORD
    $('input[type="password"]').attr('placeholder', 'Password');
    
    // AJOUTER COMPOSANT AU GAZ
     $('.compositionGaz li input[name="proportions"]').attr("placeholder", "Fill");
    
    // AJOUT ELEMENT CHIMIQUE
    $('.overlay-ajoutElementChimique input').attr('placeholder', 'Please fill in the field');
    
    //IMPORTER DONNEES CSV
    $('input[type="textarea"]').attr('placeholder', 'Comments');
    $('#overlay-importZipBottom input').val("Import");
    
    //NOUVEAU SITE
    $('#nouveauSiteContent #nouveauSiteTopLeft input').attr('placeholder', 'Please fill in this field');
    
    //CONTACT
    $('.ajouterNomContact input[name="nC1"]').attr('placeholder','Last Name');
    $('.ajouterNomContact input[name="nC2"]').attr('placeholder','First Name');
    $('.ajouterFonctionContact input[name="nC4"]').attr('placeholder','Function');
    $('.ajouterFonctionContact input[name="nC5"]').attr('placeholder','Service');
    $('.ajouterTelContact input[name="nC6"]').attr('placeholder','Phone');
    $('.ajouterTelContact input[name="nC7"]').attr('placeholder','Email');
    $('.ajouterMenuContact .ajouterContact input[name="nC7"]').attr('value','Save');
    
}


function traductionFrançaise()
{
    sessionStorage.setItem("Langue", "fr");   
 
    var _t = $('body').translate({lang: "fr", t: t});
    var str = _t.g("translate");
    
     var lang = "fr"
    _t.lang(lang);
    
    $('input[name="champsSearch"]').attr('placeholder','Rechercher');
    $('input[name="s2"]').attr('placeholder','Mot de passe');
    
    
    // OVERLAY MAIL
    $('.listeCaviteMail p span.trn').text("Cavité");
    
    // OVERLAY EXPORT SITE
    $('.overlay-exportSite p.overlay-title.trn').text("Export du site");
    $('.overlay-exportSite label.trn').text("Veuillez choisir le mot de passe du fichier zip créé :");
    $('input[name="mdpExport"]').attr('placeholder', 'Mot de passe');
     $('.overlay-exportSite p.boutonVert').text("Exporter");
     $('.overlay-exportSite p.boutonGris').text("Annuler");
     $('.overlay-exportSite p.erreurExport').text("Veuillez définir un mot de passe.");
    
     // MODIF PASSWORD
    $('input[type="password"]').attr('placeholder', 'Mot de passe');
    
    
    // LISTE COMPOSANT D'UN GAZ
    $('.compositionGaz li #composantsChimiques').siblings('label.trn').text("Composant chimique");
    $('.compositionGaz li input[name="proportions"]').attr("placeholder", "Remplir");
    $('.compositionGaz li input[name="masseMolaire"]').siblings('label.trn').text("Masse molaire");
    $('.compositionGaz li input[name="pressionCritique"]').siblings('label.trn').text("Pression critique");
    $('.compositionGaz li input[name="temperatureCritique"]').siblings('label.trn').text("Température critique");
    $('.compositionGaz li input[name="densite"]').siblings('label.trn').text("Densité");
    
    // AJOUT ELEMENT CHIMIQUE
    $('.overlay-ajoutElementChimique input').attr('placeholder', 'Veuillez remplir le champs');
    
    // LISTE DES ELEMENT CHIMIQUES
    $('#listeElementsChimiques .tr:first-child div:first-child').text("Nom");
    $('#listeElementsChimiques .tr:first-child div:nth-child(2)').text("Formule");
    $('#listeElementsChimiques .tr:first-child div:nth-child(3)').text("Masse molaire (g/g-mol)");
    $('#listeElementsChimiques .tr:first-child div:nth-child(4)').text("Pression critique (atmA)");
    $('#listeElementsChimiques .tr:first-child div:nth-child(5)').text("Température critique (°C)");
    $('#listeElementsChimiques .tr:first-child div:nth-child(6)').text("Densité");
    
    // IMPORTER DONNEES CSV
    $('input[type="textarea"]').attr('placeholder', 'Commentaires');
    $('#overlay-importZipBottom input').val("Importer");
    
    // CALAGE
    $('.sectionCalageTitre div:first-child p:first-child span').text('Du');
    $('.sectionCalageTitre div:nth-child(2) p:first-child span').text('Au');    
    
    $('.sectionCalageTitre div:first-child p:nth-child(2) span:nth-child(1)').text('Pression');
    $('.sectionCalageTitre div:first-child p:nth-child(2) span:nth-child(2)').text('ds');
    $('.sectionCalageTitre div:first-child p:nth-child(2) span:nth-child(4)').text('dév min');
    $('.sectionCalageTitre div:first-child p:nth-child(2) span:nth-child(6)').text('dév max');
    $('.sectionCalageTitre div:first-child p:nth-child(2) span:nth-child(9)').text('seq');
    
    $('.sectionCalageTitre div:nth-child(2) p:nth-child(2) span:nth-child(1)').text('Température');
    $('.sectionCalageTitre div:nth-child(2) p:nth-child(2) span:nth-child(2)').text('ds');
    $('.sectionCalageTitre div:nth-child(2) p:nth-child(2) span:nth-child(4)').text('dév min');
    $('.sectionCalageTitre div:nth-child(2) p:nth-child(2) span:nth-child(6)').text('dév max');
    $('.sectionCalageTitre div:nth-child(2) p:nth-child(2) span:nth-child(9)').text('seq');
    
    $('.sectionCalageContentLeft tr:first-child th:nth-child(2), .sectionCalageContentRight tr:first-child th:nth-child(3) ').text('V stockée');
    $('.sectionCalageContentLeft tr:first-child th:nth-child(3), .sectionCalageContentRight tr:first-child th:nth-child(4)').text('Débit');
    $('.sectionCalageContentLeft tr:first-child th:nth-child(4)').text('P simul');
    $('.sectionCalageContentRight tr:first-child th:nth-child(5)').text('T simul');
    
    $('.checkboxAfficherCalage span').text('Afficher sur le graphe');
    $('#graphesGestionCalageLeft h3').text('Pression');
    $('#graphesGestionCalageRight h3').text('Température');
    $('#listeInfoGraphe .infoGraphe:nth-child(1) p:nth-child(2)').text('Déviation standard');
    $('#listeInfoGraphe .infoGraphe:nth-child(2) p:nth-child(2)').text('Déviation min');
    $('#listeInfoGraphe .infoGraphe:nth-child(3) p:nth-child(2)').text('Déviation max');
    $('#listeInfoGraphe .infoGraphe:nth-child(4) p:nth-child(2)').text('Somme des écarts quadratiques');
    $('#listeInfoGraphe .infoGraphe:nth-child(5) p:nth-child(2)').text('Déviations moyennes');
    
    
    $('#gestionCalagesBottom .boutonRouge').text('Supprimer');
    $('#gestionCalagesBottom .boutonGris').text('Annuler');
    $('.sectionCalage #gestionCalagesBottom .boutonVert').text('Enregistrer');
    $('#nouveauCalage #gestionCalagesBottom .boutonVert').text('Ajouter');
    
    
    //NOUVEAU SITE
    $('#nouveauSiteContent #nouveauSiteTopLeft input').attr('placeholder', 'Veuillez remplir ce champs');
    
    //EXPLOITANTS
    $('.boutonExploitant .modifContact').text('Modifier');
    $('.boutonExploitant .voirContact').text('Voir les contacts');
    $('.boutonExploitant .exportExploitant').text('Exporter');
    $('.modifierBoutonExploitant .boutonTurquoise').text('Enregistrer');
    $('.modifierBoutonExploitant .boutonGris').text('Annuler');
    $('.modifierBoutonExploitant .boutonRouge').text('Supprimer');
    
    
    //CONTACT
    $('.ajouterNomContact input[name="nC1"]').attr('placeholder','Name');
    
    $('.informationContact .modifierContact span').text('Modifier');
    $('.informationContact .SupprimerContact span').text('Supprimer');
    $('.informationContact .enregistrerContact span').text('Enregistrer');
    $('.informationContact .annulerContact span').text('Annuler');
    
    $('.ajouterNomContact input[name="nC1"]').attr('placeholder','Nom');
    $('.ajouterNomContact input[name="nC2"]').attr('placeholder','Prénom');
    $('.ajouterFonctionContact input[name="nC4"]').attr('placeholder','fonction');
    $('.ajouterFonctionContact input[name="nC5"]').attr('placeholder','Service');
    $('.ajouterTelContact input[name="nC6"]').attr('placeholder','Téléphone');
    $('.ajouterTelContact input[name="nC7"]').attr('placeholder','Email');
    
    
    
    $('.ajouterMenuContact .ajouterContact input[name="nC7"]').attr('value','Enregistrer');
    
}

















