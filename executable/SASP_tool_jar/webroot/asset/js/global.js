


var mouseX;
var mouseY;
$(document).mousemove( function(e) {
   mouseX = e.pageX; 
   mouseY = e.pageY;
});  


    
    

//Lorsque un utilisateur essaye de se connecter
var siteNameConnexion ; 
var passwordConnexion ; 







$( document ).ready(function() 
{    
  
    
    
    
    
    /* Afficher champs pour se connecter */
    $(document).on('click','div.boutonConnexion p',function()
    {
        $('.formConnexion').slideUp();
        $('.boutonConnexion p').slideDown();
        
        $(this).slideUp('fast');
        $(this).siblings('.formConnexion').slideDown('fast');
        $(this).parent().find('.formConnexion input[type="password"]').focus();
        
        
    });
    
        /* Afficher le formulaire pour ajouter un exploitant */
    $(document).on('click','#nouveauSiteNouveauExploitantRight p',function()
    {
       $('.overlay-nouvelExploitant').stop().fadeToggle('fast');
    });
    
    
        /* Cacher le formulaire pour ajouter un exploitant - Bouton annuler */
    $(document).on('click','#nouvelExploitantLeft .boutonGris',function()
    {
       $('.overlay-nouvelExploitant').stop().fadeOut();
    });
    
    
        /* Afficher le formulaire pour ajouter une cavité */
    $(document).on('click','p.boutonAjoutCavite',function()
    {
        $('#modifierCavite').stop().slideUp();
        $('#nouvelleCavite').stop().slideToggle();
        
    });
        
        /* Afficher le formulaire pour modifier une cavité */
    $(document).on('click','.boutonModifierCavite',function()
    {
        //si une cavité est sélectionnée
        if ($('select#choix_cavite').val() != null)
            {
                $('input[name="mC1"]').attr('ancienNomCavite', $('select#choix_cavite').val());
                $('input[name="mC1"]').val($('select#choix_cavite').val());
                $('input[name="mC2"]').val($('input[name="infoCavite1"]').val());
                $('input[name="mC3"]').val($('input[name="infoCavite2"]').val());
                $('input[name="mC4"]').val($('input[name="infoCavite3"]').val());
                $('input[name="mC5"]').val($('input[name="infoCavite4"]').val());
                
                $('#nouvelleCavite').stop().slideUp();
                $('#modifierCavite').stop().slideToggle();
            }
        
    });
    
    
    /* Supprimer une cavité */
    $(document).on('click','#choixCavite #choixCaviteRight p.boutonRouge',function()
    {
        
        //Si une cavité est sélectionnée
        if ($('select#choix_cavite').val() != null)
            {  
                //fermer les autres overlays
                $('.overlay > div').fadeOut();
                
                $('.overlay-suppressionCavite').fadeIn();
                $('.overlay').fadeIn();
            }
    });
        
    /* Supprimer une cavité */
    $(document).on('click','#suppressionCaviteBottom p.boutonVert',function()
    {
        var passwordCavite = $('.overlay-suppressionCavite input').val();
           
        if (passwordCavite != "" )
            supprimerCavite(passwordCavite);
        else
            $('p.erreurSupressionCav').slideDown();
        
    });
    
    
        /* Fermer l'overlay pour supprimer une cavité */
    $(document).on('click','#suppressionCaviteBottom p.boutonGris',function()
    {
        
        $('.overlay-suppressionCavite').fadeOut();
        $('.overlay').fadeOut()
        
    });
    
    
          /**     Afficher import de site / exploitants / ...    **/
    $(document).on('click','p.importZipFile, .overlay-optionCorrectionDonnees .importerCSV', function()
    {
        $('.overlay-optionCorrectionDonnees').fadeOut();
        $('.overlay').fadeIn();
        $('.overlay-importZip').fadeIn();
    });
    
    
          /**     Cacher import de site / exploitants / ...    **/
    $(document).on('click','.overlay-importZip p.boutonGris', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-importZip').fadeOut();
    });
    
   
    
       
        
    /**
    *   Page : index.html
    *
    *    Au submit du formulaire IMPORT SITE, vérifier qu'il y a bien un fichier
    **/
    $(document).on('submit','.overlay-importZip form.importSite', function()
    {
        $('*').css('cursor','wait');
        
        var fileRemplie =  $("input[name='importS1']").val();
        var boolFileRemplie = fileRemplie.length != 0;
        
        var mdpImportRempli =  $("input[name='importS3']").val().length != 0 ;
        
        if (!fileRemplie)
            $("input[name='importS1']").css('border-bottom','1px solid #e90d3f');
        else
            $("input[name='importS1']").css('border-bottom','1px solid #757575');
            
        if (!mdpImportRempli)
            $("input[name='importS3']").css('border-bottom','1px solid #e90d3f');
        else
            $("input[name='importS3']").css('border-bottom','1px solid #757575');
            
        if ( !(boolFileRemplie && mdpImportRempli) ) //s'il y a une erreur, on rétablit le curseur
            reinitialiserCurseur();
        else
            $('div#chargementPage').fadeIn();
        
        return (boolFileRemplie && mdpImportRempli);
    });
    
    
    /**
    *   Page : exploitant.html
    *
    *    Au submit du formulaire IMPORT EXPLOITANT, vérifier qu'il y a bien un fichier
    **/
    $(document).on('submit','.overlay-importZip form.importExploitant', function()
    {
        $('*').css('cursor','wait');
        
        var fileRemplie =  $("input[name='importE1']").val();
        var boolFileRemplie = fileRemplie.length != 0;
        
        
        
        if (!fileRemplie)
            $("input[name='importE1']").css('border-bottom','1px solid #e90d3f');
        else
            $("input[name='importE1']").css('border-bottom','1px solid #757575');
            
        
        if ( !boolFileRemplie ) //s'il y a une erreur, on rétablit le curseur
            reinitialiserCurseur();
        else
            $('div#chargementPage').fadeIn();
            
        return (boolFileRemplie);
    });
        
    
    /**
    *   Page : caviteSite.html
    *
    *    Au submit du formulaire IMPORT CAVITE, vérifier qu'il y a bien un fichier
    **/
    $(document).on('submit','.overlay-importZip form.importCavite', function()
    {
        $('*').css('cursor','wait');
        
        var fileRemplie =  $("input[name='importC1']").val();
        var boolFileRemplie = fileRemplie.length != 0;
        
        var mdpImportRempli =  $("input[name='importC3']").val().length != 0 ;
        
        if (!fileRemplie)
            $("input[name='importC1']").css('border-bottom','1px solid #e90d3f');
        else
            $("input[name='importC1']").css('border-bottom','1px solid #757575');
            
        if (!mdpImportRempli)
            $("input[name='importC3']").css('border-bottom','1px solid #e90d3f');
        else
            $("input[name='importC3']").css('border-bottom','1px solid #757575');
               
        if ( !(boolFileRemplie && mdpImportRempli) ) //s'il y a une erreur, on rétablit le curseur
            reinitialiserCurseur();
        else
            $('div#chargementPage').fadeIn();
        
        return (boolFileRemplie && mdpImportRempli);
    });
    
    
        
    
    /**
    *   Page : gererDonneesRelevees.html
    *
    *   Au submit du formulaire, insérer les champs (nomCavité, nom du site) dans les values
    **/
    $(document).on('submit','.overlay-importZip form.importDonnees', function()
    {
        $('*').css('cursor','wait');
        
        if( $("input[name='importCSV5']").val().length == 0)
            $("input[name='importCSV5']").val("");
        
        $('input[name="importCSV3"]').val(sessionStorage.getItem("NomCaviteActuel")); //récuprérer nom du site actuel et l'insérer
        
        $('input[name="importCSV2"]').val(sessionStorage.getItem("NomSiteActuel")); //récuprérer nom du site actuel et l'insérer
        
        
        var fileRemplie = $("input[name='importCSV1']").val();
        var boolFileRemplie = fileRemplie.length != 0;
        
        
        if (!boolFileRemplie)
            $("input[name='importCSV1']").css('border-bottom','1px solid #e90d3f');
        else
            $("input[name='importCSV1']").css('border-bottom','1px solid #757575');
            
            
        if (boolFileRemplie)
            {
                reinitialiserCurseur();
                $('div#chargementPage').fadeIn();
            }
        
        return boolFileRemplie;
    });
    
    
    
    
    
        /* Afficher les tableaux - Pages : Gestion des calages */
    $(document).on('click','#section2CalagesTitre img',function()
    {
        $('#section2CalagesContent').stop().slideToggle();
        
        if ($('#section2CalagesTitre img').attr('src') == "../asset/img/caviteSite/flecheBasVert.png")
            $(this).attr('src','../asset/img/caviteSite/flecheHautVert.png');
        else
            $(this).attr('src','../asset/img/caviteSite/flecheBasVert.png');
    });
     
    
    
        /**     Afficher Overlay    **/
    $(document).on('click','.menuContact p.SupprimerContact', function()
    {
        $('.overlay').fadeIn();
        $('.overlay-suppressionContact').fadeIn();
        
        //Récupérer les informations du contact pour une suppression éventuelle
        
        var siteNameContact = $(this).parent().siblings('.sitesContact').find('span.siteNameContact').html();
        var idContact = $(this).parent().parent().attr('id');
        var exploitantContact = $(this).parent().parent().find('p.entrepriseContact').html();
          
        
      
        if (siteNameContact == null)
            siteNameContact = "";
      
        sessionStorage.setItem("suppressionContactSiteName", siteNameContact); //Assigner le nom du site du contact
        sessionStorage.setItem("suppressionContactId", idContact); //Assigner le id du contact
        sessionStorage.setItem("suppressionContactExploitant", exploitantContact); //Assigner le nom de l'exploitant du contact  
        
        
    });
    
        /**     Cacher l'overlay    **/
    $(document).on('click','.overlay-suppressionContact p.boutonGris', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-suppressionContact').fadeOut();
    });
    
     
    
    
    /**
    *   Page : contacts.html
    *
    *   Afficher / cacher section de modification d'un contact + rétablir valeur si action est de cacher
    */  
    $(document).on('click','.menuContact p.modifierContact', function()
    {
        var contactAModifier = $(this).parent().parent().attr('id');
        
        if ( $('#'+ contactAModifier + ' .modifierInformationContact').css('display') == 'block')
        {
            var formAnnuler = $('#'+ contactAModifier + ' form');

            $(formAnnuler).find('input').each(function()
            {
                $(this).val(  $(this).attr('initialvalue') );
            });
        
        }
        
        $('#'+ contactAModifier + ' .modifierInformationContact').stop().slideToggle();
    });
    
    
    /**
    *   Page : contacts.html
    *
    *   Cacher section de modification d'un contact et rétablir les valeurs
    */
    $(document).on('click','.modifierMenuContact p.annulerContact', function()
    {
        var formAnnuler = $(this).parent().parent();
        
        $(formAnnuler).parent().stop().slideUp();
    
        $(formAnnuler).find('input').each(function()
        {
            $(this).val(  $(this).attr('initialvalue') );
        });
        
    });    
    
    
    
    /** 
    *   Page : contacts.html
    *
    *   Enregistrer les modifications pour un contact
    **/
    $(document).on('click','.modifierMenuContact p.enregistrerContact', function()
    {
        var ancienNomContact =  $(this).parent().siblings('.modifierNomContact').find('input[name="mC12"]').val();
        
        var ancienPrenomContact =   $(this).parent().siblings('.modifierNomContact').find('input[name="mC11"]').val()
    
        var tousChampsRempli = true;
        
        var img =   $(this).parent().siblings('.modifierNomContact').find('#file-input-nE6').val();
        var boolImgRemplie = img.length != 0;
        
        if(boolImgRemplie)
            $(this).parent().parent().attr('action','/api/updateContactWithImage');
        else
            $(this).parent().parent().attr('action','/api/updateContact');
            
         
        
        //Vérifier les champs
        $(this).parent().parent().find('input').each(function()
        {
            if ($(this).val() == "" && $(this).attr('type') != "file" ) //Si la valeur d'un champs est vide et que ce n'est pas le champs file pour insérer image
                { 
                    $(this).css('border-color','#e90d3f'); //Styliser le champs
                    tousChampsRempli = false; //Minimum un des champs est vide
                }
            else
                {
                    $(this).css('border-color','rgba(177, 177, 177, 0.40)');
                }
        });
        
        if(tousChampsRempli)
            {
                $(this).parent().parent().submit();
                $('div#chargementPage').fadeIn();
            }
    });
    
    
    
    
    
          /** Afficher overlay  de désassignation d'un contact **/
    $(document).on('click','.menuContact p.desassignerContact', function()
    {
        var contactAModifier = $(this).parent().parent().attr('id');
        
        
        var siteNameContact = $(this).parent().siblings('.sitesContact').find('span.siteNameContact').html();
        var idContact = $(this).parent().parent().attr('id');
        
        
        sessionStorage.setItem("desassignationContactSiteName", siteNameContact); //Assigner le nom du site du contact
        sessionStorage.setItem("desassignationContactId", idContact); //Assigner le id du contact
        
        $('.overlay').fadeIn();
        $('.overlay-desassignationContact').fadeIn();
    });
        
          /** Cacher overlay  de désassignation d'un contact **/
    $(document).on('click','.overlay-desassignationContact p.boutonGris', function()
    {        
        $('.overlay').fadeOut();
        $('.overlay-desassignationContact').fadeOut();
    });
    
    
        /** Afficher section pour ajouter un contact **/
    $(document).on('click','div#ajouterContactExploitant p.boutonVert', function()
    {
        $('div.ajouterInformationContact').stop().slideToggle('fast');
    });
    
    
        /** Cacher section d'ajout d'un contact **/
    $(document).on('click','#contacts .ajouterMenuContact p.annulerContact', function()
    {
        $('div.ajouterInformationContact').stop().fadeToggle('fast');
    });
    
    
    
        /** Afficher section pour ajouter un contact lié à un site **/
    $(document).on('click','#ajouterContactSite p.boutonVert', function()
    {
        $('.overlay').fadeIn('fast');
        $('.overlay-choixAjouterContact').fadeIn('fast');
    });
    
    
        /** Cacher section d'ajout d'un contact **/
    $(document).on('click','.overlay-choixAjouterContact p.boutonGris', function()
    {
        $('.overlay').fadeOut('fast');
        $('.overlay-choixAjouterContact').fadeOut('fast');
    });
    
    
    
    
    
        /** Afficher section pour assigner un contact qui n'a pas de site **/
    $(document).on('click','.overlay-choixAjouterContact p.ajouterContactBottomAssigner', function()
    {
        $('.overlay-choixAjouterContact').fadeOut('fast');
        $('.overlay-assignerContactSite').fadeIn('fast');
    });
    
    
        /** Cacher section pour assigner un contact qui n'a pas de site **/
    $(document).on('click','.overlay-assignerContactSite p.boutonGris', function()
    {
        $('.overlay-choixAjouterContact').fadeIn('fast');
        $('.overlay-assignerContactSite').fadeOut('fast');
    });
    
    
        /** Cacher section d'ajout d'un contact **/
    $(document).on('click','.overlay-assignerContactSite p.boutonVert', function()
    {
        var tableauContactAAssigner = $('.overlay-assignerContactSite select').val();
                
        assignerContactsSite(tableauContactAAssigner);
    });
        
       
        /** Afficher section pour créer un contact **/
    $(document).on('click','.overlay-choixAjouterContact p.ajouterContactBottomCreer', function()
    {
        $('.overlay-choixAjouterContact').fadeOut('fast');
        $('.overlay-creerContactSite').fadeIn('fast');
    });
    
    
        /** Cacher section pour créer un contact **/
    $(document).on('click','.overlay-creerContactSite p.boutonGris', function()
    {
        $('.overlay-choixAjouterContact').fadeIn('fast');
        $('.overlay-creerContactSite').fadeOut('fast');
    });
    
    
    /**
    *   Page : parametreSite.html
    *
    *   Ajouter une ligne pour ajouter un composant
     **/
    $(document).on('click','.ajoutComposant', function()
    {
        if(  $(this).parent().parent().siblings('ul').find('li:last-child input[name="proportions"]').val() != "")
            {
                if (sessionStorage.getItem("Langue") == "fr")     
                    $(this).parent().siblings('ul').append('<li class=""><div class="infoComposant"> <label class="trn">Composant chimique</label><select id="composantsChimiques" list="composantsChimiques" class="searchfieldjs" id="choix_composant">' + stringListeComposantOptionGeneral + '</select> </div> <div class="infoComposant"> <label>Proportions (%)</label> <input type="text" name="proportions" id="" class="searchfieldjs" placeholder="Remplir" value="" disabled="disabled">  </div> <div class="infoComposant">   <label class="trn">Masse molaire</label><input type="text" name="masseMolaire" id="" class="searchfieldjs" value="" disabled="disabled">  </div> <div class="infoComposant">   <label class="trn">Pression critique</label> <input type="text" name="pressionCritique" id="" class="searchfieldjs" value="" disabled="disabled">  </div> <div class="infoComposant">     <label class="trn">Température critique</label><input type="text" name="temperatureCritique" id="" class="searchfieldjs" value="" disabled="disabled"> </div>  <div class="infoComposant">     <label class="trn">Densité</label><input type="text" name="densite" id="" class="searchfieldjs" value="" disabled="disabled"> </div>  <div class="infoComposant supprimerComposant"><img src="../asset/img/exploitants/supprimer.png"/> </div> </li>');
                else
                     $(this).parent().siblings('ul').append('<li class=""><div class="infoComposant"> <label class="trn">Chemical component</label><select id="composantsChimiques" list="composantsChimiques" class="searchfieldjs" id="choix_composant">' + stringListeComposantOptionGeneral + '</select> </div> <div class="infoComposant"> <label>Proportions (%)</label> <input type="text" name="proportions" id="" class="searchfieldjs" placeholder="Fill" value="" disabled="disabled">  </div> <div class="infoComposant">   <label class="trn">Molar mass</label><input type="text" name="masseMolaire" id="" class="searchfieldjs" value="" disabled="disabled">  </div> <div class="infoComposant">   <label class="trn">Critical pressure</label> <input type="text" name="pressionCritique" id="" class="searchfieldjs" value="" disabled="disabled">  </div> <div class="infoComposant">     <label class="trn">Critical temperature</label><input type="text" name="temperatureCritique" id="" class="searchfieldjs" value="" disabled="disabled"> </div>  <div class="infoComposant">     <label class="trn">Density</label><input type="text" name="densite" id="" class="searchfieldjs" value="" disabled="disabled"> </div>  <div class="infoComposant supprimerComposant"><img src="../asset/img/exploitants/supprimer.png"/> </div> </li>');
                
            }
           
            
    });
    
    
        /** Supprimer un composant **/
    $(document).on('click','.compositionGaz .supprimerComposant', function()
    {
        $(this).parent().slideUp("fast", function()
        {
             $(this).remove();
        });
        
    });
    
        
    /**
    *   Page : parametreSite.html
    *
    *   Afficher Overlay pour ajouter un nouvel élément chimique  
    */
    $(document).on('click','#menuComposition p.nouvelElement', function()
    {
        $('.overlay').fadeIn();
        $('.overlay-ajoutElementChimique').fadeIn();
    });
    
    
    /**
    *   Page : parametreSite.html
    *
    *   Cacher l'overlay qui permet d'ajouter un nouvel élément chimique  
    */   
    $(document).on('click','.overlay-ajoutElementChimique p.boutonGris', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-ajoutElementChimique').fadeOut();
    });
    
    
    /**
    *   Page : parametreSite.html
    *
    *   Afficher l'overlay pour avoir la liste des éléments chimiques  
    */
    $(document).on('click','#menuComposition p.listeElements', function()
    {
        $('.overlay').fadeIn();
        $('.overlay-listeElementsChimiques').fadeIn();
    });
    
     /**
    *   Page : parametreSite.html
    *
    *   Cacher l'overlay qui permet d'afficher la liste des éléments chimiques  
    */
    $(document).on('click','#overlay-listeElementsChimiquesBottom p.boutonRouge', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-listeElementsChimiques').fadeOut();
    });
    
 
    
    /**
    *   Page : parametreSite.html
    *
    *   Afficher l'overlay demander si l'utilisateur veut enregistrer les modifications de l'élément  
    */
    $(document).on('click','div.validerModif img', function()
    {
        $('.overlay-double').fadeIn();
        $('.overlay-validationModification').fadeIn();
        
        modificationElementAncienNom = $(this).parent().parent().find('input[name="nomElement"]').attr('anciennom');
        modificationElementNouveauNom = $(this).parent().parent().find('input[name="nomElement"]').val();
        modificationElementNouveauFormule = $(this).parent().parent().find('input[name="formule"]').val();
        modificationElementNouveauMasseMolaire = $(this).parent().parent().find('input[name="masseMolaire"]').val();
        modificationElementNouveauPression =  $(this).parent().parent().find('input[name="pression"]').val();
        modificationElementNouveauTemperature = $(this).parent().parent().find('input[name="temperature"]').val();
        modificationElementNouveauDensite = $(this).parent().parent().find('input[name="densite"]').val();

        
        //Parse le string en int
        modificationElementNouveauMasseMolaire = parseFloat(modificationElementNouveauMasseMolaire);
        modificationElementNouveauPression = parseFloat(modificationElementNouveauPression);
        modificationElementNouveauTemperature = parseFloat(modificationElementNouveauTemperature);
        modificationElementNouveauDensite = parseFloat(modificationElementNouveauDensite);
        
    });
    
    
    /**
    *   Page : parametreSite.html
    *
    *   Fermer l'overlay pour annuler la modification d'un élément  
    **/
    $(document).on('click','#validationModificationBottom p.boutonGris', function()
    {
        $('.modifElementChampsVide').fadeOut();
        $('.overlay-double').fadeOut();
        $('.overlay-validationModification').fadeOut();
    });
    
    /**
    *   Page : parametreSite.html
    *
    *   Vérifier les champs pour savoir si on peut eregistrer les modifications choisies pour un élément  
    **/
    $(document).on('click','#validationModificationBottom p.boutonVert', function()
    {
            
        
        if (modificationElementNouveauNom == "" || modificationElementNouveauFormule == "" || modificationElementNouveauMasseMolaire == "" || modificationElementNouveauPression == "" || modificationElementNouveauTemperature == "" || !validateChampsNombre(modificationElementNouveauMasseMolaire) || !validateChampsNombre(modificationElementNouveauPression) || !validateChampsNombre(modificationElementNouveauTemperature) || !validateChampsNombre(modificationElementNouveauDensite))
             $('.modifElementChampsVide').slideDown();
        else
            modificationElement();
    });
    
     
    /**
    *   Page : parametreSite.html
    *
    *   A la modification d'un champs lorsque l'utilisateur souhaite modifier des informations d'un élément, vérifier le champs
    */
    $(document).on('change','#listeElementsChimiques input[name="masseMolaire"], #listeElementsChimiques input[name="pression"], #listeElementsChimiques input[name="temperature"] ', function()
    {
        
        if( $(this).val() == "" || !validateChampsNombre($(this).val()) )
            $(this).css("border-color","#e90d3f");
        else
            $(this).css("border-color","rgba(177, 177, 177, 0.29)");
     
    });
    
    
    /**
    *   Page : parametreSite.html
    *
    *   Fermer l'overlay pour informant l'enregistrer qui n'a pas été fait
    **/
    $(document).on('click','.overlay-erreurNouveauNomElement p.boutonGris', function()
    {
        $('.overlay-double').fadeOut();
        $('.overlay-erreurNouveauNomElement').fadeOut();
    });
    
      
    /**
    *   Page : parametreSite.html
    *
    *   Afficher l'overlay demander si l'utilisateur veut supprimer un élément  
    */
    $(document).on('click','div.supprimerElement img', function()
    {
        $('.overlay-double').fadeIn();
        $('.overlay-validationSupression').fadeIn();
        
        suppressionElementNom = $(this).parent().parent().find('input[name="nomElement"]').attr('anciennom');
    });
    
    
    /*   Page : parametreSite.html
    *
    *   Fermer l'overlay pour supprimer un élément  
    **/
    $(document).on('click','#validationSuppressionBottom p.boutonGris', function()
    {
        $('.overlay-double').fadeOut();
        $('.overlay-validationSupression').fadeOut();
    });
    
    
   /**
    *   Page : parametreSite.html
    *
    *   Supprimer un élément  
    **/
    $(document).on('click','#validationSuppressionBottom p.boutonVert', function()
    {  
            suppressionElement();
    });
       
    
    
    
  
    
    
    
    
    /*  Assigner nom de l'exploitant dans une variable pour récupérer la liste des contacts associés */
    $(document).on('click','.informationExploitant p.voirContact', function()
    {
        //Récupérer le nom de l'exploitant dont on veut voir les contacts
        var exploitant = $(this).parent().parent().parent().find('p.ancienNomExploitant').html();
            
        sessionStorage.setItem("NomExploitantActuel", exploitant)
    });
    
    
    
    
        
    /**  Fermer le menu Option de correction si on ne clique pas dessus   */
    $(document).on('click','.overlay-optionCorrectionDonnees p.boutonGris', function()
    {
        $('.overlay-optionCorrectionDonnees').fadeOut();
    });
    
    
    
    
    
    
        /**  Afficher la liste des données (température / pression / volume) pour les corriger   */
    $(document).on('click','div.overlay-optionCorrectionDonnees p.corriger', function()
    {
       $('div.overlay-correctionManuelleDonnees').fadeToggle();
        
        $(this).parent().fadeToggle();
    });
    
    
    
        /**  Afficher le champs input pour ajouter une valeur (température / pression / volume)   */
    /*  $(document).on('click','div.overlay-correctionManuelleDonnees td.sansDonnee p.ajouter', function()
    {
            $('div.overlay-correctionManuelleDonnees td.sansDonnee input').hide();
            $('div.overlay-correctionManuelleDonnees td.sansDonnee p').show();
            $(this).hide();
            $(this).siblings('input').fadeIn('fast');
    });*/
    
    
    
    
    /**  Cacher la section et sauvegarder les données (température / pression / volume)*/
    $(document).on('click','div#overlay-correctionManuelleDonneesBottom p.boutonVert, div#overlay-conflitDonneesDonneesBottom p.boutonVert', function()
    {
        tableauFinalCorrectionManuel = [];
        erreurNulCorrectionManuel = false;
        var champsErreur = false;
        var commentaire = $('textarea[name="logCommentaire"]').val();
        erreurNulCorrectionDate = "";
        
        $('.overlay-correctionManuelleDonnees #listeDonneesVides tr').each(function()
        {
            if ( $(this).attr('raison') == "incoherent") //On traite différement si nous sommes sur une ligne où une (des) valeurs est incohérente ou manquante
                {
                    valPression =   $(this).find('td:nth-child(3)').attr('valchamps');
                    valTemp =     $(this).find('td:nth-child(5)').attr('valchamps');
                    valVolume =      $(this).find('td:nth-child(7)').attr('valchamps');
                    valDebit =      $(this).find('td:nth-child(9)').attr('valchamps');
                    
                    $(this).find('.sansDonnee input').each(function()
                    {
                        if ($(this).val() != ""  )
                        {
                            if ($(this).attr('typeChamps') == "pres")
                                valPression = $(this).val();
                       
                            if ($(this).attr('typeChamps') == "temp")
                                valTemp = $(this).val();

                            if ($(this).attr('typeChamps') == "vol")
                                valVolume = $(this).val();  
                            
                            if ($(this).attr('typeChamps') == "deb")
                                valDebit = $(this).val();   
                        }
                        
                        if ( !validateChampsNombre( $(this).val() ) ) //Le champs est incorrect
                        {
                            $(this).css('color','#e90d3f');
                            champsErreur = true; 
                         }
                    });
                     
                    if (!champsErreur) //Si le champs n'est pas incorrect
                        tableauFinalCorrectionManuel.push({ 'Date': $(this).find("td:first-child").text(), 'Pression':valPression, 'Temperature' :valTemp, 'Volume': valVolume, 'Debit':valDebit  });
                }
            else 
                {
                    //récupérer l'ancienne valeur du champs input   
                     valPression =   $(this).find('td:nth-child(2) input').val();
                     valTemp =   $(this).find('td:nth-child(4) input').val();
                     valVolume =   $(this).find('td:nth-child(6) input').val();
                     valDebit =   $(this).find('td:nth-child(8) input').val();
                     
                      if (valPression != "" || valTemp != "" || valVolume != "" || valDebit != "" )
                        { //si un des champs est remplis (pour renseignrer à l'utilisateur qu'éventuellement qu'un des autres champs de la ligne n'est pas renseigné)
                            
                            if (valPression != "" && valTemp != "" && valVolume != "" && valDebit != "" )
                            { //tous les champs sont renseignés
                                
                                 if (!validateChampsNombre(valPression) && valPression != undefined )
                                 {
                                      $(this).find('td:nth-child(2) input').css('color','#e90d3f');
                                     champsErreur = true;
                                 }

                                if (!validateChampsNombre(valTemp) && valTemp != undefined)
                                 {
                                      $(this).find('td:nth-child(4) input').css('color','#e90d3f');
                                     champsErreur = true;
                                 }

                                if (!validateChampsNombre(valVolume) && valVolume != undefined)
                                 {
                                      $(this).find('td:nth-child(6) input').css('color','#e90d3f');
                                     champsErreur = true;
                                 }

                                if (!validateChampsNombre(valDebit) && valDebit != undefined)
                                 {
                                      $(this).find('td:nth-child(8) input').css('color','#e90d3f');
                                      champsErreur = true;
                                 }
                                
                                if (!champsErreur)  // Les champs sont valides
                                {
                                    tableauFinalCorrectionManuel.push({ 'Date': $(this).find("td:first-child").text(), 'Pression':valPression, 'Temperature' :valTemp, 'Volume': valVolume, 'Debit':valDebit });
                                }
                            }

                            else
                            {
                                erreurNulCorrectionDate = erreurNulCorrectionDate + $(this).find("td:first-child").text() + " <br> ";
                                erreurNulCorrectionManuel = true;
                            }
                        }
                }
         });
           
        tableauFinalCorrectionManuel.shift(); //supprimer la premiere ligne du tableau
        tableauFinalCorrectionManuel.shift();
            
        if (!champsErreur && !erreurNulCorrectionManuel )//pas d'erreurs de détectées et TOUS les champs d'une ligne NULL sont remplis
        {
            confirmDataCorrectionManuelle(tableauFinalCorrectionManuel, commentaire);
            $('div#chargementPage').fadeIn();
        }
            
        
        if (!champsErreur && erreurNulCorrectionManuel) //pas d'erreurs détectées et UN champs est manquant dans une ligne de NULL
        {
            $('.overlay-validationCorrection').fadeIn();
            $('.overlay-validationCorrection .overlay-validationCorrectionDate').html(erreurNulCorrectionDate);
        }
        
    });
    
    
    
        /**  Cacher la section et sauvegarder les données (température / pression / volume)*/
    $(document).on('click','div#overlay-validationCorrectionBottom p.boutonVert', function()
    {
        var commentaire = $('textarea[name="logCommentaire"]').val();
        
        confirmDataCorrectionManuelle(tableauFinalCorrectionManuel, commentaire);
    });
    
        /**  Cacher la section et sauvegarder les données (température / pression / volume)*/
    $(document).on('click','div#overlay-validationCorrectionBottom p.boutonGris', function()
    {
            $('.overlay-validationCorrection').fadeOut();
    });
    
    
    
    
    
    
   
    
        
        /**  Cacher la section et annuler l'ajout de données (température / pression / volume)   */
    $(document).on('click','div#overlay-correctionManuelleDonneesBottom p.boutonGris, #overlay-conflitDonneesDonneesBottom p.boutonGris', function()
    {
        $('.overlay-correctionManuelleDonnees input').val("");
        
        $(this).parent().parent().parent().fadeOut();
        
        
    });
    
    

        
    /**  
    *   Page : caviteSite.html  
    *
    *   Au click du bouton de suppresion d'un site, affficher overlay
    **/
    $(document).on('click','div#caviteSite #supressionCavite p.boutonRouge', function()
    {
        //fermer les autres overlays
        $('.overlay > div').fadeOut();
        
        $('.overlay').fadeIn();
        $('.overlay-suppressionSite').fadeIn();
    });
    
    
    
    /**  
    *       Page caviteSite.html  
    *
    *   Au click du bouton de modification d'un mot de passe, affficher overlay  
    **/
    $(document).on('click','div#caviteSite #supressionCavite p.boutonGris', function()
    {
        //fermer les autres overlays
        $('.overlay > div').fadeOut();
        
        $('.overlay').fadeIn();
        $('.overlay-modificationMdp').fadeIn();
    });
    
    
    
    
    /**  
    *   Page : caviteSite.html
    *
    *   Suppresion d'un site, quitter l'overlay, bouton "Annuler"
    **/   
    $(document).on('click','#suppressionSiteBottom p.boutonGris', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-suppressionSite').fadeOut();
    });
    
        
    /**  
    *   Page : caviteSite.html
    *
    *   Modification du mot de passe d'un site, quitter l'overlay, bouton "Annuler"
    **/   
    $(document).on('click','#modifMdpSiteBottom p.boutonGris', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-modificationMdp').fadeOut();
    });
    
    
    /**  
    *   Page : caviteSite.html  
    *
    *   Au click du bouton de modification d'un site, affficher overlay
    **/
    $(document).on('click','div#caviteSite .modifierSite', function()
    {
        //fermer les autres overlays
        $('.overlay > div').fadeOut();
        
        $('.overlay').fadeIn();
        $('.overlay-modificationSite').fadeIn();
    });
    
    /**  
    *   Page : caviteSite.html
    *
    *   Modification d'un site, quitter l'overlay, bouton "Annuler"
    **/   
    $(document).on('click','.overlay-modificationSite p.boutonGris', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-modificationSite').fadeOut();
    });
    
        
    /**  
    *   Page : caviteSite.html
    *
    *   Modification d'un site, quitter l'overlay, bouton "Annuler"
    **/   
    $(document).on('click','.overlay-modificationSite p.boutonVert', function()
    {
        var img =  $('input#file-input-mS5').val();
        var boolImgRemplie = img.length != 0;
        
        if(boolImgRemplie)
            $('form#formModificationSite').attr('action','/api/updateSiteWithImage');
        else
            $('form#formModificationSite').attr('action','/api/updateSite');
        
        //Si les champs sont vide, les remplir avec les valeurs initiales 
        $('form#formModificationSite input[type="text"]').each(function()
        {
            if ( $(this).val() == "" )
                $(this).val( $(this).attr('initialValue') );
        });
        
        sessionStorage.setItem("NomSiteActuel", $('input[name=mS1]').val());    
        
        $('form#formModificationSite').submit();
        
    });
    
        
        
        
    /**  
    *   Page : caviteSite.html  
    *
    *   Au click du bouton de modification d'un site, affficher overlay
    **/
    $(document).on('click','#topMenu .boutonMailCavite', function()
    {
        //fermer les autres overlays
        $('.overlay > div').fadeOut();
        
        $('.overlay').fadeIn();
        $('.overlay-mail').fadeIn();
    });
    
    /**  
    *   Page : caviteSite.html
    *
    *   Modification d'un site, quitter l'overlay, bouton "Annuler"
    **/   
    $(document).on('click','.overlay-mail p.boutonGris', function()
    {
        $('.overlay-mail input').val("");
        $('.overlay-mail input').prop('checked', false);
        
        $('.overlay').fadeOut();
        $('.overlay-mail').fadeOut();
    });
    
        
    /**  
    *   Page : caviteSite.html
    *
    *   Modification d'un site, quitter l'overlay, bouton "Annuler"
    **/   
    $(document).on('click','.overlay-mail p.boutonVert', function()
    {
        //récupérer liste des checkbox qui ont été cochés
        var tabCaviteCheck = [];
        
        //récupérer champs email (traité par le serveur)
        var champsMails = $('input#mailCavite').val() ;  //liste des mails
        
        var champsMdp = $('input#mailMdp').val() ;  //mot de passe du zip créé 
        
         $('.overlay-mail input[type="checkbox"]').each(function()
            {
                if($(this).prop('checked')) //si case est cochée
                {
                    tabCaviteCheck.push({'Name': $(this).val(), 'Type' : 'Cavite'  });
                }
             
                
            });
        
        if (champsMails == "")
            $('input#mailCavite').css('border-bottom', '1px solid #e90d3f')
        else
            $('input#mailCavite').css('border-bottom', '1px solid #757575')
        
        if (champsMdp == "")
            $('input#mailMdp').css('border-bottom', '1px solid #e90d3f')
        else
            $('input#mailMdp').css('border-bottom', '1px solid #757575')
        
        
        if (tabCaviteCheck.length == 0)
            $('.erreurMailAucuneCavite').slideDown();
        else
            $('.erreurMailAucuneCavite').slideUp();
            
        if (tabCaviteCheck.length > 0 && champsMails != "" && champsMdp != "")
        {
            $('div#chargementPage').fadeIn();
            envoiMailCavite(champsMails, champsMdp, tabCaviteCheck);
        }  
        
    });
    
        
    
    
            
    /**  
    *   Page : caviteSite.html  
    *
    *   Au click du bouton de modification d'un site, affficher overlay
    **/
    $(document).on('click','#topMenu .boutonGenererRapport', function()
    {
        //fermer les autres overlays
        $('.overlay > div').fadeOut();
        
        $('.overlay').fadeIn();
        $('.overlay-genererRapport').fadeIn();
    });
    
    /**  
    *   Page : caviteSite.html
    *
    *   Modification d'un site, quitter l'overlay, bouton "Annuler"
    **/   
    $(document).on('click','.overlay-genererRapport p.boutonGris', function()
    {
        $('.overlay-genererRapport input').val("");
        $('.overlay-genererRapport input').prop('checked', false);
        
        $('.overlay-genererRapport').fadeOut();
        $('.overlay').fadeOut();
        $('div.overlay-genererRapport .erreurGeneration').fadeOut();
    });
    
    
    
    /**  
    *   Page : caviteSite.html
    *
    *   Modification d'un site, quitter l'overlay, bouton "Annuler"
    **/   
    $(document).on('click','.overlay-genererRapport p.boutonVert', function()
    {
        
        dateFin =  $('input[name="dateFinGenererRapport"]').val();
       
        var champsDateRemplies = (dateFin != "") ;
      
     
        
        if (dateDebut == "")
            $('#dateDebutGenererRapport').css('border-color','#e90d3f')
        else
            $('#dateDebutGenererRapport').css('border-color','#757575 ')
            
            
        if (champsDateRemplies)
            {
                $('p.erreurDate').slideUp();
                $('#dateFinGenererRapport').css('color','#757575');
                
                $('div#chargementPage').fadeIn();
            }
        else
            {
                $('p.erreurDate').slideUp();
                $('p.erreurDate').slideDown();
                $('#dateFinGenererRapport').css('color','#e90d3f');
            }
        
        var boolGrapheTemp = $('input[name="genererRapportTemp"]').prop('checked');
        var boolGrapheCaverne = $('input[name="genererRapportCavern"]').prop('checked');
        var boolGrapheCasingShoe = $('input[name="genererRapporCasingShoe"]').prop('checked');
        
        
        if (champsDateRemplies)
            genererRapport(dateFin, boolGrapheTemp, boolGrapheCaverne, boolGrapheCasingShoe);
        
            
    });
    
    
    
    
    
    
    
    /**
    *   Page : exploitants.html
    *
    *   Afficher formulaire pour ajouter exploitant
    **/  
    $(document).on('click','#exploitants #ajouterExploitant p:nth-child(2)',function()
    {
        $(".modifierInformationExploitant").stop().slideUp();   
        $('#nouvelExploitant').stop().slideToggle();
    });
    
  
    
    


    
    
     /** 
     *  Page : exploitants.html
     *
     *  Afficher / cacher section pour modifier un contact et réiniatiliser les champs 
     **/
    $(document).on('click','.informationExploitant p.modifContact', function()
    {
        var idExploitantAModifier = $(this).parent().parent().attr('name');
        
        if ( $('div[name="' + idExploitantAModifier + '"] .modifierInformationExploitant').css('display') == 'block')
        {
            var formAnnuler = $('div[name="' + idExploitantAModifier + '"]  form');

            $(formAnnuler).find('input').each(function()
            {
                $(this).val(  $(this).attr('initialvalue') );
            });
        }
        
        $(".modifierInformationExploitant").stop().slideUp();
        $('div[name="'+idExploitantAModifier + '"] .modifierInformationExploitant').stop().slideToggle();
    });
    
    
     /** 
     *  Page : exploitants.html
     *
     *  Cacher section de modification d'un contact
     **/    
    $(document).on('click','.informationExploitant .modifierBoutonExploitant > p.boutonGris', function()
    {
        var idExploitantAModifier = $(this).parent().parent().parent().parent().attr('name');
        
        $(this).parent().parent().parent().find('input').each(function()
        {
            $(this).val(  $(this).attr('initialvalue') );
        });
    
                   
        $('div[name="'+idExploitantAModifier + '"] .modifierInformationExploitant ').stop().slideUp();
    });
    
   

    /**
    *   Page : nouveauSite.html
    *
    *   Savoir ou non si une image a été attribué au site
    **/
      $(document).on('change','#nouvelExploitantLeft input[name="nE6"]', function()
    {
        var img =  $("input[name='nE6']").val();
        var boolImgRemplie = img.length != 0;
          
          if (boolImgRemplie)
              {
                $("#nouvelExploitantLeft img").css('border','0.17em solid rgb(46, 204, 113)');
                $("#nouvelExploitantLeft input").css('border-color','rgb(46, 204, 113)');
                  
              }
          else
              {
                  $("#nouvelExploitantLeft img").css('border','none');
                $("#nouvelExploitantLeft input").css('border-color','rgb(46, 204, 113)');
              }    
    });
    
    
    
    
    
    
    
    /**
    *   Page : exploitants.html et nouveauSite.html
    *
    *   Au submit du formulaire, vérifier les champs pour savoir ou non si le submit se lance
    **/
    $(document).on('submit','form#ajoutExploitant', function()
    {
        return verifierFormAjoutExploitant();
    });
    
    
    /**
    *   Page : nouveauSite.html
    *
    *   Au submit du formulaire, vérifier les champs pour savoir ou non si le submit se lance
    **/
    $(document).on('submit','form#ajoutSite', function()
    {
        return verifierFormAjoutSite();
    });
    
    
    /**
    *   Page : nouveauSite.html
    *
    *   Savoir ou non si une image a été attribué au site
    **/
      $(document).on('change','#nouveauSiteTopRight input[name="nS5"]', function()
    {
        var img =  $("input[name='nS5']").val();
        var boolImgRemplie = img.length != 0;
          
          if (boolImgRemplie)
              {
                $("#nouveauSiteTopRight img").css('border','0.17em solid rgb(46, 204, 113)');
              }
          else
              {
                  $("#nouveauSiteTopRight img").css('border','none');
              }    
    });
    
    
    
    
    
    /**
    *   Page : contact.html
    *
    *   Au submit du formulaire, vérifier les champs pour savoir ou non si le submit se lance
    **/
    $(document).on('submit','form#ajoutContact', function()
    {
        return verifierFormAjoutContact();
    });
    
        
    
    
    

    
        
    /**
    *   Page : exploitants.html
    *
    *   Au submit du formulaire, vérifier qu'il y a bien une image
    **/
    $(document).on('submit','#gestionExploitantBottom form', function()
    {
        var imgRemplie =  $("input[name='importE1']").val();
        var boolImgRemplie = imgRemplie.length != 0;
        
        return boolImgRemplie;
    });
    
 
        
    
        
    /**
    *   Page : site.html
    *
    *    Au submit du formulaire, vérifier qu'il y a bien une image
    **/
    $(document).on('submit','#caviteSiteBottom form', function()
    {
        var imgRemplie =  $("input[name='importC1']").val();
        var boolImgRemplie = imgRemplie.length != 0;
        
        return boolImgRemplie;
    });
    
    
    
    
    
    
    
    
    /**
    *
    *   Vérifier qu'un email est valide
    */    
    function validateEmail(email)
    {
        var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/ ;
        if (reg.test(email))
        {
            return true; 
        }
        else{
            return false;
        }
    } 
    
    
    
    /** 
    *  Page : exploitants.html et nouveauSite.html
    *
    *  Vérifier le formulaire d'ajout d'exploitant
    **/ 
   function verifierFormAjoutExploitant()
    {
        //savoir si l'image est remplie
      //  var imgRemplie =  $("input[name='nE6']").val();
    //    var boolImgRemplie = imgRemplie.length != 0;
        
        var ChampsRemplis = ($("input[name='nE1']").val() != "" && $("input[name='nE2']").val() != "" && $("input[name='nE3']").val() != "" && $("input[name='nE4']").val() != ""  &&  $("input[name='nE5']").val() != "");
        
        
        var emailValide = validateEmail($("input[name='nE5']").val());
        
         if ( $("input[name='nE5']").val() != "" && !emailValide )
        {
            $('p.mailInvalide').slideDown();
        }
        else
             $('p.mailInvalide').slideUp();
        
        
        //Si tous les champs sont remplis
        if(ChampsRemplis && emailValide)
        {        
            //Si nous somme sur la page de création d'un site
            if ( $('.overlay-nouvelExploitant').length == 1 )
            {
                sessionStorage.setItem("NomSiteNouveau", $('input[name="nS1"]').val() );
                sessionStorage.setItem("AdresseSiteNouveau", $('input[name="nS2"]').val() );
                sessionStorage.setItem("VilleSiteNouveau", $('input[name="nS3"]').val() );
                sessionStorage.setItem("PaysSiteNouveau", $('input[name="nS4"]').val() );
                sessionStorage.setItem("MdpSiteNouveau", $('input[name="nS7"]').val() );
                sessionStorage.setItem("MdpConfirmSiteNouveau", $('input[name="nS8"]').val() );
            }
            
           return true;
        }
        
        else
        {
            if($("input[name='nE1']").val() == "" )
                {
                    $("label[name='nE1']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nE1']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nE1']").css('color','black').css('font-weight','inherit');
                     $("input[name='nE1']").css('border-color','#757575');
                }
            
            if($("input[name='nE2']").val() == "")
                {
                    $("label[name='nE2']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nE2']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nE2']").css('color','black').css('font-weight','inherit');
                     $("input[name='nE2']").css('border-color','#757575');
                }
            
            if($("input[name='nE3']").val() == "")
                {
                    $("label[name='nE3']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nE3']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nE3']").css('color','black').css('font-weight','inherit');
                     $("input[name='nE3']").css('border-color','#757575');
                }
            
            if($("input[name='nE4']").val() == "")
                {
                    $("label[name='nE4']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nE4']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nE4']").css('color','black').css('font-weight','inherit');
                     $("input[name='nE4']").css('border-color','#757575');
                }
            
            if($("input[name='nE5']").val() == "" || !emailValide)
                {
                    $("label[name='nE5']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nE5']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nE5']").css('color','black').css('font-weight','inherit');
                     $("input[name='nE5']").css('border-color','#757575');
                } 
            
           return false;
        }
    }
    

    
    
    /** 
    *  Page : contact.html 
    *
    *  Vérifier le formulaire d'ajout de contact
    **/ 
   function verifierFormAjoutContact()
    {
        //Attribuer le nom de l'exploitant à l'input caché
        $("input[name='nC3']").val(sessionStorage.getItem("NomExploitantActuel"));
        
        
        var ChampsRemplis = ($("input[name='nC1']").val() != "" && $("input[name='nC2']").val() != "" && $("input[name='nC4']").val() != "" && $("input[name='nC5']").val() != ""  &&  $("input[name='nC6']").val() != "" &&  $("input[name='nC7']").val() != "");
        
        
        var emailValide = validateEmail($("input[name='nC7']").val());
        
         if ( $("input[name='nC7']").val() != "" && !emailValide )
        {
            $('p.mailInvalide').slideDown();
        }
        else
             $('p.mailInvalide').slideUp();
        
        
        //Si tous les champs sont remplis
        if(ChampsRemplis && emailValide)
        {        
            $('div#chargementPage').fadeIn();
            return true;
        }
        
        else
        {
            if($("input[name='nC1']").val() == "" )
                {
                    $("label[name='nC1']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC1']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nC1']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC1']").css('border-color','#757575');
                }
            
            if($("input[name='nC2']").val() == "")
                {
                    $("label[name='nC2']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC2']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nC2']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC2']").css('border-color','#757575');
                }
            
            if($("input[name='nC4']").val() == "")
                {
                    $("label[name='nC4']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC4']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nC4']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC4']").css('border-color','#757575');
                }
            
            if($("input[name='nC5']").val() == "")
                {
                    $("label[name='nC5']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC5']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nC5']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC5']").css('border-color','#757575');
                }
            if($("input[name='nC6']").val() == "")
                {
                    $("label[name='nC6']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC6']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nC6']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC6']").css('border-color','#757575');
                }
            
            if($("input[name='nC7']").val() == "" || !emailValide)
                {
                    $("label[name='nC7']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC7']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nC7']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC7']").css('border-color','#757575');
                } 
            
           return false;
        }
    }
    

    
    
        
    
    /** 
    *  Page : nouveauSite.html
    *
    *  Vérifier le formulaire d'ajout d'un site
    **/
   function verifierFormAjoutSite()
    {
        //savoir si l'image est remplie
      //  var imgRemplie =  $("input[name='nS5']").val();
    //    var boolImgRemplie = imgRemplie.length != 0;
        
        var ChampsRemplis = ($("input[name='nS1']").val() != "" && $("input[name='nS2']").val() != "" && $("input[name='nS3']").val() != "" && $("input[name='nS4']").val() != ""  &&  $("input[name='nS6']").val() != "" &&  $("input[name='nS7']").val() != "" &&  $("input[name='nS8']").val() != "");
        
        var mdpIdentiques = ( $("input[name='nS7']").val() == $("input[name='nS8']").val() );
        
        //Si tous les champs sont remplis 
        if(ChampsRemplis)
        {        
            //Si les mots de passes écris sont identiques
            if(mdpIdentiques)
                {
                    return true;
                }
            else
                {
                    $('#ajoutSite p.mdpInvalide').fadeIn();
                    
                    //Remettre par défaut le style des autres labels et input
                    $("label").css('color','black').css('font-weight','inherit');
                    $("input").css('border-color','#757575');
                    
                    //Mettre style aux mots de passe
                    $("label[name='nS7'], label[name='nS8'] ").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nS7'], input[name='nS8']").css('border-color','#e90d3f');
                    
                    return false; //mots de passes pas identiques
                }
        }
        
        else
        {
            $('#ajoutSite p.erreurChampsVide').fadeIn();
            
            for(var i = 1; i <= 8; i++)
            {
                if (i != 5) //si nous sommes à l'image (name = 5)
                    {
                         if($("input[name='nS" + i + "']").val() == "" )
                        {
                            $("label[name='nS" + i + "']").css('color','#e90d3f').css('font-weight','bold');
                            $("input[name='nS" + i + "']").css('border-color','#e90d3f');
                        }
                        else
                        {
                            $("label[name='nS" + i + "']").css('color','black').css('font-weight','inherit');
                             $("input[name='nS" + i + "']").css('border-color','#757575');
                        }
                    }
            }
            
           return false;
        }
    }
    

    
    
    

    /**
    *   Page : caviteSite.html
    *
    *   Afficher les données associés à une cavité
    **/
    $(document).on("change"," select#choix_cavite", function()
    {
        $('.erreurDonneesIncoherente').hide(); //Cacher les erreurs liées au données des cavités
        $('.erreurDonneesNull').hide(); 
        $('#caviteMenuBottom a:last-child').attr('href','gererCalage.html');
       // $('#caviteMenuBottom a:last-child p').css('background-color','#2ecc71')
        $('#caviteMenuBottom a:last-child p').removeClass('boutonGris');
        
        var nomCavite = $("select#choix_cavite").val(); 
       
        $('span.nomCavite').text(nomCavite); 
        $('input[name="infoCavite1"]').val(infoChaqueCavite[nomCavite][0]);
        $('input[name="infoCavite2"]').val(infoChaqueCavite[nomCavite][1]);
        $('input[name="infoCavite3"]').val(infoChaqueCavite[nomCavite][2]);
        $('input[name="infoCavite4"]').val(infoChaqueCavite[nomCavite][3]);

        sessionStorage.setItem("NomCaviteActuel", nomCavite);
        
        $('#nouvelleCavite').slideUp();
        $('#modifierCavite').slideUp();
        
        //Appeler à l'affichage des courbes
        getDataTempCavitePointReel(); //Récuperer les données de la courbes réelle 

        getDataPresCavitePointReel(); //Récuperer les données de la courbes réelle 

        getDataVolumeCavitePointReel(); //Récuperer les données de la courbes réelle
     
        getDataTempCasingShoePoint();
                    
        getDataPresCasingShoePoint();

        getDataTempCavernePoint();
                    
        getDataPresCavernePoint();
        
        //rétablir l'écran de chargement
        $('div#chargementPage').fadeIn();
        boolPresCaviteCharge = false;
        boolVolCaviteCharge = false;
        boolTempCaviteCharge = false;
        
        
    });
    
    
    
      

    /**
    *   Page : parametreSite.html
    *
    *   Afficher les données associés à un composant du gaz d'un site lorsque l'on modifie un select
    **/
    $(document).on("change"," select#composantsChimiques", function()
    {
        var nomComposant = $(this).val(); //Nom du composant que l'on vient de choisir  
        
        if (nomComposant == "Sélectionnez un composant")
        {
             $(this).parent().parent().find('input[name="masseMolaire"]').val("");
             $(this).parent().parent().find('input[name="pressionCritique"]').val("");
             $(this).parent().parent().find('input[name="temperatureCritique"]').val("");
             $(this).parent().parent().find('input[name="proportions"]').attr("disabled","disabled");
             $(this).parent().parent().find('input[name="proportions"]').val("");
             $(this).parent().parent().find('input[name="densite"]').val("");
        }
        else
            {  $(this).parent().parent().find('input[name="masseMolaire"]').val(infoChaqueComposant[nomComposant][0] + " g/mol");
                $(this).parent().parent().find('input[name="pressionCritique"]').val(infoChaqueComposant[nomComposant][1] + " MPa");
                $(this).parent().parent().find('input[name="temperatureCritique"]').val(infoChaqueComposant[nomComposant][2] + " K");
                $(this).parent().parent().find('input[name="densite"]').val(infoChaqueComposant[nomComposant][3] + " kg/m3");
                $(this).parent().parent().find('input[name="proportions"]').removeAttr("disabled")
            }
       
        
    });
    
    

    
    /**
    *   Page : parametreSite.html
    *
    *   Faire les calculs automatique au changement d'une proportion d'un élément
    **/
    $(document).on("change","div.compositionGaz input[name='proportions'], select#composantsChimiques", function()
    {
        afficherCompositionTotale();
        
    });
    
    

    
    /**
    *   Page : caviteSite.html
    *
    *   AJOUT CAVITE - Au submit du formulaire, vérifier les champs pour savoir ou non si le submit se lance
    **/
    $(document).on('click','#nouvelleCavite .boutonVert', function()
    {
        nomCaviteNouvelle = $('input[name="nC1"]').val();
        hauteurCaviteNouvelle = $('input[name="nC2"]').val();
        profondeurCaviteNouvelle = $('input[name="nC4"]').val();
        diametreCaviteNouvelle = $('input[name="nC3"]').val();
        volumeCaviteNouvelle = $('input[name="nC5"]').val();

      
        
        //tous les champs remplis
        var champsCavRemplis = (nomCaviteNouvelle != "" && hauteurCaviteNouvelle != "" && profondeurCaviteNouvelle != "" && diametreCaviteNouvelle != "" && volumeCaviteNouvelle != "");
        
   
        
        //Si les champs sont bien des nombres
        var champsCavNombre = (validateChampsNombre(hauteurCaviteNouvelle) && validateChampsNombre(profondeurCaviteNouvelle) && validateChampsNombre(diametreCaviteNouvelle) && validateChampsNombre(volumeCaviteNouvelle));
        

        
        if (champsCavRemplis && champsCavNombre)
            ajoutCavite();
        else
            {
                $('#nouvelleCavite p.caviteErreurChamps').slideDown();
                
                 if($("input[name='nC1']").val() == "")
                {
                    $("input[name='nC1']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC1']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='nC1']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC1']").css('border-color','#757575');
                }
                
                 if($("input[name='nC2']").val() == "" || !validateChampsNombre(hauteurCaviteNouvelle))
                {
                    $("input[name='nC2']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC2']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='nC2']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC2']").css('border-color','#757575');
                }
                
                 if($("input[name='nC3']").val() == "" || !validateChampsNombre(profondeurCaviteNouvelle))
                {
                    $("input[name='nC3']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC3']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='nC3']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC3']").css('border-color','#757575');
                }
                
                 if($("input[name='nC4']").val() == "" || !validateChampsNombre(diametreCaviteNouvelle))
                {
                    $("input[name='nC4']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC4']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='nC4']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC4']").css('border-color','#757575');
                }
                
                 if($("input[name='nC5']").val() == "" || !validateChampsNombre(volumeCaviteNouvelle))
                {
                    $("input[name='nC5']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nC5']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='nC5']").css('color','black').css('font-weight','inherit');
                     $("input[name='nC5']").css('border-color','#757575');
                }
                
            }
        
    });
    
    
   
    
    /**
    *   Page : caviteSite.html
    *
    *   MODIFIER CAVITE - Au submit du formulaire, vérifier les champs pour savoir ou non si le submit se lance
    **/
    $(document).on('click','#modifierCavite .boutonVert', function()
    {
        nomCaviteNouvelle = $('input[name="mC1"]').val();
        hauteurCaviteNouvelle = $('input[name="mC2"]').val();
        profondeurCaviteNouvelle = $('input[name="mC4"]').val();
        diametreCaviteNouvelle = $('input[name="mC3"]').val();
        volumeCaviteNouvelle = $('input[name="mC5"]').val();

      
        
        //tous les champs remplis
        var champsCavRemplis = (nomCaviteNouvelle != "" && hauteurCaviteNouvelle != "" && profondeurCaviteNouvelle != "" && diametreCaviteNouvelle != "" && volumeCaviteNouvelle != "");
        
   
        
        //Si les champs sont bien des nombres
        var champsCavNombre = (validateChampsNombre(hauteurCaviteNouvelle) && validateChampsNombre(profondeurCaviteNouvelle) && validateChampsNombre(diametreCaviteNouvelle) && validateChampsNombre(volumeCaviteNouvelle));
        

        
        if (champsCavRemplis && champsCavNombre)
            enregistrerCavite();
        else
            {
                $('#modiferCavite p.caviteErreurChamps').slideDown();
                
                 if($("input[name='mC1']").val() == "")
                {
                    $("input[name='mC1']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='mC1']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='mC1']").css('color','#ccc').css('font-weight','inherit');
                     $("input[name='mC1']").css('border-color','#757575');
                }
                
                 if($("input[name='mC2']").val() == "" || !validateChampsNombre(hauteurCaviteNouvelle))
                {
                    $("input[name='mC2']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='mC2']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='mC2']").css('color','#ccc').css('font-weight','inherit');
                     $("input[name='mC2']").css('border-color','#757575');
                }
                
                 if($("input[name='mC3']").val() == "" || !validateChampsNombre(profondeurCaviteNouvelle))
                {
                    $("input[name='mC3']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='mC3']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='mC3']").css('color','#ccc').css('font-weight','inherit');
                     $("input[name='mC3']").css('border-color','#757575');
                }
                
                 if($("input[name='mC4']").val() == "" || !validateChampsNombre(diametreCaviteNouvelle))
                {
                    $("input[name='mC4']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='mC4']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='mC4']").css('color','#ccc').css('font-weight','inherit');
                     $("input[name='mC4']").css('border-color','#757575');
                }
                
                 if($("input[name='mC5']").val() == "" || !validateChampsNombre(volumeCaviteNouvelle))
                {
                    $("input[name='mC5']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='mC5']").css('border-color','#e90d3f');
                }
                else
                {
                    $("input[name='mC5']").css('color','#ccc').css('font-weight','inherit');
                     $("input[name='mC5']").css('border-color','#757575');
                }
                
                
            }
        
    });
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
    *   Page : caviteSite.html
    *
    *   Rediriger l'utilisateur sur la page lié au graphe sur lequel il clique
    **/
    $(document).on("click","#graphesCaviteRight #grapheTemperature", function()
    {    
        document.location.href="grapheTemp.html";
    });
    
    $(document).on("click","#graphesCaviteLeft #graphePression", function()
    {    
        document.location.href="graphePression.html";
    });
    
    $(document).on("click","#graphesCaviteBas #grapheVolume", function()
    {    
        document.location.href="grapheVolume.html";
    });
    
    $(document).on("click","#graphesCaviteLigne1 #graphePressionCasingShoe", function()
    {    
        document.location.href="graphePressionCasingShoe.html";
    });
    
    $(document).on("click","#graphesCaviteLigne1 #grapheTemperatureCasingShoe", function()
    {    
        document.location.href="grapheTempCasingShoe.html";
    });
    
    $(document).on("click","#graphesCaviteLigne2 #graphePressionCaverne", function()
    {    
        document.location.href="graphePressionCavern.html";
    });
    
    $(document).on("click","#graphesCaviteLigne2 #grapheTemperatureCaverne", function()
    {    
        document.location.href="grapheTempCavern.html";
    });
    
    
    /**
    *   Page : exploitants.html
    *
    *   Afficher l'overlay pour confirmer la suppresion d'un exploitant
    **/
    $(document).on('click','.modifierBoutonExploitant p.boutonRouge', function()
    {
        //récupérer name de l'exploitant à supprimer et le donner au bouton pour le récupéré plus tard
        var nameExploitant = $(this).parent().parent().parent().parent().attr("name");
        $('.overlay-suppressionExploitant #suppressionExploitantBottom p.boutonRouge').attr("name",nameExploitant);
        
        $('.overlay').fadeIn();
        $('.overlay-suppressionExploitant').fadeIn();
        
    });
    
         
    /**
    *   Page : exploitants.html
    *
    *  Annuler la modification d'un exploitant
    **/
    $(document).on('click','.overlay-suppressionExploitant p.boutonGris', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-suppressionExploitant').fadeOut();
    }); 
    
    
    /**
    *   Page : exploitants.html
    *
    *  Fermer l'erreur de supprésion d'un exploitant
    **/
    $(document).on('click','.overlay-erreurSupressionExploitant p.boutonGris', function()
    {
        $('.overlay').fadeOut();
        $('.overlay-erreurSupressionExploitant').fadeOut();
    });
    
    
    /**
    *   Page : exploitant.html
    *
    *   Récupérer les données lors de la modification d'un exploitant
    */
    $(document).on('click','.modifierBoutonExploitant p.boutonTurquoise', function()
    {
        ancienNomExploitant =  $(this).parent().parent().parent().siblings('.nomExploitant').find('p')[0].textContent;
        
       
        
        var img =  $('div[name="' + ancienNomExploitant + '"] input[type="file"]').val();
        var boolImgRemplie = img.length != 0;
        
        if(boolImgRemplie)
            $('div[name="' + ancienNomExploitant + '"] form').attr('action','/api/updateExploitantWithImage');
        else
            $('div[name="' + ancienNomExploitant + '"] form').attr('action','/api/updateExploitant');
            
        $('div#chargementPage').fadeIn();
        
        $('div[name="' + ancienNomExploitant + '"] form').submit();
        
        
    });
   
    
    
   
    /**
    *   Page : exploitant.html
    *
    *   Au submit du formulaire de modification d'un formulaire, vérifier les champs pour savoir ou non si le submit se lance
    **/
      $(document).on('click','.modifierInformationExploitant p.boutonTurquoise', function()
    {
        var ancienNomExploitant;
        
        ancienNomExploitant = $(this).parent().parent().find('input[name="mE8"]').val();

    });
    
    
    
       
    
   
    /**
    *   Page : contact.html
    *
    *   Savoir ou non si une image a été attribué à un nouveau contact
    **/
    $(document).on('change','input[name="nC10"]', function()
    {
        var img =  $("input[name='nC10']").val();
        var boolImgRemplie = img.length != 0;
          
          if (boolImgRemplie)
              {
                  $("div.ajouterInformationContact img").css('border','0.17em solid rgb(46, 204, 113)');
                  $("input[name='nC9']").val("True");
              }
          else
              {
                  $("input[name='nC9']").val("False");
                  $("div.ajouterInformationContact img").css('border','none');
              }    
    });
     
   
    /**
    *   Page : exploitant.html
    *
    *   Savoir ou non si une image a été attribué à un nouveau exploitant
    **/
    $(document).on('change','input[name="nE6"]', function()
    {
        var img =  $("input[name='nE6']").val();
        var boolImgRemplie = img.length != 0;
          
          if (boolImgRemplie)
              {
                  $("#nouvelExploitantLeft img").css('border','0.17em solid rgb(46, 204, 113)');
                  $("input[name='nE8']").val("True");
              }
          else
              {
                  $("input[name='nE8']").val("False");
                  $("div#nouvelExploitantLeft img").css('border','none');
                  
              }    
    });
    
    
    /**
    *   Page : nouveauSite.html
    *
    *   Savoir ou non si une image a été attribué à un nouveau site
    **/
    $(document).on('change','input[name="nS5"]', function()
    {
        var img =  $("input[name='nS5']").val();
        var boolImgRemplie = img.length != 0;
          
          if (boolImgRemplie)
              {
                  $("#nouveauSiteTopRight img").css('border','0.17em solid rgb(46, 204, 113)');
                  $("input[name='nS10']").val("True");
              }
          else
              {
                  $("input[name='nS10']").val("False");
                  $("div#nouveauSiteTopRight img").css('border','none');
                  
              }    
    });
    
    
    
    
    
    
    /**
    *   Page : gererCalage.html
    *
    *   Afficher un calage et cacher les autres
    **/
    $(document).on('click','.sectionCalageTitre img', function()
    {
        
        //Si on veut afficher un calage
        if ($(this).attr('src') == "../asset/img/caviteSite/flecheBasBlanc.png")
        {
            
             $('#chargementPageVierge').show();
             
            //le calage auquel on a cliqué sur la flêche poru le fermer
            var calageImg = $(this).parent();
           
            $(calageImg).find('img').remove(); //Supprimer l'image pour que l'utilisateur ne puisse pas recliqué dessus tant que l'animation n'est pas terminée
             
            //Styliser aussitôt tous les calages à cacher pour harmoniser les animations
            $('.sectionCalage .sectionCalageTitre div p').css('color','white');
            $('.sectionCalage .sectionCalageTitre div p input').css('color','white');
            $('.sectionCalage').css('background-color','#2ecc71');
            $('.sectionCalage .sectionCalageTitre input').css('background-color','#2ecc71');
            
             

            //Cacher tous les calages
           $('.sectionCalageActive .sectionCalageContenu').stop().slideUp(500,function()
           {
            // Cacher le précédent calage
                $('.sectionCalage').removeClass('sectionCalageActive');
                $('.sectionCalage .sectionCalageTitre img').attr('src','../asset/img/caviteSite/flecheBasBlanc.png');
               
             
                //var imgThis = $(calageImg).find('img');
               
               
               //Styliser aussitôt le calage choisi pour harmoniser les animations
               $(calageImg).parent().find('.sectionCalageTitre div p').css('color','#2ecc71');
               $(calageImg).parent().find('.sectionCalageTitre div p input').css('color','#2ecc71');
               $(calageImg).parent().css('background-color','white');
               $(calageImg).parent().find('.sectionCalageTitre input').css('background-color','white');
               $(calageImg).parent().find('.sectionCalageTitre input').css('background-color','white');


                $(calageImg).parent().find('.sectionCalageContenu').slideDown(600, function()
                {
                    $(calageImg).parent().addClass('sectionCalageActive');
                    $(calageImg).append('<img src="../asset/img/caviteSite/flecheHautVert.png" />')
                    
                    $('#chargementPageVierge').hide();
                }); 

            });
            
         }
        else
            {                
                 $('#chargementPageVierge').show();
                
                //le calage auquel on a cliqué sur la flêche poru le fermer pour l'ouvrir

                if($(this).attr('src') == '../asset/img/caviteSite/flecheHautVert.png' ) //Si ouvert
                {
                    var calageImg = $(this).parent();
                    $(this).remove(); //Supprimer l'image pour que l'utilisateur ne puisse pas recliqué dessus tant que l'animation n'est pas terminée

                    //Styliser aussitôt pour harmoniser les animations

                    $('.sectionCalage .sectionCalageTitre div p').css('color','white');
                    $('.sectionCalage .sectionCalageTitre div p input').css('color','white');
                    $('.sectionCalage').css('background-color','#2ecc71');
                    $('.sectionCalage .sectionCalageTitre input').css('background-color','#2ecc71');

                      //Cacher le calage
                    $('.sectionCalageActive .sectionCalageContenu').stop().slideUp(500,function()
                    {
                        //Remettre l'image qu'on avait supprimé
                        $(calageImg).append('<img src="../asset/img/caviteSite/flecheBasBlanc.png" />')
                        
                        
                        $('#chargementPageVierge').hide();
                    });
                }
                else //S'il est fermé
                {
                     var calageImg = $(this).parent();
                    $(this).remove(); //Supprimer l'image pour que l'utilisateur ne puisse pas recliqué dessus tant que l'animation n'est pas terminée

                    //Styliser aussitôt pour harmoniser les animations

                    $('.sectionCalage .sectionCalageTitre div p').css('color','#2ecc71');
                    $('.sectionCalage .sectionCalageTitre div p input').css('color','#2ecc71');
                    $('.sectionCalage').css('background-color','white');
                    $('.sectionCalage .sectionCalageTitre input').css('background-color','white');

                      //afficher le calage
                    $('.sectionCalageActive .sectionCalageContenu').stop().slideDown(500,function()
                    {
                        //Remettre l'image qu'on avait supprimé
                        $(calageImg).append('<img src="../asset/img/caviteSite/flecheHautVert.png" />');
                        
                        $('#chargementPageVierge').hide();
                    });

                }


            }
        
    });
    
    
    /*
    *    Page : gererCalage.html
    *
    *   Cacher un calage et rétablir les valeurs d'origines
    **/
       $(document).on('click','#gestionCalagesBottom .boutonGris', function()
    {
        
        //Trouver le calage à fermer   
        var calageAFermer = $(this).parent().parent().parent();
           
        //le calage auquel on a cliqué sur la flêche pour    le fermer
        var calageImg = $(calageAFermer).find('.sectionCalageTitre');
       
        $(calageAFermer).find('.sectionCalageTitre img').remove(); //Supprimer l'image pour que l'utilisateur ne puisse pas recliqué dessus tant que l'animation n'est pas terminée

        //Styliser aussitôt pour harmoniser les animations

        $('.sectionCalage .sectionCalageTitre div p').css('color','white');
        $('.sectionCalage .sectionCalageTitre div p input').css('color','white');
        $('.sectionCalage').css('background-color','#2ecc71');
        $('.sectionCalage .sectionCalageTitre input').css('background-color','#2ecc71');

          //Cacher le calage
        $(calageAFermer).find('.sectionCalageContenu').stop().slideUp(500,function()
        {
            //Remettre l'image qu'on avait supprimé
           $(calageAFermer).find('.sectionCalageTitre').append('<img src="../asset/img/caviteSite/flecheBasBlanc.png" />');
        });
              
           
           
           
        //Rétablir les différentes valeurs initiales

        $(calageAFermer).find('#dateDebut').val($(calageAFermer).find('#dateDebut').attr('initialvalue'));
        $(calageAFermer).find('#dateFin').val($(calageAFermer).find('#dateFin').attr('initialvalue'));


        $(calageAFermer).find('.sectionCalageContentLeft input').each(function()
        {
            $(this).val(  $(this).attr('initialvalue') );
        });
           
        $(calageAFermer).find('.sectionCalageContentRight input').each(function()
        {
            $(this).val(  $(this).attr('initialvalue') );
        });
           
         var idCalageAFerme = $(calageAFermer).attr('idcalage');
           
           
    
        //Réafficher les courbes initilaes
        afficherDoubleGraphCalages(tabDataTempJsonReel[idCalageAFerme], tabDataTempJsonSimule[idCalageAFerme], tabDataPresJsonReel[idCalageAFerme], tabDataPresJsonSimule[idCalageAFerme], idCalageAFerme);
        //    -> calagePressionSd
           
        $(calageAFermer).find('.calagePresSd').text($(calageAFermer).find('.calagePressionSd').attr('initialValue'));
        $(calageAFermer).find('.calagePresDevMin').text($(calageAFermer).find('.calagePressionDevMin').attr('initialValue'));
        $(calageAFermer).find('.calagePresDevMax').text($(calageAFermer).find('.calagePressionDevMax').attr('initialValue'));
        $(calageAFermer).find('.calagePresSeq').text($(calageAFermer).find('.calagePressionSeq').attr('initialValue'));
        $(calageAFermer).find('.calagePresSde').text($(calageAFermer).find('.calagePressionSde').attr('initialValue'));
           
        $(calageAFermer).find('.calageTempSd').text($(calageAFermer).find('.calageTemperatureSd').attr('initialValue'));
        $(calageAFermer).find('.calageTempDevMin').text($(calageAFermer).find('.calageTemperatureDevMin').attr('initialValue'));
        $(calageAFermer).find('.calageTempDevMax').text($(calageAFermer).find('.calageTemperatureDevMax').attr('initialValue'));
        $(calageAFermer).find('.calageTempSeq').text($(calageAFermer).find('.calageTemperatureSeq').attr('initialValue'));
        $(calageAFermer).find('.calageTempSde').text($(calageAFermer).find('.calageTemperatureSde').attr('initialValue'));
           
           
        
    });
    
    
    
    
    /*
    *    Page : gererCalage.html
    *
    *   Afficher un nouveau calage vierge
    **/
    
    $(document).on('click','#caviteStyleTopBouton .nouveauCalage', function()
    {        
        $('div#chargementPageVierge').show();
        
        //Si il n'y a pas d'autres ajout qui doivent être fait pour l'instant
        if ($('.sectionCalage:first-child()').find('.sectionCalageContenu #gestionCalagesBottom p:nth-child(2)').text() == "Ajouter")
        {
            $('p.nouveauCalageErreur').fadeIn().delay(3000).fadeOut();
            $('div#chargementPageVierge').hide();
        }
        
        else //Si un peut affiche un calage à remplir
            {
                //Fermer les autres calages 
                $('.sectionCalage').css('background-color','#2ecc71');
                $('.sectionCalage .sectionCalageTitre input').css('background-color','#2ecc71');  
                $('.sectionCalage .sectionCalageTitre div p').css('color','white');
                $('.sectionCalage .sectionCalageTitre div p input').css('color','white');
                $('.sectionCalage').css('background-color','#2ecc71');
                $('.sectionCalage .sectionCalageTitre input').css('background-color','#2ecc71');
                $('.sectionCalage .sectionCalageTitre input').css('background-color','#2ecc71');

                
                //N'est pas effectué si il n'y a pas d'autres calages ouvert
                $('.sectionCalageActive .sectionCalageContenu').slideUp(500,function()
                {                    
                     // Cacher précédent calage
                    $('.sectionCalage').removeClass('sectionCalageActive');
                    $('.sectionCalage .sectionCalageTitre img').attr('src','../asset/img/caviteSite/flecheBasBlanc.png');
                    
                    if (sessionStorage.getItem("Langue") == "fr")
                        //Ajouter un calage à la liste
                        $('#listeCalage').prepend('<div id="nouveauCalage" class="sectionCalage"><div class="sectionCalageTitre"> <div> <p><span  class="trn">Du</span> : <input type="text" name="dateDebut" id="dateDebut" placeholder="jj-mm-aaaa"></p>   <p><span  class="trn">Pression</span> : <span  class="trn">ds</span> 00  / <span  class="trn">dév min</span> 00 / <span  class="trn">dév max</span> 00 / 00 <span  class="trn">seq</span> / 00 <span  class="trn">sde</span> </p> </div>   <div><p><span  class="trn">Au</span> : <input type="text" name="dateFin" id="dateFin" placeholder="jj-mm-aaaa"></p><p><span  class="trn">Température</span> : <span  class="trn">sd</span> 00  / <span  class="trn">dév min</span> 00 / <span  class="trn">dév max</span> 00 / 00 <span  class="trn">seq</span> / 00 <span  class="trn">sde</span> </p></div> <div class="checkboxAfficherCalage"> <p> <input type="checkbox" name="checkbox" value="checkbox"><span  class="trn">Afficher sur le graphe</span></p></div> <img src="../asset/img/caviteSite/flecheHautVert.png"/></div> <div class="sectionCalageContenu"> <div class="sectionCalageContent">  <div class="sectionCalageContentLeft"><table><tr><th></th> <th class="trn">V stocké</th><th class="trn">Débit</th>   <th class="trn">P simul</th>  <th class="trn">DP arma</th>  </tr> <tr> <td> Coeff P-3</td> <td><input type="text" name="case11P"></td><td><input type="text" name="case12P"></td><td><input type="text" name="case12P"></td> <td> <b>-</b> </td></tr> <tr> <td> Coeff P-2</td> <td><input type="text" name="case21P"></td>  <td><input type="text" name="case22P"></td><td><input type="text" name="case23P"></td>  <td><input type="text" name="case24P"></td></tr><tr><td> Coeff P-1</td> <td><input type="text" name="case31P"></td><td><input type="text" name="case32P"></td> <td><input type="text" name="case33P"></td><td><input type="text" name="case33P"></td>  </tr>  <tr><td> Coeff P-0</td><td><input type="text" name="case41P"></td><td><input type="text" name="case42P"></td>  <td><input type="text" name="case43P"></td> <td><input type="text" name="case44P"></td> </tr> </table> </div>  <div class="sectionCalageContentRight"><table> <tr> <th></th><th class="trn">PArma</th><th class="trn">V stocké</th>  <th class="trn">Débit</th><th class="trn">T simul</th> <th class="trn">DP arma</th>  </tr>  <tr><td> Coeff T-3</td><td><input type="text" name="case11T"></td><td><input type="text" name="case12T"></td> <td><input type="text" name="case13T"></td><td><input type="text" name="case14T"></td> <td> <b>-</b> </td> </tr> <tr>  <td> Coeff T-2</td><td><input type="text" name="case21T"></td> <td><input type="text" name="case22T"></td><td><input type="text" name="case23T"></td> <td><input type="text" name="case24T"></td> <td><input type="text" name="case25T"></td> </tr>   <tr>  <td> Coeff T-1</td> <td><input type="text" name="case31T"></td>    <td><input type="text" name="case32T"></td>  <td><input type="text" name="case33T"></td>  <td><input type="text" name="case34T"></td>  <td>  <b>-</b> </td>  </tr>  <tr>  <td> Coeff T-0</td><td><input type="text" name="case41T"></td>  <td><input type="text" name="case42T"></td><td><input type="text" name="case43T"></td>  <td><input type="text" name="case43T"></td> <td>  <b>-</b> </td> </tr>   </table>  </div>  <p class="boutonVert simulerCourbes trn">Simuler</p> <p class="simulerCourbesErreur trn">Pas assez de données pour pouvoir simuler entre ces dates</p> </div> <div id="graphesGestionCalage">     <div id="graphesGestionCalageLeft"> <h3 class="trn">Pression</h3>  <div id="graphePression">  <div id="chartContainerPressionCalage0" style="height: 190px; width: 100%;"></div></div> <div id="listeInfoGraphe">  <div class="infoGraphe"> <p>00</p> <p class="trn">Déviation standard</p>   </div> <div class="infoGraphe"><p>00</p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p>00</p><p class="trn">Déviation max</p> </div>  <div class="infoGraphe">  <p>00</p>  <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe"><p>00</p><p class="trn">Déviations moyennes</p></div>  </div>  </div>  <div id="graphesGestionCalageRight"><h3 class="trn">Température</h3>    <div id="grapheTemperature">  <div id="chartContainerTempCalage0" style="height: 190px; width: 100%;"></div> </div> <div id="listeInfoGraphe"><div class="infoGraphe"><p>00</p> <p class="trn">Standard deviation</p></div>  <div class="infoGraphe"> <p>00</p> <p class="trn">Déviation min</p></div><div class="infoGraphe"> <p>00</p> <p class="trn">Déviation max</p> </div>  <div class="infoGraphe"> <p>00</p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe"> <p>00</p> <p class="trn">Déviations moyennes</p>  </div> </div> </div>  </div> <div id="gestionCalagesBottom"> <p class="boutonGris trn">Annuler</p> <p class="boutonVert ajoutCalage trn">Ajouter</p>  <p class="erreurDate trn">Les champs dates ne sont pas remplis ou incorrects.</p> <p class="erreurChampsCoef trn">Des champs coefficients ne sont pas remplis ou incorrects.</p> </div> </div> </div></div>  ');
                    else
                         $('#listeCalage').prepend('<div id="nouveauCalage" class="sectionCalage"><div class="sectionCalageTitre"> <div> <p><span  class="trn">From</span> : <input type="text" name="dateDebut" id="dateDebut" placeholder="jj-mm-aaaa"></p>   <p><span  class="trn">Pressure</span> : <span  class="trn">sd</span> 00  / <span  class="trn">min dev</span> 00 / <span  class="trn">max dev</span> 00 / 00 <span  class="trn">sqd</span> / 00 <span  class="trn">sde</span> </p> </div>   <div><p><span  class="trn">To</span> : <input type="text" name="dateFin" id="dateFin" placeholder="jj-mm-aaaa"></p><p><span  class="trn">Temperature</span> : <span  class="trn">ds</span> 00  / <span  class="trn">min dev</span> 00 / <span  class="trn">max dev</span> 00 / 00 <span  class="trn">sqd</span> / 00 <span  class="trn">sde</span> </p></div> <div class="checkboxAfficherCalage"> <p> <input type="checkbox" name="checkbox" value="checkbox"><span  class="trn">Show on the graph</span></p></div> <img src="../asset/img/caviteSite/flecheHautVert.png"/></div> <div class="sectionCalageContenu"> <div class="sectionCalageContent">  <div class="sectionCalageContentLeft"><table><tr><th></th> <th class="trn">stored V</th><th class="trn">Flow</th>   <th class="trn">simul P</th>  <th class="trn">DP arma</th>  </tr> <tr> <td> Coeff P-3</td> <td><input type="text" name="case11P"></td><td><input type="text" name="case12P"></td><td><input type="text" name="case12P"></td> <td> <b>-</b> </td></tr> <tr> <td> Coeff P-2</td> <td><input type="text" name="case21P"></td>  <td><input type="text" name="case22P"></td><td><input type="text" name="case23P"></td>  <td><input type="text" name="case24P"></td></tr><tr><td> Coeff P-1</td> <td><input type="text" name="case31P"></td><td><input type="text" name="case32P"></td> <td><input type="text" name="case33P"></td><td><input type="text" name="case33P"></td>  </tr>  <tr><td> Coeff P-0</td><td><input type="text" name="case41P"></td><td><input type="text" name="case42P"></td>  <td><input type="text" name="case43P"></td> <td><input type="text" name="case44P"></td> </tr> </table> </div>  <div class="sectionCalageContentRight"><table> <tr> <th></th><th class="trn">PArma</th><th class="trn">stored V</th>  <th class="trn">Flow</th><th class="trn">simul T</th> <th class="trn">DP arma</th>  </tr>  <tr><td> Coeff T-3</td><td><input type="text" name="case11T"></td><td><input type="text" name="case12T"></td> <td><input type="text" name="case13T"></td><td><input type="text" name="case14T"></td> <td> <b>-</b> </td> </tr> <tr>  <td> Coeff T-2</td><td><input type="text" name="case21T"></td> <td><input type="text" name="case22T"></td><td><input type="text" name="case23T"></td> <td><input type="text" name="case24T"></td> <td><input type="text" name="case25T"></td> </tr>   <tr>  <td> Coeff T-1</td> <td><input type="text" name="case31T"></td>    <td><input type="text" name="case32T"></td>  <td><input type="text" name="case33T"></td>  <td><input type="text" name="case34T"></td>  <td>  <b>-</b> </td>  </tr>  <tr>  <td> Coeff T-0</td><td><input type="text" name="case41T"></td>  <td><input type="text" name="case42T"></td><td><input type="text" name="case43T"></td>  <td><input type="text" name="case43T"></td> <td>  <b>-</b> </td> </tr>   </table>  </div>  <p class="boutonVert simulerCourbes trn">Simulate</p> <p class="simulerCourbesErreur trn">Not enough data to simulate between these dates</p> </div> <div id="graphesGestionCalage">     <div id="graphesGestionCalageLeft"> <h3 class="trn">Presure</h3>  <div id="graphePression">  <div id="chartContainerPressionCalage0" style="height: 190px; width: 100%;"></div></div> <div id="listeInfoGraphe">  <div class="infoGraphe"> <p>00</p> <p class="trn">Standard deviation</p>   </div> <div class="infoGraphe"><p>00</p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p>00</p><p class="trn">Max deviation</p> </div>  <div class="infoGraphe">  <p>00</p>  <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe"><p>00</p><p class="trn">Average deviations</p></div>  </div>  </div>  <div id="graphesGestionCalageRight"><h3 class="trn">Temperature</h3>    <div id="grapheTemperature">  <div id="chartContainerTempCalage0" style="height: 190px; width: 100%;"></div> </div> <div id="listeInfoGraphe"><div class="infoGraphe"><p>00</p> <p class="trn">Standard deviation</p></div>  <div class="infoGraphe"> <p>00</p> <p class="trn">Min deviation</p></div><div class="infoGraphe"> <p>00</p> <p class="trn">Max deviation</p> </div>  <div class="infoGraphe"> <p>00</p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe"> <p>00</p> <p class="trn">Average deviations</p>  </div> </div> </div>  </div> <div id="gestionCalagesBottom"> <p class="boutonGris trn">Cancel</p> <p class="boutonVert ajoutCalage trn">Add</p>  <p class="erreurDate trn">The date fields are not filled or incorrect.</p> <p class="erreurChampsCoef trn">Des champs coefficients ne sont pas remplis ou incorrects.</p> </div> </div> </div></div>  ');
                    
                    
                    
                     //Styliser aussitôt pour harmoniser les animations
                       $('#nouveauCalage').css('background-color','white');
                       $('#nouveauCalage .sectionCalageTitre div p').css('color','#2ecc71');
                       $('#nouveauCalage .sectionCalageTitre div p input').css('color','#2ecc71');
                       $('#nouveauCalage .sectionCalageTitre input').css('background-color','white');
                       $('#nouveauCalage .sectionCalageTitre input').css('background-color','white');
                    
                    $('#nouveauCalage .sectionCalageContenu').slideDown(500, function()
                    {
                        $('#nouveauCalage').addClass('sectionCalageActive');
                        
                        
                        
                         
                        /*  Traduire le champs Date des input **/
                        $( "#dateDebut, #dateFin" ).datepicker(
                        {
                            altField: "#datepicker",
                            closeText: 'Fermer',
                            prevText: 'Précédent',
                            nextText: 'Suivant',
                            currentText: 'Aujourd\'hui',
                            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                            monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                            dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                            dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                            weekHeader: 'Sem.',
                            dateFormat: 'dd-mm-yy'
                        });

                        $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
                        $( "#dateDebut" ).datepicker();
                        $( "#dateFin" ).datepicker();
                        
                        
                        
                        $('div#chargementPageVierge').hide();
                    });
                    
                    
                    $('.simulerCourbes').click(actionSimulerCalage);
                    
                });
                
                
                
                //S'il n'y a pas aucun calages actuellement
                if ($('.sectionCalage').length == 0)
                    {  
                        if (sessionStorage.getItem("Langue") == "fr")
                           //Ajouter un calage à la liste
                            $('#listeCalage').prepend('<div id="nouveauCalage" class="sectionCalage"><div class="sectionCalageTitre"> <div> <p><span class="trn">Du</span> : <input type="text" name="dateDebut" id="dateDebut" placeholder="jj-mm-aaaa"></p>   <p><span class="trn">Pression</span> : <span class="trn">ds</span> 00  / <span class="trn">dév min</span> 00 / <span class="trn">dév max</span> 00 / 00 <span class="trn">seq</span> / 00 <span class="trn">sde</span> </p> </div>   <div><p><span class="trn">Au</span> : <input type="text" name="dateFin" id="dateFin" placeholder="jj-mm-aaaa"></p><p><span class="trn">Température</span> : <span class="trn">ds</span> 00  / <span class="trn">dév min</span> 00 / <span class="trn">dév max</span> 00 / 00 <span class="trn">seq</span> / 00 <span class="trn">sde</span> </p></div>  <div class="checkboxAfficherCalage"> <p> <input type="checkbox" name="checkbox" value="checkbox"><span class="trn">Afficher sur le graphe</span></p></div><img src="../asset/img/caviteSite/flecheHautVert.png"/></div> <div class="sectionCalageContenu"> <div class="sectionCalageContent">  <div class="sectionCalageContentLeft"><table><tr><th></th> <th class="trn">V stocké</th><th class="trn">Débit</th>   <th class="trn">P simul</th>  <th class="trn">DP arma</th>  </tr> <tr> <td> Coeff P-3</td> <td><input type="text" name="case11P"></td><td><input type="text" name="case12P"></td><td><input type="text" name="case12P"></td> <td> <b>-</b> </td></tr> <tr> <td> Coeff P-2</td> <td><input type="text" name="case21P"></td>  <td><input type="text" name="case22P"></td><td><input type="text" name="case23P"></td>  <td><input type="text" name="case24P"></td></tr><tr><td> Coeff P-1</td> <td><input type="text" name="case31P"></td><td><input type="text" name="case32P"></td> <td><input type="text" name="case33P"></td><td><input type="text" name="case33P"></td>  </tr>  <tr><td> Coeff P-0</td><td><input type="text" name="case41P"></td><td><input type="text" name="case42P"></td>  <td><input type="text" name="case43P"></td> <td><input type="text" name="case44P"></td> </tr> </table> </div>  <div class="sectionCalageContentRight"><table> <tr> <th></th><th class="trn">PArma</th><th class="trn">V stocké</th>  <th class="trn">Débit</th><th class="trn">T simul</th> <th class="trn">DP arma</th>  </tr>  <tr><td> Coeff T-3</td><td><input type="text" name="case11T"></td><td><input type="text" name="case12T"></td> <td><input type="text" name="case13T"></td><td><input type="text" name="case14T"></td> <td> <b>-</b> </td> </tr> <tr>  <td> Coeff T-2</td><td><input type="text" name="case21T"></td> <td><input type="text" name="case22T"></td><td><input type="text" name="case23T"></td> <td><input type="text" name="case24T"></td> <td><input type="text" name="case25T"></td> </tr>   <tr>  <td> Coeff T-1</td> <td><input type="text" name="case31T"></td>    <td><input type="text" name="case32T"></td>  <td><input type="text" name="case33T"></td>  <td><input type="text" name="case34T"></td>  <td>  <b>-</b> </td>  </tr>  <tr>  <td> Coeff T-0</td><td><input type="text" name="case41T"></td>  <td><input type="text" name="case42T"></td><td><input type="text" name="case43T"></td>  <td><input type="text" name="case43T"></td> <td>  <b>-</b> </td> </tr>   </table>   </div><p class="boutonVert simulerCourbes trn">Simuler</p><p class="simulerCourbesErreur trn">Pas assez de données pour pouvoir simuler entre ces dates</p>   </div> <div id="graphesGestionCalage">     <div id="graphesGestionCalageLeft"> <h3 class="trn">Pression</h3>  <div id="graphePression">  <div id="chartContainerPressionCalage0" style="height: 190px; width: 100%;"></div></div> <div id="listeInfoGraphe">  <div class="infoGraphe"> <p>00</p> <p class="trn">Déviation standard</p>   </div> <div class="infoGraphe"><p>00</p><p class="trn">Déviation min</p> </div> <div class="infoGraphe"> <p>00</p><p class="trn">Déviation max</p> </div>  <div class="infoGraphe">  <p>00</p>  <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe"><p>00</p><p class="trn">Déviations moyennes</p></div>  </div>  </div>  <div id="graphesGestionCalageRight"><h3 class="trn">Température</h3>    <div id="grapheTemperature">  <div id="chartContainerTempCalage0" style="height: 190px; width: 100%;"></div> </div> <div id="listeInfoGraphe"><div class="infoGraphe"><p>00</p> <p class="trn">Déviation standard</p></div>  <div class="infoGraphe"> <p>00</p> <p class="trn">Déviation min</p></div><div class="infoGraphe"> <p>00</p> <p class="trn">Déviation max</p> </div>  <div class="infoGraphe"> <p>00</p> <p class="trn">Somme des écarts quadratiques</p> </div> <div class="infoGraphe"> <p>00</p> <p class="trn">Déviations moyennes</p>  </div> </div> </div>  </div> <div id="gestionCalagesBottom"> <p class="boutonGris trn">Annuler</p> <p class="boutonVert ajoutCalage trn">Ajouter</p>  <p class="erreurDate trn">Les champs dates ne sont pas remplis ou incorrects.</p> <p class="erreurChampsCoef trn">Des champs coefficients ne sont pas remplis ou incorrects.</p> </div> </div> </div></div> ');
                        else
                             $('#listeCalage').prepend('<div id="nouveauCalage" class="sectionCalage"><div class="sectionCalageTitre"> <div> <p><span class="trn">From</span> : <input type="text" name="dateDebut" id="dateDebut" placeholder="jj-mm-aaaa"></p>   <p><span class="trn">Pressure</span> : <span class="trn">sd</span> 00  / <span class="trn">min dev</span> 00 / <span class="trn">max dev</span> 00 / 00 <span class="trn">sqd</span> / 00 <span class="trn">sde</span> </p> </div>   <div><p><span class="trn">To</span> : <input type="text" name="dateFin" id="dateFin" placeholder="jj-mm-aaaa"></p><p><span class="trn">Temperature</span> : <span class="trn">sd</span> 00  / <span class="trn">min dev</span> 00 / <span class="trn">max dev</span> 00 / 00 <span class="trn">sqd</span> / 00 <span class="trn">sde</span> </p></div>  <div class="checkboxAfficherCalage"> <p> <input type="checkbox" name="checkbox" value="checkbox"><span class="trn">Show on the graph</span></p></div><img src="../asset/img/caviteSite/flecheHautVert.png"/></div> <div class="sectionCalageContenu"> <div class="sectionCalageContent">  <div class="sectionCalageContentLeft"><table><tr><th></th> <th class="trn">stored V</th><th class="trn">Flow</th>   <th class="trn">simul P</th>  <th class="trn">DP arma</th>  </tr> <tr> <td> Coeff P-3</td> <td><input type="text" name="case11P"></td><td><input type="text" name="case12P"></td><td><input type="text" name="case12P"></td> <td> <b>-</b> </td></tr> <tr> <td> Coeff P-2</td> <td><input type="text" name="case21P"></td>  <td><input type="text" name="case22P"></td><td><input type="text" name="case23P"></td>  <td><input type="text" name="case24P"></td></tr><tr><td> Coeff P-1</td> <td><input type="text" name="case31P"></td><td><input type="text" name="case32P"></td> <td><input type="text" name="case33P"></td><td><input type="text" name="case33P"></td>  </tr>  <tr><td> Coeff P-0</td><td><input type="text" name="case41P"></td><td><input type="text" name="case42P"></td>  <td><input type="text" name="case43P"></td> <td><input type="text" name="case44P"></td> </tr> </table> </div>  <div class="sectionCalageContentRight"><table> <tr> <th></th><th class="trn">PArma</th><th class="trn">stored V</th>  <th class="trn">Flow</th><th class="trn">simul T</th> <th class="trn">DP arma</th>  </tr>  <tr><td> Coeff T-3</td><td><input type="text" name="case11T"></td><td><input type="text" name="case12T"></td> <td><input type="text" name="case13T"></td><td><input type="text" name="case14T"></td> <td> <b>-</b> </td> </tr> <tr>  <td> Coeff T-2</td><td><input type="text" name="case21T"></td> <td><input type="text" name="case22T"></td><td><input type="text" name="case23T"></td> <td><input type="text" name="case24T"></td> <td><input type="text" name="case25T"></td> </tr>   <tr>  <td> Coeff T-1</td> <td><input type="text" name="case31T"></td>    <td><input type="text" name="case32T"></td>  <td><input type="text" name="case33T"></td>  <td><input type="text" name="case34T"></td>  <td>  <b>-</b> </td>  </tr>  <tr>  <td> Coeff T-0</td><td><input type="text" name="case41T"></td>  <td><input type="text" name="case42T"></td><td><input type="text" name="case43T"></td>  <td><input type="text" name="case43T"></td> <td>  <b>-</b> </td> </tr>   </table>   </div><p class="boutonVert simulerCourbes trn">Simulate</p><p class="simulerCourbesErreur trn">Not enough data to simulate between these dates</p>   </div> <div id="graphesGestionCalage">     <div id="graphesGestionCalageLeft"> <h3 class="trn">Pressure</h3>  <div id="graphePression">  <div id="chartContainerPressionCalage0" style="height: 190px; width: 100%;"></div></div> <div id="listeInfoGraphe">  <div class="infoGraphe"> <p>00</p> <p class="trn">Standard deviation</p>   </div> <div class="infoGraphe"><p>00</p><p class="trn">Min deviation</p> </div> <div class="infoGraphe"> <p>00</p><p class="trn">Max deviation</p> </div>  <div class="infoGraphe">  <p>00</p>  <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe"><p>00</p><p class="trn">Average deviations</p></div>  </div>  </div>  <div id="graphesGestionCalageRight"><h3 class="trn">Temperature</h3>    <div id="grapheTemperature">  <div id="chartContainerTempCalage0" style="height: 190px; width: 100%;"></div> </div> <div id="listeInfoGraphe"><div class="infoGraphe"><p>00</p> <p class="trn">Standard deviation</p></div>  <div class="infoGraphe"> <p>00</p> <p class="trn">Min deviation</p></div><div class="infoGraphe"> <p>00</p> <p class="trn">Max deviation</p> </div>  <div class="infoGraphe"> <p>00</p> <p class="trn">Sum of the quadratic deviations</p> </div> <div class="infoGraphe"> <p>00</p> <p class="trn">Average deviations</p>  </div> </div> </div>  </div> <div id="gestionCalagesBottom"> <p class="boutonGris trn">Cancel</p> <p class="boutonVert ajoutCalage trn">Add</p>  <p class="erreurDate trn">The date fields are not filled or incorrect.</p> <p class="erreurChampsCoef trn">Coefficient fields are not filled or incorrect.</p> </div> </div> </div></div> ');
                    
                    
                    
                    
                    
                     //Styliser aussitôt pour harmoniser les animations
                       $('#nouveauCalage').css('background-color','white');
                       $('#nouveauCalage .sectionCalageTitre div p').css('color','#2ecc71');
                       $('#nouveauCalage .sectionCalageTitre div p input').css('color','#2ecc71');
                       $('#nouveauCalage .sectionCalageTitre input').css('background-color','white');
                       $('#nouveauCalage .sectionCalageTitre input').css('background-color','white');
                    
                    $('#nouveauCalage .sectionCalageContenu').slideDown(500, function()
                    {
                        $('#nouveauCalage').addClass('sectionCalageActive');
                        
                       
                        
                         /*  Traduire le champs Date des input **/
                        $( "#dateDebut, #dateFin" ).datepicker(
                        {
                            altField: "#datepicker",
                            closeText: 'Fermer',
                            prevText: 'Précédent',
                            nextText: 'Suivant',
                            currentText: 'Aujourd\'hui',
                            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                            monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                            dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                            dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                            weekHeader: 'Sem.',
                            dateFormat: 'dd-mm-yy'
                        });

                        $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
                        $( "#dateDebut" ).datepicker();
                        $( "#dateFin" ).datepicker();
                        
                        
                        $('div#chargementPageVierge').hide();
                        
                        $('.simulerCourbes').click(actionSimulerCalage);
                        
                    });
                    }
            }
        
        
       

    
        
    });
    
    
    
    
      /*  Traduire le champs Date des input **/
    $( "#dateDebut, #dateFin, #dateDebutGenererRapport, #dateFinGenererRapport" ).datepicker(
    {
        altField: "#datepicker",
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        dateFormat: 'dd-mm-yy'
    });

    $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
    $( "#dateDebut" ).datepicker();
    $( "#dateFin" ).datepicker();
    $( "#dateDebutGenererRapport" ).datepicker();
    $( "#dateFinGenererRapport" ).datepicker();
    
    
    
     if (sessionStorage.getItem("Langue") == "en")
     {
         console.log("en")
         var _t = $('body').translate({lang: "fr", t: t});
         var str = _t.g("translate");
         _t.lang("en");
         traductionAnglaise();
     }
    else if(sessionStorage.getItem("Langue") == "fr")
    {
        console.log("fr")
        var _t = $('body').translate({lang: "fr", t: t});
        var str = _t.g("translate");
        _t.lang("fr");
        traductionFrançaise();
    }
    else 
         sessionStorage.setItem("Langue", "fr");   
   
    
});



    /**
    *
    *   Vérifier qu'un champs est bien un nombre
    */    
    function validateChampsNombre(champs)
    {
        var reg = /*/^\d+$/*//^-?\d*(\.\d+)?$/ ;
        if (reg.test(champs) && champs != "-")
        {
            return true; 
        }
        else{
            return false;
        }
    } 
    
  
    /** 
    *  Page : parametreSite.html 
    *
    *  Vérifier le formulaire d'ajout d'un élément chimique
    **/ 
   function verifierFormAjoutElementChimique()
    {

        //Savoir si chaque champs a bien été remplis
        var ChampsRemplis = ($("input[name='nEC1']").val() != "" && $("input[name='nEC2']").val() != "" && $("input[name='nEC3']").val() != "" && $("input[name='nEC4']").val() != "" && $("input[name='nEC5']").val() != "");
        
        
        //N'autoriser que des nombres ces champs 
        var champs2Valide = validateChampsNombre($("input[name='nEC2']").val());
        var champs3Valide = validateChampsNombre($("input[name='nEC3']").val());
        var champs4Valide = validateChampsNombre($("input[name='nEC4']").val());
        
        
        
        
         if ( $("input[name='nEC2']").val() != "" && !champs2Valide )
        {
            $('p.champsNombreInvalide2').slideDown();
        }
        else
             $('p.champsNombreInvalide2').slideUp();
        
        
        if ( $("input[name='nEC3']").val() != "" && !champs3Valide )
        {
            $('p.champsNombreInvalide3').slideDown();
        }
        else
             $('p.champsNombreInvalide3').slideUp();
        
        
        if ( $("input[name='nEC4']").val() != "" && !champs4Valide )
        {
            $('p.champsNombreInvalide4').slideDown();
        }
        else
             $('p.champsNombreInvalide4').slideUp();
        
        
        
        
        
        //Si tous les champs sont remplis et valide
        if(ChampsRemplis && champs2Valide && champs3Valide && champs4Valide)
        {    
           return true;
        }
        
        else
        {
           
            
            if($("input[name='nEC2']").val() == "")
                {
                    $("label[name='nEC2']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nEC2']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nEC2']").css('color','black').css('font-weight','inherit');
                     $("input[name='nEC2']").css('border-color','#757575');
                }
            
            if($("input[name='nEC3']").val() == "")
                {
                    $("label[name='nEC3']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nEC3']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nEC3']").css('color','black').css('font-weight','inherit');
                     $("input[name='nEC3']").css('border-color','#757575');
                }
            
            
            if($("input[name='nEC4']").val() == "")
                {
                    $("label[name='nEC4']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nEC4']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nEC4']").css('color','black').css('font-weight','inherit');
                     $("input[name='nEC4']").css('border-color','#757575');
                }
            
              if($("input[name='nEC5']").val() == "")
                {
                    $("label[name='nEC5']").css('color','#e90d3f').css('font-weight','bold');
                    $("input[name='nEC5']").css('border-color','#e90d3f');
                }
            else
                {
                    $("label[name='nEC5']").css('color','black').css('font-weight','inherit');
                     $("input[name='nEC5']").css('border-color','#757575');
                }
            
           
            
            
           return false;
        }
    }
    



    /** 
    *  Page : parametreSite.html 
    *
    *  Vérifier le formulaire d'envoie des composants d'un gaz
    **/ 
   function verifierAjoutGaz()
    {
        
        var sommeProportion = 0;
        var boolSommeProp = false; //la somme est suppérieur à 100%
        
        var boolProportionNonNegativeNulle = true; //un champs est vide, ou égal à 0 ou négatif
        
        var nombreComposant = $('.compositionGaz ul > *').length;
        var nomComposant ="";
            
        for (var i = 1; i <= nombreComposant ; i++)
            {
                 var temp = $('.compositionGaz ul li:nth-child(' + i +') input[name="proportions"]').val();
                temp = temp.replace(" ", "");
                sommeProportion += parseFloat(temp);
                
                
                if (temp <= 0) //Si au moins une des proportion est égale ou inférieur à 0 ou est vide, il y a une erreur
                {
                        boolProportionNonNegativeNulle = false;
                        
                        $('.compositionGaz ul li:nth-child(' + i +') input[name="proportions"]').siblings('p.erreurProportion').slideDown();
                        
                        $('.compositionGaz ul li:nth-child(' + i +') input[name="proportions"]').css('border-color','#e90d3f').css('color','#e90d3f');
                        
                }
                
                if (!validateChampsNombre(temp)) //Si le champs n'est pas un nombre
                {
                    $('.compositionGaz ul li:nth-child(' + i +') input[name="proportions"]').siblings('p.erreurProportion').slideDown();
                    
                    
                    $('.compositionGaz ul li:nth-child(' + i +') input[name="proportions"]').css('border-color','#e90d3f').css('color','#e90d3f');
                }
                else if (validateChampsNombre(temp) && temp > 0) //Ne pas enlever l'erreur si la proportion est restée erronée
                {
                    $('.compositionGaz ul li:nth-child(' + i +') input[name="proportions"]').siblings('p.erreurProportion').slideUp();  
                }
                
                
                //Remplir le tableau à envoyer pour enregistrer les données

                 listeComposantGaz[i-1] = new Array( $('.compositionGaz ul li:nth-child(' + i +') select').val() , temp);
                
                
                
            }
        
        
        //Si somme des proportions est inférieur ou égal à 100 %
        if (sommeProportion <= 100)
            {
                boolSommeProp = true;
                $('.erreurProportionsSup100').slideUp();
            }
        else
            $('.erreurProportionsSup100').slideDown();
         
       

        
        
        //Si tous les champs sont remplis et valide
        if(boolProportionNonNegativeNulle && boolSommeProp)
        {    
            return true;
        }
        
        else
        {
             return false;
        }
    }
    




    /**
    *   Page : caviteSite.html, contactsSite.html, gererCalage.html, gererDonneesRelevees.html
    *
    *   Afficher overlay pour demander un export du site
    **/
    $(document).on('click','li.exportSite', function()
    {
        if (sessionStorage.getItem("Langue") == "fr")
            $('.overlay').append('<div class="overlay-exportSite"> <p class="overlay-title trn">Export du site</p> <label for="mdpExport" class="trn">Veuillez choisir le mot de passe du fichier zip créé :</label> <input type="password" name="mdpExport" id="mdpExport" class="searchfieldjs" placeholder="Mot de passe" > <div id="exportSiteBottom">  <p class="boutonVert trn">Exporter</p><p class="boutonGris trn">Annuler</p><p class="erreurExport trn">Veuillez définir un mot de passe.</p>   </div></div>');
        else
            $('.overlay').append('<div class="overlay-exportSite"> <p class="overlay-title trn">Export of the site</p> <label for="mdpExport" class="trn">Please choose the created zip file password :</label> <input type="text" name="mdpExport" id="mdpExport" class="searchfieldjs" placeholder="Password" > <div id="exportSiteBottom">  <p class="boutonVert trn">Export</p><p class="boutonGris trn">Cancel</p><p class="erreurExport trn">Please set a password.</p>   </div></div>');
        
        $(".overlay").fadeIn();
        $(".overlay-exportSite").fadeIn();
    });



    /**
    *   Page : caviteSite.html, contactsSite.html, gererCalage.html, gererDonneesRelevees.html
    *
    *   Cacher overlay qui demande un export du Site  
    **/
    $(document).on('click','.overlay-exportSite .boutonGris', function()
    {        
        $(".overlay").fadeOut();
        $(".overlay-exportSite").fadeOut(function()
        {
            //Supprimer l'overlay créé précédament
            $(".overlay-exportSite").remove();
            
        });
        
        
    });


    /**
    *   Page : caviteSite.html, contactsSite.html, gererCalage.html, gererDonneesRelevees.html
    *
    *   Envoyer le mot de passe choisis par l'utilisateur pour le zip   
    **/
    $(document).on('click','.overlay-exportSite .boutonVert', function()
    {           
        passwordExport = $(this).parent().parent().find('input[name="mdpExport"]').val();
        
        if(passwordExport == "")
            $('p.erreurExport').slideDown('fast');
        else
            {
                $('div#chargementPage').fadeIn();
                exportSite(passwordExport );
            }
        
    });



    /**
    *   Page : exploitants.html
    *
    *   Afficher overlay pour demander un export de l'exploitant
    **/
    $(document).on('click','.exportExploitant', function()
    {
        $('.overlay').append(' <div class="overlay-exportExploitant"> <p class="overlay-title">Export de l\'exploitant </p> <p>Voulez-vous vraiment exporter l\'exploitant ? </p><div id="exportExploitantBottom">  <p class="boutonVert"> Exporter</p><p class="boutonGris"> Annuler</p> </div></div>');
        
        $(".overlay").fadeIn();
        $(".overlay-exportExploitant").fadeIn();
        
        sessionStorage.setItem("NomExploitantExport", $(this).parent().parent().attr('name'));
        
    });


    /**
    *   Page : exploitants.html
    *
    *   Cacher overlay qui demande un export du Site  
    **/
    $(document).on('click','.overlay-exportExploitant .boutonGris', function()
    {        
        $(".overlay").fadeOut();
        $(".overlay-exportExploitant").fadeOut(function()
        {
            //Supprimer l'overlay créé précédament
            $(".overlay-exportExploitant").remove();
            
        });
        
    });


    /**
    *   Page : exploitants.html
    *
    *   Valider l'export de l'exploitant
    **/
    $(document).on('click','.overlay-exportExploitant .boutonVert', function()
    {           
        exportExploitant();
        
    });



    /**
    *   Page : caviteSite.html
    *
    *   Afficher overlay pour demander un export de la cavité  
    **/
    $(document).on('click','.exportCavite', function()
    {
        $('.overlay').append(' <div class="overlay-exportCavite"> <p class="overlay-title">Export de la cavité </p> <label for="mdpExport">Veuillez choisir le mot de passe du fichier zip créé :</label> <input type="password" name="mdpExport" id="mdpExport" class="searchfieldjs" placeholder="Mot de passe" > <div id="exportCaviteBottom">  <p class="boutonVert"> Exporter</p><p class="boutonGris"> Annuler</p><p class="erreurExport">Veuillez définir un mot de passe.</p> </div></div>');
        
        
        //fermer les autres overlays
        $('.overlay > div').fadeOut();
        
        $(".overlay").fadeIn();
        $(".overlay-exportCavite").fadeIn();
        
       
        
    });


    /**
    *   Page : caviteSite.html
    *
    *   Cacher overlay qui demande un export du Site  
    **/
    $(document).on('click','.overlay-exportCavite .boutonGris', function()
    {        
        $(".overlay").fadeOut();
        $(".overlay-exportCavite").fadeOut(function()
        {
            //Supprimer l'overlay créé précédament
            $(".overlay-exportCavite").remove();
            
        });
        
    });


    /**
    *   Page : caviteSite.html
    *
    *   Envoyer le mot de passe choisis par l'utilisateur pour le zip   
    **/
    $(document).on('click','.overlay-exportCavite .boutonVert', function()
    {           
        passwordExport = $(this).parent().parent().find('input[name="mdpExport"]').val();
        
        if(passwordExport == "")
            $('p.erreurExport').slideDown('fast');
        else
            {
                exportCavite(passwordExport);
                $('div#chargementPage').fadeIn();
            }
        
    });






    

    
    /**
    *   Page : caviteSite.html
    *
    *   Au submit du formulaire, vérifier les champs pour savoir ou non si le submit se lance  => AJOUT CALAGE
    **/
    $(document).on('click','#nouveauCalage .ajoutCalage', function()
    {
        $('*').css('cursor','wait');
        
        
        var listeDataPressionRemplis = true; //Savoir si toutes les coef pour la pression ont été donnés
        var listeDataTemperatureRemplis = true; //Savoir si toutes les coef pour la temperature ont été donnés
        
        dateDebut = $(this).parent().parent().parent().find('.sectionCalageTitre input[name="dateDebut"]').val();
        dateFin =  $(this).parent().parent().parent().find('.sectionCalageTitre input[name="dateFin"]').val();
        

        
        
        
        //Savoir si les champs PRESSION sont remplis et corrects 
        listeDataPression = [];
        $('#nouveauCalage .sectionCalageContentLeft input').each(function()
        {
            listeDataPression.push($(this).val());
            if ($(this).val() == ""  || !validateChampsNombre($(this).val()) ) //Si la valeur d'un champs est vide ou n'est pas un nombre
                { 
                    $(this).css('border-color','#e90d3f'); //Styliser le champs
                    $(this).css('color','#e90d3f'); 
                    listeDataPressionRemplis = false; //Minimum un des champs est vide
                }
            else
                {
                    $(this).css('border-color','rgba(177, 177, 177, 0.40)'); //Styliser le champs
                    $(this).css('color','#b1b1b1'); 
                }
        });
        
         //Savoir si les champs TEMPERATURE sont remplis et corrects 
        listeDataTemperature = [];
        $('#nouveauCalage .sectionCalageContentRight input').each(function()
        {
            listeDataTemperature.push($(this).val());
            
            if ($(this).val() == ""  || !validateChampsNombre($(this).val()) ) //Si la valeur d'un champs est vide ou n'est pas un nombre
                { 
                    $(this).css('border-color','#e90d3f'); //Styliser le champs
                    $(this).css('color','#e90d3f'); 
                    listeDataTemperatureRemplis = false; //Minimum un des champs est vide
                }
            else
                {
                    $(this).css('border-color','rgba(177, 177, 177, 0.40)'); //Styliser le champs
                    $(this).css('color','#b1b1b1'); 
                }
        });
        
        //Savoir si les champs dates sont remplis et corrects 
        var champsDateRemplies = ( dateDebut != "" && dateFin != "" );
        
        
        if (champsDateRemplies) 
            {
                var champsDateCorrects = false; //
                
                if ($("#nouveauCalage #dateFin").datepicker('getDate').getFullYear() > $("#nouveauCalage #dateDebut").datepicker('getDate').getFullYear())
                    champsDateCorrects = true;
                else
                    if (($("#nouveauCalage #dateFin").datepicker('getDate').getFullYear() == $("#nouveauCalage #dateDebut").datepicker('getDate').getFullYear()) && (($("#nouveauCalage #dateFin").datepicker('getDate').getMonth() +1) > ($("#nouveauCalage #dateDebut").datepicker('getDate').getMonth() +1 )))
                        champsDateCorrects = true;
                    else
                        if ( (($("#nouveauCalage #dateFin").datepicker('getDate').getMonth() +1) == ($("#nouveauCalage #dateDebut").datepicker('getDate').getMonth() +1 )) && ($("#nouveauCalage #dateFin").datepicker('getDate').getDate() > $("#nouveauCalage #dateDebut").datepicker('getDate').getDate()))
                            champsDateCorrects = true;
            }
        
            
        
        
        
        
        
        if (champsDateCorrects && champsDateRemplies)
            {
                $('#nouveauCalage p.erreurDate').slideUp();
                $('.gererCalages .sectionCalageActive .sectionCalageTitre input').css('color','#2ecc71');
            }
        else
            {
                $('#nouveauCalage p.erreurDate').slideUp();
                $('#nouveauCalage p.erreurDate').slideDown();
                $('.gererCalages .sectionCalageActive .sectionCalageTitre input').css('color','#e90d3f');
            }
        
        if (listeDataPressionRemplis && listeDataTemperatureRemplis)
            $('p.erreurChampsCoef').slideUp();
        else
            {
                $('#nouveauCalage p.erreurChampsCoef').slideUp();
                $('#nouveauCalage p.erreurChampsCoef').slideDown();
            }
        
        checkBoxCalage =  $('#nouveauCalage input[type="checkbox"]').prop('checked');
        
        if (champsDateRemplies && champsDateCorrects && listeDataPressionRemplis && listeDataTemperatureRemplis) //Envoyer l'enregistrement si toutes les conditions sont respectées
             ajoutCalage();
        else
            reinitialiserCurseur();
        
        
    });
    






    
    
   
    /**
    *   Page : caviteSite.html
    *
    *   Au submit du formulaire, vérifier les champs pour savoir ou non si le submit se lance
    **/
    $(document).on('click','.sectionCalage .enregistrerCalage', function()
    {
        $('*').css('cursor','wait');
        
        var calageAModifier = $(this).parent().parent().parent();
        
        idCalageModifie = $(calageAModifier).attr('idcalage');
        
        var listeDataPressionRemplis = true; //Savoir si toutes les coef pour la pression ont été donnés
        var listeDataTemperatureRemplis = true; //Savoir si toutes les coef pour la temperature ont été donnés
        
        dateDebut = $(calageAModifier).find('.sectionCalageTitre input[name="dateDebut"]').val();
        dateFin =  $(calageAModifier).find('.sectionCalageTitre input[name="dateFin"]').val();
        
        //Savoir si les champs PRESSION sont remplis et corrects 
        listeDataPression = [];
        $(calageAModifier).find('.sectionCalageContentLeft input').each(function()
        {
            listeDataPression.push($(this).val());
            if ($(this).val() == ""  || !validateChampsNombre($(this).val()) ) //Si la valeur d'un champs est vide ou n'est pas un nombre
                { 
                    $(this).css('border-color','#e90d3f'); //Styliser le champs
                    $(this).css('color','#e90d3f'); 
                    listeDataPressionRemplis = false; //Minimum un des champs est vide
                }
            else
                {
                    $(this).css('border-color','rgba(177, 177, 177, 0.40)'); //Styliser le champs
                    $(this).css('color','#b1b1b1'); 
                }
        });
        
         //Savoir si les champs TEMPERATURE sont remplis et corrects 
        listeDataTemperature = [];
        $(calageAModifier).find('.sectionCalageContentRight input').each(function()
        {
            listeDataTemperature.push($(this).val());
            
            if ($(this).val() == ""  || !validateChampsNombre($(this).val()) ) //Si la valeur d'un champs est vide ou n'est pas un nombre
                { 
                    $(this).css('border-color','#e90d3f'); //Styliser le champs
                    $(this).css('color','#e90d3f'); 
                    listeDataTemperatureRemplis = false; //Minimum un des champs est vide
                }
            else
                {
                    $(this).css('border-color','rgba(177, 177, 177, 0.40)'); //Styliser le champs
                    $(this).css('color','#b1b1b1'); 
                }
        });
        
        //Savoir si les champs dates sont remplis et corrects 
        var champsDateRemplies = ( dateDebut != "" && dateFin != "" );
        
        if (champsDateRemplies)
            {
                var champsDateCorrects = false; //
                
                if ($(calageAModifier).find("#dateFin").datepicker('getDate').getFullYear() > $(calageAModifier).find("#dateDebut").datepicker('getDate').getFullYear())
                    champsDateCorrects = true;
                else
                    if ((($(calageAModifier).find("#dateFin").datepicker('getDate').getFullYear() == $(calageAModifier).find("#dateDebut").datepicker('getDate').getFullYear()) && ($(calageAModifier).find("#dateFin").datepicker('getDate').getMonth() +1) > ($(calageAModifier).find("#dateDebut").datepicker('getDate').getMonth() +1 )))
                        champsDateCorrects = true;
                    else
                        if ( (($(calageAModifier).find("#dateFin").datepicker('getDate').getMonth() +1) == ($(calageAModifier).find("#dateDebut").datepicker('getDate').getMonth() +1 )) && ($(calageAModifier).find("#dateFin").datepicker('getDate').getDate() > $(calageAModifier).find("#dateDebut").datepicker('getDate').getDate()))
                            champsDateCorrects = true;
            }
        
        if (champsDateCorrects && champsDateRemplies)
            {
                $('p.erreurDate').slideUp();
                $(calageAModifier).find('.sectionCalageTitre input').css('color','#2ecc71');
            }
        else
            {
                $('p.erreurDate').slideUp();
                $('p.erreurDate').slideDown();
                $(calageAModifier).find('.sectionCalageTitre input').css('color','#e90d3f');
            }
        
        if (listeDataPressionRemplis && listeDataTemperatureRemplis)
            $(calageAModifier).find('p.erreurChampsCoef').slideUp();
        else
            {
                $(calageAModifier).find('p.erreurChampsCoef').slideUp();
                $(calageAModifier).find('p.erreurChampsCoef').slideDown();
            }
        
        checkBoxCalage =  $(calageAModifier).find('input[type="checkbox"]').prop('checked');
        
        if (champsDateRemplies && champsDateCorrects && listeDataPressionRemplis && listeDataTemperatureRemplis) //Envoyer l'enregistrement si toutes les conditions sont respectées
             enregistrerCalage();
        else
            reinitialiserCurseur();
        
                  
                
   });


    /**
    *   Page : caviteSite.html
    *
    *   Vérifier les dates pour afficher les courbes TEMP - PRESS - VOL en fonction de ces dates
    **/
    $(document).on('click','#caviteSite #validateDateCavite', function()
    {        
        dateDebutCavite = $('input#dateDebut').val();
        dateFinCavite = $('input#dateFin').val();
        
        
          //Savoir si les champs dates sont remplis et corrects 
        var champsDateRemplies = ( dateDebutCavite != "" && dateFinCavite != "" );
        
        var champsDateCorrects = false; 
                
        if (champsDateRemplies) 
            {
                if ($("input#dateFin").datepicker('getDate').getFullYear() > $("input#dateDebut").datepicker('getDate').getFullYear())
            champsDateCorrects = true;
                else
                    if (($("input#dateFin").datepicker('getDate').getFullYear() == $("input#dateDebut").datepicker('getDate').getFullYear()) && (($("input#dateFin").datepicker('getDate').getMonth() +1) > ($("input#dateDebut").datepicker('getDate').getMonth() +1 )))
                        champsDateCorrects = true;
                    else
                        if ( (($("input#dateFin").datepicker('getDate').getMonth() +1) == ($("input#dateDebut").datepicker('getDate').getMonth() +1 )) && ($("input#dateFin").datepicker('getDate').getDate() > $("input#dateDebut").datepicker('getDate').getDate()))
                            champsDateCorrects = true;
            }
        
            
        if (champsDateCorrects)
            {
                getDonneesCaviteAvecDate(dateDebutCavite, dateFinCavite);
                $('div#chargementPage').fadeIn();
            }
        
        
        if (champsDateCorrects && champsDateRemplies)
            {
                $('input#dateDebut, input#dateFin').css('border-color','#757575');
            }
        else
            {
                $('input#dateDebut, input#dateFin').css('border-color','#e90d3f');
            }
        
    });



   /**
    *   Page : caviteSite.html 
    *
    *   Réinitialiser les courbes TEMP - PRESS - VOL en fonction
    **/
    $(document).on('click','#caviteSite #reloadDateCavite', function()
    {
        $('div#chargementPage').fadeIn();
        
        getDataTempCavitePointReel(); //Récuperer les données de la courbes réelle 

        getDataPresCavitePointReel(); //Récuperer les données de la courbes réelle 

        getDataVolumeCavitePointReel(); //Récuperer les données de la courbes réelle 
        
        //Vider les champs date
        $('#dateDebut').val("");
        $('#dateFin').val("");
        
    });





    /**
    *   Page : graphePression.html 
    *
    *   Vérifier les dates pour afficher les courbes TEMP - PRESS - VOL en fonction de ces dates et infos liées
    **/
    $(document).on('click','#caviteSiteGraphe #validateDateCavite', function()
    {
        dateDebutCavite = $('input#dateDebut').val();
        dateFinCavite = $('input#dateFin').val();
        
        
          //Savoir si les champs dates sont remplis et corrects 
        var champsDateRemplies = ( dateDebutCavite != "" && dateFinCavite != "" );
        
        var champsDateCorrects = false; 
                
        if (champsDateRemplies) 
            {
                if ($("input#dateFin").datepicker('getDate').getFullYear() > $("input#dateDebut").datepicker('getDate').getFullYear())
            champsDateCorrects = true;
                else
                    if (($("input#dateFin").datepicker('getDate').getFullYear() == $("input#dateDebut").datepicker('getDate').getFullYear()) && (($("input#dateFin").datepicker('getDate').getMonth() +1) > ($("input#dateDebut").datepicker('getDate').getMonth() +1 )))
                        champsDateCorrects = true;
                    else
                        if ( (($("input#dateFin").datepicker('getDate').getMonth() +1) == ($("input#dateDebut").datepicker('getDate').getMonth() +1 )) && ($("input#dateFin").datepicker('getDate').getDate() > $("input#dateDebut").datepicker('getDate').getDate()))
                            champsDateCorrects = true;
            }
        
            
        if (champsDateCorrects)
            getDonneesGraphesCaviteAvecDate(dateDebutCavite, dateFinCavite);
        
        
        if (champsDateCorrects && champsDateRemplies)
            {
                $('input#dateDebut, input#dateFin').css('border-color','#757575');
            }
        else
            {
                $('input#dateDebut, input#dateFin').css('border-color','#e90d3f');
            }
        
    });

   /**
    *   Page : graphePression.html 
    *
    *   Réinitialiser les courbes TEMP - PRESS - VOL en fonction
    **/
    $(document).on('click','#caviteSiteGraphe #reloadDateCavite', function()
    {
        if (document.title == "Graphe Pression")
            {
                initialiserGraphesDonneePression();
                getInfoGraphePression();
            }
        else 
            if (document.title == "Graphe Température")
            {
                initialiserGraphesDonneeTemp();
                getInfoGrapheTemp();
            }
            else 
                if (document.title == "Graphe Volume")
                {
                    initialiserGraphesDonneeVolume();
                }
        //Vider les champs date
        $('#dateDebut').val("");
        $('#dateFin').val("");
        
    });




    /**
    *   Page : gererCalage.html 
    *
    *   Afficher validation de suppression d'un calage
    **/
    $(document).on('click','.sectionCalage .boutonRouge', function()
    {
        $('.overlay').fadeIn();
        $('.overlay-suppressionCalage').fadeIn();
        
        
        //Récupérer les informations du contact pour une suppression éventuelle
        idCalageSuppression = $(this).parent().parent().parent().attr('idcalage');
        
      
        
      });
    
    /**
    *   Page : gererCalage.html 
    *
    *   Cacher validation de suppression d'un calage
    **/
    $(document).on('click','.overlay-suppressionCalage p.boutonGris', function()
    {        
        $('.overlay').fadeOut();
        $('.overlay-suppressionCalage').fadeOut();
    });

     
    /**
    *   Page : gererCalage.html 
    *
    *   Cacher validation de suppression d'un calage
    **/
    $(document).on('click','.overlay-suppressionCalage p.boutonRouge', function()
    {
        suppressionCalage();
    });

  
   /**
    *   Page : gererCalage.html 
    *
    *   Simuler courbe pression et température
    **/
    function actionSimulerCalage()
    {
        $('*').css('cursor','wait');
        $('div#chargementPage').fadeIn();
        
        
        $(this).unbind("click");
        

        var calageAModifier = $(this).parent().parent().parent();
        
        
        idCalageSimule = $(calageAModifier).attr('idcalage');
        
        
        var listeDataPressionRemplis = true; //Savoir si toutes les coef pour la pression ont été donnés
        var listeDataTemperatureRemplis = true; //Savoir si toutes les coef pour la température ont été donnés
        
        dateDebut = $(calageAModifier).find('.sectionCalageTitre input[name="dateDebut"]').val();
        dateFin =  $(calageAModifier).find('.sectionCalageTitre input[name="dateFin"]').val();

        
        //Savoir si les champs PRESSION sont remplis et corrects 
        listeDataPression = [];
        $(calageAModifier).find('.sectionCalageContentLeft input').each(function()
        {
            listeDataPression.push($(this).val());
            if ($(this).val() == ""  || !validateChampsNombre($(this).val()) ) //Si la valeur d'un champs est vide ou n'est pas un nombre
                { 
                    $(this).css('border-color','#e90d3f'); //Styliser le champs
                    $(this).css('color','#e90d3f'); 
                    listeDataPressionRemplis = false; //Minimum un des champs est vide
                }
            else
                {
                    $(this).css('border-color','rgba(177, 177, 177, 0.40)'); //Styliser le champs
                    $(this).css('color','#b1b1b1'); 
                }
        });
        
        //Savoir si les champs TEMPERATURE sont remplis et corrects 
        listeDataTemperature = [];
         $(calageAModifier).find('.sectionCalageContentRight input').each(function()
        {
            listeDataTemperature.push($(this).val());
            
            if ($(this).val() == ""  || !validateChampsNombre($(this).val()) ) //Si la valeur d'un champs est vide ou n'est pas un nombre
                { 
                    $(this).css('border-color','#e90d3f'); //Styliser le champs
                    $(this).css('color','#e90d3f'); 
                    listeDataTemperatureRemplis = false; //Minimum un des champs est vide
                }
            else
                {
                    $(this).css('border-color','rgba(177, 177, 177, 0.40)'); //Styliser le champs
                    $(this).css('color','#b1b1b1'); 
                }
        });
        
        //Savoir si les champs dates sont remplis et corrects 
        var champsDateRemplies = ( dateDebut != "" && dateFin != "" );
        
        var champsDateCorrects = false; //
                
     
        
        //S'il a une date faire la suite. (obligé de faire une condition, sinon ça plante, impossible de de refaire une simulation)    
        if ($(calageAModifier).find("#dateFin").datepicker('getDate') != null)
            {  
                
                if ($(calageAModifier).find("#dateFin").datepicker('getDate').getFullYear() > $(calageAModifier).find("#dateDebut").datepicker('getDate').getFullYear())
                    champsDateCorrects = true;
                else
                    if ((($(calageAModifier).find("#dateFin").datepicker('getDate').getFullYear() == $(calageAModifier).find("#dateDebut").datepicker('getDate').getFullYear()) && ($(calageAModifier).find("#dateFin").datepicker('getDate').getMonth() +1) > ($(calageAModifier).find("#dateDebut").datepicker('getDate').getMonth() +1 )))
                        champsDateCorrects = true;
                    else
                        if ( (($(calageAModifier).find("#dateFin").datepicker('getDate').getMonth() +1) == ($(calageAModifier).find("#dateDebut").datepicker('getDate').getMonth() +1 )) && ($(calageAModifier).find("#dateFin").datepicker('getDate').getDate() > $(calageAModifier).find("#dateDebut").datepicker('getDate').getDate()))
                            champsDateCorrects = true;





                if (champsDateCorrects && champsDateRemplies)
                    {
                         $(calageAModifier).find('p.erreurDate').slideUp();
                        $(calageAModifier).find('.sectionCalageTitre input').css('color','#2ecc71');
                    }
                else
                    {
                         $(calageAModifier).find('p.erreurDate').slideUp();
                         $(calageAModifier).find('p.erreurDate').slideDown();
                        $(calageAModifier).find('.sectionCalageTitre input').css('color','#e90d3f');
                    }

                if (listeDataPressionRemplis && listeDataTemperatureRemplis)
                    $(calageAModifier).find('p.erreurChampsCoef').slideUp();
                else
                    {
                        $(calageAModifier).find('p.erreurChampsCoef').slideUp();
                        $(calageAModifier).find('p.erreurChampsCoef').slideDown();
                    }


                if (champsDateRemplies && champsDateCorrects && listeDataPressionRemplis && listeDataTemperatureRemplis) //Envoyer l'enregistrement si toutes les conditions sont respectées
                     simulerCalage(dateDebut, dateFin);
                else
                    {
                        reinitialiserCurseur();
                        $('div#chargementPage').fadeOut();
                    }


              
                
            }
        
        initBoutonSimulerCalage(this);
        
        
        
    }



   /**
    *   Page : gererCalage.html 
    *
    *   Autoriser le reclick sur le bouton des simulation
    **/
    function initBoutonSimulerCalage(ele)
    {
        setTimeout(function()
        {
           $(ele).bind("click",actionSimulerCalage); 
        }, 1000); 
        
    }



    function afficherCompositionTotale()
    {
        var nombreComposant = $('.compositionGaz ul > *').length;
        var nomComposant ="";
            
        var masseMolaire;
        var masseMolaireTotale= 0;
              
        var pression;
        var pressionTotale= 0;
             
        var temperature;
        var temperatureTotale= 0;

        var densite;
        var densiteTotale= 0;

        var proportion;
        
        for (var i = 1; i <= nombreComposant ; i++)
            {
                masseMolaire = $('.compositionGaz ul li:nth-child(' + i +') input[name="masseMolaire"]').val();
                masseMolaire = masseMolaire.replace(" g/mol","");
                
                pression = $('.compositionGaz ul li:nth-child(' + i +') input[name="pressionCritique"]').val();
                pression = pression.replace(" MPa","");
                
                temperature = $('.compositionGaz ul li:nth-child(' + i +') input[name="temperatureCritique"]').val();
                temperature = temperature.replace(" K","");

                densite = $('.compositionGaz ul li:nth-child(' + i +') input[name="densite"]').val();
                densite = densite.replace(" kg/m3","");
                
                proportion = $('.compositionGaz ul li:nth-child(' + i +') input[name="proportions"]').val();
                
                masseMolaireTotale += masseMolaire * (proportion/100.0);
                pressionTotale += pression * (proportion/100.0);
                temperatureTotale += temperature * (proportion/100.0);

                // todo -> modifier la formule par celle donnee
//                densiteTotale += densite * (proportion/100.0);

              
            }
        densiteTotale = masseMolaireTotale / 1000 / (22.4/1000.) / 1.20531;
        $('.compositionGazFinal input[name="masseMolaire"]').val(masseMolaireTotale.toFixed(4) + " g/mol");
        $('.compositionGazFinal input[name="pressionCritique"]').val(pressionTotale.toFixed(4) + " MPa");
        $('.compositionGazFinal input[name="temperatureCritique"]').val(temperatureTotale.toFixed(4) + " K");
        $('.compositionGazFinal input[name="densite"]').val(densiteTotale.toFixed(4) + " kg/m3");
    }







    /**
    *   Page : gererDonneesRelevees.html
    *
    *   Valider le changement des données lors de conflits   
    **/
    $(document).on('click','#overlay-conflitDonneesDonneesBottom p.boutonVert', function()
    {           
        confirmDataConflit();
    });


    /**
    *   Page : gererDonneesRelevees.html
    *
    *   Refuser le changement des données lors de conflits   
    **/
    $(document).on('click','#overlay-conflitDonneesDonneesBottom p.boutonGris', function()
    {           
        annuleDataConflit();
    });


    

    /**
    *   Page : gererDonneesRelevees.html
    *
    *   Bloquer la page des calages
    **/
    function bloquerPageCalage()
    {
        $('.erreurDonneesNull').show();
        $('#caviteMenuBottom a:last-child').removeAttr('href');
         //$('#caviteMenuBottom a:last-child p').css('background-color','#b1b1b1')
         $('#caviteMenuBottom a:last-child p').addClass('boutonGris')
    }



    /**
    *   Après avoir charger quelque chose et donc modifié le curseur
    *
    *   Réinitialiser le cursor en fonction de ce qu'on survole
    *
    **/
    function reinitialiserCurseur()
    {
        $('*').css('cursor','default');
        $('.boutonRouge, .boutonGris, .boutonVert').css('cursor','pointer');
        $('input').css('cursor','auto');
    }



    /**
    *   Page : index.html
    *
    *   Faire une recherche de site (bouton)
    **/
    $(document).on('click','div#searchform2 div[name="search2btn"]',function()
    {       
        var champs = $('input[name="champsSearch"]').val();
            
        if (champs != "")
        {
            searchSite(champs);    
        }
    });

    /**
    *   Page : index.html
    *
    *   Faire une recherche de site (touche entrée)
    **/
    function boutonEntreeRechercheSite(event)
    {
        if (event.keyCode == 13)
        {
            var champs = $('input[name="champsSearch"]').val();

            if (champs != "")
                searchSite(champs);
        }
            
    }



    /**
    *   Page : index.html
    *
    *   Recharger la liste des sites
    **/
      $(document).on('click','#reinitialiserListeSites',function()
    {       
       location.reload();
    });

    
    
    
    


    
    
    
    



